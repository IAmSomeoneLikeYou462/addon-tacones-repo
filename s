<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<series>
	<serie>
		<title>¿ QUIEN ES ANNA?</title>
		<info>(2022) 9 episodios. Una periodista investiga el caso de Anna Delvey, la legendaria heredera y estrella de Instagram que le robó el corazón y el dinero a la élite social de Nueva York.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/jzCnl9wACp0eUMa3IG5e0aSJIXD.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fiKQKFsjahOr8qj7QsUUHO9E70t.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/jzCnl9wACp0eUMa3IG5e0aSJIXD.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fiKQKFsjahOr8qj7QsUUHO9E70t.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>9fdea7b49c6cc9a79fdf435b4241802a3f1688f5</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>cff31504d1e18049bc18183014d41b0bea84004a</id1.2>
			<ep1.3>7 AL 9</ep1.3>
			<id1.3>2aa328eeaddb0f3f64600e65db30af43306722c2</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>¿DONDE ESTA MARTA?</title>
		<info>(2021) 3 episodios. La desaparición de la adolescente Marta del Castillo acaparó todos los titulares. Familiares, amigos y policías, entre otros, opinan sobre un caso que sigue sin resolver.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/uaoy8Bd5MxSzOQIpESTDgziChvw.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/m3aZKRAclYUAKgxJS6HlwQAi82h.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/uaoy8Bd5MxSzOQIpESTDgziChvw.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/m3aZKRAclYUAKgxJS6HlwQAi82h.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>cc13f0c554afbd289f887760d3b4a0dda5d75d3d</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>¿QUIEN MATO A SARA?</title>
		<info>(2021) 3 temporadas. 25 episodios.  Tras 18 años de encarcelamiento injusto, Alex Guzmán es liberado con el plan perfecto para descubrir quién mató a su hermana Sara y por qué la familia Lazcano lo culpó del crimen. Lo que no sabe, es que la búsqueda de pruebas lo llevará por un desvío mucho más peligroso de lo que imaginaba. Cuando finalmente se enfrente al verdadero culpable, Alex deseará no haber buscado su venganza.</info>
		<year>2021</year>
		<genre>Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/jnit57q25N5VvVrK4pj4Uj8BLe7.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/pLG4ihU1d2XkQbASQDjsFu9U7d9.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/jnit57q25N5VvVrK4pj4Uj8BLe7.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/pLG4ihU1d2XkQbASQDjsFu9U7d9.jpg</fanart.1>
			<ep1.1>1 AL 10 (1080)</ep1.1>
			<id1.1>789d3a748a718da250c69df1564ecb3a1f00bc68</id1.1>
			<ep1.2>1 NO FUE UN ERROR</ep1.2>
			<id1.2>814895abb2e0c3cb1e2520f2c231a05642d4b863</id1.2>
			<ep1.3>2 GENTE MALA</ep1.3>
			<id1.3>5172d5d6c85eb1c3c856e2a726dc9c11e3c3c36f</id1.3>
			<ep1.4>3 CARIÑOS SARA</ep1.4>
			<id1.4>3b38e8edd4bb2a9d2cc4a580c7a2a10291b92ed8</id1.4>
			<ep1.5>4 EL MONSTRUO DE LA FAMILIA</ep1.5>
			<id1.5>350872f0e26f1bbc9b8ae77723a364223445952c</id1.5>
			<ep1.6>5 SEGURO DE VIDA</ep1.6>
			<id1.6>1c46cfa0b69501c00473fe031732621f4bab053e</id1.6>
			<ep1.7>6 CACERIA</ep1.7>
			<id1.7>8cc554aea3df54ed40b084608be881f484178fc6</id1.7>
			<ep1.8>7 EL MIEDO Y LA CULPA</ep1.8>
			<id1.8>e48dcb38ea29e58a00eb91a7010f595fdd9108c6</id1.8>
			<ep1.9>8 DONDE LOS SUEÑOS SE HACEN REALIDAD</ep1.9>
			<id1.9>aeb0df7d0e68a7eb3470cb5f140f96349c1bbc16</id1.9>
			<ep1.10>9 VER EL MUNDO ARDER</ep1.10>
			<id1.10>fa39361adc274384032aa13d854b6b832e421145</id1.10>
			<ep1.11>10 DOS TUMBAS</ep1.11>
			<id1.11>1f413ed6666152e9b6ca34fb7864c8588980052b</id1.11>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/hG1wscoTZO59gQ0jqWtYYZGQQ7l.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/dYvIUzdh6TUv4IFRq8UBkX7bNNu.jpg</fanart.2>
			<ep2.1>1 AL 8</ep2.1>
			<id2.1>41d6805b556b2b8e257ace22a040386e265aec82</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/6sYMmhT4uzMPRGp8ujTFI8B9XsF.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/aUG2SakkJKyKXw4obSPC678fd37.jpg</fanart.3>
			<ep3.1>1 Episodio</ep3.1>
			<id3.1>def5179bd4dfde2bf8a4b06f594bed7b1703c333</id3.1>
			<ep3.2>2 Episodio</ep3.2>
			<id3.2>4d83e94b3ebac7e68f227d6c0580fb335d08d825</id3.2>
			<ep3.3>3 Episodio</ep3.3>
			<id3.3>fd3ece4d52f228ab9610906c63850242b0fee219</id3.3>
			<ep3.4>4 Episodio</ep3.4>
			<id3.4>f6763a729ae873408fec25b10396b0b186d7b9e4</id3.4>
			<ep3.5>5 Episodio</ep3.5>
			<id3.5>e09d740b20f9e7aabb1a2d9f1cafd8f1cba15f44</id3.5>
			<ep3.6>6 Episodio</ep3.6>
			<id3.6>16b2e30f5023bec80c01be33d63c7e07be6f2afe</id3.6>
			<ep3.7>7 Episodio</ep3.7>
			<id3.7>2defde848dc389259e2eba72793498c392da60e5</id3.7>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>¿SABES QUIEN ES?</title>
		<info>(2022) 8 episodios. Cuando una visita al centro comercial acaba en violencia, una joven descubre un lado desconocido de su madre.</info>
		<year>2022</year>
		<genre>Thriller. Drama </genre>
		<thumb>https://www.themoviedb.org/t/p/original/fJJY9Bepf4M3HhWQqPPmVNiswL0.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/eF3pfdinIPjXz3IXqezlGMFmkPL.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fJJY9Bepf4M3HhWQqPPmVNiswL0.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/eF3pfdinIPjXz3IXqezlGMFmkPL.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>e03d020340aed6e29ebabb65dffa7d94105d5b11</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>fc16f321746f68d0c5fdc4225a6abdaacfd82fa2</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>06425600ee1f91d10c10756032cdaf5ceb43bc6f</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>0a628b102661d649395a74bce3ef57ab57b115e1</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>aa09d7ffb7e331d0e02fc31b15cf77f76e3276c1</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>9ee43f0efd6ee01cb56864509483a8ee0eeaecb2</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>4f3d7e329d5f8563f0ab87548a079ff2f83ab5c7</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>ff02781753ff56e2878e61590c1bc390d86ef58b</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>30 MONEDAS</title>
		<info>(2020) 8 episodios. Judas traicionó a Jesucristo por 30 monedas de plata. 2000 años más tarde, una de ellas aparece en un pueblo remoto de España, desencadenando una serie de fuerzas sobrenaturales que amenazan con destapar secretos del Vaticano y aniquilar a la raza humana. En medio de todo estará el padre Vergara, un exorcista, boxeador y ex convicto exiliado en una parroquia del pequeño pueblo donde aparece la moneda. Vergara quiere olvidar y ser olvidado, pero sus enemigos lo encontrarán muy pronto... Cuando Vergara es relacionado con una serie de fenómenos paranormales ocurridos en el pueblo, Paco, el ingenuo alcalde, y Elena, una inquieta veterinaria, tratarán de desvelar los secretos de su pasado y el significado de la antigua moneda que Vergara mantiene oculta.</info>
		<year>2020</year>
		<genre>Thriller. Fantástico. Terror</genre>
		<thumb>https://image.tmdb.org/t/p/original/gmL6MSH3jK2T7zYvzo9dIZb393c.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/f8RGyElWxLiL1lLzC9KcPuUZIzf.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/gmL6MSH3jK2T7zYvzo9dIZb393c.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/f8RGyElWxLiL1lLzC9KcPuUZIzf.jpg</fanart.1>
			<ep1.1>1 AL 8 (1080)</ep1.1>
			<id1.1>aebb3d41bc20bfec556c1d623b32ffaa1e8604e1</id1.1>
			<ep1.2>1 TELARAÑAS</ep1.2>
			<id1.2>5412444a493a9341f46335625bba97102d676fbd</id1.2>
			<ep1.3>2 OUIJA</ep1.3>
			<id1.3>946b19980770efccc019d4f237099184c35a625c</id1.3>
			<ep1.4>3 EL ESPEJO</ep1.4>
			<id1.4>2deb700f484456d470cf39af9a42d074d9051d05</id1.4>
			<ep1.5>4 RECUERDOS</ep1.5>
			<id1.5>1ae6e7efe1cfe911ad15be5bdc614e67ee0202a6</id1.5>
			<ep1.6>5 EL DOBLE</ep1.6>
			<id1.6>d624306275dcee8687fe9c3b01b9fa5fb8a0cb51</id1.6>
			<ep1.7>6 GUERRA SANTA</ep1.7>
			<id1.7>d88e9c835e32b0250c386313fc832ace9e0213a8</id1.7>
			<ep1.8>7 LA CAJA DE CRISTAL</ep1.8>
			<id1.8>2c032371ebdae873536e623f625ff3d9b1eeb633</id1.8>
			<ep1.9>8 SACRIFICIO</ep1.9>
			<id1.9>74f5b54c94480187bf0bc6e0789193820a613459</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>9-1-1: LONE STAR</title>
		<info>(2020-2021) 2 temporadas. Un bombero de Nueva York se muda con su hijo a Austin, Texas, donde salva vidas al mismo tiempo que intenta solucionar sus problemas personales.</info>
		<year>2020</year>
		<genre>Drama. Acción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/eMCD1oYcv09CxDtY5KADxNAdUGj.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/s2jckv31cMn3oapgIyXRD11Y3lo.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/vE1iYEM52EHkhZSzcEnQP1Uvei8.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/Awd4X9pO0ZsE9h9MUGYimKReqmP.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>d17b50127cbdf2c924d31a249b00a4b5577cdb31</id1.1>
			<ep1.2>2 YIIIJAAA</ep1.2>
			<id1.2>40b31c92cb6676d7f619e3c08d7f16fe27ac7199</id1.2>
			<ep1.3>3 ORGULLO TEJANO</ep1.3>
			<id1.3>031c31338770c91c1343ab7ba2540100119344ba</id1.3>
			<ep1.4>4 DE FUERZA MAYOR</ep1.4>
			<id1.4>e72d189b2a5cccd96bcc09faced1a89c6ca02eeb</id1.4>
			<ep1.5>5 SEMENTALES</ep1.5>
			<id1.5>7cc8e9ef17b16897b4041005c791a08de576ff2a</id1.5>
			<ep1.6>6 AMIGOS ASI</ep1.6>
			<id1.6>9a1d46515c92ff274b3fca894c469962c7cd0b60</id1.6>
			<ep1.7>7 MAL CONSEJO</ep1.7>
			<id1.7>0e783cb5352f59962c4d709cefd6db5b6b15aee4</id1.7>
			<ep1.8>8 MONSTUO DENTRO</ep1.8>
			<id1.8>77e653afa9c1e37f4ee0741e509e54ba4b34bca5</id1.8>
			<ep1.9>9 DESPERTAR</ep1.9>
			<id1.9>b3be71bbb7f0d270d9c90b3332b59b65827222bb</id1.9>
			<ep1.10>10 AUSTIN, TENEMOS UN PROBLEMA</ep1.10>
			<id1.10>19a399c22580362399074ea3483fa9beb80b630f</id1.10>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/mrzE6bXc8g9NQd8IBKB25louWq8.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/g67qcWSqjLMcyIuG621Su9IwYWm.jpg</fanart.2>
			<ep2.1>1 VOLVER AL RUEDO</ep2.1>
			<id2.1>d5b1c48674efeb92f2eed87c053e0f1a3b75c049</id2.1>
			<ep2.2>2 1150 GRADOS</ep2.2>
			<id2.2>df19a24d7624b12ab1e5fb509fa788dd1b5ed621</id2.2>
			<ep2.3>3 RESISTE</ep2.3>
			<id2.3>2af2aea8688c1cbe9d183f7ee3ff3f0579ff1c5c</id2.3>
			<ep2.4>4 AMIGOS CON DERECHO A ROCE</ep2.4>
			<id2.4>79841c875843f88c752aad624f6d23cc39c0f692</id2.4>
			<ep2.5>5 CONVERSACIONES DIFICILES</ep2.5>
			<id2.5>feae29f8781bc3df7e730993d93bbb3539263e44</id2.5>
			<ep2.6>6 TODAS Y SUS HERMANOS</ep2.6>
			<id2.6>5432338a36b1b6498150443e7a03e596e6aa912f</id2.6>
			<ep2.7>7 DESPLAZADOS</ep2.7>
			<id2.7>76ccdd1d4d571c07df6aff6c2a5b0cea87ad9e85</id2.7>
			<ep2.8>8 MALA LLAMADA</ep2.8>
			<id2.8>bdf7d8e494fa157b64a05abccd42ffa60e0fc94c</id2.8>
			<ep2.9>9 SALVACION</ep2.9>
			<id2.9>1b3fe634edc33f928d594fcde794488612a466ff</id2.9>
			<ep2.10>10 UNA PEQUEÑA AYUDA DE MIS AMIGOS</ep2.10>
			<id2.10>b64d9f205801f530fb6ee87d833a6a269c260fcd</id2.10>
			<ep2.11>11 QUEMADURA LENTA</ep2.11>
			<id2.11>09a5a911c687efe7522d5167e832b1f66245d8ad</id2.11>
			<ep2.12>12 EL GRAN CALOR</ep2.12>
			<id2.12>5f39cb4fbc59c0e019945e4c868e4e258849c7e2</id2.12>
			<ep2.13>13 UN DIA</ep2.13>
			<id2.13>e6233411f300940c318de88cb13ad1123bd72b34</id2.13>
			<ep2.14>14 POLVO AL POLVO</ep2.14>
			<id2.14>f3c97949a9e330f2ec6ce0487291a002a0ba0d7d</id2.14>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ABSENTIA</title>
		<info>(2017) 3 temporadas. Mientras persigue a un asesino en serie, una agente del FBI desaparece y es dada por muerta, dejando solos a su marido, también agente, y a su hijo de 3 años. Pero 6 años después reaparece para sorpresa de todos. Sin recordar nada de lo sucedido desde su secuestro, tiene que adaptarse a una nueva realidad en la que su marido ha rehecho su vida con otra mujer y su hijo no la conoce. Además, una nueva serie de asesinatos hacen pensar a algunos de sus compañeros que ella podría ser la autora.</info>
		<year>2017</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/1pQvYUtSx7q1EtHv93z1QutrTkd.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/A1x0NTRerLlmiTGrx2PeSiy6nsq.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/1pQvYUtSx7q1EtHv93z1QutrTkd.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/A1x0NTRerLlmiTGrx2PeSiy6nsq.jpg</fanart.1>
			<ep1.1>1 EL REGRESO</ep1.1>
			<id1.1>VSZFEQW75M7CDSUIMLQ555IVFDQLKQWS</id1.1>
			<ep1.2>2 REINICIAR</ep1.2>
			<id1.2>9ad2949ce854cc2fd4fc9451a3435ef1b3aa6f95</id1.2>
			<ep1.3>3 EL ESPECTACULO DE EMILY</ep1.3>
			<id1.3>1ee912bd7f1312864eb9df91e81f2b54aa0988ba</id1.3>
			<ep1.4>4 YO, TU, EL, YO</ep1.4>
			<id1.4>70e0977da5c2688f7cf6665df3efa4d8ea1ae552</id1.4>
			<ep1.5>5 EXCAVAR</ep1.5>
			<id1.5>89966239dec3f82071be38eab4f3f655a05ea1f7</id1.5>
			<ep1.6>6 NADIE ES INOCENTE</ep1.6>
			<id1.6>eb69198f71edc77f5f0d1548958ace589f2287c5</id1.6>
			<ep1.7>7 A Y B</ep1.7>
			<id1.7>3cbbd2e5bc1d57acde1e6b0055940fff341e129d</id1.7>
			<ep1.8>8 CHICO VALIENTE</ep1.8>
			<id1.8>54f79ee62db2e24ee05a91ddc839d2db7ada647e</id1.8>
			<ep1.9>9 JUEGO DE NIÑOS</ep1.9>
			<id1.9>add0c90faf5c693599c4f2f5248002c8a99aa781</id1.9>
			<ep1.10>10 PECADO ORIGINAL</ep1.10>
			<id1.10>25d49cafbd87a0d4f16c20efd0a866a4064f77f9</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/jIYIzD7KPFfURrjBDLU5gBDBMrD.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/OwDuNYVpVsC4kBcGvniaEraUM7.jpg</fanart.2>
			<ep2.1>1 BAJAS</ep2.1>
			<id2.1>3bf7d02ca7cf1c66bbca734ec84b878eb8aecfbe</id2.1>
			<ep2.2>2 LOCURA</ep2.2>
			<id2.2>01ee5d3ac04c7f055edd87a0688f739b25ba8e6d</id2.2>
			<ep2.3>3 CULPABLE</ep2.3>
			<id2.3>63f83a0787d539fe00a7cbd05106066618203c50</id2.3>
			<ep2.4>4 AGRESORES</ep2.4>
			<id2.4>f2dae2c543bf5e1b15eba076e0de35df4421be86</id2.4>
			<ep2.5>5 ORDEN DE BUSQUEDA</ep2.5>
			<id2.5>629f99f1455042c8b110a4518eba1889560041c6</id2.5>
			<ep2.6>6 TAPADERA</ep2.6>
			<id2.6>26aa85668e646071e52ea13552486fc4ac72ebfc</id2.6>
			<ep2.7>7 BUM</ep2.7>
			<id2.7>6deea53f184ed7ca2d3f79c5b404ef477b31475a</id2.7>
			<ep2.8>8 AGRESION</ep2.8>
			<id2.8>b83a86dd1aeac6328e8a466db5decdb49cd4cfd7</id2.8>
			<ep2.9>9 COMPROMETIDA</ep2.9>
			<id2.9>a4961e97a46cd25e12d97a573436b30c2d3c400b</id2.9>
			<ep2.10>10 COMPLICE</ep2.10>
			<id2.10>22910d4312ddfc27947fc0d2f32213b3d7bfed68</id2.10>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/kj0K3UgfPSGAJcuM2qFZGHCLA73.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/a4kdFZMeYgoChht3dBoZbclkjKK.jpg</fanart.3>
			<ep3.1>1 TABULA RASA</ep3.1>
			<id3.1>1319b245a89a741aac810f1283e25fd10311ff7c</id3.1>
			<ep3.2>2 AL 3</ep3.2>
			<id3.2>8c638907625b7f0adc27a9e541c5a539b8fcedba</id3.2>
			<ep3.3>4 AL 8</ep3.3>
			<id3.3>d6ea1d7e1bd5c5d5d2160c1f78538a74217b6b17</id3.3>
			<ep3.4>9 TENEBRIS</ep3.4>
			<id3.4>c0b76f5d193f035d9de893a380031dc1a1d565e2</id3.4>
			<ep3.5>10 ITERUM NATA</ep3.5>
			<id3.5>afe5f836e3195ce9c766f4da1a5e0cd282b4a337</id3.5>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ALBA</title>
		<info>(2021) 13 episodios. Alba es una joven sin miedo. Sin embargo, tras volver a su pueblo por vacaciones, una prometedora noche de fiesta se tiñe de tragedia cuando un grupo de chicos la agrede sexualmente. Al día siguiente, se despierta desnuda en la playa.Un año antes, cuando dejó atrás su previsible vida, no podía imaginar que en la gran capital se reencontraría con Bruno, su vecino de siempre, y por el que jamás se había interesado. Lo que, separados por una simple calle, nunca sucedió, se hace posible en una ciudad de cuatro millones de habitantes. En un encuentro casual, tan improbable como mágico, Alba y Bruno se enamoran perdidamente. Nada podía separarles. Hasta esa noche fatídica.Inexplicablemente, tres de los cuatro agresores, son los mejores amigos de Bruno. Pero cuando Alba descubre la identidad del cuarto hombre, el final de la pesadilla es, en realidad, el principio.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/yYAVJDedf6oB8lLOcFqQZhh1L0n.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/4Qlw70ARL2ZobTj1Xmu3aOxSMfk.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/yYAVJDedf6oB8lLOcFqQZhh1L0n.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/4Qlw70ARL2ZobTj1Xmu3aOxSMfk.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>1918dd6d4d360d8dfa44f2c2ddfffa8ebf36a1d8</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>4445b4986755541567b07c7b7b97f8c611e414cc</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>612c669e8f2344c57a30329343202279041f1c15</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>073e607e6147225bf288a33088ad4e3e9397984a</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>18059a0c88d3a11fc56cf8deff6d7dc14690cd84</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>146680d404465ee01a19c0121aff749d69721448</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>6d52ee6a408a310450954ce0aa669a459b8e2a4b</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>5f7d1339ebec6e354cc3557dec73dd688e6fd10e</id1.8>
			<ep1.9>9 Episodio</ep1.9>
			<id1.9>00ef43ab07220a07ea1350af2c2c8f5143fd2096</id1.9>
			<ep1.10>10 Episodio</ep1.10>
			<id1.10>9e4083202cd1882a3b89156110ba992067838f01</id1.10>
			<ep1.11>11 Episodio</ep1.11>
			<id1.11>f4663ea296aea0135c59bc2a04d5dcf1c75f3943</id1.11>
			<ep1.12>12 Episodio</ep1.12>
			<id1.12>15dbf82f691d181a2aac26fc6e81349647fcda7d</id1.12>
			<ep1.13>13 Episodio</ep1.13>
			<id1.13>1a1ea89df4044e8cee83b2dfc11463226bb25e32</id1.13>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ALEF</title>
		<info>(2000) 2 temporadas. 16 episodios. Dos detectives intentan desentrañar el misterio de los asesinatos que siguieron al descubrimiento de un cuerpo en el Bósforo.</info>
		<year>2020</year>
		<genre>Acción. Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/6CyNjKgbgs6dTBKNkUJsEfMnX1B.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/q4B1PGjWB6NYYe1Gamix5pCEXEr.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/6CyNjKgbgs6dTBKNkUJsEfMnX1B.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/q4B1PGjWB6NYYe1Gamix5pCEXEr.jpg</fanart.1>
			<ep1.1>1 AL 8 (1080)</ep1.1>
			<id1.1>20E84973796B6C9555273DA665FB7571569FB886</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ALGUIEN ESTA MINTIENDO</title>
		<info>(2021) 8 Episodios. Un castigo reúne a cinco alumnos de instituto muy diferentes. Pero un asesinato (y varios secretos) los mantendrán unidos durante un misterioso juego del gato y el ratón.</info>
		<year>2021</year>
		<genre>Intriga. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/hKMpbHY7xqFKaBR8W7jE61JlQB6.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/tn8bg1KDNX74slOCytA5xnqzi2W.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/hKMpbHY7xqFKaBR8W7jE61JlQB6.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/tn8bg1KDNX74slOCytA5xnqzi2W.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>1a050c59641d912b9b80dfe9a2c8657ad69e6207</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>2a0a736614f6b99fb0bd47d83fa8af59bbf62ce5</id1.2>
			<ep1.3>7 AL 8</ep1.3>
			<id1.3>71e833896b62980c8268c1485fbf25e9c213c9dc</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ALGUIEN TIENE QUE MORIR</title>
		<info>(2020) 3 episodios. España, años 50. La historia comienza con un joven al que sus padres le piden que vuelva de México para que puedan conocer a su prometida. Él regresa acompañado de Lázaro, un misterioso bailarín de ballet.</info>
		<year>2020</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/1m9BE0izm65ObtioKuKvCbfIko4.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/xk2Re8x8zqRDDKjJfcQC3Iu5UR4.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/1m9BE0izm65ObtioKuKvCbfIko4.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/xk2Re8x8zqRDDKjJfcQC3Iu5UR4.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>962a7e8ab9b26ff300de3508792de74cf682ec3b</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ALICE IN BORDERLAND</title>
		<info>(2020) 8 episodios. Un joven obsesionado con los videojuegos y dos amigos suyos se ven atrapados en una extraña versión de Tokio donde deberán competir en peligrosos juegos para sobrevivir.</info>
		<year>2020</year>
		<genre>Ciencia ficción. Fantástico. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/m6f9b9PENqNWAF5lfZ1f4F2YUCs.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8edzqU74USlnfkCzHtLxILUfQW3.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/m6f9b9PENqNWAF5lfZ1f4F2YUCs.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8edzqU74USlnfkCzHtLxILUfQW3.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>ADFC046FA3A1905712FB0A92741C495E6F93254F</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>AMERICAN CRIME STORY:  EL CASO LEWINSKY</title>
		<info>(2021) 10 episodios. Narra el escándalo del Presidente Bill Clinton y la becaria de la Casa Blanca Monica Lewinsky en 1998, y los eventos posteriores durante la presidencia de Clinton, basados en el libro de Jeffrey Toobin "A Vast Conspiracy: The Real Story of the Sex Scandal That Nearly Brought Down a President."</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/7fsY2ocRn5A1xUCs8KHtV4c1aBl.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dgpmyFwMYDZXj3AagNQppVhvgnx.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7fsY2ocRn5A1xUCs8KHtV4c1aBl.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/dgpmyFwMYDZXj3AagNQppVhvgnx.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>2c97973b34e09f7d39d97ddb9a9e4b2d934a9df8</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>1dc2f22b0a121f1485d63dd469ef6ca2d5d31b9e</id1.2>
			<ep1.3>5 ¿ESCUCHAS LO MISMO QUE YO?</ep1.3>
			<id1.3>caaaf725285a5b1531b40dec5d81da73d78f8c78</id1.3>
			<ep1.4>6 MALTRATADA</ep1.4>
			<id1.4>4dd3cdc4bc6c9f13edd9c2947924701386c23d18</id1.4>
			<ep1.5>7 EL ASESINATO DE MONICA LEWINSKY</ep1.5>
			<id1.5>d44bf113535d648fea11b8dc0e1dff927224bb7c</id1.5>
			<ep1.6>8 APOYANDO A TU HOMBRE</ep1.6>
			<id1.6>fb267f7231581642750b1861f9fdd9c6e796d15b</id1.6>
			<ep1.7>9 EL GRAN JURADO</ep1.7>
			<id1.7>58926083b1decf6b54204b8424b335df94ea42bb</id1.7>
			<ep1.8>10 EL DESIERTO</ep1.8>
			<id1.8>8a1cc00a8b33a3856184f5886b1a10915920e0b0</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>AMERICAN HORROR STORIES</title>
		<info>(2021) 7 episodios.  Spinoff de la franquicia "American Horror Story", que contará con varios episodios independientes entre sí al estilo "Masters of Horror" o "Black Mirror".</info>
		<year>2021</year>
		<genre>Terror</genre>
		<thumb>https://www.themoviedb.org/t/p/original/jLhQ6P95ENHafPNisx0kQuNYCfO.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/idiZ5I1H4bk5t5ZipOnTbxncI6a.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/jLhQ6P95ENHafPNisx0kQuNYCfO.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/idiZ5I1H4bk5t5ZipOnTbxncI6a.jpg</fanart.1>
			<ep1.1>1 EL HOMBRE (O MUJER) DE LATEX</ep1.1>
			<id1.1>2dd64eec20abfaf3dc33c4b68e41ddf87d18076e</id1.1>
			<ep1.2>EL HOMBRE (O MUJER) DE LATEX (2)</ep1.2>
			<id1.2>c1673e96d4442710e250f68cbee2dfa53289824f</id1.2>
			<ep1.3>3 AUTOCINE</ep1.3>
			<id1.3>2947f0036f9826bc21820a181865fec619b22404</id1.3>
			<ep1.4>4 LA LISTA NEGRA</ep1.4>
			<id1.4>b35d67c7786e79c626053a291a89c5669e4244a5</id1.4>
			<ep1.5>5 BA'AL</ep1.5>
			<id1.5>2af5cfa0558db66c5b61c9629c6a0dddbc803dda</id1.5>
			<ep1.6>6 SALVAJE</ep1.6>
			<id1.6>97284ae451409281b89bd92a218c0cc7a8e92cae</id1.6>
			<ep1.7>7 FIN DEL JUEGO</ep1.7>
			<id1.7>5cee8a8397a15cb908863b3a7128dbb06f147761</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>AMERICAN RUST</title>
		<info>(2021) 9 episodios. En una ciudad del cinturón obrero del suroeste de Pensilvania, el jefe de policía (Jeff Daniels) se verá obligado a decidir hasta dónde está dispuesto a llegar en su dilema entre el deber y el corazón cuando el hijo de la mujer que ama (Maura Tierney) es acusado de asesinato.</info>
		<year>2021</year>
		<genre>Drama </genre>
		<thumb>https://www.themoviedb.org/t/p/original/w0IdvPF5f1YzhqMwZe2qqp6MPEY.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/n00mqaR8e8QRrh9cNkpT9DoOPdM.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/w0IdvPF5f1YzhqMwZe2qqp6MPEY.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/n00mqaR8e8QRrh9cNkpT9DoOPdM.jpg</fanart.1>
			<ep1.1>1 LA FABRICA</ep1.1>
			<id1.1>cea1e4b5890329d77060a328e6b1971e85d2e6fa</id1.1>
			<ep1.2>2 FELICES RETORNOS</ep1.2>
			<id1.2>62fb2c909659a7fb934d4de3cd6a470a6688a33b</id1.2>
			<ep1.3>3 PERDONA NUESTRAS OFENSAS</ep1.3>
			<id1.3>f5b803b539d348b92d2f477d9ce1b079359f515f</id1.3>
			<ep1.4>4 ME LLAMO BILLY</ep1.4>
			<id1.4>87c262ea5ea75c4fe1999bd17d404238b9359b7b</id1.4>
			<ep1.5>5 JOJO AMERI-GO</ep1.5>
			<id1.5>c25300848257cc9646a2afdb7f5943bb0136dcaf</id1.5>
			<ep1.6>6 COBRO DE DEUDAS</ep1.6>
			<id1.6>74337c1f62ca4bc737d74e760057be758b7dd192</id1.6>
			<ep1.7>MONTAÑAS AZULES</ep1.7>
			<id1.7>a939a22950309b93c970279c563995a676d8f6c3</id1.7>
			<ep1.8>8 SAN SEBASTIAN</ep1.8>
			<id1.8>bb555332eb7e590f0417caf493b13e893d7e0c43</id1.8>
			<ep1.9>9 DINAMARCA</ep1.9>
			<id1.9>fc9d795bd25dc7bbd06e4f99e3fe3b64d0fd1263</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ANA BOLENA</title>
		<info>(2021) 3 episodios. Sigue los últimos meses de la vida de Ana Bolena, su lucha con la sociedad patriarcal de la Inglaterra Tudor, su deseo de asegurar un futuro para su hija, Elizabeth, y su incapacidad para dar a Henry un heredero varón.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/hBDQUB2elhHMHb23RzpAlXBfYlo.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3ALdtNi48aL3p73RhvF8iY0dZRh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/hBDQUB2elhHMHb23RzpAlXBfYlo.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3ALdtNi48aL3p73RhvF8iY0dZRh.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>32bb3d86b9797127f88409f3feb75868a40e93f7</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>29f025e18cf7566c5b7c918996839d57928b975a</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>158122fe934f1e69dcee5c954b8c315b337221ae</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ANA TRAMEL: EL JUEGO</title>
		<info>(2021) 6 episodios. Ana Tramel es una brillante abogada penalista que vive sus horas más bajas. Una llamada de su hermano Alejandro, al que hace años que no ve, le hace ponerse de nuevo en marcha: ha sido acusado de asesinar al director del casino Gran Castilla. Rodeada de un pequeño equipo de confianza, se tendrá que enfrentar a una enorme corporación de la industria del juego.</info>
		<year>2021</year>
		<genre>Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/z2U4dwEMgmPxMzFt4OfCBfOjink.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nU3fHKW6sZMOG0INvEgKh3oUoXZ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/z2U4dwEMgmPxMzFt4OfCBfOjink.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nU3fHKW6sZMOG0INvEgKh3oUoXZ.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>c420bb2e406cc0f87b633871a5c6180533e32140</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>26b37b1be9c768dc564d13f86453374cc7c88900</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ANATOMIA DE UN ESCANDALO</title>
		<info>(2022) 6 episodios. Un escándalo de consentimiento sexual se desata entre la élite británica privilegiada y las mujeres atrapadas a su paso</info>
		<year>2022</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/e1pUQkXbFNI6xH4oDuHnyJoOSib.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8zPoUJECL6X8G5T95OuJDorHdtV.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/e1pUQkXbFNI6xH4oDuHnyJoOSib.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8zPoUJECL6X8G5T95OuJDorHdtV.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>51ae18b551f3cdd133bbec2d0b61dd45ae85b0e7</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>ac8ed5f270157e519fb8e11d5ad0fdb714703e1a</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>f0d5c354fe27f03c6d3d433bca61a5ccd5f95b58</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>10647a24bcc7866f8d4c61d16537f1db4256fb69</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>ca15ede9e40cb4f1ddd173003587abcc860f88e4</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>b1504e8d262a4704b3cbfeb19cb0d81587db4377</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>AND JUST LIKE THAT</title>
		<info>(2021) 10 episodios. Secuela de la aclamada "Sex and the City" (1998-2004) en formato miniserie y ambientada en la actualidad. Sin el personaje de Samantha (Kim Cattrall), la serie sigue a las neoyorquinas Carrie, Miranda y Charlotte mientras navegan por el viaje desde la complicada realidad de la vida y la amistad de los 30 hasta la realidad aún más complicada de la vida y la amistad a los 50.</info>
		<year>2021</year>
		<genre>Romance. Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/13Esk3YutOAfm9RYJ33wHlHJPHv.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7MJuYB1hblyis4kTKPCwSlWuKoi.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/13Esk3YutOAfm9RYJ33wHlHJPHv.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7MJuYB1hblyis4kTKPCwSlWuKoi.jpg</fanart.1>
			<ep1.1>1 HOLA, SOY YO</ep1.1>
			<id1.1>9b42e656c6c6a2c68da98db540418936f4c65368</id1.1>
			<ep1.2>2 VESTIDITO NEGRO</ep1.2>
			<id1.2>82d6bcc01e973792a2f24c2d2d68ed6b67e50412</id1.2>
			<ep1.3>3 EN ROMA…</ep1.3>
			<id1.3>830c6c77881e149d11287c3f63dff5fc9080ae9e</id1.3>
			<ep1.4>4 ALGUNAS DE MIS MEJORES AMIGAS…</ep1.4>
			<id1.4>9ea34a0de7cf1d469baa4c543c8d7a7b6e0f08ce</id1.4>
			<ep1.5>5 TRAGICAMENTE A LA ULTIMA</ep1.5>
			<id1.5>6db0231c86384611d020022006919b95316b3b89</id1.5>
			<ep1.6>6 DIWALI</ep1.6>
			<id1.6>9ef99d61d8de837e41d8f5b99e943a38fe610840</id1.6>
			<ep1.7>7 EL SEXO Y LA VIUDA</ep1.7>
			<id1.7>1f34a7f490512e34dc66c83a0c25a7b382ef42c8</id1.7>
			<ep1.8>8 EMBRUJADA MOLESTA Y DESCONCERTADA</ep1.8>
			<id1.8>5e03834986952fd9170450cef1efa0ca9d3ad58d</id1.8>
			<ep1.9>9 SIN CONDICIONES</ep1.9>
			<id1.9>f8b6594bb7c3316f183ef2e3d592a14607976821</id1.9>
			<ep1.10>10 LA LUZ</ep1.10>
			<id1.10>6ef3d5f0c10a3313abba02483ccf6a347d1b7189</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ANDRES INIESTA: EL HEROE INESPERADO</title>
		<info>(2020) 1 episodio. Documental que ofrece una mirada íntima a la vida y carrera de Andrés Iniesta, abarcando el inicio de su trayectoria en La Masía, la academia juvenil del FC Barcelona, hasta su ascenso meteórico a la fama jugando para el Barça y la Selección Española, donde sus habilidades en el campo llevaron al conjunto nacional a llevarse el Mundial en 2010, y su llegada al Vissel Kobe en Japón. Junto al testimonio del propio Iniesta, el documental le rinde homenaje con entrevistas a jugadores de renombre mundial como Lionel Messi, Neymar, Luis Suárez, Pep Guardiola o Xavi Hernández. El documental está escrito por el periodista Marcos López, quien ha seguido de cerca la carrera de Iniesta y escribió su biografía oficial, "Andrés Iniesta: la jugada de mi vida".</info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/b1rVIl8DMtcJh2adu2SER4x3D3v.jpg</thumb>
		<fanart>https://i.imgur.com/VyjwYMM.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/b1rVIl8DMtcJh2adu2SER4x3D3v.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/VyjwYMM.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>ca6f79ae8d5f02d2c7e962e380714cebfc738199</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ANIMAL KINGDOM</title>
		<info>(2016) 5 temporadas. 62 epsidodios.  Un joven de 17 años va a vivir con su abuela y sus tíos en una ciudad de playa del sur de California después de que su madre sufra una sobredosis. Allí comienza a disfrutar de la vida pero no tarda en descubrir que la fortun familiar viene de una serie de actividades delictivas.</info>
		<year>2016</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/5nRopHfJnBsCQxr8a56svu63V6H.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/c4Y9IQpJ9KQxuRRfTW6425Z07is.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/j1Nog8ArfdbQXYwMjMUpnHyi8y5.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/9c6fuzHFKmSE3UBcZ470SvsEJtW.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>A927118C0F35803B308AC205AC7BB1C96DB67DCE</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/l9Ld8CWM5Lx1XjLdvIFcIuACEcb.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/uUnunhPAClhgQVXCyR3abdbcBaE.jpg</fanart.2>
			<ep2.1>1 AL 13</ep2.1>
			<id2.1>770EDB8D657E11BAE2921A71C6B2D2F36E033848</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/yfeUKQPfhcemXBKKswq1KtxXFsQ.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/sqj8VbIpBPls1sMLIYErcszskYZ.jpg</fanart.3>
			<ep3.1>1 AL 13</ep3.1>
			<id3.1>85741BB3A87E5B776E5CE1FB19427CC342A036F1</id3.1>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/tpnHrx2UuBF0lQiZsGP40t4P9ml.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/vROmQIKHKo0ZRaDFcVyq5eJTsoi.jpg</fanart.4>
			<ep4.1>1 AL 13</ep4.1>
			<id4.1>CB2E430455364A89E0CB65AC5F61A9C20F906B26</id4.1>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/wOPVaXMZ61Z0t2QezdQWvZbPCLi.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/eQJwfyMqSra10ck8HOoiCrbQR32.jpg</fanart.5>
			<ep5.1>1 AL 13</ep5.1>
			<id5.1>38F5CE4942704FA906C41646DEDA80B87C538E72</id5.1>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ANTIDISTURBIOS</title>
		<info>(2020) 6 episodios. Seis antidisturbios ejecutan un problemático desahucio en el centro de Madrid, pero el desalojo se complica y sucede una tragedia. Un equipo de Asuntos Internos de la Policía será el encargado de investigar los hechos, ante los cuales los seis policías podrían enfrentarse a una acusación de graves consecuencias. El grupo de agentes busca una salida por su cuenta, complicando aún más la situación. La joven Laia Urquijo, una de las agentes de Asuntos Internos, se obsesiona con el caso y comienza a investigar, descubriendo poco a poco mucho más de lo que jamás hubiera imaginado.</info>
		<year>2020</year>
		<genre>Thriller. Drama. Acción</genre>
		<thumb>https://image.tmdb.org/t/p/original/cNvviNPWJFJMbFXgiOukfG02VhX.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/aNUTY1pQKe66rSOW67TCQB5RzKK.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/cNvviNPWJFJMbFXgiOukfG02VhX.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/aNUTY1pQKe66rSOW67TCQB5RzKK.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>d819a7a8e4f3cdefd5ec5b5cefea22eaef0dc42d</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ARCHIVO 81</title>
		<info>(2022) 8 episodios. Contratado para restaurar una colección de cintas, un archivista terminó convirtiendo la obra y las investigaciones de una cineasta en el objeto de un peligroso culto.</info>
		<year>2022</year>
		<genre>Terror. Drama. Ciencia ficción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/l1jeSEm6a88GFcLzlHQr60D0dOi.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5N0Zc0rmcdhP7JJc71egjzU6dDQ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/l1jeSEm6a88GFcLzlHQr60D0dOi.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5N0Zc0rmcdhP7JJc71egjzU6dDQ.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>f1bd9b8839ba31335687651488ee7cc964141dcf</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>7cb1cb01b712d613e96c403b5a3237fbe35f3077</id1.2>
			<ep1.3>7 AL 8</ep1.3>
			<id1.3>48a93b7a5cc2de3b90a5ea7e45db8625ff98a666</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ARDE MADRID</title>
		<info>(2018) 8 episodios. Serie sobre la vida de la actriz norteamericana Ava Gardner en Madrid, vista desde la óptica de las personas que estaban al servicio de la estrella. Ambientada en 1961, la historia comienza cuando a Ana Mari (Inma Cuesta), una solterona coja e instructora en la Sección Femenina, le hacen el encargo, por orden de Franco, de entrar a trabajar en la vivienda de Ava Gardner con la intención de espiarla. Para ello tiene que fingir que está casada con Manolo (Paco León), chófer de la actriz y buscavidas a tiempo completo. Ambos serán testigos, junto a otra joven criada (Anna Castillo), de las continuas fiestas que acaban con los nervios de los vecinos, la pareja formada nada menos que por el general Juan Domingo Perón y su esposa, recién exiliados de Argentina... El glamour de un icono de Hollywood y su libertad en la cama frente al analfabetismo sexual de los españoles, y el derroche versus la miseria, esta es la historia de la España franquista más gris y cómo fue testigo del torbellino que provocó la estancia de "el animal más bello del mundo" en la capital española.</info>
		<year>2018</year>
		<genre>Comedia. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/kPSbTjQ6wev3iGHh8fmcyn12aL7.jpg</thumb>
		<fanart>https://i.imgur.com/rQRMMSv.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/kPSbTjQ6wev3iGHh8fmcyn12aL7.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/rQRMMSv.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>766eeedff5b292293f42596d9c28fb2299a2a270</id1.1>
			<ep1.2>5 AL 8</ep1.2>
			<id1.2>e3824165af4b82ba87d7bc7497e79768ba8f3426</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ARMA LETAL</title>
		<info>(2016) 3 temporadas. 53 episodios. Martin Riggs es un antiguo agente especial de los SEAL ahora reconvertido en agente del cuerpo de policía de Los Ángeles. Riggs, natural de Texas, es de carácter volátil y suicida debido a la reciente muerte de su mujer embarazada. Por esto, su llegada a la ciudad causará una gran impresión, especialmente para su nuevo compañero de trabajo, el detective Roger Murtaugh, quien acaba de reincorporarse al cuerpo de policía después de haber sufrido un ataque al corazón y debe de mantenerse alejado de las situaciones que puedan causar estrés en su vida diaria. Lethal Weapon es una serie policial de acción que mezcla elementos de drama y comedia. Inspirada en la película del mismo nombre dirigida por Richard Donner (Los Goonies) y con un guión escrito por Shane Black (Kiss Kiss Bang Bang). Con un reparto encabezado por Clayne Crawford (Un paseo para recordar) en el papel de Martin Riggs y Damon Wayans (Mi esposa y mis hijos) como el agente Roger Murtaugh, esta serie sirve como reboot televisivo para la conocida saga de películas de los ochenta y noventa. Esta serie está desarrollada por el productor Matt Miller (Forever, Chuck) para Fox.</info>
		<year>2016</year>
		<genre> Acción. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/mB6hOcYQI5qOz9nMWBbcXvGodHw.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/311nGktbCga4YV9QyxOaH8RlVOL.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/iUOdcRvRXL2CyBnKVYZMHkprbcI.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/hf6gEvWSfVsPQpDXUGk40dTwsEm.jpg</fanart.1>
			<ep1.1>1 AL 18</ep1.1>
			<id1.1>47626876321FBC188DF5B21F83DF046D2C2FCDE5</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/grGOtTZ0OfNQoWnNtdJOiY1LB8e.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/yqZ5ACKeNJ30mylUEzvtWZu4pGU.jpg</fanart.2>
			<ep2.1>1 AL 22</ep2.1>
			<id2.1>28619C26ED3498E4FC33B8AB379951F3C8DE8AB7</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/jbMZkxLHVDSKiZh0HppCthRW96k.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/4p56VQGCk5RvwzAblyNqJUcI9sp.jpg</fanart.3>
			<ep3.1>1 AL 15</ep3.1>
			<id3.1>481293385B47F45DFF6532B4E413AD260D585696</id3.1>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BANG BANG BABY</title>
		<info>(2022) 10 episodios. Drama criminal ambientado en 1986. Alice tiene dieciséis años y vive en un pequeño pueblo del norte de Italia. Su vida de adolescente tímida cambia bruscamente al descubrir que su padre, al que creía muerto, sigue vivo. Para Alice, es el comienzo de un descenso al infierno: por amor a su padre, se sumerge de lleno en el peligroso mundo de la mafia y se convierte en la miembro más joven de la organización.</info>
		<year>2022</year>
		<genre>Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/7nuEdV5L60JlYnyFFPPR9NxZnUF.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/jQvO5oxCfeENSfTQXrDdSrFOlgL.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7nuEdV5L60JlYnyFFPPR9NxZnUF.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/jQvO5oxCfeENSfTQXrDdSrFOlgL.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>fe06f0610e4b9b90e2001add05de03dc3b916472</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>1fff126076857ce8d808a441d6bf0a556b290f0b</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>d97b1deb4fc3f582bb56e1292d8f87b288934eba</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>5f25118881feebe4be7200d6b5a55df2024fd0b3</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>90f6e336d4580c75144d6b01e5a6fa7f5d0f6235</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>2a80f41a60445d611ea573f6a06587c2f81c2f58</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>d1320ba06445f594b3d33b6a76675091dea454a7</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>82326da9ab60d8d7f65de8630b1ece8a436f726d</id1.8>
			<ep1.9>9 Episodio</ep1.9>
			<id1.9>68983c2f3655798203b95b98d3dbd8b2c070593b</id1.9>
			<ep1.10>10 Episodio</ep1.10>
			<id1.10>b9f3933c0653c1c005ca88f90fed371f86f4db07</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BARRY</title>
		<info>(2018) 3 temporadas. 24 episodios. Barry es un asesino a sueldo que descubre su vocación de actor durante una misión en Los Angeles. A partir de ahí tendrá que decidir si debe dedicarse a lo que se le da bien pero no le motiva, o dejarse llevar por su sueño y meterse en el mundillo de la interpretación.</info>
		<year>2018</year>
		<genre>Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/gxSRN9huAmJP53nB0tHNT3zLwdV.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/RicS5yhtsn03MoHMvaDDFNzic8.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/gxSRN9huAmJP53nB0tHNT3zLwdV.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/RicS5yhtsn03MoHMvaDDFNzic8.jpg</fanart.1>
			<ep1.1>1 DEJA TU MARCA</ep1.1>
			<id1.1>98292b232dd70475964fdaac9caa79015915e2d2</id1.1>
			<ep1.2>2 USALO</ep1.2>
			<id1.2>b365070a0d57c5605881f91c2c4929912d59c8d0</id1.2>
			<ep1.3>3 ASUME EL RIESGO</ep1.3>
			<id1.3>2c5b126b615e56e694226faea1ab637ed03b7521</id1.3>
			<ep1.4>COMPROMETETE... CONTIGO</ep1.4>
			<id1.4>5e5e8a6abcf46962a59f3a01b23efdec8a0c7e1b</id1.4>
			<ep1.5>5 HAZ TU TRABAJO</ep1.5>
			<id1.5>c5d5210b903721e99cd242a66d7c1d507ee77207</id1.5>
			<ep1.6>6 ESCUCHA CON LOS OIDOS,REACCIONA CON LA CARA</ep1.6>
			<id1.6>2192755e44e83997a6ff7b649caf836244ee06d6</id1.6>
			<ep1.7>7 ALTO,RAPIDO Y SIN PARAR</ep1.7>
			<id1.7>92619e89258e6b1de1ecaaf508a8a3610dafcc3c</id1.7>
			<ep1.8>8 CONOCE TU VERDAD</ep1.8>
			<id1.8>df25817d5af0dc4bd09d697cad1ac29124406345</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/qXq4yyARgAS5QqhwsNyYitfpokR.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/RicS5yhtsn03MoHMvaDDFNzic8.jpg</fanart.2>
			<ep2.1>1 EL ESPECTACULO DEBE CONTINUAR ¿SEGURO?</ep2.1>
			<id2.1>632b884d5c45647d819b7ea4b32478f839f4b3bd</id2.1>
			<ep2.2>2 EL PODER DEL NO</ep2.2>
			<id2.2>b7b3cefc6153f2e7b3f41cd5b727100d2e335f95</id2.2>
			<ep2.3>3 PASADO = PRESENTE X FUTURO PARTIDO POR AYER</ep2.3>
			<id2.3>1cee9f695740b15bbeb35ac8401b06e8675887a1</id2.3>
			<ep2.4>4 ¡¿QUE?!</ep2.4>
			<id2.4>c549e8720acf39abf051203f1dad179fee48789e</id2.4>
			<ep2.5>5 RONNY / LILY</ep2.5>
			<id2.5>ea359f536981e38802b7291e9f18114d2d51d196</id2.5>
			<ep2.6>6 LA VERDAD TIENE UN ANILLO</ep2.6>
			<id2.6>880effcf7bd7c0ebb4c298eac9357565aad3fb0c</id2.6>
			<ep2.7>7 LA AUDICION</ep2.7>
			<id2.7>987e0c8603aefab305cd69b652620b16cc8d438e</id2.7>
			<ep2.8>8 BERKMAN &gt; BLOCK</ep2.8>
			<id2.8>b0a4bc625f86c5c750306d34a320c48f7e59625e</id2.8>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/j1XpwD11f0BAEI7pX6UdMhUVX2F.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/g1BRILW3LLd6LELSCVthlXb27ug.jpg</fanart.3>
			<ep3.1>1 PERDONANDO A JEFF</ep3.1>
			<id3.1>21947c68a420ed5d3727c5ed90e4722bacdf3e7c</id3.1>
			<ep3.2>2 LIMONADA</ep3.2>
			<id3.2>468d588f02eb8f9f44d1941bb5267a59564e4207</id3.2>
			<ep3.3>3 BEN MENDELSOHN</ep3.3>
			<id3.3>f84f721ea4f66ae40e31e47abecbd3be50e9ccbd</id3.3>
			<ep3.4>4 TODAS LAS SALSAS</ep3.4>
			<id3.4>6f0d0106488704e240a2d6efdff9b3a7f081efc3</id3.4>
			<ep3.5>5 UNVERDADERODESASTRE</ep3.5>
			<id3.5>b7969de73c8a1dde8fa766a83fea1a4b39a2b13d</id3.5>
			<ep3.6>6 710N</ep3.6>
			<id3.6>5aafee45840f53c1edc4651e4186b86d6b69826d</id3.6>
			<ep3.7>7 DULCES TRASEROS</ep3.7>
			<id3.7>906aaaae3a413c9865c54992cf3473ebf0591e2f</id3.7>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BIENVENIDOS A EDEN</title>
		<info>(2022) 8 episodios. ¿Eres feliz? Con esta pregunta Zoa y otros cuatro atractivos jóvenes, muy activos en las redes sociales, son invitados a la fiesta más exclusiva de la historia en una isla secreta, organizada por la marca de una nueva bebida. Lo que comienza siendo un viaje excitante pronto se transformará por completo en el viaje de sus vidas. Pero el paraíso no es realmente lo que parece… Bienvenidos a Edén.</info>
		<year>2022</year>
		<genre>Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/iZSmq8iOupIIirR7rsvLqp5Ghi2.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fOSsIrZdvVUtD0kVDmv26Rax4Fo.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/iZSmq8iOupIIirR7rsvLqp5Ghi2.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fOSsIrZdvVUtD0kVDmv26Rax4Fo.jpg</fanart.1>
			<ep1.1>1 EL VIAJE DE TU VIDA</ep1.1>
			<id1.1>ebbc822211c9e1c646ce6e535682283c42e04de8</id1.1>
			<ep1.2>2 EVALUACION</ep1.2>
			<id1.2>e69ba05c921ec5e1fc16f1f4ddd6b668103d28a7</id1.2>
			<ep1.3>3 FIESTA DE DESPEDIDA</ep1.3>
			<id1.3>9e12f61d2b7ea55509a9088412fb3daacee92629</id1.3>
			<ep1.4>4 LA OTRA ORILLA</ep1.4>
			<id1.4>1b400bcd3b2f40d2427496ca3ee8973f9a6555e6</id1.4>
			<ep1.5>5 TORMENTA</ep1.5>
			<id1.5>f03723dde38ab9551a07fe8fe0a67cd8bb16e976</id1.5>
			<ep1.6>6 REBELION</ep1.6>
			<id1.6>19f973b59056bf01d4c23299e4c551c14749b993</id1.6>
			<ep1.7>7 LILITH</ep1.7>
			<id1.7>bfcc8809d61d31a1e9a393a06423d1b13b8f31a6</id1.7>
			<ep1.8>8 EL VIAJE DE VUELTA</ep1.8>
			<id1.8>9d24aff472bd3961476521bd0b01f69f55987d77</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BIENVENIDOS A LA TIERRA</title>
		<info>(2021) 6 episodios. Desde volcanes en activo hasta las profundidades del océano, Will Smith viaja a los confines de la Tierra, emprendiendo una aventura alrededor del mundo para explorar las grandes maravillas de nuestro planeta.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/lW8GyURQXYCLn1ydX39wm99SYh1.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5V2TGcU92spIJo6jCPQ8H3G3yCT.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/lW8GyURQXYCLn1ydX39wm99SYh1.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5V2TGcU92spIJo6jCPQ8H3G3yCT.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>04b6fb6925490e3956f4249aed1ff0de1e1c676e</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>e703e713beb29b6a5532f8aebb47e2239edde404</id1.2>
			<ep1.3>5 AL 6</ep1.3>
			<id1.3>46e481d5e7817ee11b497a181c9b8f733c8d8f26</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BIG LITTLE LIES</title>
		<info>(2017-2019) 2 temporadas. 14 episodios. Una oscura y misteriosa historia sobre tres madres (Madeline, Celeste y Jane) del norte de California cuyas vidas, aparentemente perfectas, se ven sorprendidas por un asesinato durante un evento para recaudar fondos del colegio de primaria. Celeste (Nicole Kidman) es una mujer con una vida familiar perfecta y un esposo ejemplar. Sin embargo, luchará por conseguir algo que le quita el sueño todas las noches. Madeline (Reese Witherspoon) es una madre atrevida, divertida, pero tendrá que soportar que su exmarido y su actual mujer vivan en la misma ciudad que ella. Por su parte, Jane (Shailene Woodley), una madre soltera y su llegada a la nueva ciudad no será todo lo placentera que pudiera imaginar.</info>
		<year>2017</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/xw8iplMAt3xprDINGB4Y9ZTVm57.jpg</thumb>
		<fanart>https://imgur.com/bqlZjTB.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/xw8iplMAt3xprDINGB4Y9ZTVm57.jpg</thumb.1>
			<fanart.1>https://imgur.com/bqlZjTB.jpg</fanart.1>
			<ep1.1>1 ALGUIEN HA MUERTO</ep1.1>
			<id1.1>6811f354902e76720b8a1348263dfc6a3aa2d5b9</id1.1>
			<ep1.2>2 MATERNIDAD FEROZ</ep1.2>
			<id1.2>4e9d7b4f63b087e80b9cb67d0940e4b4f9cbe391</id1.2>
			<ep1.3>3 VIVIENDO EL SUEÑO</ep1.3>
			<id1.3>6f71e927a51c17c564ee75b7f1dbbc517f2b298c</id1.3>
			<ep1.4>4 LA HORA DE LA VERDAD</ep1.4>
			<id1.4>899604c053e462ed8bd8ef8070bb0fc6a5eb9929</id1.4>
			<ep1.5>5 GATO ESCALDADO…</ep1.5>
			<id1.5>3cfe0a42273364c56fd83b2b712546ea36dc402c</id1.5>
			<ep1.6>6 AMOR APASIONADO</ep1.6>
			<id1.6>JSHD3NXIMEEDUC7SLXYTB7FLC3CEMVMK</id1.6>
			<ep1.7>7 RECIBES LO QUE NECESITAS</ep1.7>
			<id1.7>N45TVR6FYXRQY3CCQIRXKRCZNWIG3AFU</id1.7>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/zxGkno93ExrTMsJVllH6mzQ652z.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/8Wo2Pqjxn7Xmd7p2dxRMIJtyMV7.jpg</fanart.2>
			<ep2.1>1 QUE HAN HECHO</ep2.1>
			<id2.1>599b65c073a9a30246b060acaa799053d12c73f9</id2.1>
			<ep2.2>2 EL CORAZON DELATOR</ep2.2>
			<id2.2>8add81938574fed937445d16f1bdd8e2eb3a0981</id2.2>
			<ep2.3>3 EL FIN DEL MUNDO</ep2.3>
			<id2.3>8ed871152d043e49290d811b09f229c0b7c98e84</id2.3>
			<ep2.4>4 ELLA LO SABE</ep2.4>
			<id2.4>178a6bd896bc1e2d3421ac434097846d9d0dd9d0</id2.4>
			<ep2.5>5 MATAME</ep2.5>
			<id2.5>d06e322cfa1c3843df669a3ee5ac100aef4ad2f1</id2.5>
			<ep2.6>6 LA MALA MADRE</ep2.6>
			<id2.6>b8910b2c62e2de31cc5db6be2b04bf2663d3351f</id2.6>
			<ep2.7>7 QUIERO SABER</ep2.7>
			<id2.7>6cb522c6c7943c793b39771273aab6ae71e439bf</id2.7>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BILARDO EL DOCTOR DEL FUTBOL</title>
		<info>(2022) 4 episodios. Una docuserie reveladora e íntima que narra la vida y la carrera del entrenador de la selección argentina de fútbol, el Dr. Carlos Bilardo, a través de las opiniones de familiares, amigos, exjugadores y rivales.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bgeyUyXGVHHlCfQR1FIPl8CEHke.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/eUriUhwfIpw4x825rK1hBRtyInW.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bgeyUyXGVHHlCfQR1FIPl8CEHke.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/eUriUhwfIpw4x825rK1hBRtyInW.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>F7BAF16E0F3A5079BBE2E209E8B5F4551D3C8570</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BLACK MIRROR</title>
		<info>(2011) 5 temporadas. 21 episodios.  Serie antológica creada por Charlie Brooker ("Dead Set") que muestra el lado oscuro de la tecnología y cómo esta afecta y puede alterar nuestra vida, a veces con consecuencias tan impredecibles como aterradoras. "Black Mirror" comenzó su emisión en 2011 en el canal británico Channel 4, con dos temporadas de tres episodios cada una, y tras producirse un especial de Navidad la serie fue comprada y renovada por Netflix, que ya ha producido tres temporadas más.</info>
		<year>2011</year>
		<genre>Ciencia ficción. Thriller. Terror. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/dw7jYk7EdrkrHozG7F1Yg2eFJTm.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/p39K75uoZLwnhGlEAJxRG5xAD9y.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/sf7pNy35Bro1yq0GegNQlXiS0Ar.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/b92npz1sZwvmjUfVIrWXehrk3pc.jpg</fanart.1>
			<ep1.1>1 EL HIMNO NACIONAL</ep1.1>
			<id1.1>65F38AA5DC6BBDAF38C9D1F56211A1CCDC661EA2</id1.1>
			<ep1.2>2 15 MILLONES DE MERITOS</ep1.2>
			<id1.2>F2C270704FECB9C85D3F40CE75D0AF5DEBE22D0F</id1.2>
			<ep1.3>3 TODA TU HISTORIA</ep1.3>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/ptp2LTrOXfxMtRgLEOQdObaevDm.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/zSaEw7H0KpAFXQp8LzoBfoTUhwh.jpg</fanart.2>
			<ep2.1>1 AHORA MISMO VUELVO</ep2.1>
			<id2.1>a11001c3702ab4216352f889773e44435583abb5</id2.1>
			<ep2.2>2 OSO BLANCO</ep2.2>
			<id2.2>e4abebcc9a7e9e5d40e2635cadbf482a125b3da4</id2.2>
			<ep2.3>3 EL MOMENTO WALDO</ep2.3>
			<id2.3>9c14cd52f99f1dc9f3a8ce35ab61deec1e13711c</id2.3>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/gdM2XgkCwdQajMH5L5kLa5faIQg.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/4NLsiykQl6K0E49CIFo2dAb4KKm.jpg</fanart.3>
			<ep3.1>1 CAIDA EN PICADO</ep3.1>
			<id3.1>17093b8fca82f1270b4c7fdf8795b45c324bf464</id3.1>
			<ep3.2>2 PLAYTESTING</ep3.2>
			<id3.2>b7a59227312aa7af345e09891f7c4365fee5b095</id3.2>
			<ep3.3>3 CALLATE Y BAILA</ep3.3>
			<id3.3>d6896498f50325f4e834c70ff5ef304b91885d76</id3.3>
			<ep3.4>4 SAN JUNIPERO</ep3.4>
			<id3.4>5b7ac3472fbbb470b5cc67facdc6ba76d7ec54c3</id3.4>
			<ep3.5>5 LA CIENCIA DE MATAR</ep3.5>
			<id3.5>af5856227e60e4bdb37065dd9bcc4f7ffecd894c</id3.5>
			<ep3.6>6 ODIO NACIONAL</ep3.6>
			<id3.6>f25531547f1df5dfd63b9e049e88ba948515e041</id3.6>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/nqgx6I9qPiCpdy7u3HFR8DH2cWq.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/p39K75uoZLwnhGlEAJxRG5xAD9y.jpg</fanart.4>
			<ep4.1>1 AL 6</ep4.1>
			<id4.1>246fadb9c6c4395f64d8b9cf384b33571a8bc968</id4.1>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/vX61PMv9fpQz3eLcASnATo4yJnf.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/2ScGrTZT5tlZgL4RBzUEzK4C0GZ.jpg</fanart.5>
			<ep5.1>1 STRIKING VIPERS</ep5.1>
			<id5.1>b16ce962439e2ab5caba30e34ede775eb554edbd</id5.1>
			<ep5.2>2 AÑICOS</ep5.2>
			<id5.2>1a42036c9a65dff83f77eb910c439d8ce5de5e94</id5.2>
			<ep5.3>3 RACHEL, JACK Y ASHLEY TOO</ep5.3>
			<id5.3>400fe79f49adf8602a385b5c83d3d0edb9bae648</id5.3>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BLACK NARCISSUS</title>
		<info>(2020) 3 episodios. Un grupo de monjas se enfrenta a distintos retos en el hostil entorno de un palacio en el Himalaya que desean convertir en un convento. Adaptación de la novela de 1939 escrita por Rumer Godden.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/3eYusIhAodlw2Lgoh1k8npejW8Z.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/w9hMLn4wq5NCjt3iDAqH51wjIMZ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/3eYusIhAodlw2Lgoh1k8npejW8Z.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/w9hMLn4wq5NCjt3iDAqH51wjIMZ.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>4630d4c0e500b89d094598c91954f6eedf4cd504</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BLACK SUMMER</title>
		<info>(2019) 2 temporadas. 16 episodios. En los primeros y confusos días de un apocalipsis zombi, completos desconocidos deben aliarse para hacerse fuertes y regresar con sus seres queridos. Precuela de la serie "Z Nation" producida por The Asylum para el canal Syfy.</info>
		<year>2019</year>
		<genre>Terror. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/d27fZpY8jSZDOs1k0JCL0RY0b94.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/d5YaPM6JKlr5aeDJKi32d1t7jNX.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/d27fZpY8jSZDOs1k0JCL0RY0b94.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/d5YaPM6JKlr5aeDJKi32d1t7jNX.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>fe28b218b186c974beb0cdfa092eca7370c94d61</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/yYAVJDedf6oB8lLOcFqQZhh1L0n.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/wF2N2OtgGMR4zbHTxFTAv44yMQh.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>c9cdbc97df72ca2ded42012d4f62a4e68bb9aa01</id2.1>
			<ep2.2>5 AL 8</ep2.2>
			<id2.2>8521a314c4a7c28758f10b604d51a527e7e245e5</id2.2>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BLADE RUNNER: EL LOTO NEGRO</title>
		<info>(2021) 13 episodios. Los Ángeles, 2032. Una joven con habilidades mortales y sin recuerdos persigue a los responsables de su brutal pasado para descubrir la verdad sobre su identidad. Sus únicas pistas: un dispositivo de datos bloqueado y un tatuaje de un loto negro. Serie de animación producida por Shin'ichirō Watanabe (Cowboy Bebop) y ambientada diecisiete años antes de la película "Blade Runner 2049".</info>
		<year>2021</year>
		<genre> Animación. Ciencia ficción. Acción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/zHQJkDZ4OjqJnp4vtphxOQ7GIh6.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/phwgPLUIcv3sbI1xmkiurThR13Q.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/zHQJkDZ4OjqJnp4vtphxOQ7GIh6.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/phwgPLUIcv3sbI1xmkiurThR13Q.jpg</fanart.1>
			<ep1.1>1 LA CIUDAD DE LOS ANGELES</ep1.1>
			<id1.1>6002d06ec16c3863906ceb20c0ef3c7ebe38dd3b</id1.1>
			<ep1.2>2 LO QUE NO SOMOS</ep1.2>
			<id1.2>71fb21798720594ab1911c9a92aa60ae4fa6d00a</id1.2>
			<ep1.3>3 LA CONDICION HUMANA</ep1.3>
			<id1.3>7bb4bec91c5cf994c55a5777fe59b094d59ae310</id1.3>
			<ep1.4>4 LA CACERIA DE LOS MUÑECOS</ep1.4>
			<id1.4>060b0edcc5ca153731e902f8bbae665ac204d52c</id1.4>
			<ep1.5>5 PRESION</ep1.5>
			<id1.5>bc3902dc06bcdb8f7e0d70a2af82be0a4e988c7c</id1.5>
			<ep1.6>6 LA PERSISTENCIA DE LA MEMORIA</ep1.6>
			<id1.6>5d144b5cded082d2e5f5a73685192e7aa3b7ddb5</id1.6>
			<ep1.7>7 REALIDAD</ep1.7>
			<id1.7>d85ca9b22a7d1fe375857423201862cadc71e526</id1.7>
			<ep1.8>8 EL INFORME DAVIS</ep1.8>
			<id1.8>735df96bc95acdbec7f0862a96bf2c60c26271bf</id1.8>
			<ep1.9>9 VOLUNTAD PROPIA</ep1.9>
			<id1.9>af192da943a412e32620fa9b8211010fcc7db4cf</id1.9>
			<ep1.10>10 LUZ DE LUNA</ep1.10>
			<id1.10>c1d9b3e1df0f31e2ae05446307916419abafd212</id1.10>
			<ep1.11>11 LOS MEJORES RECUERDOS</ep1.11>
			<id1.11>ae57c2f03ae151680e556b6415bf0c227a144fea</id1.11>
			<ep1.12>12 ALMAS ARTIFICIALES</ep1.12>
			<id1.12>3cc281b34e7ed71e1cd5de95ade501bac8d002f5</id1.12>
			<ep1.13>13 TIEMPO DE MORIR</ep1.13>
			<id1.13>f15a6744fc4422be806f9bc93f7339f61bd702dd</id1.13>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BORGEN: REINO PODER Y GLORIA</title>
		<info>(2022) 8 episodios. La carrera de Birgitte Nyborg, ministra de Asuntos Exteriores, peligra cuando la pugna por el petróleo de Groenlandia amenaza con convertirse en una crisis internacional.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/cOG9Abt9S8tSXQAjiUfnggQ5Nwh.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/2ba7EoIBnNhRx9EhqjiYHoK9RxR.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/cOG9Abt9S8tSXQAjiUfnggQ5Nwh.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/2ba7EoIBnNhRx9EhqjiYHoK9RxR.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>ce1d5ce9c9ac6e12e5ce77c0a8196b9d0c84d313</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>6d2cc969f1846bb95c14cab4ee14997285bdd7b5</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>6c0dc37579f5fb671d06d7756db7ec44d6856f60</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>3b96294b375dfdcb5512856939e3a2cb54be7254</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>e2db40e835249cedfcd9ee3d92956b3a227fe00f</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>b12a09a81888e19d426956c93ae0beb2ccb2a427</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>120a8962a720dcef0cae243ba2355b8291d7f17a</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>6da30f49df99fe6649653b981507665938131dd3</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BOSCH: LEGACY</title>
		<info>(2022) 10 episodios. Bosch se embarca en el siguiente capítulo de su carrera y se encuentra trabajando con su antigua enemiga, Honey Chandler.</info>
		<year>2022</year>
		<genre>Drama. Intriga </genre>
		<thumb>https://www.themoviedb.org/t/p/original/9Z7L7wSB6nLwWk7coUgm1ytQbOB.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/wVGiwvE5CtlVPrP0oEO3W70ft6D.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/9Z7L7wSB6nLwWk7coUgm1ytQbOB.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/wVGiwvE5CtlVPrP0oEO3W70ft6D.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>b95426e2d8eae016f0aa0df01665d84acc1c1bf2</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>c8403be5939e214ba017ba841583b5499750b4ab</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>d1ee63ff8e77d04b1409eff3e4428f7adf843937</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>0f76eb5e42ee321fbcd3aaacd294323e2befc69e</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>6000f87fd7ee1e271b95e7002a95ec887201792f</id1.5>
			<ep1.6> 6 Episodio</ep1.6>
			<id1.6>e4e7938996a0df790f9f262962e799000987d4eb</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>17ab99eb0d6a0ccb66003adb322dbe29ff031854</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>2f6d19cd7062748dfd4061c4e60a8467c522f671</id1.8>
			<ep1.9>9 Episodio</ep1.9>
			<id1.9>31becb3d164c795d2733be54cce4378c666788f2</id1.9>
			<ep1.10>10 Episodio</ep1.10>
			<id1.10>cbdab4215d0f3f05b8e4940d5ad2592725117087</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BOSQUE ADENTRO</title>
		<info>(2020) 6 episodios. La evidencia encontrada en el cadáver de una víctima de homicidio le da esperanzas a un fiscal de que su hermana, desaparecida desde hace 25 años, podría estar viva.</info>
		<year>2020</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/cA0o5dT1xCAQNKl8ShYNf84AySO.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8eyC1kj5vYcNMyhvpJt16ZE6bLX.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/cA0o5dT1xCAQNKl8ShYNf84AySO.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8eyC1kj5vYcNMyhvpJt16ZE6bLX.jpg</fanart.1>
			<ep1.1>1 AL 6 (1080)</ep1.1>
			<id1.1>8C334C6A256AB377E8454409D43A0A848E854F1E</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BREAKING BAD</title>
		<info>(2008-2013) 5 temporadas. 62 episodios. Tras cumplir 50 años, Walter White (Bryan Cranston), un profesor de química de un instituto de Albuquerque, Nuevo México, se entera de que tiene un cáncer de pulmón incurable. Casado con Skyler (Anna Gunn) y con un hijo discapacitado (RJ Mitte), la brutal noticia lo impulsa a dar un drástico cambio a su vida: decide, con la ayuda de un antiguo alumno (Aaron Paul), fabricar anfetaminas y ponerlas a la venta. Lo que pretende es liberar a su familia de problemas económicos cuando se produzca el fatal desenlace.</info>
		<year>2008</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/ggFHVNu6YYI5L9pCfOacjizRGt.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/v8KMtA8MqTGdJVPa3CCFsLhJtvL.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/iEiPvmgItLzTROvzKlFuuGYI1rc.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/sp1RSDvoVsbvDouQx1A75ebU35e.jpg</fanart.1>
			<ep1.1>1 AL 7 (1080)</ep1.1>
			<id1.1>46bbb2af2380642f603daf10c210175b000fe0cb</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/7LUswjn6jgLxdJ4e6sgwkULXsZI.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/tsRy63Mu5cu8etL1X7ZLyf7UP1M.jpg</fanart.2>
			<ep2.1>1 AL 13 (1080)</ep2.1>
			<id2.1>063942c2a0d61da17ccafd6ae13d1a8d711d6c58</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/pbEAxyuVzApIShO9VLQ2loPKYcM.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/84XPpjGvxNyExjSuLQe0SzioErt.jpg</fanart.3>
			<ep3.1>1 AL 13 (1080)</ep3.1>
			<id3.1>3519ac6b8da6a56ce44545dff181898152ef60c1</id3.1>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/dOJ5xloGImOvWBfDkWZVYgyLQe6.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/wKHImjeHwVIiWJWFIJtWUKkA5QJ.jpg</fanart.4>
			<ep4.1>1 AL 13 (1080)</ep4.1>
			<id4.1>4cafc93d8ca6b68ccfe70bf02d44caadc6caa481</id4.1>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/ztkUQFLlC19CCMYHW9o1zWhJRNq.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/yXSzo0VU1Q1QaB7Xg5Hqe4tXXA3.jpg</fanart.5>
			<ep5.1>1 AL 16</ep5.1>
			<id5.1>b548484f86ff2ff57725e03adb3e45a1b5828f3d</id5.1>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>BRUJA ESCARLATA Y VISION</title>
		<info>(2021) 9 episodios. Combinando el estilo clásico de las sitcoms con el MCU (Universo Cinematográfico de Marvel), cuenta la historia Wanda Maximoff y Vision, dos seres con superpoderes que viven una vida idílica en las afueras de una ciudad hasta que un día comienzan a sospechar que no todo es lo que parece.</info>
		<year>2021</year>
		<genre>Comedia. Fantástico. Ciencia ficción. Drama. Acción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/8J6GDmq6kZmP1nonTjx6aIBGHlb.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7ogDBoLZE2Y1AK5yEJhBPQBic5t.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8J6GDmq6kZmP1nonTjx6aIBGHlb.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7ogDBoLZE2Y1AK5yEJhBPQBic5t.jpg</fanart.1>
			<ep1.1>1 AL 9 (4K)</ep1.1>
			<id1.1>02f552b0264b606d77ba5f21b2e1e51420f2a561</id1.1>
			<ep1.2>1 AL 2</ep1.2>
			<id1.2>39adf42fecc65fabba31e8f295dc9b9b2d488181</id1.2>
			<ep1.3>3 AHORA EN COLOR</ep1.3>
			<id1.3>04e86d5a1048367908ae75178b234605e4bc21c2</id1.3>
			<ep1.4>4 INTERRUMPIMOS ESTE PROGRAMA</ep1.4>
			<id1.4>bf4b296f287f029f26feb9126967b1395e985e60</id1.4>
			<ep1.5>5 EN UN EPISODIO MUY ESPECIAL…</ep1.5>
			<id1.5>7aa7e35b20c118b9b6a98bd6062f93a78299654d</id1.5>
			<ep1.6>6 ¡TERRORIFICO ESTRENO DE HALLOWEEN![</ep1.6>
			<id1.6>56415efcbf1e958e175d9885061411411f32b40c</id1.6>
			<ep1.7>7 CAE LA CUARTA PARED</ep1.7>
			<id1.7>5f1266653095342624c5275f1b8256d8d35a24f6</id1.7>
			<ep1.8>8 EN EPISODIOS ANTERIORES</ep1.8>
			<id1.8>289501bcf913419cb5e8aaaa39a4d9cced83feff</id1.8>
			<ep1.9>9 EL FINAL DE LA SERIE</ep1.9>
			<id1.9>06f4c803d713099edd9e72519f9e85caddced4a3</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CABALLERO LUNA</title>
		<info>(2022)  6 episodios. Un trabajador de un museo que lucha contra un trastorno de identidad disociativo, recibe los poderes de un dios egipcio de la luna. Pronto descubre que estos poderes pueden ser tanto una bendición como una maldición.</info>
		<year>2022</year>
		<genre>Fantástico. Acción. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/infBhHmGjuGfTZmP2FBLdVfzKuM.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/quRTAOdfNS0hsIezhMi4c7q7ZO1.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/infBhHmGjuGfTZmP2FBLdVfzKuM.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/quRTAOdfNS0hsIezhMi4c7q7ZO1.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>B7062A8B9DC0019C62295F4A64D1553C49918499</id1.1>
			<ep1.2>3 UN TIPO AFABLE</ep1.2>
			<id1.2>8BEFCD8C69CB9559207EE1A0DF102DFB57422835</id1.2>
			<ep1.3>4 LA TUMBA</ep1.3>
			<id1.3>D6E93F93A2F9D4194EAE83F694DBB20FCED8BDC9</id1.3>
			<ep1.4>5 ASILO</ep1.4>
			<id1.4>1AACA9A7454A956ECD0B6E9C661251E722350CBD</id1.4>
			<ep1.5>6 DIOSES Y MONSTRUOS</ep1.5>
			<id1.5>00DCE6E355417E3B9B1D7561F7F113088ECAD78D</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CAMINANTES</title>
		<info>(2020) 8 episodios. El grupo de jóvenes peregrinos se enfrentarán a situaciones límite mientras recorren el Camino de Santiago a su paso por la Selva de Irati (Navarra), donde está ambientada esta terrorífica experiencia.</info>
		<year>2020</year>
		<genre>Terror. Aventuras</genre>
		<thumb>https://image.tmdb.org/t/p/original/fWIz7iroZ01pZ7IhBxJcb7zsq0Z.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/utJVTcLUGpLlSpGHHFNdMnNS9Lt.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/fWIz7iroZ01pZ7IhBxJcb7zsq0Z.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/utJVTcLUGpLlSpGHHFNdMnNS9Lt.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>1caf812346c6f023e4eeb3081c05d27de5f2aa54</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CARDO</title>
		<info>(2021) 6 episodios. Miniserie centrada en la generación nacida en los año noventa y que gira alrededor de una chica, María (Ana Rujas) que decide abandonar el mundo de la televisión, en el que ha trabajado como presentadora y protagonizando anuncios, para hacer frente a sus problemas, que son unos cuantos. María por fuera rebosa belleza, pero por dentro se siente un cardo.</info>
		<year>2021</year>
		<genre>Comedia. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fBX3kZCAS5O3gyKCYCVaevAChPi.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/9sOepzlYvewcqZ5BVPLUUz7XID0.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fBX3kZCAS5O3gyKCYCVaevAChPi.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/9sOepzlYvewcqZ5BVPLUUz7XID0.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>120074132f0970b07431dc527de335c734fff538</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>0bc09a60c2cd9cdda8aee79694d3923c32f6fae7</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>59b45e92098328736bc53fd63aa210f5aa0bd67a</id1.3>
			<ep1.4>Episodio 4</ep1.4>
			<id1.4>9524ea4338617166ae78817ca588e5c4e8c4385a</id1.4>
			<ep1.5>Episodio 5</ep1.5>
			<id1.5>0ab60705fbadd041b5fcd07b579971cb2f5f532b</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CASTLE ROCK</title>
		<info>(2018-2019) 2 temporadas. 20 episodios. Serie de terror psicológico ambientada en el universo de Stephen King. "Castle Rock" combina la mitología y la narración de algunos de los personajes más célebres de las novelas de Stephen King tejiendo una saga épica de oscuridad y luz, que transcurre en un área de unos cuantos kilómetros cuadrados en Maine. Castle Rock es el pequeño pueblo donde transcurren varias novelas de King como "Cujo", "La Mitad Oscura", "La Tienda" e incluso hay referencias a ella en "The Body" o "Cadena Perpetua". Cada una de las temporadas tendrá personajes e historias diferentes, pero estarán relacionadas entre ellas de algún modo. La primera temporada consta de diez episodios.</info>
		<year>2018</year>
		<genre>Fantástico. Intriga. Terror</genre>
		<thumb>https://www.themoviedb.org/t/p/original/w0M5XGVgPX23UJETkp9pR5R88Qm.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/7T2BU8EM63gewJUNXfGlOiPetH1.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8BFoVWvYUuxX5P8AWXKZkNvxO16.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/qyRJPtJkJz7wtS4UJnJez9S3WOM.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>4265950705a1d3fe5b17465c29cea59a9c2f1f00</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/6dnUpv9ghx84pgxlMOb4uuJrWDs.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/yBGMmqtAChyGCwN0zBuxaOjSzsU.jpg</fanart.2>
			<ep2.1>1 QUE EL RIO SIGA SU CURSO</ep2.1>
			<id2.1>984dd2b51591820c3b5db26c95096a2e32d9ad80</id2.1>
			<ep2.2>2 NUEVA JERUSALEN</ep2.2>
			<id2.2>404d6912f3a64622b34776847b0be2d407464415</id2.2>
			<ep2.3>3 LOS LAZOS QUE NOS UNEN</ep2.3>
			<id2.3>d9ef31f8d72ede6f66aaa7889e619f187d0a4804</id2.3>
			<ep2.4>4 RECUPERADA LA ESPERANZA</ep2.4>
			<id2.4>1b1acdc3d8da5066063111f5f08f14d78849a822</id2.4>
			<ep2.5>5 EL REINO DE LAS RISAS</ep2.5>
			<id2.5>be73e56c1a1462c4c1576424a81220b223d773b4</id2.5>
			<ep2.6>6 LA MADRE</ep2.6>
			<id2.6>56bd71c2b4e07f1215cd8d81f51f84ff459183a3</id2.6>
			<ep2.7>7 LA PALABRA</ep2.7>
			<id2.7>a10e566f99d2eafee9582c6ec82201cef35bde41</id2.7>
			<ep2.8>8 SUCIOS</ep2.8>
			<id2.8>e4f613b0495b436665a2647e1ed0a581e35f7c75</id2.8>
			<ep2.9>9 CAVEAT EMPTOR</ep2.9>
			<id2.9>b25063038b1f9d57a1cb3dd2015274b28b11cef2</id2.9>
			<ep2.10>10 LIMPIOS</ep2.10>
			<id2.10>a8c33a5508c4ea67978f1cb701bb4da7a2c1210d</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CHAPELWAITE</title>
		<info>(2021) 10 episodios. El capitán Charles Boone hace que su familia de tres hijos se traslade a su antigua casa en un pueblo pequeño y aparentemente tranquilo de Preacher's Corners, Maine, tras la muerte de su mujer en el mar.</info>
		<year>2021</year>
		<genre>Drama. Terror. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/v7Oix3Zkhg3ftRxmLmyDBJc9ycp.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/25yTJaPYBTPitytxvwQbSSJwSOf.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/v7Oix3Zkhg3ftRxmLmyDBJc9ycp.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/25yTJaPYBTPitytxvwQbSSJwSOf.jpg</fanart.1>
			<ep1.1>1 LA SANGRE LLAMA A LA SANGRE</ep1.1>
			<id1.1>4346437f2993578eaeec0c0a8fb9002fc1bbdc58</id1.1>
			<ep1.2>2 MEMENTO MORI</ep1.2>
			<id1.2>4d6f9b3ead319f7c1f4a7b60cf40d070b83bf806</id1.2>
			<ep1.3>3 EL LEGADO DE LA LOCURA</ep1.3>
			<id1.3>91dd2b8fcdaa2367de7c80e248ac8233af234f03</id1.3>
			<ep1.4>4 LO PROMETIDO</ep1.4>
			<id1.4>4f0ef70875e82df7caa48b6f7601d5f51226c61a</id1.4>
			<ep1.5>5 AL 6</ep1.5>
			<id1.5>6ca1bf562a5547ad0f7ea52dada98bd12f874b0d</id1.5>
			<ep1.6>7 DE VERMIS MYSTERIIS</ep1.6>
			<id1.6>06b15d0ef900a9bd1e56206bd5b1b39137e13842</id1.6>
			<ep1.7>8 AL 10</ep1.7>
			<id1.7>9ddef650cb2028f78518c689893d944d5b2ecdb2</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CHUCKY</title>
		<info>(2021) 8 episodios. Despues de que un muñeco Chucky clasico aparece en una venta de garaje, una idilica ciudad estadounidense se ve sumida en el caos cuando una serie de horribles asesinatos comienzan a exponer las hipocresias y secretos de la ciudad.</info>
		<year>2021</year>
		<genre>Terror</genre>
		<thumb>https://www.themoviedb.org/t/p/original/iF8ai2QLNiHV4anwY1TuSGZXqfN.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/jl3YHsEmLQjbTQb4wJWVPSSebFR.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/iF8ai2QLNiHV4anwY1TuSGZXqfN.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/jl3YHsEmLQjbTQb4wJWVPSSebFR.jpg</fanart.1>
			<ep1.1>1 MUERTE POR ACCIDENTE</ep1.1>
			<id1.1>13a0f5cc7f1991af9e55f71ecc3781df7530b165</id1.1>
			<ep1.2>2 DAME ALGO PARA COMER</ep1.2>
			<id1.2>6e45b78ee5a1a93fb4e791f41094bd3edbbf9663</id1.2>
			<ep1.3>3 ME GUSTA QUE ME ABRACEN</ep1.3>
			<id1.3>04a0d1eb804f1584275c9b570addaed3144dfa2f</id1.3>
			<ep1.4>4 DEJALO IR</ep1.4>
			<id1.4>d6ef40bf16a28d016b299fa4ef920b026c36f5ee</id1.4>
			<ep1.5>5 PEQUEÑAS MENTIRIJILLAS</ep1.5>
			<id1.5>f90b6051798bd4ecce7c493257928b32c3abb910</id1.5>
			<ep1.6>6 CABO QUEER</ep1.6>
			<id1.6>3d08726225feed8abcabc2d72612c5cb80cc896a</id1.6>
			<ep1.7>7 DOBLE DOLOR DOBLE PERDIDA</ep1.7>
			<id1.7>50fa275f771298ec7dc2f6bccc50a091ee42b437</id1.7>
			<ep1.8>8 UN ASUNTO PARA INMORTALIZAR</ep1.8>
			<id1.8>50fa275f771298ec7dc2f6bccc50a091ee42b437</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CIUDAD INVISIBLE</title>
		<info>(2021) 7 episodios. Un detective que investiga un caso de asesinato queda atrapado en una batalla entre el mundo visible y un reino subterráneo habitado por criaturas fantásticas.</info>
		<year>2021</year>
		<genre>Drama. Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/hh4uZv4lVkTBsBqtHCA5B4EMHBR.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/lXzu1IZmzDulGeqqrknoE7c5hak.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/hh4uZv4lVkTBsBqtHCA5B4EMHBR.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/lXzu1IZmzDulGeqqrknoE7c5hak.jpg</fanart.1>
			<ep1.1>1 AL 7</ep1.1>
			<id1.1>38969de2c567cf989b704fd315cd65bceeeccf65</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CLARICE</title>
		<info>(2021) 13 episodios. 'Clarice' se desarrolla en 1993, un año después de los eventos de El silencio de los corderos. La serie profundiza en la historia personal no contada de Clarice Starling, cuando regresa al trabajo para perseguir asesinos en serie y depredadores sexuales mientras se mueve por el mundo político de Washington, D.C.</info>
		<year>2021</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/7OFxU0bBO0HDL4klXmM1ahJPbv8.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nGDk9Ri42IoVYjzRAijAyMDT8iy.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7OFxU0bBO0HDL4klXmM1ahJPbv8.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nGDk9Ri42IoVYjzRAijAyMDT8iy.jpg</fanart.1>
			<ep1.1>1 EL FIN DEL SILENCIO</ep1.1>
			<id1.1>9c9474b6aab76c611efd4c02063fbfe6f0cab994</id1.1>
			<ep1.2>2 FANTASMAS DE LA AUTOPISTA 20</ep1.2>
			<id1.2>aa0e2618eacf120256a047813dbabd7c310028a4</id1.2>
			<ep1.3>3 ¿ESTAS BIEN?</ep1.3>
			<id1.3>69098ea424397707acfac85e6a10801cb25bfcec</id1.3>
			<ep1.4>4 NO PUEDES GOBERNARME</ep1.4>
			<id1.4>5c0cc3893a5642ecd162549d9ac6f00ab11b7ea5</id1.4>
			<ep1.5>5 RECONCILIARSE CON DIOS</ep1.5>
			<id1.5>423da5e8c5e5ea7db34d9af9db2d1762f0029c97</id1.5>
			<ep1.6>6 ¿QUE SE SIENTE SIENDO TAN GUAPA?</ep1.6>
			<id1.6>bd08d40fcda1bad13dd660c3af4ee8a5ac1c19b6</id1.6>
			<ep1.7>7 LA ESPANTOSA VERDAD</ep1.7>
			<id1.7>0671f67f546af8ac61d14853b3a6c3499f01f533</id1.7>
			<ep1.8>8 UNA CUENTA MAS</ep1.8>
			<id1.8>cdfeb8a92806d010571be2c05fad0597c91f3d34</id1.8>
			<ep1.9>9 E SILENCIO ES EL PURGATORIO</ep1.9>
			<id1.9>ccf74510a7592c7af559560ab452ccb8b7b608ba</id1.9>
			<ep1.10>10 HUERFANO DE MADRE</ep1.10>
			<id1.10>05c48f58939072c17d055a0d2d8ae324d6f22a56</id1.10>
			<ep1.11>11 EL TALON DE AQUILES</ep1.11>
			<id1.11>a9a6dac21b8c65d2fcba6859950c28e2af5028c9</id1.11>
			<ep1.12>12 PADRE TIEMPO</ep1.12>
			<id1.12>3e1de05d4deeb3bc0fb8d91bb99e1fd101e63b2e</id1.12>
			<ep1.13>13 LA FAMILIA ES LA LIBERTAD</ep1.13>
			<id1.13>7ff733773eba0acdaf82a3d69089965b8cb42d32</id1.13>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>COBRA KAI</title>
		<info>(2018) 4 temporadas.40 episodios. Secuela de "Karate Kid". 30 años después de su enfrentamiento final en 1984 en el torneo de karate All Valley, Johnny Lawrence (William Zabka) está en un momento malo de su vida. Sin embargo, tras ayudar a Miguel (Xolo Maridueña), un niño que sufre bullying, Johnny decide volver a abrir el dojo Cobra Kai. El problema es que esto vuelve a desatar la rivalidad con Daniel LaRusso (Ralph Macchio), que ahora es un hombre de negocios felizmente casado, pero al que le falta algo tras la muerte de su mentor, el Sr. Miyagi.</info>
		<year>2018</year>
		<genre>Comedia. Drama. Acción</genre>
		<thumb>https://image.tmdb.org/t/p/original/s8JYJ6Z1lIBrkh8rsIOrCtO0cT4.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/2oRDmSZsMB1Gukv52AP227zu5w5.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/5UEqWUmPrTsetHAGLYgMHvKFgMs.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/xWmDBmsabRm6IE5FcfU9CTQCPtl.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>ab500e24eed42c6f9f7908a72776894e8b1fdf6f</id1.1>
			<ep1.2>6 AL 10</ep1.2>
			<id1.2>6c0bb100835cacca4fad40eeb0f94306f90c7a9f</id1.2>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/eTMMU2rKpZRbo9ERytyhwatwAjz.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/gL8myjGc2qrmqVosyGm5CWTir9A.jpg</fanart.2>
			<ep2.1>1 AL 10</ep2.1>
			<id2.1>fbc138f2897042e1a8f7661117ec35685fc1fcf8</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/obLBdhLxheKg8Li1qO11r2SwmYO.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/gL8myjGc2qrmqVosyGm5CWTir9A.jpg</fanart.3>
			<ep3.1>1 AL 10</ep3.1>
			<id3.1>fde3a499402b61d67c238d1fc26cb2105324ff36</id3.1>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/sjFAkuXtGI9BJotG7HwOT7xqaXY.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/g63KmFgqkvXu6WKS23V56hqEidh.jpg</fanart.4>
			<ep4.1>1 AL 10</ep4.1>
			<id4.1>6C59FA11F258B9A65A2D262887C9CB89AF899F2B</id4.1>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>COLGAR LAS ALAS</title>
		<info>(2020) 6 episodios.Iker Casillas no ha sido solo un portero, es una leyenda para todos los aficionados al fútbol, para todos los amantes del deporte. El guardameta madrileño paró el tiempo cuando despejó con la bota aquel mano a mano a Robben, contuvimos la respiración cuando nos salvó en los penaltis contra Italia, nos dejamos llevar por el éxtasis cuando levantó la Copa del Mundo al cielo de Sudáfrica y nos puso el corazón en un puño cuando hizo público su infarto.</info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/dOw6JXznaOF6JI2q1NpZKkU7y2W.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/3BeYNoaJ3p5YUJgEZ38XKyfYJDQ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/dOw6JXznaOF6JI2q1NpZKkU7y2W.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/3BeYNoaJ3p5YUJgEZ38XKyfYJDQ.jpg</fanart.1>
			<ep1.1>1 EL INFARTO</ep1.1>
			<id1.1>ae36e45c1bdc63123f2d1495d5a0c0b93160c805</id1.1>
			<ep1.2>2 EMPEZAR, VOLVER</ep1.2>
			<id1.2>f72dfc8e8dcbbe94f34cdb43552588063e92933e</id1.2>
			<ep1.3>3 GLASGOW</ep1.3>
			<id1.3>b3632145996b295d9c09c7f87c877be3390ef1e0</id1.3>
			<ep1.4>4 CAMBIAR DE PIEL</ep1.4>
			<id1.4>274a7f3934ca94748d9f974c406e18f6535c9249</id1.4>
			<ep1.5>5 AL 6</ep1.5>
			<id1.5>da70ae1e184be2de592252341ea05d7dd792b205</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CONVERSACIONES ENTRE AMIGOS</title>
		<info>(2022) 12 episodios. Dos estudiantes universitarios de Dublín, Frances y Bobbi, y la extraña e inesperada conexión que forjan con una pareja casada, Melissa y Nick.</info>
		<year>2022</year>
		<genre>Drama. Romance</genre>
		<thumb>https://www.themoviedb.org/t/p/original/kk6kXltSgeAA1yWiRaZ1sOexm1V.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/zVtgdo5DWXThOUVLO48q98QWHay.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/kk6kXltSgeAA1yWiRaZ1sOexm1V.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/zVtgdo5DWXThOUVLO48q98QWHay.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>18e7429756357bbf3c4bb5384bc84a3d924f086d</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>cec7be24d20fa1459f4bb6ade9cfd1c46540b8da</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>0ad83716e0038cde653ea5e456d7b21762b83ae2</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>CREEDME ( UNBELIEVABLE )</title>
		<info>(2019).8 episodios. Narra la verdadera historia de Marie, una adolescente que fue acusada de denunciar falsamente haber sido violada, y las dos detectives que siguieron un camino sinuoso para llegar a la verdad. Basada en el artículo ganador del Premio Pulitzer "An Unbelievable Story of Rape".</info>
		<year>2019</year>
		<genre>Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/jHOrNJNM03Lsjdw7nsw7TlqBOhd.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/yG5oWkFH8xle6xkD2JB7HWfuSGx.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/jHOrNJNM03Lsjdw7nsw7TlqBOhd.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/yG5oWkFH8xle6xkD2JB7HWfuSGx.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>e994e3cfc625fc24f8580145c053e197639645ce</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DARK</title>
		<info>(2017-2020) 3 temporadas. 26 episodios. Tras la desaparición de un joven, cuatro familias desesperadas tratan de entender lo ocurrido a medida que van desvelando un retorcido misterio que abarca tres décadas... Saga familiar con un giro sobrenatural, "Dark" se sitúa en un pueblo alemán, donde dos misteriosas desapariciones dejan al descubierto las dobles vidas y las relaciones resquebrajadas entre estas cuatro familias.</info>
		<year>2017</year>
		<genre>Intriga. Drama. Ciencia ficción</genre>
		<thumb>https://image.tmdb.org/t/p/original/5LoHuHWA4H8jElFlZDvsmU2n63b.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/51z0nVsJ42kxvlb0ITXdAvmaAqu.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/5LoHuHWA4H8jElFlZDvsmU2n63b.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/51z0nVsJ42kxvlb0ITXdAvmaAqu.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>b1010e611a89a5d7a5541c5017ad164970f9b803</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/4IPFQ7gb50dEDXSML7ENtt2TiN5.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/vMNgEJ1jAZHKRa5BFMOMEf2chCD.jpg</fanart.2>
			<ep2.1>1 AL 8</ep2.1>
			<id2.1>12f50c16e03c40744c729abe72c6b1bd058f9dfe</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/apbrbWs8M9lyOpJYU5WXrpFbk1Z.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/iRl1rWK3s00qJOUpiDtKykDG4cK.jpg</fanart.3>
			<ep3.1>1 AL 4</ep3.1>
			<id3.1>62233d0d0c1f49603555a7abe9b89e340cbc2e9f</id3.1>
			<ep3.2>5 AL 8</ep3.2>
			<id3.2>51b41b2cc88bee71eae1b10ab1ec99e7ddea7c07</id3.2>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DEADWOOD</title>
		<info>(2004-2006) 3 temporadas. 36 episodios. Ambientada en los Estados Unidos, en la época de la fiebre del oro (1876). Poco después de la última acción del general Custer, en Black Hills surge una ciudad fronteriza, que es testigo de la implacable lucha de los pioneros por el poder. Entre los colonos había gente muy heterogénea: un hombre de leyes retirado, el intrigante propietario de un salón, el legendario Wild Bill Hickok o Calamity Jane; todos compartían un constante desasosiego espiritual y una gran voluntad de supervivencia.</info>
		<year>2004</year>
		<genre>Western. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/4Yp35DVbVOAWkfQUIQ7pbh3u0aN.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/wxd6ZQlFP9wkgAlzpSEVD9eu0yt.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/fRrp3EXRcX7C2mE47ADrO1yYKq3.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/AcVCp1RVilFKT3DWre0GjP7vzsq.jpg</fanart.1>
			<ep1.1>1 AL 12</ep1.1>
			<id1.1>f4e151e9e225ec02b6a47356001fbac6e1243cc1</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/sVsafEWZyFgM8m4JwwxX4tMoYQo.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/dGzPJnh8YcUS4J10sNunohaXMVn.jpg</fanart.2>
			<ep2.1>1 AL 12</ep2.1>
			<id2.1>4bcf68716e16ce96dec20a13ae528e83faca8381</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/rD2efYWFsVvCbpBURs7aKsHbtrl.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/h05wAWe6TJj8EoIP5umqimRLv6y.jpg</fanart.3>
			<ep3.1>1 AL 12</ep3.1>
			<id3.1>cde2227847296e2c0c37f8baafef24b3b596d0f9</id3.1>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DEFENDER A JACOB</title>
		<info>(2020) 8 episodios. Basada en la novela homónima, que sigue la historia de Andy Barber, un ayudante del fiscal que, ante un caso de asesinato, ve cómo todas las pruebas conducen a su propio hijo. Dividido entre su deber de defender la justicia y el amor a su hijo, deberá intentar resolver el crimen, cuya víctima es un chico de 14 años, compañero de clase de su hijo.</info>
		<year>2020</year>
		<genre>Intriga. Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/ngIV04Va4cotMFegh7xrV8kwEUR.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/9wCsImxDW4MCVP47vVjny0Wa06s.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/ngIV04Va4cotMFegh7xrV8kwEUR.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/9wCsImxDW4MCVP47vVjny0Wa06s.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>9ebf907543df68023eff43c6094bc9fecc852155</id1.1>
			<ep1.2>2 TODO ESTA BIEN</ep1.2>
			<id1.2>7c068a05acc0cf8443a597d003900a07d0ccde95</id1.2>
			<ep1.3>3 PIEDRAS</ep1.3>
			<id1.3>4036f7985ad4c9393c9adc28cf73d1421666b249</id1.3>
			<ep1.4>4 CONTROL DE DAÑOS</ep1.4>
			<id1.4>43ee5ef58e5f0246e92b11dbe5f11ec9e4cd76a5</id1.4>
			<ep1.5>5 VISITANTES</ep1.5>
			<id1.5>bc2ac4b2b4962b71ce894f08ca81c64338f7ecac</id1.5>
			<ep1.6>6 DESEOS</ep1.6>
			<id1.6>a965ebac228e9f450463cde6b69badcaacc2066e</id1.6>
			<ep1.7>7 AL 8</ep1.7>
			<id1.7>a66f519cd7bd1ef22ef8f50697a43fa295276828</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DESPLAZADOS</title>
		<info>(2020) 6 episodios. Las vidas de cuatro desconocidos se cruzan en un centro de detención de inmigrantes en pleno desierto australiano: una azafata aérea que huye de una peligrosa secta; un refugiado afgano y su familia que escapan de la persecución; un joven padre que se aleja de un trabajo sin futuro y un burócrata al que se le acaba el tiempo para ocultar un escándalo nacional.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/igI3KQGPEjlQ3yQ0w6w0sgusefn.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/cBNMwk8nXINUVC9bBOqD3SM2afZ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/igI3KQGPEjlQ3yQ0w6w0sgusefn.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/cBNMwk8nXINUVC9bBOqD3SM2afZ.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>2bc2a717cbb366b5e84544db948ff4e5fb673818</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DEVS</title>
		<info>(2020) 8 episodios. Una ingeniera informática investiga los secretos del departamento de desarrollo de su empresa, puesto que cree que es responsable de la desaparición de su novio.</info>
		<year>2020</year>
		<genre>Ciencia ficción. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/uv63iNWOh69bSJYJQZjiX6n8B3m.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/5yf5yfogIxhHhDU7F2Nuk1BsTYQ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/uv63iNWOh69bSJYJQZjiX6n8B3m.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/5yf5yfogIxhHhDU7F2Nuk1BsTYQ.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>ef0e9bd76a6777c16cf46d26b6541f0960b4b27f</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>f4a5af6d22955c054bc66ffa513cfeb3dbbb5114</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>1f9a670009220f4c06dd88885ac7a4bd743b90b0</id1.3>
			<ep1.4>Episodio 4</ep1.4>
			<id1.4>3dbc08ac75d34e36f80402b22da0caa4dcc7614c</id1.4>
			<ep1.5>Episodio 5</ep1.5>
			<id1.5>bef6a2b5ffd214a3b61aa791963dbec170f22d08</id1.5>
			<ep1.6>6 AL 8</ep1.6>
			<id1.6>86c87605ccad00dc86cb8d581e0377d261c28aca</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DEXTER NEW BLOOD</title>
		<info>(2021) 10 episodios. Diez años después del final de la serie, Dexter Morgan vive con un nombre falso en la pequeña ciudad de Iron Lake, Nueva York. Lleva una vida normal y es un miembro respetado de la sociedad, pero a raíz de eventos inesperados resurge su necesidad de dar rienda suelta a sus oscuros instintos.</info>
		<year>2021</year>
		<genre> Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/onnYrRDxvfEAjeXHxq1BoyUF419.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3STIpPmCd0qCl2DfeKxdM3NDK8e.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/onnYrRDxvfEAjeXHxq1BoyUF419.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3STIpPmCd0qCl2DfeKxdM3NDK8e.jpg</fanart.1>
			<ep1.1>1 OLA DE FRIO</ep1.1>
			<id1.1>4d116c4c6c5774998ef018b7a611b6adf2903481</id1.1>
			<ep1.2>2 TORMENTA DE MIERDA</ep1.2>
			<id1.2>1f0b7b5717a8535f0cd52c282109b7afc232eb77</id1.2>
			<ep1.3>3 SEÑALES DE HUMO</ep1.3>
			<id1.3>a34917a1361e459cae9346f0e9524731f84e0a5f</id1.3>
			<ep1.4>4 H ES POR HEROE</ep1.4>
			<id1.4>3802896cfe68918b64965652e3ca19b0f32c9838</id1.4>
			<ep1.5>5 FUGITIVO</ep1.5>
			<id1.5>d019b3b7bdf94f6dd66cff84f5d400b7238697aa</id1.5>
			<ep1.6>6 DEMASIADOS SANDWICHES DE ATUN</ep1.6>
			<id1.6>30b2d770208a5bedf8aec23947ac662e60a41a9c</id1.6>
			<ep1.7>7 EN LA PIEL DEL DIABLO</ep1.7>
			<id1.7>5b2529811a9c5f8195af842e5207864ae77b1247</id1.7>
			<ep1.8>8 JUEGO SUCIO</ep1.8>
			<id1.8>858eba8bcbb62b3d044e267a01f3f313eaa49646</id1.8>
			<ep1.9>9 NEGOCIO FAMILIAR</ep1.9>
			<id1.9>ca1d72604894792a1906a28246f89e83ab83f6a2</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DICKINSON</title>
		<info>(2019) 3 temporadas. 30 episodios. Biopic sobre la aclamada poeta norteamericana Emily Dickinson (1830-1886) que explora los conflictos sociales, familiares y de género que sufrió la autora</info>
		<year>2019</year>
		<genre>Drama. Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/c9hrNFheRPzQaR93DbO5HDLvEKA.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fxFhNxfvRUYh7fmZloGuCvKncKv.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/jn12eB2GK3X8OLBgXlfscfWqzL1.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/iC2a4rJgrvLkAzqOm03rl5PB9XQ.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>6a2f61cad7638da4d42b8f344882b8cd3db23696</id1.1>
			<ep1.2>6 UNA BREVE, PERO PACIENTE ENFERMEDAD</ep1.2>
			<id1.2>6a02c1bbb8e3db9eb78bacf4f9ada97537a694f6</id1.2>
			<ep1.3>7 PERDEMOS- PORQUE GANAMOS</ep1.3>
			<id1.3>78827b19d82f4bd410ef008735ff686585cbe761</id1.3>
			<ep1.4>8 HAY CIERTA DESVIACION DE LA LUZ</ep1.4>
			<id1.4>065135613d6df3b45c7d2dc9a7b2f60aece458f4</id1.4>
			<ep1.5>9 LA "FE" ES UNA GRAN INVENTO</ep1.5>
			<id1.5>f17a6ee75c6e74342e8d450d4e238cc56ef4b14f</id1.5>
			<ep1.6>10 SENTI UN FUNERAL, EN EL CEREBRO</ep1.6>
			<id1.6>5ee8a89fda175ef6a68867fc4d10d73997e98dce</id1.6>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/3tvm4WdGKw3Db0sMh9Pt4dckePn.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/5OHT8pe72FI7iIbsGQsJCDMyPxv.jpg</fanart.2>
			<ep2.1>1 AL 3</ep2.1>
			<id2.1>77b0ac08d94bdc72c4ad836e8e0b0a5d14ea84ba</id2.1>
			<ep2.2>4 LA MARGARITA SIGUE SUMISA AL SOL</ep2.2>
			<id2.2>07000f04257845f2ddf9c70b9b0f999d8f5cfe6c</id2.2>
			<ep2.3>5 EL FRUTO PROHIBIDO UN SABOR TIENE</ep2.3>
			<id2.3>194d8008f1f586eebb9a1ecf9c3d90032dbb85b6</id2.3>
			<ep2.4>6 ABRE LA ALONDRA</ep2.4>
			<id2.4>f125b22865a2e8f40f4664c6cb4b71d8d1ef85d6</id2.4>
			<ep2.5>7 LA ETERNIDAD SE COMPONE DE AHORAS</ep2.5>
			<id2.5>3fbe79f81b4474d286ba19b6fff557e9b1bc1d6f</id2.5>
			<ep2.6>8 ¡YO SOY NADIE! ¿QUIEN ERES TU?</ep2.6>
			<id2.6>2f59e61137816fe46f03545878dc0cd57c25ef2f</id2.6>
			<ep2.7>9 ME GUSTA VER LA AGONIA</ep2.7>
			<id2.7>3946d8de4bad11269dc23c6ea003fd79020665b4</id2.7>
			<ep2.8>10 NO PUEDES APAGAR UN FUEGO</ep2.8>
			<id2.8>ab2fefee7208daaf57250f82b28be763148a9c7d</id2.8>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/dDdcAfHBZ6Aalv53iR6o35CSLWA.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/eE3p6giSSYGc1SAgzYifMWLNQHa.jpg</fanart.3>
			<ep3.1>1 AL 3</ep3.1>
			<id3.1>8304ac37aaf410c6adc6a276d6bb751ee1307365</id3.1>
			<ep3.2>4 ESTA ES MI CARTA AL MUNDO</ep3.2>
			<id3.2>85a24bf706deb62a97c3208636e90154f0a13b61</id3.2>
			<ep3.3>5 CANTO DESDE EL CORAZON</ep3.3>
			<id3.3>6ff4d41a5762b433bb7048eb4053bd501ec9060e</id3.3>
			<ep3.4>6 UN POCO DE LOCURA EN PRIMAVERA</ep3.4>
			<id3.4>697d20af93e63694d7925d7b6e19f07636397625</id3.4>
			<ep3.5>7 EL FUTURO NUNCA HABLO</ep3.5>
			<id3.5>f65fde56276e5cdb42e8e36deffc5a4e1d09efc8</id3.5>
			<ep3.6>8 MI VIDA HABIA SOPORTADO UN ARMA CARGADA</ep3.6>
			<id3.6>fcef2287d4f2fd480030d44872b0e9fd8f3858fc</id3.6>
			<ep3.7>9 ESTA ERA UNA POETA</ep3.7>
			<id3.7>27c85d1c5d4385d7a7ef99b66d9b1c17826536ab</id3.7>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DIME QUIEN SOY</title>
		<info>(2020) 9 episodios. A través de un libro que recibe en su pequeña editorial, Javier conocerá la turbulenta biografía de Amelia Garayoa, una mujer que, movida por sus ideales, es capaz de dejar toda su vida atrás para luchar por la libertad. A través del encuentro con los cuatro hombres que marcan su vida: Santiago (su marido), Pierre, Albert y Max; Amelia se verá involucrada en los acontecimientos más relevantes de la historia del siglo XX, desde el alzamiento franquista hasta la liberación de Berlín, pasando por la represión comunista en la Rusia de Stalin, la barbarie del gueto de Varsovia, la Roma de los últimos años del Duce o el declive de la Alemania Nazi en la Atenas ocupada. Con un ojo en el pasado y otro en el presente, esta es la historia de la Europa de mediados del siglo XX, personificada en una mujer que nunca terminará de pagar el precio de sus propias contradicciones.</info>
		<year>2020</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/t7fwAUYSktWpZtSav0FCpQcvTI3.jpg</thumb>
		<fanart>https://i.imgur.com/silcUMM.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/t7fwAUYSktWpZtSav0FCpQcvTI3.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/silcUMM.jpg</fanart.1>
			<ep1.1>1 AMELIA</ep1.1>
			<id1.1>a58aa29ae6578ba2cd965d0309e14b9595df0a1f</id1.1>
			<ep1.2>2 PIERRE</ep1.2>
			<id1.2>ccbb8f386269631fe4d8c7dc854a62c2b1057669</id1.2>
			<ep1.3>3 ANUSHKA</ep1.3>
			<id1.3>ffc9e2291e5808ed5fcac54a8cf48725f10582c3</id1.3>
			<ep1.4>4 SANTIAGO</ep1.4>
			<id1.4>3fcd2bb8eeee96f0e08d3a3fe9c9f6c69b6deb70</id1.4>
			<ep1.5>5 ALBERT</ep1.5>
			<id1.5>489b705fe3100bbd29c12c5d1346f8f9ebafa4dd</id1.5>
			<ep1.6>6 AL 7</ep1.6>
			<id1.6>219310a665242b10fa9e38ef51b84f30217e233d</id1.6>
			<ep1.7>8 AL 9</ep1.7>
			<id1.7>09377287dc270be0cb12c550099d816107f8d84a</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DOLORES: LA VERDAD SOBRE EL CASO WANNINKHOF</title>
		<info>(2021)6 episodios. Dolores: La verdad sobre el caso Wanninkhof es una serie documental en la que por primera vez en 20 años Dolores Vázquez, víctima de una de los mayores errores judiciales de la historia reciente de España, habla públicamente sobre su doloroso pasado. En septiembre de 2000, Dolores Vázquez fue detenida como principal sospechosa del asesinato de la joven Rocío Wanninkhof, la hija de su expareja. El caso llegó a ser tan mediático, que esto presionó a la policía para que encontrar respuestas rápidamente. La ‘cabeza de turco’ fue la propia Dolores que fue estigmatizada por su sexualidad, su aparente indiferencia hacia el caso y por su carácter. Fue descrita como una asesina inteligente, calculadora y fría. Esto hizo que fuese encarcelada durante 519 días por un crimen que nunca cometió.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/ojU7ye6w0leax0PJCE5btyGmiXx.jpg</thumb>
		<fanart>https://i.imgur.com/L659MuM.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/ojU7ye6w0leax0PJCE5btyGmiXx.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/L659MuM.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>4128266235521a55134cd05bb50e3c9c8a144ac7</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>e236abd6d0a4404b09657dea89b369cf15b08c56</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DOPESICK: HISTORIA DE UNA ADICCION</title>
		<info>(2021) 8 episodios. La historia de cómo una empresa desencadenó la peor epidemia de drogas en la historia de Estados Unidos. Mire en el epicentro de la lucha de Estados Unidos con la adicción a los opioides, desde una comunidad minera angustiada de Virginia, hasta los pasillos de la DEA y la opulencia de "un por ciento" Big Pharma Manhattan.</info>
		<year>2021</year>
		<genre>Drama </genre>
		<thumb>https://www.themoviedb.org/t/p/original/qW8Gpddy29faTcD7VuyKjwLXbKU.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/2XITvxbKjdLO8jtZMKDyTByhekm.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qW8Gpddy29faTcD7VuyKjwLXbKU.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/2XITvxbKjdLO8jtZMKDyTByhekm.jpg</fanart.1>
			<ep1.1>1 AL 8 (1080)</ep1.1>
			<id1.1>D17E2377EDC471074C64DB3E0F6A549803C8F638</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>DOS VERANOS</title>
		<info>(2022) 6 episodios.  Un grupo de amigos se reúne treinta años después de que uno de los integrantes muriera en un accidente. Unas relajantes vacaciones se convierten en una pesadilla, cuando algunos de los amigos son chantajeados con imágenes de esa terrible semana hace tres décadas.</info>
		<year>2022</year>
		<genre>Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/2xzkqEdrKuXn0rTYAe88CFWOWo4.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/mGVmsxprlaEp4iNqSkj5AP7QMJC.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/2xzkqEdrKuXn0rTYAe88CFWOWo4.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/mGVmsxprlaEp4iNqSkj5AP7QMJC.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>c7146c131fe4ef0d5e258b5175f50edd40dc4a77</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>c71da79852d47a72004dbc755b0af352abefde50</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>a59eed9950a47259fea0ffa657500579929317ed</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>7505109e2c24e0579b77af768816da68ece0023e</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>2eab85cbf58f258f53eb4acc95f2fa563613e11a</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>6cd9889c9f9649f76b4b1f95b81d53d691bb748f</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL ABOGADO DEL LINCOLN</title>
		<info>(2022) 10 episodios. Un idealista iconoclasta dirige su práctica jurídica desde la parte trasera de su Lincoln Town Car en esta serie basada en las novelas más vendidas de Michael Connelly.</info>
		<year>2022</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/4jSaIqEU8CPBBNn4iVCK6wzPjSx.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/keHmjFaC4inGeZipmvIjXSRaZw7.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/4jSaIqEU8CPBBNn4iVCK6wzPjSx.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/keHmjFaC4inGeZipmvIjXSRaZw7.jpg</fanart.1>
			<ep1.1>1 VUELTA A LA CARGA</ep1.1>
			<id1.1>a39f806c7897fb7f4c8e18fd42ef04503810b09f</id1.1>
			<ep1.2>2 LA PIEZA MAGICA</ep1.2>
			<id1.2>2572fb0e79453690b070fab8dfc8855cfa98ce9d</id1.2>
			<ep1.3>3 IMPETU</ep1.3>
			<id1.3>861939e30f37cc8006d9a65e947104d92e349b16</id1.3>
			<ep1.4>4 LA TEORIA DEL CAOS</ep1.4>
			<id1.4>3e6c18c486ff963057f0efa683f8bec3cb319a79</id1.4>
			<ep1.5>5 DOCE LEMMINGS EN UNA CAJA</ep1.5>
			<id1.5>6a1820cf94020d986374043982c69b9d935a94ef</id1.5>
			<ep1.6>6 CORRUPTO</ep1.6>
			<id1.6>3478a6cde5d2801b54627f0f1dbe04600edd6aa2</id1.6>
			<ep1.7>7 EL LEMMING NUMERO 7</ep1.7>
			<id1.7>e08e47047535cc3155e608d206621421700e0f36</id1.7>
			<ep1.8>8 EL REGRESO DE LA PIEZA MAGICA</ep1.8>
			<id1.8>6be0baaaf60e2c9b5defe4a98b4d43d549b26c3c</id1.8>
			<ep1.9>9 EL VALLE INQUIETANTE</ep1.9>
			<id1.9>80d2ab7274b963273a055a04b55dba34d06d1871</id1.9>
			<ep1.10>10 EL VEREDICTO</ep1.10>
			<id1.10>3e97d64d291e63d54d9cd1d7433c4ff6ae15f647</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL ACCIDENTE</title>
		<info>(2017) 13 episodios. Una mujer busca a su marido desaparecido tras un accidente de avión. Lucía (Inma Cuesta) acompañó a su esposo José (Quim Gutiérrez) al aeropuerto, pero éste no figura en la lista de pasajeros, y está desaparecido. A medida que Lucía va investigando sobre su José, al que consideraba hasta entonces un marido modélico, descubrirá detalles que le hacen preguntarse si conocía realmente al hombre con el que ha formado una familia...</info>
		<year>2017</year>
		<genre>Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/z7qtTuvsLC0Pp0yf8hLUBZUltJ0.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/iUlSxDmLh5isgaLvonbkdeltQ64.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/z7qtTuvsLC0Pp0yf8hLUBZUltJ0.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/iUlSxDmLh5isgaLvonbkdeltQ64.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>9f6beef4f3a6da9946745a056856600aeb0f736b</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>2e4adf918f42223f1cf8c9206f9e69e5d0844cbc</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CASO ALCASSER</title>
		<info>(2019) 5 episodios. Relato documental que analiza e investiga uno de los crímenes más mediáticos de la historia de España. Este triple asesinato convulsionó en 1992 los cimientos de la sociedad española y cruzó fronteras, no solo por su brutalidad sino también por la impactante retransmisión y cobertura que los medios hicieron de él. Producida por Bambú Producciones, equipo responsable de "Lo que la verdad esconde: El caso Asunta".</info>
		<year>2019</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/sLUi7UG7hUfVNPVuoaoD078nDnH.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/2lPwLXNYoZ2mGIO7RcGlpvOnmTZ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/sLUi7UG7hUfVNPVuoaoD078nDnH.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/2lPwLXNYoZ2mGIO7RcGlpvOnmTZ.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>5F73CF68AE7577584B6A2C78E5BD6D848487BB87</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CASO HARTUNG</title>
		<info>(2021) 6 episodios. En un parque infantil de una tranquila zona residencial de Copenhague, la policía encuentra a una joven brutalmente asesinada a la que le han seccionado una mano. Sobre la chica cuelga un muñequito hecho con castañas. El caso se asigna a la ambiciosa y joven detective Naia Thulin y a su nuevo compañero, Mark Hess.</info>
		<year>2021</year>
		<genre>Thriller. Intriga</genre>
		<thumb>https://i.imgur.com/mcSSYeO.jpg</thumb>
		<fanart>https://i.imgur.com/7xV8P6l.jpg</fanart>
		<t1>
			<thumb.1>https://i.imgur.com/mcSSYeO.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/7xV8P6l.jpg</fanart.1>
			<ep1.1>1 AL 6 (1080)</ep1.1>
			<id1.1>a1046723209de0862c8e7c0a3976872d2f7b08ef</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CID</title>
		<info>(2020-2021) 2 temporadas. 10 episodios. 'El Cid' cuenta la historia del hombre detrás de la leyenda, Rodrigo Díaz de Vivar, también conocido como “Ruy”, desde que era niño hasta que se convirtió en un héroe de guerra, mientras intenta encontrar su lugar dentro de una sociedad donde las intrigas de la Corona de León intenta controlarlo. El Cid es un héroe por todos conocido pero también uno de los personajes más misteriosos y complejos de la historia de España. La trama tiene lugar en el siglo XI, una de las épocas más fascinantes de la historia de España, donde cristianos, árabes y judíos convivieron en la Península Ibérica, enfrentándose en guerras y/o forjando alianzas. Una historia de aventuras, amor, intriga, traición y lucha entre quienes ostentan el poder y quienes poseen la auténtica autoridad.</info>
		<year>2020</year>
		<genre>Aventuras. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/eNmtSyKksleNP9osXkVRR4mLb2z.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/5HGsJWpxzSSLM5Kj6BLhgIfU738.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/eNmtSyKksleNP9osXkVRR4mLb2z.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/5HGsJWpxzSSLM5Kj6BLhgIfU738.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>17a039702d6e7515d922adca806483b2bdaf8abb</id1.1>
			<ep1.2>1 LA CONJURA</ep1.2>
			<id1.2>2cc584de62d6980655b62d4b477583f9a9e21a2d</id1.2>
			<ep1.3>2 ORDALIA</ep1.3>
			<id1.3>e341b75f5a0790f4fd444f62c8c7ce4969f35be7</id1.3>
			<ep1.4>3 BARAKA</ep1.4>
			<id1.4>6a42a4a83e46a659a5136333ba14603d83ca79df</id1.4>
			<ep1.5>4 CAMPEADOR</ep1.5>
			<id1.5>506c123c0c58e7bc9788050c239a973b2a5be7b0</id1.5>
			<ep1.6>5 EXPIACION</ep1.6>
			<id1.6>6ca41378462241d5b81b06f32e56b3018a512919</id1.6>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/qgKB7PgoAIUXNAs8xr4OjThgp4C.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/a8tQAuHrXtU4nvDtZPmW2naeTJe.jpg</fanart.2>
			<ep2.1>1 AL 5</ep2.1>
			<id2.1>11311c5a90dad06c41e48baaf6ef33572d2af830</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL COLAPSO</title>
		<info>(2019) 8 episodios. 20 minutos cada uno, filmados todos ellos en plano secuencia. Un suceso -del que desconocemos las causas y el origen- ha provocado el colapso de la sociedad -la francesa y se sobrentiende que la mundial-, y es la espoleta que provoca una serie de historias independientes en diferentes localizaciones, que comparten la desesperación y la huida de las personas que intentan sobrevivir.</info>
		<year>2019</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/7MKWj44Kfh4c0yqQVIxl1is7JdZ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/s0kjnqY3wOpxgTac2F8vGCKUHbZ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/7MKWj44Kfh4c0yqQVIxl1is7JdZ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/s0kjnqY3wOpxgTac2F8vGCKUHbZ.jpg</fanart.1>
			<ep1.1>1 EL SUPERMERCADO</ep1.1>
			<id1.1>5ee1401f2e97686ea85c4e571b00f820d41eccc6</id1.1>
			<ep1.2>2 LA GASOLINERA</ep1.2>
			<id1.2>8a545ed31aa54abab304799296a492e3baa155d2</id1.2>
			<ep1.3>3 EL AERÓDROMO</ep1.3>
			<id1.3>c6c3263147f4bcf1c19bb46ffea355016ff54a7e</id1.3>
			<ep1.4>4 LA ALDEA</ep1.4>
			<id1.4>fedbd4c4c632e8abc55c69fc000638e4f45d2a22</id1.4>
			<ep1.5>5 LA CENTRAL</ep1.5>
			<id1.5>8ef9e4604bb4001279970e337e87c6107187714a</id1.5>
			<ep1.6>6 LA RESIDENCIA</ep1.6>
			<id1.6>f636df734fb892bbb9335d23de5137feb394ba40</id1.6>
			<ep1.7>7 LA ISLA</ep1.7>
			<id1.7>7b80755b0ed3c4c8e7511a142d57aca7979352d6</id1.7>
			<ep1.8>8 LA EMISION</ep1.8>
			<id1.8>17a7478aa1579684eb2e87baaaaf3d566df8b387</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CORAZON DE SERGIO RAMOS</title>
		<info>(2019) 8 capítulos. A través de una serie de acontecimientos en su vida común, esta miniserie documental trata de realizar un retrato sobre la personalidad de una de los futbolistas españoles más populares y exitosos de las últimas décadas: Sergio Ramos. Desde su relación sentimental a su labor como padre, pasando por los tejemanejes de su profesión como futbolista al estrecho vínculo que le une con su familia y más particularmente con su hermano René, el día a día del futbolista de Camas mezcla deporte con arte y pasión a raudales.</info>
		<year>2019</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/v26uNtj4CyB1iP4DR6WJyCjHZtF.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/nxuXLRM5nbbf4rFz9Jt12bmrYqb.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/v26uNtj4CyB1iP4DR6WJyCjHZtF.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/nxuXLRM5nbbf4rFz9Jt12bmrYqb.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>7D017B1DC0267C6E36D57968B0D6A5DF66CBC4D2</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CORAZON DEL IMPERIO</title>
		<info>(2021) 6 episodios. Roma domino Occidente durante mas de 1000 anos. Sus hijos fueron los emperadores de medio mundo pero sus hijas no fueron tan facilmente reconocibles. En este documental ficcionado, los Marco Antonio, Julio Cesar o Cesar Augusto ceden el papel protagonista a emperatrices, senadoras, esclavas, virgenes vestales y gladiadoras. A mujeres que no se conformaron con dar a luz a emperadores, sino que decidieron quien ocupaba cada cargo. Cleopatra, Livia, Julia Mesa, Fulvia… nombres de mujeres cuya historia hay que reescribir.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/4r0z02H6fWgfQqtmYVaueBk71DG.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/qnuFyzDvxW8lLXUGkxxo0AYQXJ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/4r0z02H6fWgfQqtmYVaueBk71DG.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/qnuFyzDvxW8lLXUGkxxo0AYQXJ.jpg</fanart.1>
			<ep1.1>1 GLADIADORAS</ep1.1>
			<id1.1>568514ee2245eb4b2e66e671b5cb91b9cd2250d5</id1.1>
			<ep1.2>2 CLEOPATRA Y FULVIA ( 1 PARTE)</ep1.2>
			<id1.2>18f9f4d18f3c8847e6b8bafe19b15e8b468daac4</id1.2>
			<ep1.3>3 CLEOPATRA Y FULVIA ( 2 PARTE)</ep1.3>
			<id1.3>b6df2a106900368f18779e766cc81fba9591952a</id1.3>
			<ep1.4>4 LIVIA</ep1.4>
			<id1.4>51c714344d229106df03c68245ac3cff7b1df6ea</id1.4>
			<ep1.5>5 JULIA Y HELIOGABALO</ep1.5>
			<id1.5>8790491bbfcb274286825f58a3d9dabacfc2c5f3</id1.5>
			<ep1.6>6 BACANTES Y VESTALES</ep1.6>
			<id1.6>6771ad93d65b5ed4dea63f5a6f166462c2a41872</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CRIMEN DE LA GUARDIA URBANA</title>
		<info>(2022) 4 episodios. El 4 de mayo de 2017 los Mossos encontraron un coche quemado en uno de los caminos del Pantano de Foix. En el interior del vehículo localizaron los restos de un cuerpo calcinado. El coche pertenecía a Pedro Rodríguez, un agente de la Guardia Urbana de Barcelona. Oficialmente, no constaba como desaparecido. Los mossos reconstruyen sus últimas horas de vida y comenzaron a encontrar incongruencias en las declaraciones de las personas más cercanas al desaparecido. "El crimen de la guardia urbana" forma parte de "Crímenes", una serie de 'true crime' que relata sucesos reales ocurridos en la historia reciente de Cataluña. A lo largo de los cuatro episodios, encontramos el estilo narrativo y el sello del periodista catalán Carles Porta que dirige, narra e introduce los espectadores en los laberintos de esta historia. Durante todo el episodio nos acompaña su voz en off, con un tono y estilo que atrapa y conecta con el espectador.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gxTBhcJd6mUR0Zhwkqz6TBCM0Ul.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/o20Xs7LhxmdcUJGTcA8UA7wzG1u.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gxTBhcJd6mUR0Zhwkqz6TBCM0Ul.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/o20Xs7LhxmdcUJGTcA8UA7wzG1u.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>4023296138af9b8379d0216dafa72bcc5a1017e4</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>984141e443cd5eafd13cc05a019cf4d006a3c34e</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL CUENTO DE LA CRIADA</title>
		<info>(2017) 4 temporadas. 46 episodios.  En un futuro distópico donde se ha implantado una dictadura fundamentalista, una joven se ve forzada a vivir como una concubina para dar hijos a su señor. Tras el asesinato del presidente de los Estados Unidos y la mayoría del Congreso, se instaura en el país un régimen teocrático basado en los más estrictos valores puritanos. Los Estados Unidos de América, desde ese momento, pasan a ser conocidos como la República de Gilead. En esa nueva sociedad, la mayor parte de los valores modernos occidentales han quedado desterrados. La mujer pasa a un segundo plano, siendo prácticamente un objeto cuyo único valor está en sus ovarios, pues hay un problema de fertilidad en Gilead. Adaptación de la novela de Margaret Atwood.</info>
		<year>2017</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/1ryCwZaZFAlG0c1w8XiMHeAxxYy.jpg</thumb>
		<fanart>https://i.imgur.com/cduOgYT.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/ty1YzVgigx1Af8GZj4r8aSef2zH.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/pyEUp9PgtyMP2hMpAhHkmLcrgMM.jpg</fanart.1>
			<ep1.1>1 AL 10 (1080)</ep1.1>
			<id1.1>29baab72f0a48b813e2db4b2499fcb36d587803d</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/633ofLc9Ikf1JqRxjX2oJsu5WKO.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/da2znbpM6sCSfeziPyKFKh19aih.jpg</fanart.2>
			<ep2.1>1 AL 13 (1080)</ep2.1>
			<id2.1>452b46c55581a391d2e60f8d9c18c1b7cffc0e45</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/tWL0vGrnziM0xcMlghm7wEYkEyk.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/uSjlkgEROY2soG1FIOUOlNwuWhx.jpg</fanart.3>
			<ep3.1>1 NOCHE</ep3.1>
			<id3.1>e8a0d5efa70ea6ef2d37f5bba9cd08e0a107fbe2</id3.1>
			<ep3.2>2 EL CORAJE DE DOS MADRES</ep3.2>
			<id3.2>135a79a23d0da2ce78781e047e1c8598dc979e44</id3.2>
			<ep3.3>3 VIGILAMOS</ep3.3>
			<id3.3>668310baa0b4ec71ef25de0d98f742662c5d048d</id3.3>
			<ep3.4>4 DIOS BENDIGA A LA NIÑA</ep3.4>
			<id3.4>989c0db1b6bef14accf1da2a41f605bd99ad9bc1</id3.4>
			<ep3.5>5 AMOR DE MADRE</ep3.5>
			<id3.5>0344995DC2BF17BC6C7C0615D6B65A4A0513A1CE</id3.5>
			<ep3.6>6 FAMILIA</ep3.6>
			<id3.6>be2452f7daa9507dc6630007b760cdba8105cb00</id3.6>
			<ep3.7>7 CON SU MIRADA</ep3.7>
			<id3.7>b4bb0a4253fd0974ba0b8a3eaa0cbeaac446836c</id3.7>
			<ep3.8>8 INCAPAZ</ep3.8>
			<id3.8>d281c876767faae947eae0e571fc769df5c46a3d</id3.8>
			<ep3.9>9 HEROICA</ep3.9>
			<id3.9>1b171751469f6e78f43c0b094754b5c22cf7b5e0</id3.9>
			<ep3.10>10 DAR FE</ep3.10>
			<id3.10>3b0eb77ef108348b6f32085211f72d6f40314faa</id3.10>
			<ep3.11>11 MENTIROSOS</ep3.11>
			<id3.11>8d54bef36e197aa8067f568c34f05c15f9144b31</id3.11>
			<ep3.12>12 SACRIFICIO</ep3.12>
			<id3.12>f5e1fc82c3bbf60b2f3f290db57a27f2c3f62b60</id3.12>
			<ep3.13>13 MAYDAY</ep3.13>
			<id3.13>23d2202d7cf1e24cf8e1d2b6ff2ce811490a795a</id3.13>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/o6oByj1AC6YcDbbjw4usTL3fAuL.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/jXB3OoWPkojsOP2O2OoLCeAIDRS.jpg</fanart.4>
			<ep4.1>1 AL 3</ep4.1>
			<id4.1>53377dbdb426a52b12f007f665745d442a8ac131</id4.1>
			<ep4.2>4 LECHE</ep4.2>
			<id4.2>c3dbf3ed88435eb203eaf3d49f44f23921ea978e</id4.2>
			<ep4.3>5 CHICAGO</ep4.3>
			<id4.3>d2b56b42d4c32ebce099e6c377be4a6e16c45f66</id4.3>
			<ep4.4>6 PROMESAS</ep4.4>
			<id4.4>0c88fd70057f46cc5b8b767bed413e690b3620ec</id4.4>
			<ep4.5>7 HOGAR</ep4.5>
			<id4.5>f697af4a0a2ee0f5ee61f91834d4359fa0e280f2</id4.5>
			<ep4.6>8 TESTIMONIO</ep4.6>
			<id4.6>608ed80149e640396a2576787ff403d6426a74d9</id4.6>
			<ep4.7>9 PROGRESO</ep4.7>
			<id4.7>80950405d46223eed644b3c82c1eaa9d6ef88b28</id4.7>
			<ep4.8>10 LA NATURALEZA</ep4.8>
			<id4.8>fcbe9ce1f429d2d34e9c1cd774a6f5562c397d7a</id4.8>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL DESAFIO: 11M</title>
		<info>(2022) 4 episodios. El foco se pondrá en los acontecimientos nunca vistos que rodearon el terrible ataque terrorista del 11 de marzo de 2004, en el que los servicios de emergencia y cientos de héroes anónimos colaboraron para ayudar a las víctimas del atentado, y convirtieron a Madrid en un ejemplo de solidaridad, valentía y unión ciudadana. La docuserie contará con los testimonios de supervivientes, además de entrevistas con periodistas de renombre, jueces, altos cargos de la Policía, líderes políticos y testigos, que ofrecerán su visión personal de lo acontecido durante el ataque a una de las ciudades más concurridas de Europa.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/EUf9IArTZzm1ah7Kbtlln84AqH.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/AvkyqSwfSdQV9yNJgwUSDiIEaVk.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/EUf9IArTZzm1ah7Kbtlln84AqH.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/AvkyqSwfSdQV9yNJgwUSDiIEaVk.jpg</fanart.1>
			<ep1.1>1 AMANECER</ep1.1>
			<id1.1>75c7848cd94ed7d8c856e03fd12762c016fc9fd9</id1.1>
			<ep1.2>2 GANAR</ep1.2>
			<id1.2>1b61607dca9f6d0e57adec64c4292d5f0a6c187a</id1.2>
			<ep1.3>3 NEUTRALIZAR</ep1.3>
			<id1.3>9cdf524d5f54a5cd50961ece8e49daeab9ee1f30</id1.3>
			<ep1.4>4 CONDENAR</ep1.4>
			<id1.4>3843bddf490514b9541e62e9708abe03d970a7b0</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL DESHIELO</title>
		<info>(2022) 6 episodios. Después de que se encuentre el cuerpo de una mujer joven en las aguas heladas del río Oder de Polonia, la detective Katarzyna Zawieja, recién viuda, dirige la investigación para localizar al asesino. Cuando se hace evidente que la víctima dio a luz poco antes de su muerte, Zawieja, que nunca se da por vencida fácilmente, va en busca del recién nacido desaparecido. Todavía conmocionada por el presunto suicidio de su marido, emprende un esfuerzo exhaustivo para resolver el caso, aunque eso signifique enfrentarse a sus propias circunstancias y ponerse a sí misma, y a su frágil familia, en el punto de mira de los criminales más peligrosos del país. La búsqueda implacable de la mujer por la verdad mientras se atreve a enfrentar sus propios demonios y encontrarse a sí misma.</info>
		<year>2022</year>
		<genre>Thriller. Intriga. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/tSn83DGeshKmDTvMEWyMqEg74E1.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/cOXQs0ZX5VohMqAyUDdtmJ2Oect.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/tSn83DGeshKmDTvMEWyMqEg74E1.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/cOXQs0ZX5VohMqAyUDdtmJ2Oect.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>ec98ec66bd55d8a176e08dce226816e8f5a54632</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>43bc0eaddbc554d49d2bc79769d78d87f2be20c7</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>2d45a8cef975b347f9ae5c660fe3d303150a8085</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>be7954d0bfbfab4f4e1812e5de0650bcb6454465</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>5bd010549d06a563a67bdd4733e5375a8da4010e</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>80aba92d6117ac48c647d64417971f35b9a56cde</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL DESORDEN QUE DEJAS</title>
		<info>(2020) 8 episodios. Con la intención de darle una oportunidad a su matrimonio, Raquel (Inma Cuesta), una joven profesora de literatura, acepta un trabajo en el instituto del pueblo donde creció su marido. "Y tú, ¿cuánto vas a tardar en morir?” Así de contundente es la nota que Raquel encuentra entre los trabajos a corregir el primer día de trabajo. Su ilusión por impartir clases se dará de bruces con unos alumnos que la reciben con esa macabra bienvenida. Pronto descubrirá quién era la profesora a la que sustituye, Viruca (Bárbara Lennie), y cómo ha marcado la vida de todos. Raquel iniciará su propia búsqueda para descubrir la verdad en un lugar en el que todos parecen guardar secretos.</info>
		<year>2020</year>
		<genre>Intriga. Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/mjHka6e9mg1zjgXTvsHMgPaYf2K.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/8FDb3FUNGEeiPmHG00Onlu6xVnE.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/mjHka6e9mg1zjgXTvsHMgPaYf2K.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/8FDb3FUNGEeiPmHG00Onlu6xVnE.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>a5f91ae11120ce74034321fb75377f5b9fd01763</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL EMBARCADERO</title>
		<info>(2019-2020) 2 temporadas. 16 episodios. La joven arquitecta Alejandra (Verónica Sánchez) acaba de ganar junto a su socia (Marta Milans) la obra de un rascacielos millonario. Llama a su marido, Óscar (Morte), para contárselo; él está en Frankfurt. Horas después, un guardia civil le dice que han encontrado el coche de su marido hundido en L’Albufera... con él dentro. ¿Se ha suicidado? ¿Estaba en Frankfurt? Entre sus pertenencias, un segundo móvil que escondía su segunda vida con otra mujer, Verónica (Irene Arcos)...</info>
		<year>2019</year>
		<genre>Intriga. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/7RKzTzhSKqLs5AREwoG223bdHsL.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/85P2BhNd00WzvMyKQrj5PviEOwY.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/7RKzTzhSKqLs5AREwoG223bdHsL.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/85P2BhNd00WzvMyKQrj5PviEOwY.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>ee347301ac263621753ae3fa9850025b87aa2747</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>cf7a9372be6f40b8189634674a87519982252612</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>102faf0e3e231e20a1d6c6e491dccd100265a5ff</id1.3>
			<ep1.4>Episodio 4</ep1.4>
			<id1.4>49f49b1604ba58c7eed0788f25d71b5fe82e1b6a</id1.4>
			<ep1.5>Episodio 5</ep1.5>
			<id1.5>1e161cf99a002c1234fd837e2c0297800fbf14b6</id1.5>
			<ep1.6>Episodio 6</ep1.6>
			<id1.6>7f92f96dd48ef9d3eed826e8d9d9345f00d90e70</id1.6>
			<ep1.7>Episodio 7</ep1.7>
			<id1.7>68f0a99b8c7f4daf86ff0848028a933cc2e20dc9</id1.7>
			<ep1.8>Episodio 8</ep1.8>
			<id1.8>22bc0c34dea678b87aa7eebaec08bbf658c05da7</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/dFuTT1nRzSFknNQuFEvtpb5QZ3O.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/canBPgqn7259Or4cbPeQ4eLnMHE.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>2b0aca929adcbf9111538da99cd5188077e06e98</id2.1>
			<ep2.2>5 AL 8</ep2.2>
			<id2.2>8ae7aa0d09fafb1a5e3757163ca6d9c83ff20258</id2.2>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL HOMBRE QUE CAYO A LA TIERRA</title>
		<info>(2022) 7 episodios. 'The Man Who Fell To Earth' sigue a un personaje alienígena (Chiwetel Ejiofor) que llega a la Tierra en un punto de inflexión en la evolución humana y debe enfrentarse a su propio pasado para determinar nuestro futuro. Naomie Harris interpreta a Justin Falls, una brillante científica e ingeniera que debe enfrentarse a sus propios demonios en su intento por salvar dos mundos.</info>
		<year>2022</year>
		<genre>Ciencia ficción</genre>
		<thumb>https://i.imgur.com/abPnjHr.jpg</thumb>
		<fanart>https://i.imgur.com/qt9FPmy.jpg</fanart>
		<t1>
			<thumb.1>https://i.imgur.com/abPnjHr.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/qt9FPmy.jpg</fanart.1>
			<ep1.1>1 HOLA CHICO DEL ESPACIO</ep1.1>
			<id1.1>c986c42728c3fd3bb59a27884d92b88894060938</id1.1>
			<ep1.2>2 SUCIO Y LIGERAMENTE ATURDIDO</ep1.2>
			<id1.2>c982c19f86a7a4b5c3ad789a9fe9419b525968e9</id1.2>
			<ep1.3>3 LOS NUEVOS ANGELES DEL MAÑANA</ep1.3>
			<id1.3>4c3c7ecff9b538a46e702af63c104fad0a6a0849</id1.3>
			<ep1.4>4 BAJO PRESION</ep1.4>
			<id1.4>89d9718a2f4ee09bcc39d6eb652935ab2d8055b7</id1.4>
			<ep1.5>5 ENSUEÑO DE LA ERA LUNAR</ep1.5>
			<id1.5>6b6bb74b0d3d30516368a46f0a5df00c9339449b</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL INOCENTE</title>
		<info>(2021) 8 episodios. Una noche, hace nueve años, Mateo intercedió inocentemente en una pelea y terminó convirtiéndose en un homicida. Ahora es un ex-convicto que no da nada por sentado. Su mujer, Olivia, está embarazada, y los dos están a punto de conseguir la casa de sus sueños. Pero una llamada impactante e inexplicable desde el móvil de Olivia vuelve a destrozar la vida de Mateo por segunda vez.</info>
		<year>2021</year>
		<genre>Thriller. Intriga. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gA49CDurmOaF9T09gwI6GfIlBeM.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5HSYWnkb7hFaRY3RQZTj052neQ1.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gA49CDurmOaF9T09gwI6GfIlBeM.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5HSYWnkb7hFaRY3RQZTj052neQ1.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>f1058e26e82051782710c31ba6e66581f27f7378</id1.1>
			<ep1.2>1 AL 8</ep1.2>
			<id1.2>94bc437d5a2fd6fd5fb1d09383a115cc7a3d74e4</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL INTERNADO: LAS CUMBRES</title>
		<info>(2021) 2 temporadas. 16 episodios. Reinicio de 'El internado' cuya primera temporada constará de ocho episodios que transcurrirán en un colegio ubicado junto a un antiguo monasterio en un lugar inaccesible entre las montañas. En esta ocasión, los alumnos son chavales problemáticos y rebeldes que vivirán bajo la estricta y severa disciplina que impone el centro para reinsertarlos a la sociedad. El bosque circundante alberga antiguas leyendas, amenazas que siguen vigentes y que les sumergirán en aventuras trepidantes y terroríficas</info>
		<year>2021</year>
		<genre>Drama. Intriga. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/kl07N07l4XNjXF48oujzWXs40Dw.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8OrceoLR3ARlthFw0daqW7saXit.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/kl07N07l4XNjXF48oujzWXs40Dw.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8OrceoLR3ARlthFw0daqW7saXit.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>3780a55190d46422d584d2f0dee568efcf40d2e6</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/xsL92wzeNZkq6If1FDReaEdyHM6.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/aMPOqBQ2gWQxNQ8Ef7BzIqyhghS.jpg</fanart.2>
			<ep2.1>1 Episodio</ep2.1>
			<id2.1>4373d9623e1e908089e94014fc7a9df72486f7bc</id2.1>
			<ep2.2>2 Episodio</ep2.2>
			<id2.2>037557ec6772b3f48e422949537294da259353c5</id2.2>
			<ep2.3>3 Episodio</ep2.3>
			<id2.3>037557ec6772b3f48e422949537294da259353c5</id2.3>
			<ep2.4>4 Episodio</ep2.4>
			<id2.4>f7faf136446b22a59f89c667aa9a617d77da6194</id2.4>
			<ep2.5>5 Episodio</ep2.5>
			<id2.5>8f4f6cd491cb2d7eb4248241062efca50b0b7812</id2.5>
			<ep2.6>6 Episodio</ep2.6>
			<id2.6>c35662dc2ca08e70124f4c15949f47ec0aae7263</id2.6>
			<ep2.7>7 Episodio</ep2.7>
			<id2.7>a7e8834ac6905f350d1c890a0223c47c65ab783a</id2.7>
			<ep2.8>8 Epsisodio</ep2.8>
			<id2.8>c9478d43c164a1279144b51ca97518716155d01d</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL JUEGO DEL CALAMAR</title>
		<info>(2021) 9 episodios. Cientos de jugadores con problemas económicos aceptan una extraña invitación para competir en juegos infantiles. Dentro les esperan un premio tentador y desafíos letales.</info>
		<year>2021</year>
		<genre>Thriller. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/iRbJ4r1W0XwaajfhiBhB8sSmYE5.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3xF78MnaTAoX3hkygglbCUEywU.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/iRbJ4r1W0XwaajfhiBhB8sSmYE5.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3xF78MnaTAoX3hkygglbCUEywU.jpg</fanart.1>
			<ep1.1>1 AL 9</ep1.1>
			<id1.1>D84CCB6BDE87AAE5EDDCBDE0A0CE480C4029C6C1</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL LIBRO DE BOBA FETT</title>
		<info>(2021) 7 episodios. El legendario cazarrecompensas Boba Fett y la mercenaria Fennec Shand deben navegar por el inframundo de la galaxia cuando regresen a las arenas de Tatooine para reclamar el territorio que una vez gobernó Jabba el Hutt y su sindicato del crimen.</info>
		<year>2021</year>
		<genre>Ciencia ficción. Acción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/5NsDb5r2KsCL3G0tBjOyzI3BEWI.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/6wERdTXVSntT2dSNHeAwB78DaYV.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/5NsDb5r2KsCL3G0tBjOyzI3BEWI.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/6wERdTXVSntT2dSNHeAwB78DaYV.jpg</fanart.1>
			<ep1.1>1 FORASTERO EN TIERRA EXTRAÑA</ep1.1>
			<id1.1>71A23A08175FD43715C6E0048ABE2E8DA5AC41F4</id1.1>
			<ep1.2>2 LAS TRIBUS DE TATOOINE</ep1.2>
			<id1.2>454B330B0B32C2578BEE3FCF4B2AAD08EE1B169B</id1.2>
			<ep1.3>3 LAS CALLES DE MOS ESPA</ep1.3>
			<id1.3>19E34CBB3F49E004587E21FA062FF76D54218EE0</id1.3>
			<ep1.4>4 AMENAZA DE TORMENTA</ep1.4>
			<id1.4>C5326158BC99FECE3B935359F6A0C5FB09391FAE</id1.4>
			<ep1.5>5 EL RETORNO DEL MANDALORIANO</ep1.5>
			<id1.5>C56866AD2F9979C80DE5D97F26F9C19BD3D71EC1</id1.5>
			<ep1.6>6 DESDE EL DESIERTO LLEGA UN EXTRAÑO</ep1.6>
			<id1.6>D9AA6B39AABBB7900798F31223CDA7995A38D8A4</id1.6>
			<ep1.7>7 EN EL NOMBRE DEL HONOR</ep1.7>
			<id1.7>363A8998DC7CDE61D5369A637E7B8A59334239CE</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL METODO KOMINSKY</title>
		<info>(2018-2021) 3 temporadas. 22 episodios. Una comedia dramática sobre un veterano actor, Sandy (Michael Douglas), cuya carrera nunca llegó a funcionar del todo, y que se gana la vida dando clases de interpretación, y su agente Norman (Alan Arkin), recientemente enviudado y muy pesimista con todo lo que le rodea... Del creador de 'Big Bang' y 'Dos hombres y medio'.</info>
		<year>2018</year>
		<genre>Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/jdxwWoxeMhPtu8mLb1AzaJgTnL.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/pUkTpbnA1GaCDGXMTl8MXv7oG6J.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/vnK8b6bNl1koSthTGbzgMPnh7Ky.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/azloRadFpXxYlIeQ3YaVFyD5Hh4.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>6cf2788984dc4ff575bb11c79a8b378554f15545</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/2pfUwysvHuCo89RIQsd4kt4MJcF.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/rlD69tjmWcbcjY94imq7uaB0Tj8.jpg</fanart.2>
			<ep2.1>1 AL 8</ep2.1>
			<id2.1>d1c758525e6bcde81d2b6c9fadd2a33478283ac0</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/jdxwWoxeMhPtu8mLb1AzaJgTnL.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/pUkTpbnA1GaCDGXMTl8MXv7oG6J.jpg</fanart.3>
			<ep3.1>1 AL 6</ep3.1>
			<id3.1>a73f2be558de953f8e7f1efeed7c1a0662f30429</id3.1>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL PACIFICADOR</title>
		<info>(2022) 8 episodios. La historia de origen de Peacemaker, un hombre que cree en la paz a cualquier precio, no importa cuántas personas tenga que matar para conseguirla.</info>
		<year>2022</year>
		<genre>Accion. Aventuras. Comedia </genre>
		<thumb>https://www.themoviedb.org/t/p/original/lBw4rwfoLsHAs5pTISgIOFnRk2c.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8VElCi4ZthGwcr8YBfbD5qvHExX.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/lBw4rwfoLsHAs5pTISgIOFnRk2c.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8VElCi4ZthGwcr8YBfbD5qvHExX.jpg</fanart.1>
			<ep1.1>1  AL 3</ep1.1>
			<id1.1>B98DB4DC573003A954E4B897B222A5CAA0FA35CC</id1.1>
			<ep1.2>4 EL PENE MENOS TRANSITADO</ep1.2>
			<id1.2>4D71FA278A3A2340788D3C1CC3798A2A268DCD79</id1.2>
			<ep1.3>5 MONO DORY</ep1.3>
			<id1.3>C20E80BEF37C6397DA90C0ABBAC6C4208B8C0BEC</id1.3>
			<ep1.4>6 LAMENTO DESPUES DE LEER</ep1.4>
			<id1.4>E199E3F783BAE4225BCB5A229478971D3B0323A8</id1.4>
			<ep1.5>7 DEJA DE DRAGONEAR MI CORAZON POR TODOS LADOS</ep1.5>
			<id1.5>CC596F11ED983727707D5C7A03FAF575A7AAE87D</id1.5>
			<ep1.6>8 ES VACA O NUNCA</ep1.6>
			<id1.6>B67EDFA5F34BA619F5900AFDEC6B435C80F0AA90</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL PIONERO</title>
		<info>(2019) 4 capítulos. Documental sobre el empresario Jesús Gil, una de las figuras políticas más populares en España durante la década de los años 90.</info>
		<year>2019</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/lz4y5GCBmxMwiZUHvodQfiD1yqJ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/8YHQM4IqNysGCS9jtkzjeiPj1ul.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/lz4y5GCBmxMwiZUHvodQfiD1yqJ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/8YHQM4IqNysGCS9jtkzjeiPj1ul.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>cdeaeb522f2c4e800504dad8667aa153fdbaf720</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>85f47e0a3311f97d1b00cf0c568065dd4952941a</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>440ad436170919c957371ccc50b61a51196c6dab</id1.3>
			<ep1.4>Episodio 4</ep1.4>
			<id1.4>5178ab0deaa986d03ead38534ed267aca9a0c7c5</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL PRINCIPITO ES OMAR MONTES</title>
		<info>(2021) 4 Episodios. Serie documental que sigue la actual etapa en la vida del cantante madrileño Omar Montes, el artista español más escuchado durante el 2020 en la plataforma Spotify. Mediante entrevistas personales, conversaciones con amigos y familiares e imágenes inéditas, el documental echará la vista atrás para descubrir cómo se fraguó su carrera y su éxito.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/3Zglu18edxzqIWVb29bJqCC1FJA.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nxlbERnDm9wwhjYDyXY0JZ2tNeJ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/3Zglu18edxzqIWVb29bJqCC1FJA.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nxlbERnDm9wwhjYDyXY0JZ2tNeJ.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>088bd937768ebb22fff0e20e3d55715830406b23</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>b30d11d45225ca64eee0a3eb80cf33f8ef1a0439</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL SIMBOLO PERDIDO</title>
		<info>(2021) 10 episodios. Cuando su mentor y amigo Peter Solomon, director del Instituto Smithsonian, es misteriosamente secuestrado, Robert Langdon debe emprender una carrera contrarreloj para descifrar una serie de enigmas que lo sumergen de lleno en una conspiración con tintes masónicos. La investigación de la desaparición le lleva a reencontrarse con una vieja amiga: Katherine, la hija de Peter y el vínculo más cercano para seguir la pista a su padre. Ambos tendrán que echar mano de sus conocimientos y una buena dosis de sangre fría si quieren salvar a Solomon y evitar que uno de los secretos mejor guardados de la Historia caiga en las manos equivocadas. 'Dan Brown: El símbolo perdido' es un thriller basado en el ‘best seller’ internacional de Dan Brown que se remonta a los inicios de Robert Langdon, el reputado profesor de la Universidad de Harvard experto en simbología.</info>
		<year>2021</year>
		<genre>Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/lWkBHpxRW0flhKgWcfZzJLlErcg.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/hZ9MlxA80rLGh5OExoJEeIEBByD.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/lWkBHpxRW0flhKgWcfZzJLlErcg.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/hZ9MlxA80rLGh5OExoJEeIEBByD.jpg</fanart.1>
			<ep1.1>1 COMO ES ARRIBA, ES ABAJO</ep1.1>
			<id1.1>ba1200e24b4858a7ff8e3d6c6ec173c829ac1207</id1.1>
			<ep1.2>2 EL ARAF</ep1.2>
			<id1.2>2164023eb5ff2768138240f9d01a9b7ff18c214e</id1.2>
			<ep1.3>3 ESTORNINOS</ep1.3>
			<id1.3>667c74cd8bf9088b742562ca61d99a82b774eddb</id1.3>
			<ep1.4>4 COORDENADAS L´ ENFANT</ep1.4>
			<id1.4>a16d026256259fa4ad1948b654c5eab36ec3d9fb</id1.4>
			<ep1.5>5 MELANCOLIA</ep1.5>
			<id1.5>ecdd7fdfa183b105201bf62ebe1e190740ac7629</id1.5>
			<ep1.6>6 SEUDONIMO DIOFANTICO</ep1.6>
			<id1.6>6436cbc8b5917e63b2be9589f2c16c89ead3efc5</id1.6>
			<ep1.7>7 NEOGENESIS</ep1.7>
			<id1.7>f7a89360a99c9b4fcc8a2cfe3a81bf8a71704e8d</id1.7>
			<ep1.8>CASCADA</ep1.8>
			<id1.8>bb34532ca404fc9f6689759b1809c68f314b34bf</id1.8>
			<ep1.9>9 ORDEN OCHO</ep1.9>
			<id1.9>adf797072a6b53cf9a65788194f8a6589cbbd314</id1.9>
			<ep1.10>10 RESONANCIA</ep1.10>
			<id1.10>2469efbbff0b70bb130d93eed581db3501279660</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL TIEMPO ENTRE COSTURAS</title>
		<info>(2013) 11 episodios.  Adaptación del best-seller de María Dueñas, que narra la historia de Sira Quiroga (Adriana Ugarte), una joven costurera de la capital que ve cómo su vida da un giro completo y tendrá que salir adelante ella sola en un lugar en principio tan hóstil como la ciudad de Tánger, en Marruecos. Hasta allí llega la joven modista, empujada por el destino hacia un arriesgado compromiso en el que los patrones y las telas de su oficio se convertirán en la fachada de algo mucho más turbio y trascendente.</info>
		<year>2013</year>
		<genre>Drama. Romance. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/w2WPBiOSHsChAOBn6vDIosSS3Jq.jpg</thumb>
		<fanart>https://i.imgur.com/GTEehkx.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/w2WPBiOSHsChAOBn6vDIosSS3Jq.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/GTEehkx.jpg</fanart.1>
			<ep1.1>1 AL 11</ep1.1>
			<id1.1>45b86da7e6b34f10a146cb55f8b6b227697ed70c</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL TURISTA</title>
		<info>(2022) 6 episodios. Un hombre es perseguido por un enorme camión cisterna que intenta sacarlo de la carretera. Se despierta en el hospital, herido, pero vivo. Sin embargo, no tiene idea de quién es, por lo que la búsqueda de respuestas lo impulsa a seguir.</info>
		<year>2022</year>
		<genre>Intriga. Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/9vKkRooP5ehhfAhzORFkkSRhkvZ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/i2u22f5fSujYLF2xbUrE8imEtuz.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/9vKkRooP5ehhfAhzORFkkSRhkvZ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/i2u22f5fSujYLF2xbUrE8imEtuz.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>0d791bdbb3113535d2883512d67793917793ec0f</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>1590667e66df308417854b11a7e468752e172421</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>13a982e1104d58c48bf1f25548c8832edd570e77</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>1da7c6e5a2eaaf99f65a7969ae3a5c52217a05d5</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>40f063b0b76c0dd5af576ea9a918bf18ad0c67cd</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>b3168f18b5be876545a3535f634327c987bd60ad</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EL VISITANTE</title>
		<info>(2020) 10 episodios. El detective Ralph Anderson (Ben Mendelsohn) y Holly Gibney (Cynthia Erivo), una investigadora poco ortodoxa, investigan el brutal asesinato de Frankie Peterson, un niño de 11 años, ocurrido en un pacífico pueblo. Aunque al principio todo parece tener una explicación dentro de lo plausible, pronto se darán cuenta de que una fuerza sobrenatural parece tener mucho que ver en la desaparición del chico.</info>
		<year>2020</year>
		<genre>Thriller. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/xZnv6zYUJkUSWscEl1wwpUhmQYx.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/uT4GWv6A71Utef7yjUIeYOhDjk2.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/xZnv6zYUJkUSWscEl1wwpUhmQYx.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/uT4GWv6A71Utef7yjUIeYOhDjk2.jpg</fanart.1>
			<ep1.1>1 PESCADO EN UN BARRIL</ep1.1>
			<id1.1>ea61ae55b2c73a0aab2c815163551e9229c5a086</id1.1>
			<ep1.2>2 ROANOKE</ep1.2>
			<id1.2>c60d4608ec2a69136224539eccd1e687fd2a4834</id1.2>
			<ep1.3>3 TIO OSCURO</ep1.3>
			<id1.3>cd4e670807ccf3e93501cbdf03aaede63fb99bba</id1.3>
			<ep1.4>4 QUE VIENE EL COCO</ep1.4>
			<id1.4>6f13789fc3c259e4ecb8a0ae1d29cb952c99acb9</id1.4>
			<ep1.5>5 EL BEBEDOR DE LAGRIMAS</ep1.5>
			<id1.5>ec11be22ff8c1c7c7c609335548e7c09daf491bb</id1.5>
			<ep1.6>6 EL DEL VAMPIRO JUDIO</ep1.6>
			<id1.6>cc9449037bdefb572f3455d8624e8d373122e85d</id1.6>
			<ep1.7>7 EN LOS PINOS EN LOS PINOS</ep1.7>
			<id1.7>59691b9c7cd10bd75696d7482409d6948c463972</id1.7>
			<ep1.8>8 CABEZA DE ZORRO</ep1.8>
			<id1.8>8512c67cfa07f4d66f5bd4815327e47bdc230975</id1.8>
			<ep1.9>9 TIGRES Y OSOS</ep1.9>
			<id1.9>a2e091f960b0f17fa6c2028af36ff08f21c22ed0</id1.9>
			<ep1.10>10 DEBES NO PUEDES</ep1.10>
			<id1.10>fcdb9f14a8f1ef10a207b7abb85d33656f484ec6</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ELITE</title>
		<info>(2018) 5  temporadas. 40 episodios. Las Encinas es el colegio más exclusivo del país y al que la élite envía a sus hijos a estudiar. Un mundo privilegiado con fiestas de alta sociedad, niños ricos, sexo y drogas. En él acaban de ser admitidos tres chicos de clase obrera después de que un terremoto destruyera el colegio público y sus alumnos tuvieran que ser repartidos por otros institutos de la zona. El choque entre los que lo tienen todo y los que no tienen nada que perder crea una tormenta perfecta que acaba en un asesinato. ¿Quién ha cometido el crimen?</info>
		<year>2018</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/w81eLVq1pBpztPrJq0RowqNDhLk.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1joatrwAnvdjifn49F4I3wDKbvf.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/w81eLVq1pBpztPrJq0RowqNDhLk.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/1joatrwAnvdjifn49F4I3wDKbvf.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>137b9903324c4e79cec6bb54666a941765f1400f</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/ybIOTW950wRQzuX5H5xcqC8qDUo.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/xcZfdO0x7sMLDUFV0qJOHxMJj45.jpg</fanart.2>
			<ep2.1>1 20 HORAS DESAPARECIDO</ep2.1>
			<id2.1>bf54dee9cb4064bf339c51bb189a3e0b4b6538d9</id2.1>
			<ep2.2>2 34 HORAS DESAPARECIDO</ep2.2>
			<id2.2>efd25d56211e6a63a2450a8a32f0b9a61737d65c</id2.2>
			<ep2.3>3 36 HORAS DESAPARECIDO</ep2.3>
			<id2.3>f076fa968c2f838573daad8d14632325ed1f5ddd</id2.3>
			<ep2.4>4 59 HORAS DESAPARECIDO</ep2.4>
			<id2.4>00c4e548a46b36fbd534772eeebb590cd17c6fe4</id2.4>
			<ep2.5>5 63 HORAS DESAPARECIDO</ep2.5>
			<id2.5>44974ffe3bdb5b62d95806ca2f8b703734068aef</id2.5>
			<ep2.6>6 66 HORAS DESAPARECIDO</ep2.6>
			<id2.6>7d11fce94e7011fd5bc72c1c341d43ef1f7e5a43</id2.6>
			<ep2.7>7 84 HORAS DESAPARECIDO</ep2.7>
			<id2.7>cf9e70f1766c3dea8d4828f1ef57f5bead28a959</id2.7>
			<ep2.8>8 0 HORAS DESAPARECIDO</ep2.8>
			<id2.8>d0150749d14de148fd7ef4add91984786a1cc806</id2.8>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/3NTAbAiao4JLzFQw6YxP1YZppM8.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/1qOA3kMtQO8bjnW8M2smjA8tp10.jpg</fanart.3>
			<ep3.1>1 AL 8</ep3.1>
			<id3.1>8ab1a8bd32ce54714edcf670fd2c4c257e0ac711</id3.1>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/1aQY4oYX5XOMN0vMDqwzS7hfPIt.jpg</thumb.4>
			<fanart.4>https://i.imgur.com/nQPwPS8.jpg</fanart.4>
			<ep4.1>1 A 8 (1080)</ep4.1>
			<id4.1>5de8f91376e2856b71e625f6ee334b8bcafece49</id4.1>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/48CTZhwy2iZauhhxMo3KEWo7avF.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/uU6YW3N11qECNfz18LNGAGg3Uir.jpg</fanart.5>
			<ep5.1>1 LO MATE</ep5.1>
			<id5.1>0be55b7d03a38b3815ed306899b279b38e8b4886</id5.1>
			<ep5.2>2 TODO VALE</ep5.2>
			<id5.2>1bb91163a65b38e678ca532fbc01cc5e541b49d5</id5.2>
			<ep5.3>3 ATARME</ep5.3>
			<id5.3>b98bcb101c6eae1209f77835abeb82b3e9935688</id5.3>
			<ep5.4>4 EL CUERPO</ep5.4>
			<id5.4>8c26885038eb13dd892c9556540d55b768a6dbdf</id5.4>
			<ep5.5>5 POR FAVOR DI LA VERDAD</ep5.5>
			<id5.5>1f7393afda4ee0c17b2b7ea1144ed52509cf26bf</id5.5>
			<ep5.6>6 NO PUEDES COMPRAR MI AMOR</ep5.6>
			<id5.6>52b00dd5ec9a1a7fc2b000e414b64d34e0177474</id5.6>
			<ep5.7>7 TOXICA</ep5.7>
			<id5.7>393fdfd627498832b9f101b880863dddbd65ed32</id5.7>
			<ep5.8>8 TU LADO DEL MUNDO Y EL MIO</ep5.8>
			<id5.8>afca580cb48c9d7b2f0463b9f734615521e5caec</id5.8>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ELLOS</title>
		<info>(2021) 10 episodios. Una pareja de raza negra decide mudar a su familia a un vecindario blanco de Los Ángeles. La casa de la familia en una calle arbolada y aparentemente idílica se convierte en la zona cero donde fuerzas malévolas amenazan con burlarse de ellos, devastarlos y destruirlos.</info>
		<year>2021</year>
		<genre>Terror. Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bJw1VZ4ACt5TnAvJbHcprzgYc1E.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nRifrfdncHTRKUbk0MrAcuQmDFN.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bJw1VZ4ACt5TnAvJbHcprzgYc1E.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nRifrfdncHTRKUbk0MrAcuQmDFN.jpg</fanart.1>
			<ep1.1>1 al 10</ep1.1>
			<id1.1>2369aa17981d0739f6e8209100746c0c10a155f7</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EMILY EN PARIS</title>
		<info>(2020) 2 temporada. 20 episodios. Emily, una estadounidense de veintitantos años del Medio Oeste, se muda a París para una oportunidad de trabajo inesperada, encargada de llevar el punto de vista estadounidense a una venerable empresa de marketing francesa. Las culturas chocan mientras se adapta a los desafíos de la vida en una ciudad extranjera, al tiempo que hace malabarismos con su carrera, sus nuevas amistades y su vida amorosa.</info>
		<year>2020</year>
		<genre>Romance. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/kwMqIYOC4U9eK4NZnmmyD8pDEOi.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/mZuGuu62O9ojVqK1NuR3cvSlVL8.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/kwMqIYOC4U9eK4NZnmmyD8pDEOi.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/mZuGuu62O9ojVqK1NuR3cvSlVL8.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>595cb45249810a81ba8e607344cf801e2bb6287e</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/2xexlAAXCdRE9e3EG35oYlMHJq2.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/txASbb3KACAhfVZaJF2yAIGhTUo.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>8bee2bf50aa9f81ec8f681e2c33eb2bacf5ddcba</id2.1>
			<ep2.2>5 AL 7</ep2.2>
			<id2.2>1d3e60b6a115520c351a103bfcb4e83619e533da</id2.2>
			<ep2.3>8 AL 10</ep2.3>
			<id2.3>dd83c5e91be5e51879d9d04409e096e698d780df</id2.3>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ENTREVIAS</title>
		<info>(2022) 2 temporadas. 16 episodios. Tirso Abantos (Jose Coronado) es un ex militar que regenta una ferretería de barrio; solitario y poco dado a mostrar afecto, tras un incidente familiar se ve obligado a encargarse durante un tiempo de su nieta Irene (Nona Sobo), una joven de origen vietnamita, contestataria y rebelde a la que la hija de Tirso es incapaz de controlar. El choque entre abuelo y nieta es total y la convivencia se complica desde el primer momento, no solo por el carácter de ambos, sino por las malas compañías que frecuenta Irene en el barrio y por su decisión de fuga, a cualquier precio, con su adorado novio colombiano. En su intento por enderezarla, Tirso se verá forzado a dejar su rutina y se convertirá en un 'héroe por accidente' que plantará cara a los pandilleros que se están apoderando de las calles, a sabiendas de que este rol solo le traerá problemas.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/flHgD6YyZ6MGY64nAmCtK5pXrBd.jpg</thumb>
		<fanart>https://i.imgur.com/NKJlTRV.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/flHgD6YyZ6MGY64nAmCtK5pXrBd.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/NKJlTRV.jpg</fanart.1>
			<ep1.1>1 UN CABRONAZO DE LA VIEJA ESCUELA</ep1.1>
			<id1.1>bca12d7e5f75c8990e9f24792b7e7f2f045309bc</id1.1>
			<ep1.2>2 RADIOS ESTROPEADAS</ep1.2>
			<id1.2>47027ec5a1d00ede0cc570fecd16b108c72772f8</id1.2>
			<ep1.3>3 MORDER</ep1.3>
			<id1.3>7a5f6498655d6efcf205868429267612cf38a4f5</id1.3>
			<ep1.4>4 LO QUE SE ESCONDE BAJO LA ALFOMBRA</ep1.4>
			<id1.4>63227E2412229FCBA905BB4E5BF208318DC4F54F</id1.4>
			<ep1.5>5 PERRO VIEJO NO PERREA</ep1.5>
			<id1.5>5S3XTNG5G6RFKXUUK2SOJ5LUGBKBXKL7</id1.5>
			<ep1.6>6 QUIEN ROBA A UN LADRON</ep1.6>
			<id1.6>aadc37e214862523dbf70d7e1aac031d2c87af4c</id1.6>
			<ep1.7>7 LOS ROBIN HOOD</ep1.7>
			<id1.7>e86713eae3550a21b661a666f4334ee660701b5f</id1.7>
			<ep1.8>8 EL ULTIMO TREN</ep1.8>
			<id1.8>d73d396d2cd370a239a6649b274b3b32567ae409</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/flHgD6YyZ6MGY64nAmCtK5pXrBd.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/i1e8mmRruXUIekpEZE5LIbw827g.jpg</fanart.2>
			<ep2.1>1 COLGAR LA CARCASA</ep2.1>
			<id2.1>f656fd3fbff5ef309442a8fc051ba5ad7f0a03c0</id2.1>
			<ep2.2>2 UN VAGON OLVIDADO</ep2.2>
			<id2.2>429334b7ae404c824098ff92dba31431d65b014e</id2.2>
			<ep2.3>3 ESCUCHA AL CORAZON</ep2.3>
			<id2.3>c07e1a0544c98b0f05fe3e2f77ef25e409188e4e</id2.3>
			<ep2.4>4 SALTO AL VACIO</ep2.4>
			<id2.4>14ece8c5ee3327a15dfae2c5bd5d30f3f1cbe646</id2.4>
			<ep2.5>5 LA CRUDA REALIDAD</ep2.5>
			<id2.5>6cda0b8e02304129155b513383fa9fb68ef5b20f</id2.5>
			<ep2.6>6 TODO POR AMOR</ep2.6>
			<id2.6>3984d7ac9f5170dd0cfdb37febc16e5fdc056b63</id2.6>
			<ep2.7>7 MIRADAS Y SENSACIONES</ep2.7>
			<id2.7>425136b5ad3fab2cab218d24e43eacd20bf4453d</id2.7>
			<ep2.8>8 LA FUERZA DEL CAZADOR</ep2.8>
			<id2.8>df2c05d5011785a76ac00baa171912b644f2dcca</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ESO QUE TU ME DAS</title>
		<info>(2020) 1 episodio. En 2015, al cantante Pau Donés, de 'Jarabe de Palo', le diagnostican un cáncer con el que convivió durante 5 años. Veinte días antes de morir llamó a su amigo Jordi Évole desde el hospital, y le dijo: "Me quedan muy poquitos días de vida y quiero pasarlos en mi casa del Valle de Arán. Me gustaría que subieses, pudiésemos tener una charla, que la grabes y hagas con ella lo que quieras". 'Eso que tú me das' es el resultado de esa charla.</info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/9ce7nx1un8VoE7kJqFDfezubUw3.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8rIgGjMGCKJuul6laCaDCaKVSWK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/9ce7nx1un8VoE7kJqFDfezubUw3.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8rIgGjMGCKJuul6laCaDCaKVSWK.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>5c70ec47dbd20c5572dc3b46a37bcb3e8c83bc99</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ESTAMOS MUERTOS</title>
		<info>(2022) 12 episodios. Un virus que convierte a los infectados en zombis se propaga rápidamente dentro de un instituto. Los estudiantes luchan por escapar y sobrevivir.</info>
		<year>2022</year>
		<genre>Fantástico. Terror. Acción </genre>
		<thumb>https://www.themoviedb.org/t/p/original/8jUlLqqPVkO7tRtZsGYjWteJJeR.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8YHT2XiqMpAJ2PUwPOybyWJ6Bfd.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8jUlLqqPVkO7tRtZsGYjWteJJeR.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8YHT2XiqMpAJ2PUwPOybyWJ6Bfd.jpg</fanart.1>
			<ep1.1>1 AL 12 (1080)</ep1.1>
			<id1.1>7783F14D90041DD2920122A6B22D35B4A27DE57F</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ESTO TE VA A DOLER</title>
		<info>(2022) 7 episodios. Ambientada en una sala de partos con toda su hilaridad y altibajos que levantan el corazón, la serie ofrece una descripción brutalmente honesta de la vida como médico junior en las salas, y el costo que el trabajo puede llevarse a casa.</info>
		<year>2022</year>
		<genre>Comedia. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/mzdRGzIEPdeGo2OunpJ0agedmso.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/51IQXIFZARj7pZcaP36yfpQbFYj.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/mzdRGzIEPdeGo2OunpJ0agedmso.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/51IQXIFZARj7pZcaP36yfpQbFYj.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>e96891a7e4cb07ce0627a97edaa3d8cf11e8ea28</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>ea7c9747e47e46078cd67b2e0c154f3524495516</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>4fc4fab486913b0ca772f7d6d2765e7978ba64d9</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>c88bd18941b7536d14b43947eb60ae03036b0fd4</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>90bb475df321da8712ffceb6bc064350fa66a59c</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>90bb475df321da8712ffceb6bc064350fa66a59c</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>2df8d4b76d959c82d1488bde4ef60bfd80fb7de9</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EUPHORIA</title>
		<info>(2019) 2 temporadas. 11 episodios. Rue (Zendaya) es una joven de 17 anos que vuelve de rehabilitacion sin intencion de mantenerse sobria. En una fiesta antes del comienzo del curso conoce a Jules (Hunter Schafer), una chica recien llegada a la ciudad.... Euphoria es una reflexion sobre la adolescencia a traves de un grupo de estudiantes de instituto que tienen que hacer frente a temas recurrentes de su edad como las drogas, el sexo, la violencia, los problemas de identidad, los traumas, las redes sociales, el amor y la amistad.</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/2JNIxUGwqe82IbHSyH8dYsS35pu.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/v3EDPO2u8BS74sEh8XAKXxZqgTF.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/2JNIxUGwqe82IbHSyH8dYsS35pu.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/v3EDPO2u8BS74sEh8XAKXxZqgTF.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>873524275EBA3560AB402440BD1382B1ABAEDA18</id1.1>
			<ep1.2>2 FARDANDO</ep1.2>
			<id1.2>2CE472768BD13B2874405C96A3C5B692DA1B2C34</id1.2>
			<ep1.3>3 HAS MIRADO</ep1.3>
			<id1.3>F614BD6735545C86DC234BB03038B35AF8DDD1F0</id1.3>
			<ep1.4>4 LOS QUE VAN DE DUROS</ep1.4>
			<id1.4>15C1C9A2F4A3996CBD1ABA106AA72551D8291724</id1.4>
			<ep1.5>5 BONNIE Y CLYDE</ep1.5>
			<id1.5>8BC996A0CBD04E7E9C22F3759D4496D9776E6966</id1.5>
			<ep1.6>6 EL SIGUIENTE EPISODIO</ep1.6>
			<id1.6>211FC97C6E07E1CB4401D7201C47D334809873B7</id1.6>
			<ep1.7>7 LAS VICISITUDES DE INTENTAR MEAR DEPRIMIDA</ep1.7>
			<id1.7>5D457496BDC9EDF3BD45CF9793A822212E09AE99</id1.7>
			<ep1.8>8 ECHAR SAL A TU PASO</ep1.8>
			<id1.8>ff5036e12e2075427328926b8dc911cd54203ecb</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/dBwKEcsmhfyztspUwlRWkUNaOP3.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/oKt4J3TFjWirVwBqoHyIvv5IImd.jpg</fanart.2>
			<ep2.1>1 INTENTANDO LLEGAR AL CIELO ANTES DE QUE CIERREN LA PUERTA</ep2.1>
			<id2.1>5ce7c653f3162e1abfa8390814dc13d465cdfd12</id2.1>
			<ep2.2>2 FUERA DE CONTACTO</ep2.2>
			<id2.2>f675f18a264079a53f8dd57749d3a73ee44529bf</id2.2>
			<ep2.3>3 REFLEXIONES: GRANDES Y PEQUEÑOS MATONES</ep2.3>
			<id2.3>fc57b94ca41d657be989d19a55f59ae050e8134c</id2.3>
			<ep2.4>4 TU QUE NO PUEDES VER PIENSA EN AQUELLOS QUE PUEDEN</ep2.4>
			<id2.4>f5a5505756ae88c765d38f7594e2005df9bc6016</id2.4>
			<ep2.5>5 QUEDATE QUIETO COMO EL COLIBRI</ep2.5>
			<id2.5>7b6fa60f66f8f92e8ef8b3bb6405c1d586ed4df5</id2.5>
			<ep2.6>6 MIL ARBOLITOS DE SANGRE</ep2.6>
			<id2.6>1416cba2f9a16d0d453804eb6f8bd9ab0bbc2b62</id2.6>
			<ep2.7>7 EL TEATRO Y SU DOBLE</ep2.7>
			<id2.7>80baca4fcef934ec9288b0ef16ede584a4c1711a</id2.7>
			<ep2.8>8 TODA MI VIDA MI CORAZON HA ANHELADO UNA COSA QUE NO PUEDO NOMBRAR</ep2.8>
			<id2.8>31c548f7d90865f5e763db4d9d03b874a4b9e9eb</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>EXPRES</title>
		<info>(2022) 8 episodios. “Express” aborda la historia de una psicóloga criminal, Bárbara (Maggie Civantos), y su familia, tras convertirse en víctima de un secuestro express: una aterradora modalidad de extorsión que se está propagando por todo el mundo y, en la mayoría de los casos, termina con un violento asesinato. Tras su secuestro, Bárbara trabaja como negociadora en casos similares al suyo y su principal objetivo es comprender por qué ella se convirtió en la víctima y descubrir a las personas que resquebrajaron su vida y su familia.</info>
		<year>2022</year>
		<genre>Triller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/3rrPCr2Z4jbkZ6tvyeBEOFqVRFr.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7rtEiNst029mPm1B05cjflEyGUH.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/3rrPCr2Z4jbkZ6tvyeBEOFqVRFr.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7rtEiNst029mPm1B05cjflEyGUH.jpg</fanart.1>
			<ep1.1>1 EL NEGOCIO DEL MIEDO</ep1.1>
			<id1.1>e19e078024b8fe6ada2b1c08e5a698d162daffb4</id1.1>
			<ep1.2>2 LOS PLATILLOS CHINOS</ep1.2>
			<id1.2>e3f35039005b75763e31436b6341cd3ec4f37171</id1.2>
			<ep1.3>3 SIN MANUAL DE INSTRUCCIONES</ep1.3>
			<id1.3>3d871f1bb3da10692ea363fc5ad8c2ad02ae89c0</id1.3>
			<ep1.4>4 CAMBIO DE PLANES</ep1.4>
			<id1.4>bc0f226d98760a4ceea9b32effe466bbaf4e5fee</id1.4>
			<ep1.5>5 EL PAPEL HIGIENICO</ep1.5>
			<id1.5>c132569f56539a71ec54ec8f8a76763eebe12683</id1.5>
			<ep1.6>6 ¿ A QUIEN QUIERES MAS A PAPA O A MAMA?</ep1.6>
			<id1.6>3e6fbde2763e08b6bc4596170080129d9809695f</id1.6>
			<ep1.7>7 MALAS MADRES</ep1.7>
			<id1.7>10e9b53fde3a651f2a4ff4b0eeef6ba9d021bc7d</id1.7>
			<ep1.8>8 PIOJOS</ep1.8>
			<id1.8>60fe15a5bd2ea724d6e014b3defb50c9f12b8a86</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>FALCON Y EL SOLDADO DE INVIERNO</title>
		<info>(2021) 6 episodios. Se centra en las aventuras de dos personajes del MCU (Universo Cinematográfico de Marvel): Falcon y Soldado de Invierno. Seis meses después de recibir el manto del Capitán América, Sam Wilson se une a Bucky Barnes en una aventura que pondrá a prueba sus habilidades, destrezas y paciencia.</info>
		<year>2021</year>
		<genre>Acción. Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/uVJbiVuxNCjE9BVjj0yKsbNZ9Dt.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/JB17sIsU53NuWVUecOwrCA0CUp.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/uVJbiVuxNCjE9BVjj0yKsbNZ9Dt.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/JB17sIsU53NuWVUecOwrCA0CUp.jpg</fanart.1>
			<ep1.1>1 AL 6 (4K)</ep1.1>
			<id1.1>b1337c3ab5917066577f98fd2336839597049598</id1.1>
			<ep1.2>1 AL 6 (1080)</ep1.2>
			<id1.2>d291275b15b2fe0a4d62c546ccd2fa517eb3ebc2</id1.2>
			<ep1.3>1 NUEVO ORDEN MUNDIAL</ep1.3>
			<id1.3>2972e24a4ca795c06dcd474e99aa20add670f7e4</id1.3>
			<ep1.4>2 EL HOMBRE ESTRELLADO</ep1.4>
			<id1.4>09496c691e3303e1947e90cffe1b9995265bd239</id1.4>
			<ep1.5>3 TRAFICO DE INFLUENCIAS</ep1.5>
			<id1.5>2b059c3f7eae2db1de2aca1311125985d50e87d5</id1.5>
			<ep1.6>4 EL MUNDO NOS OBSERVA</ep1.6>
			<id1.6>4af5c9399529574973728bf7af574dcc3f1c224d</id1.6>
			<ep1.7>5 LA VERDAD</ep1.7>
			<id1.7>f1c260c9362cfedf87c4ef56b92d1ea048ba214e</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>FARIÑA</title>
		<info>(2018) 10 episodios. Años 80. Galicia. La reconversión de la pesca ha dejado a una parte de la flota gallega en tierra y cientos de armadores endeudados. No hay trabajo. En los muchos pueblos de pescadores que recorren la costa gallega, la falta de ingresos y los pagos cotidianos estrangula las economías familiares. Es el caldo de cultivo perfecto para que se produzca la gran transformación: los antiguos traficantes de tabaco dan el salto a algo más grande, más lucrativo, pero mucho más peligroso: las drogas.</info>
		<year>2018</year>
		<genre>Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/rYHpGglzDjGlRtsqb2O52ktfnmW.jpg</thumb>
		<fanart>https://imgur.com/aK0Ld7J.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/rYHpGglzDjGlRtsqb2O52ktfnmW.jpg</thumb.1>
			<fanart.1>https://imgur.com/aK0Ld7J.jpg</fanart.1>
			<ep1.1>1 1981</ep1.1>
			<id1.1>23d9de5d6889f015270008d97e1eae918033ab89</id1.1>
			<ep1.2>2 1982</ep1.2>
			<id1.2>d9f0f9918ffc8936983008942baf57808c18d7d8</id1.2>
			<ep1.3>3 1983</ep1.3>
			<id1.3>4fccc4bb6adfb3d143fb3fbc041ba3957c959c24</id1.3>
			<ep1.4>4 1984</ep1.4>
			<id1.4>2e50c962876b7746e3e8f58a160d54105a705a23</id1.4>
			<ep1.5>5 1985</ep1.5>
			<id1.5>cdc1baacf99e5b35e5a186310207aa95c1ebefd2</id1.5>
			<ep1.6>6 1986</ep1.6>
			<id1.6>27b81bc46571cb1c574d536d02d9e0ed7e5cca74</id1.6>
			<ep1.7>7 1987</ep1.7>
			<id1.7>807ad1b04301b4f6a9a1772b90f089c41b0ae60f</id1.7>
			<ep1.8>8 1988</ep1.8>
			<id1.8>c7fbaddf5d54c5636433bed39a39427fcfa1640a</id1.8>
			<ep1.9>9 1989</ep1.9>
			<id1.9>d1735ab6fed06d3b82b238dd94b836eca9d340b9</id1.9>
			<ep1.10>10 1990</ep1.10>
			<id1.10>b6d66f2f8054df402136dbd69558c3e827d550af</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>FERIA: LA LUZ MAS OSCURA</title>
		<info>(2022) 8 episodios. ¿Y si dos hermanas adolescentes descubren que sus padres son unos monstruos? Eva (Ana Tomeno) y Sofía (Carla Campra) tendrán que enfrentarse al horrible crimen que parece que han cometido sus padres quienes, antes de desaparecer, han dejado atrás 23 víctimas. ¿Cómo van a sobrevivir estas hermanas en un pueblo que las odia? ¿Es posible que conocieran tan poco a sus padres o existe algún motivo para que estuvieran implicados en este suceso? Este es el viaje que emprenderán Eva y Sofía en Feria, un pueblo donde la realidad esconde un universo fantástico.</info>
		<year>2022</year>
		<genre>Fantástico. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/m8mxgGK8z9t8pkCCpatP4XjDpLM.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nWLC2KYYzlpVmKEotZcU9za5Vc.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/m8mxgGK8z9t8pkCCpatP4XjDpLM.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nWLC2KYYzlpVmKEotZcU9za5Vc.jpg</fanart.1>
			<ep1.1>1 al 8 ( 1080)</ep1.1>
			<id1.1>63FB6A8A521FBC2F268E38D7E651E64CAAECE0F1</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>FERNANDO</title>
		<info>(2020) 5 episodios. .Docuserie que muestra un retrato íntimo del bicampeón mundial de la Fórmula 1, el español Fernando Alonso. Consta de 5 episodios y muestra la pasión de Alonso por competir al más alto nivel y su absoluta determinación por ganar. Documentando el pasado año, desde su participación en los circuitos más importantes como las 500 millas de Indianápolis, las 24 Horas de Le Mans para terminar en su primera salida en el Rally de Dakar en enero, Fernando da acceso a los seguidores del campeón de la Fórmula 1. En la docuserie también participa el círculo más cercano de Alonso, incluyendo a su manager Luis García Abad, su hermana Lorena Alonso, su pareja Linda Morselli y su amigo Carlos Sainz, quienes ayudan a desvelar al hombre detrás del campeón.</info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/yrBJiaGFQQ59X6Hvq6f7gxR8BbQ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/3eYy07cFKxzYHZ3FWRgzXZYKJWH.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/yrBJiaGFQQ59X6Hvq6f7gxR8BbQ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/3eYy07cFKxzYHZ3FWRgzXZYKJWH.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>53a897619fb5d0726e0d189cc64ef54618b5a984</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>FRAGGLE ROCK: LA DIVERSION CONTINUA</title>
		<info>(2022) 13 episodios. ¡Los musicales Fraggles de Jim Henson, amantes de la diversión, han vuelto! Acompaña a Gobo, Red, Wembley, Boober y nuevos amigos Fraggles en aventuras épicas y divertidas sobre la magia que ocurre cuando celebramos y nos preocupamos por nuestro mundo interconectado.</info>
		<year>2022</year>
		<genre>Animacion. Infantil</genre>
		<thumb>https://www.themoviedb.org/t/p/original/uMtPzxmZAumXC3e9vYpNLKMK6Je.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/d4Nb1ZUI4A5cRZKvuio75pBv5Cx.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/uMtPzxmZAumXC3e9vYpNLKMK6Je.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/d4Nb1ZUI4A5cRZKvuio75pBv5Cx.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>6fa119bcf7e4a35e632c0ff1a5ecd44055579616</id1.1>
			<ep1.2>5 AL 8</ep1.2>
			<id1.2>a6ef878d82484b8b22f82fab18f83c965d4436ca</id1.2>
			<ep1.3>9 AL 13</ep1.3>
			<id1.3>d81f8f6aa843c57191a54bf7e31182512829bade</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>FRIENDS: THE REUNION</title>
		<info>(2021) 1 episodio. El elenco de la mítica serie "Friends" -que duró 10 temporadas (1994-2004)- se reúne para un especial en la nueva plataforma HBO Max en la que recuerdan numerosas anécdotas del rodaje, reciben a celebridades y a algunos invitados que pasaron por la serie, y reflexionan sobre la importancia para ellos del show y las consecuencias de su enorme fama sobre sus vidas.</info>
		<year>2021</year>
		<genre>Documental. Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bT3c4TSOP8vBmMoXZRDPTII6eDa.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/hP7dN2B5ztQgSIN5Qvk63MY4EeO.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bT3c4TSOP8vBmMoXZRDPTII6eDa.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/hP7dN2B5ztQgSIN5Qvk63MY4EeO.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>a25659cfef9fca17e70e31b7f436e7d11d67ada4</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>G.E.O. MAS ALLA DEL LIMITE</title>
		<info>(2021)8 episodios. G.E.O. Más allá del límite es la primera serie documental que mostrará desde dentro los entresijos de uno de los cuerpos policiales de élite más prestigiosos del mundo: el Grupo Especial de Operaciones o G.E.O. Por primera vez en su historia, un equipo de cámaras tiene acceso exclusivo al duro y exigente proceso de selección de los nuevos miembros, que se extiende durante más de 7 meses. Cientos de aspirantes comienzan y solo unos pocos terminarán pasando las pruebas necesarias para convertirse en miembros del G.E.O. En cada capítulo se seguirán los progresos de este grupo de aspirantes profundizando en su historia personal, sus reacciones ante las duras pruebas y su transformación a lo largo de un proceso que pondrá al límite sus capacidades físicas y mentales. Sometidos a un estrés extremo, privados de sueño y expuestos a condiciones inaguantables, los aspirantes tendrán que superar sus peores miedos para estar preparados para lo que pueda estar por venir.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/8Eg65uxLVsM1WXKdylhYs3QOgsc.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/icNwYupZNnCCBJTt4Zs6eBFMVYp.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8Eg65uxLVsM1WXKdylhYs3QOgsc.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/icNwYupZNnCCBJTt4Zs6eBFMVYp.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>b770e070ace19bc2f9c4f9988875da29ee4d87f0</id1.1>
			<ep1.2>5 AL 8</ep1.2>
			<id1.2>1780496c251dcb708069a44b6848487d100e3a95</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>GAMBITO DE DAMA</title>
		<info>(2020) 7 episodios. Kentucky, años 60. En plena Guerra Fría, la joven Beth Harmon (Anya Taylor-Joy) es una huérfana con una aptitud prodigiosa para el ajedrez, que lucha contra sus adicciones mientras trata de convertirse en la mejor jugadora del mundo ganando a los grandes maestros, en especial a los rusos.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/zU0htwkhNvBQdVSIKB9s6hgVeFK.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/6IE1VKni3iKusK4Qp0vCT2CYpF3.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/zU0htwkhNvBQdVSIKB9s6hgVeFK.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/6IE1VKni3iKusK4Qp0vCT2CYpF3.jpg</fanart.1>
			<ep1.1>1 AL 7</ep1.1>
			<id1.1>7cc73ae65e562529c569b21d05acd7854f4fda2f</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>GANGS OF LONDON</title>
		<info>(2020) 9 episodios. Durante 20 años, Finn Wallace (Colm Meaney) fue el criminal más poderoso de Londres. Miles de millones de libras fluyeron a través de su organización cada año. Pero ahora está muerto y nadie sabe quién ordenó su asesinato. el impulsivo Sean Wallace (Joe Cole), con la ayuda de la familia Dumani encabezada por Ed Dumani (Lucian Msamati), toma el lugar de su padre. Si la situación no era ya suficientemente peligrosa, la toma de poder de Sean repercute en el mundo del crimen internacional. Tal vez el único hombre que podría ayudarlo y ser su aliado es Elliot Finch (Sope Dirsù), quien hasta la fecha ha sido un simple perdedor, un esbirro de la organización criminal con un misterioso interés en la familia Wallace. En este contexto turbulento, Elliot se verá transportado al interior de la mayor organización criminal de Londres.</info>
		<year>2020</year>
		<genre>Thriller. Acción</genre>
		<thumb>https://image.tmdb.org/t/p/original/yL9OsZvVimjleQJXOlFqVVa9Zpj.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/q0yqtYo3QqZd04vwcKzxmje5a0o.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/yL9OsZvVimjleQJXOlFqVVa9Zpj.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/q0yqtYo3QqZd04vwcKzxmje5a0o.jpg</fanart.1>
			<ep1.1>1 AL 9</ep1.1>
			<id1.1>F15641B456D4F01932BEEA21EFA2A239D2B3E84F</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>GASLIT</title>
		<info>(2022) 8 episodios. Martha Mitchell (Julia Roberts) es una celebridad en Arkansas y esposa del leal Fiscal General del presidente Richard Nixon, John Mitchell (Sean Penn). A pesar de su afiliación política, es la primera persona que hace sonar públicamente la alarma sobre la implicación de Nixon en el Watergate, provocando que tanto la Presidencia como su vida personal se vayan al traste. Como Fiscal General, Mitchell es el asesor de más confianza de Nixon y su mejor amigo. Temperamental, malhablado e implacable, pero desesperadamente enamorado de su famosa y sincera esposa, se verá obligado a elegir entre Martha y el Presidente.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/6B1EgOGOR5VPR1UK4ERlot1H5yM.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/iKP3QEN2yK6bs8zURf17rN4UE8g.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/6B1EgOGOR5VPR1UK4ERlot1H5yM.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/iKP3QEN2yK6bs8zURf17rN4UE8g.jpg</fanart.1>
			<ep1.1>1 VOLUNTAD</ep1.1>
			<id1.1>5f9b4c02860125f895230e18129897c3b7b3f03f</id1.1>
			<ep1.2>2 CALIFORNIA</ep1.2>
			<id1.2>01752356fa81b3312bbf88ed29556f5eb2bee446</id1.2>
			<ep1.3>3 REY JORGE</ep1.3>
			<id1.3>9f42a18405d9805ece9fef95160f652a68b35376</id1.3>
			<ep1.4>4 EL MAL EN SI MISMO</ep1.4>
			<id1.4>22459849e185a3ed7033f2a4cadf127f7c73657a</id1.4>
			<ep1.5>5 LUNA DE MIEL</ep1.5>
			<id1.5>8634e0f11eae858fd4fab468a1483d19b190e973</id1.5>
			<ep1.6>6 TUFFY</ep1.6>
			<id1.6>e8c065b54393c3d6076eaa2750cc66313f4e05d2</id1.6>
			<ep1.7>7 AÑO DE LA RATA</ep1.7>
			<id1.7>a52ce01a8eb78dcaef2d4f47bf41981bb2df6176</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>GENTLEMAN JACK</title>
		<info>(2019) 2 temporadas. 16 episodios. Anne Lister regresa a Halifax, West Yorkshire en 1832, decidida a transformar el destino de su descolorida casa ancestral Shibden Hall</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/ufyelFLvdnmilSJDkttsYQo8EdL.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/s9DGCAUeLc8Tiih571TZiCffn0U.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/ufyelFLvdnmilSJDkttsYQo8EdL.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/s9DGCAUeLc8Tiih571TZiCffn0U.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>29ebb5c05b7aca78d9d76335707a555023515612</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>7b6092c1c63f8aa9feb2b594ff1a1290d168d21c</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>a03ffc7eda116b29eaa5eb304c215bb0e3af5a06</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>7b44c49b29b262fc7187825c38a1ea4794b567c4</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>4b765849a3fe14315fbdd2aa755fddb194a3e80a</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>56a6b3a1b2367d7fa9b34f9c1c2db72b1d6235de</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>b6c5ce65cf1f57efe318246d697ab6d26179527d</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>90487b358f6c197d48c561d3a06b8ea45bc50a70</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/7JwwtfQs1bUfJDPTswXeB7wbHQq.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/iimwbuwIC37rgjhsq29A2Xediva.jpg</fanart.2>
			<ep2.1>1 Episodio</ep2.1>
			<id2.1>dffaa554c25de7722dde1c00e703af952c2ca9f1</id2.1>
			<ep2.2>2 Episodio</ep2.2>
			<id2.2>d4bef1a99715ae437484d6a710901aeb84ef991c</id2.2>
			<ep2.3>3 Episodio</ep2.3>
			<id2.3>4d6fa6f8ef92d590aee9adf2786d6e022025a90c</id2.3>
			<ep2.4>4 EPISODIO</ep2.4>
			<id2.4>b131c52423619e51867f65182c31e74bdc9d90b1</id2.4>
			<ep2.5>5 Episodio</ep2.5>
			<id2.5>21346c43f30ca265fe9b15882081be5f68f0f200</id2.5>
			<ep2.6>6 Episodio</ep2.6>
			<id2.6>d88af20eebe7a145af658b6db8b281f578438bed</id2.6>
			<ep2.7>7 Episodio</ep2.7>
			<id2.7>b3968cbc7b4ca51b5e69705dd9b695c2991e49d2</id2.7>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>GIGANTES</title>
		<info>(2018-2019) 2 temporadas. 12 episodios. Serie que narra la historia de los Guerrero, una familia que ha encontrado en el negocio de la compraventa de muebles en el rastro de Madrid la tapadera perfecta para el desarrollo de sus actividades criminales. Una policía que lleva años siguiendo a este clan intentará detener su poder, que se extiende por Andalucía y por la alta sociedad europea.</info>
		<year>2018</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/6hvENuXTDbLHB3vBUXOTPdkLEOr.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/yZRE0pPRTuhTDvGwrRVxlMspBEC.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/6hvENuXTDbLHB3vBUXOTPdkLEOr.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/2GPug1S5AZ12uSPbvI0ETtgL9i6.jpg</fanart.1>
			<ep1.1>1 DEVASTACION</ep1.1>
			<id1.1>8a1648faceaf725337c95aa3adc278d94b6cecd9</id1.1>
			<ep1.2>2 AL 6</ep1.2>
			<id1.2>f5c79ed3405067eafe45d0e68a5e67d560bb50b0</id1.2>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/jLS59NognVFbvmANY48ZnZnMcPk.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/2liD5toyr4Jz39FTHYgAGHZFLX8.jpg</fanart.2>
			<ep2.1>1 AL 6</ep2.1>
			<id2.1>b3ca3accd2e190851ce3d4e23670774dc07c947e</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>GOOD OMENS</title>
		<info>(2019) 6 episodios. El mundo está al borde del apocalipsis mientras la humanidad se prepara para su juicio final. Pero Azirafel, un ángel algo quisquilloso, y Crowley, un demonio, no están entusiasmados con el fin del mundo, y no pueden encontrar al Anticristo.</info>
		<year>2019</year>
		<genre>Ciencia ficción. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/hzEzugrfagYisxQXemGEuI4zcQA.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/6dLDmowSDMSnkNCYqFocdhM6hJN.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/hzEzugrfagYisxQXemGEuI4zcQA.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/6dLDmowSDMSnkNCYqFocdhM6hJN.jpg</fanart.1>
			<ep1.1>1 EN EL PRINCIPIO</ep1.1>
			<id1.1>7dba2c17ad5fd3b7279fd1285a728445afe493b2</id1.1>
			<ep1.2>2 EL LIBRO</ep1.2>
			<id1.2>43da5d98e91d4325241b49431d89213adc57a5da</id1.2>
			<ep1.3>3 TIEMPOS DIFICILES</ep1.3>
			<id1.3>db7b0e32a560edf157b03b35b2a4e84cbca37ddd</id1.3>
			<ep1.4>4 SABADO POR LA MAÑANA TIEMPO DE DIVERSION</ep1.4>
			<id1.4>c5ae6eff81018da3ca96e12e6ed762da7051e791</id1.4>
			<ep1.5>5 LA OPCION DEL JUICIO DEL DIA FINAL</ep1.5>
			<id1.5>3d88d77fbfc69e16f3a57bbc362e527659b5c30b</id1.5>
			<ep1.6>6 EL ULTIMO DIA DEL RESTO DE SUS VIDAS</ep1.6>
			<id1.6>7374d00f89a4d33447eea4d79663b7fddbccf2e1</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HACHE</title>
		<info>(2019) 2 temporadas. 14 episodios. Inspirada en hechos reales, Hache narra la historia de una mujer, Helena (Adriana Ugarte) catapultada a la jefatura del tráfico de heroína en la Barcelona de los años 60</info>
		<year>2021</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/yScOKZTBlug31LF89M3Dccymw5b.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/kGGsdnUzbPNQPxiQqzV3tOQkM6U.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/yScOKZTBlug31LF89M3Dccymw5b.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/kGGsdnUzbPNQPxiQqzV3tOQkM6U.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>e0d0ee775f8c5a54a828dfc7fa6ebb2a01eff2a7</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/rRz5mV8bad6ll6zDgr1XtRDDTEo.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/bCHQOLNKDytA8PQS3nr3kh4j8HW.jpg</fanart.2>
			<ep2.1>1 AL 6</ep2.1>
			<id2.1>7934773bf8a49efcddf731b04a73be24936a4846</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HALSTON</title>
		<info>(2021) 5 episodios. El diseñador de moda Halston combinaba talento, notoriedad y una elegancia única, una combinación perfecta para convertirse en un personaje legendario. Desde sus humildes orígenes en la ciudad de Des Moines (Iowa), sus primeros pasos en el mundo de la fama se produjeron gracias a la popularidad que alcanzó su famoso sombrero rosa en exclusiva para Jackie Onassis. Tanto si se dedicaba a crear pantalones muy sugerentes o ropa cómoda para mujeres; a pasear con Liza Minelli o a viajar por el mundo con sus modelos chinas, Halston se las ingenió para crear un auténtico imperio. Su reputación se asocia a algunos de los momentos más icónicos de la moda estadounidense de los años 70 y 80. Este documental narra la historia de un hombre complejo, tanto a nivel personal como empresarial, desde el punto de vista de las personas que mejor le conocieron.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/uBRpPFcYAYLM7V3x6x7bP3Ucumj.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nnjpriaXnqkIbuikWDvUsMEAyfv.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/uBRpPFcYAYLM7V3x6x7bP3Ucumj.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nnjpriaXnqkIbuikWDvUsMEAyfv.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>dbc49fb9a713f5d592804898450cf055bdd42968</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HARRY PALMER: EL EXPEDIENTE IPCRESS</title>
		<info>(2022) 6 episodios. Durante la Guerra Fría, un ex contrabandista convertido en espía, Harry Palmer, recibe una peligrosa misión encubierta para encontrar a un científico nuclear británico. El joven Harry Palmer un sargento del ejército británico arrestado por trapichear en el mercado negro en Berlín, recibe la oferta de un caballero del servicio de inteligencia inglés: si trabaja en su unidad, se librará de ir a prisión. Harry, superviviente y adaptable, acepta convertirse en espía y se embarca en su primera misión.</info>
		<year>2022</year>
		<genre>Thriller. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/8C62nphwdK8bJtxEivePOS8unsq.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/vPijTvOqgU3J703F5CX8BsaP9PS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8C62nphwdK8bJtxEivePOS8unsq.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/vPijTvOqgU3J703F5CX8BsaP9PS.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>58f647814e3f8eae5885171b84240097abd31121</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>d291fa28edbdd95370876438e8634ee6e8b43f31</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>838f3d38a75272916cc0d400bd2bc839492de02a</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>ac25d5999a2843977032219d9471b1430ea56d11</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HEARTSTOPPER</title>
		<info>(2022) 8 episodios. Dos adolescentes británicos, Nick Nelson y Charlie Spring, estudian en una escuela primaria exclusiva para varones. Charlie, un pensador demasiado nervioso y abiertamente gay, y Nick, un jugador de rugby alegre y de buen corazón, un día se sientan juntos en clase.</info>
		<year>2022</year>
		<genre>Romance. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/wJJt1HG62h3WoGnLcRIbO2nNNkg.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/p3cAHHwyTJJoQlCtgYwb31t6rOq.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/wJJt1HG62h3WoGnLcRIbO2nNNkg.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/p3cAHHwyTJJoQlCtgYwb31t6rOq.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>4ab69715ed954e1da129ceaac3c4415be6f6a62c</id1.1>
			<ep1.2>4 SECRETO</ep1.2>
			<id1.2>3d3d4645835344de0bbbdc2c8362e4404781532d</id1.2>
			<ep1.3>5 AMIGO</ep1.3>
			<id1.3>a14159317e3da238231f69482decf1d7b5c21c77</id1.3>
			<ep1.4>6 CHICAS</ep1.4>
			<id1.4>52c01df202c79b6f92dd7d12c322c02bf4879086</id1.4>
			<ep1.5>7 MATON</ep1.5>
			<id1.5>84689b758cfbb7b444eb5fd11427799a6a293f28</id1.5>
			<ep1.6>8 NOVIO</ep1.6>
			<id1.6>66383b59e8982a2d3a68c75a5d814d40b76344ce</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HERIDAS</title>
		<info>(2022) 13 episodios. Manuela (Ugarte) es una joven que vive sola en una cabaña donde estudia los humedales andaluces. A los 28 años, su vida gira en torno a contemplar las aves. A pocos kilómetros vive totalmente desatendida Alba (Cosette Silguero), una niña de siete años cuya madre, Yolanda (María León), trabaja de noche como pole dancer y duerme de día. El día en el que Manuela encuentra a la pequeña Alba, comenzarán un viaje que obligará a Manuela a enfrentarse a los demonios de su pasado y a cuestionarse los pilares de su vida.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/wBDNXO1kv7ztbxWXglU1Slevs64.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/vsUkdw4pyohyRTbZXYITTB5qnRh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/wBDNXO1kv7ztbxWXglU1Slevs64.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/vsUkdw4pyohyRTbZXYITTB5qnRh.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>eb7a85e2fe96ffa4a8cd4b94ba0150f7a3050a8c</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>ba0ff2cfc2a785db320526a773217383cef32bb2</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>6df6a531796111e9bb940f1e461abbdf54b763ec</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>40998aaca403da249821e5489e9ac4117285938f</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>431672dc582752f2f8da9a8e335cfa337cc05766</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>a0656ccfe91ffbd085ca8b3213981a0219124fd8</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>ec79e92b76a5d437f83e93ecc0d05ba4d9117b79</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>2c8930bd7b61b82e37fb55f85ed573e05f69388e</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HERMANOS DE SANGRE</title>
		<info>(2001) 10 episodios. Narra la historia de la Easy Company, un batallón estadounidense del regimiento 506 de paracaidistas, que luchó en Europa durante la II Guerra Mundial. Incluye entrevistas a los supervivientes, recuerdos de los periodistas y cartas de los soldados... Basada en el bestseller "Band Of Brothers", de Stephen E. Ambrose.</info>
		<year>2001</year>
		<genre>Bélico. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/tFdHfzPmBSuLZn8L2L09nhxkOOH.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/bhxhLB74EIvw0ZYyzT1DU0T6Pr7.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/tFdHfzPmBSuLZn8L2L09nhxkOOH.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/bhxhLB74EIvw0ZYyzT1DU0T6Pr7.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>85d86f6fa03d71e2844751bd6ed1972f52033e56</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HEROES: SILENCIO Y ROCK  AND ROLL</title>
		<info>(2021) 1 episodio. Documental que narra la historia de la agrupación musical Héroes del Silencio desde la creación de la banda. Está contada en primpor los propios integrantes del grupo durante sus ensayos, giras, grabaciones y momentos íntimos y cotidianos.era persona por Enrique Bunbury, Juan Valdivia, Pedro Andreu y Joaquín Cardiel, y a través de ingente material videográfico y fotográfico, nunca visto antes, y generado en parte por los propios integrantes del grupo durante sus ensayos, giras, grabaciones y momentos íntimos y cotidianos.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gshYpoEqTh3N2xMSr0TqCGDuruj.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3vDBHplYDwTNqdHTB8M2KNVfGVc.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gshYpoEqTh3N2xMSr0TqCGDuruj.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3vDBHplYDwTNqdHTB8M2KNVfGVc.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>120024eca3dbc3bb5b363b3e1a590694177db080</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HIERRO</title>
		<info>(2019) 2 temporadas. 14 episodios. Candela es una jueza que acaban de destinar a El Hierro, la isla más remota del archipiélago canario. No le es ser fácil adaptarse a la vida en una comunidad que, como ella misma, tiene un fuerte carácter. Nada más llegar a la isla, Candela tiene que instruir un caso complicado: aparece asesinado Fran, un joven herrero, el mismo día en el que iba a casarse con la hija de Díaz, un oscuro empresario que enseguida se convierte en el principal sospechoso del crimen. Candela y Díaz están en lados opuestos de la ley, pero tienen un objetivo común: descubrir la verdad sobre el crimen.</info>
		<year>2019</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/55pQbuTUmWZWwfianUaEewqj8pK.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/vOYCFbDMYNIhh4rFIWwZXLupR8C.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/55pQbuTUmWZWwfianUaEewqj8pK.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/vOYCFbDMYNIhh4rFIWwZXLupR8C.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>c634afcbeae55401996c3029a75e34f994a8545c</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/mPCtR4myjwWtrlEoKDn3eOZ2yDf.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/yZM2r8eE8E0J78CtjycAxPCfcPA.jpg</fanart.2>
			<ep2.1>Episodio 1</ep2.1>
			<id2.1>4af5ca13ae9458da001e7f67e2e46279f3f891ef</id2.1>
			<ep2.2>Episodio 2</ep2.2>
			<id2.2>007f52ce7ea13e479c596267c71647bcd650c474</id2.2>
			<ep2.3>Episodio 3</ep2.3>
			<id2.3>b415383ee55a9564322e41aa8f2b3f3771a2e626</id2.3>
			<ep2.4>Episodio 4</ep2.4>
			<id2.4>715d268b695f4dd4549d3241565b88cc298c7fd6</id2.4>
			<ep2.5>5 Episodio</ep2.5>
			<id2.5>fb71a29b9af1161584fcc5f60693a882ae34f7ad</id2.5>
			<ep2.6>Episodio 6</ep2.6>
			<id2.6>ece98fd4d54b4837e8f1a534cfe9c4ea832d3506</id2.6>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HIJOS DE LA ANARQUIA</title>
		<info>(2008-2014) 7 temporadas. 92 episodios. Serie centrada sobre un club de moteros (MC) que operan ilegalmente en la ciudad ficticia de Charming (California). La historia se centra en el protagonista Jackson "Jax" Teller (Charlie Hunnam), un joven miembro de la organización, con rango de vicepresidente, que comienza a cuestionarse sus propios actos y los de su club.</info>
		<year>2008</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/zhplXNf1qwXyk40fTV4eWqVhuYk.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/n6CJ6zI1Piu4ZsRnXG1E05kLaXT.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/sfZvLJxI4yaV1yGOGbbQO1ESiYd.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/l98eBWY0p5eA47JuvbmEIXNTkJs.jpg</fanart.1>
			<ep1.1>1 AL 13</ep1.1>
			<id1.1>cc7bebcdbbea84ac57b721e53a2090ccb2e413e6</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/k5uYIleZQ9OhhSibN1oW3eXrCaG.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/7iJnJlGrb2djzOqrsrxNw4sHeeB.jpg</fanart.2>
			<ep2.1>1 AL 13</ep2.1>
			<id2.1>f99b32fcd1feeeb4c00e86c210771a6d78ef873e</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/33fRHA8ToARmja6UiIkcINTU4d1.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/qsPPVGpAh7f3lONvYeQzZIcbvyo.jpg</fanart.3>
			<ep3.1>1 AL 13</ep3.1>
			<id3.1>6d0e2a7cf51e8663767f99987061694a1f16c823</id3.1>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/mJezW9kSeFHikeofp9IS6iIEkPQ.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/n6teSMdqSBslpD4JGc5qs7gwmfs.jpg</fanart.4>
			<ep4.1>1 AL 13</ep4.1>
			<id4.1>c91f22e4c475fb955bb59e4e70b40b369ba548e3</id4.1>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/wkE2wajFjeMDAMsRSBGy66KvkAq.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/jJBf9t2eNP9nTNdZzo8sSH6hbHh.jpg</fanart.5>
			<ep5.1>1 VIUDO NEGRO</ep5.1>
			<id5.1>7d829caaf75b57613d71567d73feef44a42251e2</id5.1>
			<ep5.2>2 TOIL AND TILL</ep5.2>
			<id5.2>a58ef3a0bcbb4042e5be6942ba9df912eba750e4</id5.2>
			<ep5.3>3 JUGANDO CON MONSTRUOS</ep5.3>
			<id5.3>ee6db38214901a63af9a1b79ce8ebfb7dc355340</id5.3>
			<ep5.4>4 POBRES CORDEROS PEQUEÑOS</ep5.4>
			<id5.4>71e77cfdc8c0088bc0241bbe40089e444bf530a5</id5.4>
			<ep5.5>5 ALGUNA EXTRAÑA ERUPCION</ep5.5>
			<id5.5>4c125d4c6bbd7620713f63ba0244a1d1ada1bb02</id5.5>
			<ep5.6>6 FUMALO SI LO TIENES</ep5.6>
			<id5.6>6ab191d8bbf724d2b72a41a0f995497fb74ad046</id5.6>
			<ep5.7>7 GREENSLEEVES</ep5.7>
			<id5.7>092385c8d51ea175c8da336e3c7031bcf22e6703</id5.7>
			<ep5.8>8 LA SEPARACION DE LOS CUERVOS</ep5.8>
			<id5.8>625d0e3118d443d0e54ab5c9ca2e8de9dc83a97b</id5.8>
			<ep5.9>9 QUE PEDAZO DE TRABAJO ES UN HOMBRE</ep5.9>
			<id5.9>978748c6b3355674557e01aeca2ed7639cfb6c8d</id5.9>
			<ep5.10>10 FE Y DESESPERANZA</ep5.10>
			<id5.10>0e7dcbaa9ed4124f3aca208a5bfca90ba7da5495</id5.10>
			<ep5.11>11 TRAJES DE AFLICCION</ep5.11>
			<id5.11>597d6ea1c21cb6f5e20e6cfc966a7cc87544a4b5</id5.11>
			<ep5.12>12 ROSA ROJA</ep5.12>
			<id5.12>b96e315fa0770879b9330c016941b0f3ef2deb91</id5.12>
			<ep5.13>13 LAS COSAS DE PAPA</ep5.13>
			<id5.13>38a87cad099db7a8cf9740cac3abf08f7e451240</id5.13>
		</t5>
		<t6>
			<thumb.6>https://image.tmdb.org/t/p/original/aEvq6UBC6vYzrNSYHpeZ05jNNSm.jpg</thumb.6>
			<fanart.6>https://image.tmdb.org/t/p/original/1BqXqrQx42Aeg2ByTAgY0Mbu25N.jpg</fanart.6>
			<ep6.1>1 AL 13</ep6.1>
			<id6.1>6d821ff188685af2de93f3d69eef2353a03c8cc6</id6.1>
		</t6>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HISTORIAS DE PROTEGIDOS</title>
		<info>(2022) 5 episodios. 'Historias de Protegidos' son cinco relatos de ficción cortos que servirán como enlace entre la primera y la segunda temporada de ‘Los Protegidos: El regreso’. Todos volverán a meterse en la piel de sus míticos personajes para protagonizar estos relatos.</info>
		<year>2022</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/5a7fEXiVLkOeCILTHYbQNHHipoQ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/uu5VKcv63OcDQxGRUySlE6zYL40.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/5a7fEXiVLkOeCILTHYbQNHHipoQ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/uu5VKcv63OcDQxGRUySlE6zYL40.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>ee4b153eb73d1af389954d534bda360b02fba62c</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>a2595c75fa7d00c08089184b259aeec5195315aa</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HISTORIAS PARA NO DORMIR</title>
		<info>(2021) 4 episodios. Cuatro historias independientes que rinden tributo al formato clásico de Narciso Ibáñez Serrador, dirigidas y protagonizadas por nombres importantes del cine español. Rodrigo Cortés, Rodrigo Sorogoyen, Paco Plaza y Paula Ortiz dirigen "La broma", "El doble", "Freddy" y "El asfalto" respectivamente.</info>
		<year>2021</year>
		<genre>Terror</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fH5H0SqX7XcSu4rQEa2ZNKrBiAX.jpg</thumb>
		<fanart>https://i.imgur.com/Y0OTpRj.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fH5H0SqX7XcSu4rQEa2ZNKrBiAX.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/Y0OTpRj.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>b8b4f676cb532478e69a9f062a4d8be5df9cfde1</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>0bff78ea3562076d3b0887533468a1c865591006</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>HOME BEFORE DARK</title>
		<info>(2020) 2 temporadas. 20 episodios. Home Before Dark: Las crónicas de Hilde Lisko gira en torno a una niña que se muda desde Brooklyn, Nueva York, a una pequeña ciudad junto al lago que dejó su padre en herencia. Mientras se encuentra allí, su afán por desenterrar un caso sin resolver que todos en la ciudad, incluido su propio padre, intentaron enterrar hará que resuelva un secreto oculto durante muchos años. La serie está inspirada en la historia real de la periodista Hilde Lysiak, quien reveló la historia de un asesinato en su ciudad a los 11 años.</info>
		<year>2020</year>
		<genre>Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/A4uh92nSommxHUkEKoeNH6I53dO.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5RKO0Gza1lFrh9kvDJebi0IaHqr.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/A4uh92nSommxHUkEKoeNH6I53dO.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5RKO0Gza1lFrh9kvDJebi0IaHqr.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>2e80389ea7f5dc230a3cebd48aee899bcab36269</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/mt4P2epJrSaqrlkMP9fTUKLP9OE.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/qXAEcRfvmMtjTA085Av80DGOcv8.jpg</fanart.2>
			<ep2.1>1 NO HAY QUE RENDIRSE</ep2.1>
			<id2.1>e993bc9991bc33f344ac9a02ac17ba36ebced015</id2.1>
			<ep2.2>2 YO TE CREO</ep2.2>
			<id2.2>217e13b0df91501446327a10a81c691dbc1f2b6b</id2.2>
			<ep2.3>3 ENFRENTANDOSE AL FANTASMA</ep2.3>
			<id2.3>ab2c6d7da3774e3e4b63d4e577f0256b31e9e953</id2.3>
			<ep2.4>4 CUARTOS OSCUROS</ep2.4>
			<id2.4>7c33650008656abb26a7ba77fdabc2ad969944bf</id2.4>
			<ep2.5>5 LA CAJA NEGRA</ep2.5>
			<id2.5>041dc54c30caa91ba0a404f6589475f87e39b842</id2.5>
			<ep2.6>6 QUE HAY AHI FUERA</ep2.6>
			<id2.6>dcf8ba61adbadde1b41a4336d93ba529a8fb2815</id2.6>
			<ep2.7>7 SOLO UN PAJARO</ep2.7>
			<id2.7>4705c5d0537de553f28dad53d258d9e37927e5ba</id2.7>
			<ep2.8>8 EL MALO</ep2.8>
			<id2.8>4854192e8889a1b04a02db8bcd86d60134e6acbf</id2.8>
			<ep2.9>9 UNA VIDA IMPORTANTE</ep2.9>
			<id2.9>3c638901c9b9818ccd31ce61564b9214ad18cf65</id2.9>
			<ep2.10>10 LA PRUEBA</ep2.10>
			<id2.10>b4d09adf6e1614349a15a94d6cf2a8a5498e422f</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>INES DEL ALMA MIA</title>
		<info>(2020)  8 episodios. Inés Suárez partió en 1537 para las Indias en busca de su marido y terminó por convertirse en una conquistadora que nunca volvió a España y que encontró allí al amor de su vida, el famoso conquistador español Pedro de Valdivia. Juntos protagonizarán un romance inolvidable mientras se embarcan en una aventura que les convertirá en los principales artífices del nacimiento de una nación. Las hazañas vividas junto a su amado la llevarán hasta el lejano y desconocido Chile.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/q3lUWZSxNo7TWNxHwqHs0PQTkGA.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/q3lUWZSxNo7TWNxHwqHs0PQTkGA.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/q3lUWZSxNo7TWNxHwqHs0PQTkGA.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/q3lUWZSxNo7TWNxHwqHs0PQTkGA.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>04b5d81f525f795a68d037828eb4c4f121863f10</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>INSANIA</title>
		<info>(2021) 8 episodios. Una tragedia personal le provoca a Paula, una joven policía científica, una crisis que la lleva a ingresar en un hospital psiquiátrico. Allí se verá arrastrada hasta los rincones más oscuros y terroríficos de su mente de la mano de un misterioso doctor.</info>
		<year>2021</year>
		<genre>Terror</genre>
		<thumb>https://www.themoviedb.org/t/p/original/hFYCcL7UenTEf8YYqehwRvm1WIO.jpg</thumb>
		<fanart>https://i.imgur.com/0jzMMgc.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/hFYCcL7UenTEf8YYqehwRvm1WIO.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/0jzMMgc.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>56e230319bb708b02d6138b9c1a2cc95ae5845c4</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>a29eda22e1da30a60c3b1a72916209a66b52a438</id1.2>
			<ep1.3>3 AL 4</ep1.3>
			<id1.3>b80d29bccad977c3f27f7d4c44fd3159c86620eb</id1.3>
			<ep1.4>5 AL 6</ep1.4>
			<id1.4>f21c2d80ef6e2fd2c88c72ceedb023b42bb0d1f8</id1.4>
			<ep1.5>7 AL 8</ep1.5>
			<id1.5>6d5bcf9b52c47892cd5835e4cdd729965b6e970e</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>INSTINTO</title>
		<info>(2019) 8 episodios. Marco Mur es un empresario de éxito que arrastra un trauma desde la infancia, lo que le provoca una serie de problemas a la hora de relacionarse con las mujeres. Para satisfacer sus instintos, Marco acude con asiduidad a un club privado de sexo donde los miembros pueden dar rienda suelta a sus fantasías más inconfesables</info>
		<year>2019</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/47aLR966ZqTf1G9RkZOePD2jCPQ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/y4vYdXPqD8NYWewrMq2p8UlltXd.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/47aLR966ZqTf1G9RkZOePD2jCPQ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/y4vYdXPqD8NYWewrMq2p8UlltXd.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>6377c5ef6ce282ca3c4c682fae04630db6539671</id1.1>
			<ep1.2>3 AL 6</ep1.2>
			<id1.2>2472c2d752837c870c0f9c7e2fac52db0377acc1</id1.2>
			<ep1.3>7 AL 8</ep1.3>
			<id1.3>e600edefadb083ce0e06d5f7159642b4816ebd41</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>INTIMIDAD</title>
		<info>(2022) 8 episodios. Un video sexual de una política con futuro prometedor, filtrado a la prensa, es el catalizador de esta historia que narra la vida de cuatro mujeres que se ven forzadas a pisar la delgada línea entre lo que pertenece a la vida pública y privada. ¿Dónde están los límites de nuestra Intimidad? ¿Qué pasa con nuestras vidas cuando nuestra privacidad se convierte en la conversación de todo el mundo?</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/xUJwNJx5f39SgUtOqFIRCvZPeik.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/qL6BW5QMgVoYk8AlnMXPY4bivpW.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/xUJwNJx5f39SgUtOqFIRCvZPeik.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/qL6BW5QMgVoYk8AlnMXPY4bivpW.jpg</fanart.1>
			<ep1.1>1 MEDIO MORTAL</ep1.1>
			<id1.1>ea351ae8646dd2ecd235766f3a596597a58e9f5b</id1.1>
			<ep1.2>2 POSICION / EXPOSICION</ep1.2>
			<id1.2>e6a0e6279aad89cfc0ab87d41aa7a773d2d4a5fc</id1.2>
			<ep1.3>3 PROSOPAGNOSIA</ep1.3>
			<id1.3>706f9f57122ccff3440470962f4e6d0afdfc5cf9</id1.3>
			<ep1.4>4 LA CAIDA</ep1.4>
			<id1.4>37c7f5ce93a7359445c6c1993c218733f955e3aa</id1.4>
			<ep1.5>5 EN FUNCIONES</ep1.5>
			<id1.5>9002662569d88b4b8f0794621e8873949e903d99</id1.5>
			<ep1.6>6 SOLA</ep1.6>
			<id1.6>7b578184e430f187b0fe290c5cfd79d5c84e1f7f</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>JOVENES ALTEZAS</title>
		<info>(2022) 6 episodios. Lejos de las obligaciones reales, un joven príncipe tiene la oportunidad de descubrirse a sí mismo hasta que, de pronto, pasa a ocupar el primer lugar en la línea sucesoria.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/rs4Lzde2CKByONGG9i4b0EOsdhF.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/esMo90QAVfOpSVTUiCuhmDTnc5y.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/rs4Lzde2CKByONGG9i4b0EOsdhF.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/esMo90QAVfOpSVTUiCuhmDTnc5y.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>7e646a38482e6003c8d9aacdde364a5766413e4d</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>41e9222011672df95825737465151c08e227f676</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>JUEGO DE TRONOS</title>
		<info>(2011-2019) 8 temporadas. 73 episodios. La historia se desarrolla en un mundo ficticio de carácter medieval donde hay Siete Reinos. Hay tres líneas argumentales principales: la crónica de la guerra civil dinástica por el control de Poniente entre varias familias nobles que aspiran al Trono de Hierro; la creciente amenaza de "los otros", seres desconocidos que viven al otro lado de un inmenso muro de hielo que protege el Norte de Poniente; y el viaje de Daenerys Targaryen, la hija exiliada del rey que fue asesinado en una guerra civil anterior, y que pretende regresar a Poniente para reclamar sus derechos dinásticos. Tras un largo verano de varios años, el temible invierno se acerca a los Siete Reinos. Lord Eddard 'Ned' Stark, señor de Invernalia, deja sus dominios para ir a la corte de su amigo, el rey Robert Baratheon, en Desembarco del Rey, la capital de los Siete Reinos. Stark se convierte en la Mano del Rey e intenta desentrañar una maraña de intrigas que pondrá en peligro su vida y la de todos los suyos. Mientras tanto, diversas facciones conspiran con un solo objetivo: apoderarse del trono.</info>
		<year>2011</year>
		<genre>Aventuras. Drama. Fantástico. Acción. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/j24NiYZHsuLEdyYqUunlVCapC04.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/suopoADq0k8YZr4dQXcU6pToj6s.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/uAWrtCFIJo6gUweHwuSSqRILaIX.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/5mqTvxH9xanhQcJd0SLMSAVYaZ2.jpg</fanart.1>
			<ep1.1>1 AL 10 (720)</ep1.1>
			<id1.1>40a380ce1ab6741cc7d50a0fb435063a5d7562c8</id1.1>
			<ep1.2>1 AL 10 (1080)</ep1.2>
			<id1.2>57c02e03977b36a72a0f01cf9d4ad30e60b26706</id1.2>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/yZtRt90wNuKXuWWrmAnmDz4IEvL.jpg</thumb.2>
			<fanart.2>https://i.imgur.com/QH0EyTf.jpg</fanart.2>
			<ep2.1>1 AL 10 (720)</ep2.1>
			<id2.1>487b0ba3f89d9f27481901396ce27242b0913fb6</id2.1>
			<ep2.2>1 AL 10 (1080)</ep2.2>
			<id2.2>2542236f7135ca6cf32d910f010f404e8de36696</id2.2>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/9GCtmu571WbJZfBqeVejEdU4QJU.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/AajildpFyqmqqCJiPKGp7yF2IaB.jpg</fanart.3>
			<ep3.1>1 AL 10</ep3.1>
			<id3.1>03fe28fe34e975adc5cb3d477ad1857e53ec736d</id3.1>
			<ep3.2>1 VALAR DOHAERIS</ep3.2>
			<id3.2>ffa7c71de9d2061dac04933eac5baa95aa4dedf6</id3.2>
			<ep3.3>2  ALAS NEGRAS PALABRAS NEGRAS</ep3.3>
			<id3.3>d4b5a13ce6a9ff232f4088ee10acb305d4948cbc</id3.3>
			<ep3.4>3 EL CAMINO DEL CASTIGO</ep3.4>
			<id3.4>939db972353ed6dc9f3b85955969398dabd81dd5</id3.4>
			<ep3.5>4 Y AHORA SU GUARDIA HA TERMINADO</ep3.5>
			<id3.5>606ddaf7e3b17d92504567523accb64a7337ff2b</id3.5>
			<ep3.6>5 BESADA POR EL FUEGO</ep3.6>
			<id3.6>a9e0d969ce08b09c54f741385b076ba12151811d</id3.6>
			<ep3.7>6 EL ASCENSO</ep3.7>
			<id3.7>d2c7e70982a24fb446c17f158d33d6adb5fbba57</id3.7>
			<ep3.8>7 EL OSO Y LA DONCELLA</ep3.8>
			<id3.8>5b53a7855e5edaddc82daf9844aaa2633bab81a3</id3.8>
			<ep3.9>8 LOS SEGUNDOS HIJOS</ep3.9>
			<id3.9>d93081c7a5fe572875e9aa7c6dc7e26d7f7c7f7b</id3.9>
			<ep3.10>9 LAS LLUVIAS DE CASTAMERE</ep3.10>
			<id3.10>F0836217E8195EE22FF9365B504369B5A3D0A81A</id3.10>
			<ep3.11>10 MHYSA</ep3.11>
			<id3.11>aac09f3568b1ea2ce8df5bdb353b5ac51d1a663b</id3.11>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/AuuDrWTkGQcvyDbkxaU3gKrPolW.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/1KzqC5t8sAILJbYOB2nxnb2Tdxl.jpg</fanart.4>
			<ep4.1>1 AL 10 (1080)</ep4.1>
			<id4.1>7631229c402d812d5312ab3347b0bad34b26ceb3</id4.1>
			<ep4.2>1 DOS ESPADAS</ep4.2>
			<id4.2>8db184a62e006e46f54afe7cf936a6b1533412ac</id4.2>
			<ep4.3>2 EL LEON Y LA ROSA</ep4.3>
			<id4.3>7cac7b04eb9bfae4515b619aa1069258bdbf600a</id4.3>
			<ep4.4>3 ROMPEDORA DE CADENAS</ep4.4>
			<id4.4>ef0a3439d9c0e4d1b0f0ca63204d4e6add826647</id4.4>
			<ep4.5>4 GUARDAJURAMENTOS</ep4.5>
			<id4.5>ec3c371808efd11203b634a0712983fe9f25b92d</id4.5>
			<ep4.6>5 EL PRIMERO DE SU NOMBRE</ep4.6>
			<id4.6>659f424cdb73d527c4824181d2c5c57419637c7c</id4.6>
			<ep4.7>6 LAS LEYES DE LOS DIOSES Y LOS HOMBRES</ep4.7>
			<id4.7>8d9060b4603872ef3a51913dc38cd65b92a373a6</id4.7>
			<ep4.8>7 SINSORTE</ep4.8>
			<id4.8>91f6a8e199be9eac071e8d315dc45d74ffc422af</id4.8>
			<ep4.9>8 LA MONTAÑA Y LA SERPIENTE</ep4.9>
			<id4.9>c962b67be40ca8833778a7a42f5d819dc45dff38</id4.9>
			<ep4.10>9 LOS VIGILANTES DEL MURO</ep4.10>
			<id4.10>506b2035a6bde7105ff7deb96a9f48384d48226a</id4.10>
			<ep4.11>10 LOS NIÑOS</ep4.11>
			<id4.11>15354f2cc6c2daeb6d297672f9dce77c80dbf697</id4.11>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/kAMRCBWyYuLxC3r3hSaWCOUZWbe.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/qLsx6viZ0NBYQ6nhZ4OOrWXuiBj.jpg</fanart.5>
			<ep5.1>1 AL 10 (1080)</ep5.1>
			<id5.1>4b57a6d2efcd7a3128b02cacef17a2dacff7edc4</id5.1>
			<ep5.2>1 LAS GUERRAS VENIDERAS</ep5.2>
			<id5.2>2bba8c7a5bc6577d10945d88fb2c4b32c4e28afc</id5.2>
			<ep5.3>2 LA CASA DE NEGRO Y BLANCO</ep5.3>
			<id5.3>3ecdc223c454736b754b1a0cf62aa09638f4fb46</id5.3>
			<ep5.4>3 GORRION SUPREMO</ep5.4>
			<id5.4>eda3d84e1738ef961fda29ce2f35a57871057f31</id5.4>
			<ep5.5>4 HIJOS DE LA ARPIA</ep5.5>
			<id5.5>ecc3c68166ba9923a68bd67e955237d6da12fbae</id5.5>
			<ep5.6>5 MATEN AL CHICO</ep5.6>
			<id5.6>7a5c3c788659567424be1b476d71d0f01c073e47</id5.6>
			<ep5.7>6 NUNCA DOBLEGADO NUNCA ROTO</ep5.7>
			<id5.7>35c1bb2bbe0668e13e044b0642f18af150f68cc2</id5.7>
			<ep5.8>7 EL REGALO</ep5.8>
			<id5.8>eb7f28e78a001054846ee52230b17f7c930a5f73</id5.8>
			<ep5.9>8 CASA AUSTERA</ep5.9>
			<id5.9>9361e5edc5a6e29eeabe60ca85cbb61d84f54eba</id5.9>
			<ep5.10>9 DANZA DE DRAGONES</ep5.10>
			<id5.10>00e9dd0ada29eca8cbf82fd40b86277300a56609</id5.10>
			<ep5.11>10 MISERICORDIA</ep5.11>
			<id5.11>c78a11592bedd6eb18e2250b59cd81ce964d958d</id5.11>
		</t5>
		<t6>
			<thumb.6>https://image.tmdb.org/t/p/original/qBDWhOGuzpDkfuG0Es62ghqFFQg.jpg</thumb.6>
			<fanart.6>https://www.themoviedb.org/t/p/original/haASBIzQIrXgvrK0bDdHr4A7gBT.jpg</fanart.6>
			<ep6.1>1 AL 10 (1080)</ep6.1>
			<id6.1>18742a21a276037c9eb825b0d2dd5a897a63efd1</id6.1>
		</t6>
		<t7>
			<thumb.7>https://image.tmdb.org/t/p/original/kHe9Gl4Nv2eBKOe0h7jd7eKZaIJ.jpg</thumb.7>
			<fanart.7>https://image.tmdb.org/t/p/original/oIgSNOdKrILdo1MHL0hqxwbb9sG.jpg</fanart.7>
			<ep7.1>1 AL 7 (1080)</ep7.1>
			<id7.1>d41ca8e3865f40ce1fe52df82a126d2fa7a13d41</id7.1>
			<ep7.2>1 ROCADRAGON</ep7.2>
			<id7.2>dd03689075e16525a5a0e6037e8121c6e532b765</id7.2>
			<ep7.3>2 BAJO LA TORMENTA</ep7.3>
			<id7.3>e97b7a480e5f7dbdfb538a21e82051d9a939eeeb</id7.3>
			<ep7.4>3 LA JUSTICIA DE LA REINA</ep7.4>
			<id7.4>6821812c7af445ae73e2bf5ad59c7c2d5228608d</id7.4>
			<ep7.5>4 BOTINES DE GUERRA</ep7.5>
			<id7.5>eed66412f4c74e4beaf3d75e30d491677d7d623d</id7.5>
			<ep7.6>5 GUARDAORIENTE</ep7.6>
			<id7.6>b2fc69737eb37a3b8fcad73586ba83c49db7ace0</id7.6>
			<ep7.7>6 MAS ALLA DEL MURO</ep7.7>
			<id7.7>0a9e9e208096f86545dfa0cdc8e196e4d189c0f9</id7.7>
			<ep7.8>7 EL DRAGON Y EL LOBO</ep7.8>
			<id7.8>f56bde4f21481ecc21e4630d5e46ada7bec58368</id7.8>
		</t7>
		<t8>
			<thumb.8>https://www.themoviedb.org/t/p/original/3hDtRuwTfQQYRst3kjhvp4Cogjw.jpg</thumb.8>
			<fanart.8>https://www.themoviedb.org/t/p/original/suopoADq0k8YZr4dQXcU6pToj6s.jpg</fanart.8>
			<ep8.1>1 AL 6 (4K)</ep8.1>
			<id8.1>fedc07afed9d7acaaddc7a0c7f26104195ec0002</id8.1>
			<ep8.2>1 INVERNALIA</ep8.2>
			<id8.2>b1d973caf20ac551b5468a87dd7dd740e81316a8</id8.2>
			<ep8.3>2 UN CABALLERO DE LOS 7 REINOS</ep8.3>
			<id8.3>2c77f8a94401898e2cc50ffbfb99d56126806ee3</id8.3>
			<ep8.4>3 LA LARGA NOCHE</ep8.4>
			<id8.4>80baeeb5401c01e965b489f5cdd4787007b392d0</id8.4>
			<ep8.5>4 EXODO</ep8.5>
			<id8.5>3a0a90a3e9dcd223286ad7bb150af36139bd9b2c</id8.5>
			<ep8.6>5 LAS CAMPANAS</ep8.6>
			<id8.6>d46af93e960e8048782ed37ede34339856883122</id8.6>
			<ep8.7>6 EL TRONO DE HIERRO</ep8.7>
			<id8.7>5bfaed8a0c501c1fb4a0eab698b8dcd4db1c12b3</id8.7>
		</t8>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>JUPITER'S LEGACY</title>
		<info>(2021) 8 episodios. "Jupiter's Legacy" sigue la historia de los primeros superhéroes del mundo, que recibieron sus poderes allá por la década de 1930. Tras dedicar casi un siglo a proteger a la humanidad, ahora esta primera generación debe pasar el testigo a sus hijos para que continúen con su legado. Pero las tensiones van en aumento cuando los jóvenes, ansiosos por demostrar su valía, no logran estar a la altura ni de la legendaria reputación pública de sus progenitores ni tampoco de los exigentes estándares personales de estos.</info>
		<year>2021</year>
		<genre>Aventuras. Acción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/5i7lfhguANEY382GOcrAWJQXF1u.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/tx01yj8oKDYPJfSCVzinHh4LSmi.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/5i7lfhguANEY382GOcrAWJQXF1u.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/tx01yj8oKDYPJfSCVzinHh4LSmi.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>44a7f3f6884f04747993886ec3b7e3435828f9af</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>JUSTO ANTES DE CRISTO</title>
		<info>(2019-2020) 2 temporadas. 12 episodios.  Año 31 a. de C. Manio Sempronio, un acomodado patricio, mata sin querer a un senador y es condenado a muerte. Incapaz de acabar con su propia vida, pide que le conmuten la pena por cualquier cosa, lo que sea. Así es enviado como legionario a Tracia, la tierra donde su padre, El Magnífico, forjó su leyenda como militar. A la carga de recuperar el honor familiar, hay que sumarle que le acompaña su esclavo Agorastocles: un amigo, un hermano... Otra carga. Éste tiene ambiciones propias que, como todo esclavo, sólo puede vivir a través de su amo. En Tracia reina la calma desde hace décadas. El campamento es gobernado por el General de la Legión, un anciano al cuidado de su hija Valeria, mujer acostumbrada a los tejemanejes de Roma que lleva a su padre por donde quiere. La llegada de Manio, desesperado por ser un héroe, pondrá patas arriba, en un tiempo record, tanto la estabilidad militar de la zona como la vida cotidiana de todos los allí destinados..</info>
		<year>2019</year>
		<genre>Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/c6x4aR9FJPV2gHJMZDz8LKS2Rf7.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/zb2OYMUm2bgDe7hBFnFsEMGpmh6.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/c6x4aR9FJPV2gHJMZDz8LKS2Rf7.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/zb2OYMUm2bgDe7hBFnFsEMGpmh6.jpg</fanart.1>
			<ep1.1>1 EL EXTRAORDINARIO</ep1.1>
			<id1.1>d07bee4fd81186b52cfbe246fef19fffacaa892b</id1.1>
			<ep1.2>2 EL OTRO</ep1.2>
			<id1.2>05fb8b90686c3ebff359553aa02dda831a857fed</id1.2>
			<ep1.3>3 UN MOMENTO DE PAZ</ep1.3>
			<id1.3>e4fd0a44fb9af80ce5786165f56f6f1cc44710d3</id1.3>
			<ep1.4>4 LA PATRULLA PERDIDA</ep1.4>
			<id1.4>d9a175f92e079a393e04a5253b9ad325532dd60d</id1.4>
			<ep1.5>5 AMISTAD</ep1.5>
			<id1.5>bebceba9c824ebea63fc1f1e99a72c2cbd9a104f</id1.5>
			<ep1.6>6 CORNELIO PISON</ep1.6>
			<id1.6>38377abe5f5055fb95b3160cef73c4fb0b5c1f2f</id1.6>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/up3gRW1Pgyf5tIM6PAbfJ0FoAst.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/bfHWe0jBSqWunKK6pQtuM8W38hr.jpg</fanart.2>
			<ep2.1>1 AL 6</ep2.1>
			<id2.1>dc9feccae48003a0433e44391feb56f3b8409233</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>KILLING EVE</title>
		<info>(2018) 4 temporadas. 32 episodios. Una asesina y una detective se enfrentan en una persecución a nivel internacional en la que ambas mujeres se obsesionarán la una con la otra. Por un lado está Villanelle (Jodie Comer), una asesina psicópata sumida en el lujo de su profesión, y por otro lado, Eve Polastri (Sandra Oh), una agente del MI6. Cansada y aburrida de pasarse los días sentada en su escritorio, la perspicaz criminóloga Eve busca más acción en su vida profesional y un día se encuentra al mando de un grupo secreto cuyo objetivo es dar caza a Villanelle.</info>
		<year>2018</year>
		<genre>Thriller. Drama. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/m7erjnrcoJD7sBrNZmQ6rpIYZI4.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/rfJc5ngaPD1tVtdoscgREw2wLVp.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/uNgZ7HzbXj2u7roLUpZTEja45Ud.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/tS2FRdMh3FgISkWag62J3vWHtur.jpg</fanart.1>
			<ep1.1>1 BONITA CARA</ep1.1>
			<id1.1>3e952605b5019358aa10903122df9faf54e43d1a</id1.1>
			<ep1.2>2 VOY A TRATAR CON EL MAS TARDE</ep1.2>
			<id1.2>f318beebd94852fbc880215405138d6a296ca085</id1.2>
			<ep1.3>3 NO TE CONOZCO</ep1.3>
			<id1.3>bdd1e2eb21ee32b3f050fa6b7f326f68d0aaf663</id1.3>
			<ep1.4>4 LO SIENTO CARIÑO</ep1.4>
			<id1.4>dce9d22293c0d9b67b9bb557b41a6e71d519277a</id1.4>
			<ep1.5>5 TENGO FETICHE CON LOS BAÑOS</ep1.5>
			<id1.5>47d59ca49be765939c745cc0520750dbc69f6a77</id1.5>
			<ep1.6>6 MARCHANDO AL AGUJERO</ep1.6>
			<id1.6>563eb4609fa2688ba07fe82974bde6aa7e5baa83</id1.6>
			<ep1.7>7 NO QUIERO SER LIBRE</ep1.7>
			<id1.7>5f670e9d525580a901548b46a5828a91b021abd9</id1.7>
			<ep1.8>8 DIOS,ESTOY CANSADA</ep1.8>
			<id1.8>2fa05fbf8f8642a81656c5e1da727c7efec77431</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/cmLJFWOklp4PpUkUfeCFIKntbTH.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/jHyGUhWBMif5AD6mImr6anYyTlJ.jpg</fanart.2>
			<ep2.1>1 SABES COMO DIPONER DE UN CUERPO</ep2.1>
			<id2.1>8133b65b30f76d0ecda5616a63efe6b70ebd9ed0</id2.1>
			<ep2.2>2 AGRADABLE Y ASEADO</ep2.2>
			<id2.2>0bc435c3fe7ab408e49de4e8d1abcd77d4028eb2</id2.2>
			<ep2.3>3 LA ORUGA HAMBRIENTA</ep2.3>
			<id2.3>d6d9de2eb8cf09d625fd4e698f47c1bab7dc88a2</id2.3>
			<ep2.4>4 TIEMPOS DESESPERADOS</ep2.4>
			<id2.4>a87f2bf76f72a6a73f4ffce087a0a7304a5850a</id2.4>
			<ep2.5>5 HASTA QUE NOS OLAMOS</ep2.5>
			<id2.5>595577476ea100886692237cc0cdfb5bbe45baf2</id2.5>
			<ep2.6>6 ESPERO QUE TE GUSTE MISIONERO</ep2.6>
			<id2.6>67DB322FDD660727359A7EC62536CFE1D8B82438</id2.6>
			<ep2.7>7 DESPIERTO POR COMPLETO</ep2.7>
			<id2.7>0b11b1a533aeb37719dec4ab20b122cf7232f060</id2.7>
			<ep2.8>8 ERES MIA</ep2.8>
			<id2.8>711c4628fc469a1a4b7ba4039a04aefa178da9cf</id2.8>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/68pWzkhyjB0TobpYAQQKRwpaDW.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/scoyQvVpyrJIS29dIffRdKgMW9S.jpg</fanart.3>
			<ep3.1>1 LENTAMENTE LENTAMENTE MONO PEGADIZO</ep3.1>
			<id3.1>e410687d9e7b52bf73569ab34c519fdd98d1ad5a</id3.1>
			<ep3.2>2 LA GERENCIA APESTA</ep3.2>
			<id3.2>532887dd4e3f04620a5d04a758cdf3db7a28a631</id3.2>
			<ep3.3>3 LAS REUNIONES TIENEN GALLETAS</ep3.3>
			<id3.3>e3c242998745996870735947d3e5730e2ecd329e</id3.3>
			<ep3.4>4 AUN LO TIENES</ep3.4>
			<id3.4>31bf7b1b7886a87d4f7e775b0d4acae2e0154363</id3.4>
			<ep3.5>5 ¿ERES DE PINNER?</ep3.5>
			<id3.5>589961a615daf36b78b74e962e632d5e97c740e0</id3.5>
			<ep3.6>6 FIN DEL JUEGO</ep3.6>
			<id3.6>b4171b205eb94ccbec7563c7382222555ee381ce</id3.6>
			<ep3.7>7 MONSTRUO HERMOSO</ep3.7>
			<id3.7>cccfe8e15d7ab63a01575c78e845c224f036d409</id3.7>
			<ep3.8>8 ¿ESTAS LIDERANDO O SOY YO?</ep3.8>
			<id3.8>c85c1ddd65e7b444eaf723939835b85ab828216c</id3.8>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/4wKhTVw8aGq5AZMa0Q1spERdi7n.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/2eR0H27Us1tvbjd95oW7WhiKioC.jpg</fanart.4>
			<ep4.1>1 SOLO MOJAME</ep4.1>
			<id4.1>cd87af28e3389cb2f762cbcf66a6fa0ddefc1f2a</id4.1>
			<ep4.2>2 QUE NO TE COMAN</ep4.2>
			<id4.2>400376a8f0d4b0df35471c12dacf47f0ac7c646e</id4.2>
			<ep4.3>3 UN ARCO IRIS CON BOTAS BEIGE</ep4.3>
			<id4.3>f2130d5d46d8df70238e47cc181c129864578862</id4.3>
			<ep4.4>4 ESTO ES UNA AGONIA Y ESTOY HAMBRIENTA</ep4.4>
			<id4.4>494630743879766c2b3d7cec5d9906d5deabe9a8</id4.4>
			<ep4.5>5 NO TE ENCARIÑES</ep4.5>
			<id4.5>51e0d5d23a06d4f30f3d9ff7813ee17a3eb844e9</id4.5>
			<ep4.6>6 OH CARIÑO SOY LA GANADORA</ep4.6>
			<id4.6>a4195db6c44267e1cc25f97b06a5a1ea044a4165</id4.6>
			<ep4.7>7 HACER QUE LAS COSAS MUERTAS SE VEAN BIEN</ep4.7>
			<id4.7>56d1ebd58a973c265acd09de63b730c41df20f82</id4.7>
			<ep4.8>8 HOLA PERDEDORES</ep4.8>
			<id4.8>ea03df0be3160f5c88da7f8e3a8f863d6f30d95e</id4.8>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>KUNG FU</title>
		<info>(2021) 2 temporadas. 26 episodios. Una joven adolescente estadounidense de ascendencia china decide dejar la universidad para aventurarse a vivir una nueva vida en un monasterio chino. Sin embargo, cuando regresa a su pueblo natal y descubre que está plagado de crimen y corrupción, emplea las artes marciales y la filosofía Shaolin para proteger su comunidad y tratar de ajusticiar a los criminales. Todo ellos mientras busca al asesino de su maestro Shaolin, que ahora la ha puesto a ella en su diana.</info>
		<year>2021</year>
		<genre> Acción. Drama </genre>
		<thumb>https://www.themoviedb.org/t/p/original/sJbzjaCnGdxlaRUCP7UmJGL1RVZ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3bH7urg3TYXDq3OQzpxOgmX8Gu6.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/sJbzjaCnGdxlaRUCP7UmJGL1RVZ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3bH7urg3TYXDq3OQzpxOgmX8Gu6.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>a34122e925ee33df8f16ce7793647633a03b2719</id1.1>
			<ep1.2>2 SILENCIO</ep1.2>
			<id1.2>a521b6ac1152c65c63d7460bc732836fa56d354f</id1.2>
			<ep1.3>3 PACIENCIA</ep1.3>
			<id1.3>c4fd90dd5ca14e1ebbc1fa2c63f1da9b0dcc8909</id1.3>
			<ep1.4>4 MANO</ep1.4>
			<id1.4>37cee75d3c19c312009294dd1654e6cc055f619d</id1.4>
			<ep1.5>5 SANTUARIO</ep1.5>
			<id1.5>f70343e283c8c6cc7abf2198c1975a14eeda53d4</id1.5>
			<ep1.6>6 IRA</ep1.6>
			<id1.6>abc2b2a1234f989a38892aeae1f6e735ad837e9c</id1.6>
			<ep1.7>7 ORIENTACION</ep1.7>
			<id1.7>a3ac694d368710c789026a49134dc81a116b405b</id1.7>
			<ep1.8>8 DESTINO</ep1.8>
			<id1.8>10bf7b6d09fad17fd58353e917384f49c2982105</id1.8>
			<ep1.9>9 AISLAMIENTO</ep1.9>
			<id1.9>2489dd955ba3a8ab97044e487f72bd058989882d</id1.9>
			<ep1.10>10 DECISION</ep1.10>
			<id1.10>f00b86e0588ff1489ffc494cbded6473e1f0ce2a</id1.10>
			<ep1.11>11 APEGO</ep1.11>
			<id1.11>bc4d487b31a7be4531321ea00a142bf324a11fbf</id1.11>
			<ep1.12>12 SACRIFICIO</ep1.12>
			<id1.12>bcc8386564734d539336551baa08df9cfe2599ea</id1.12>
			<ep1.13>13 TRANSFORMACION</ep1.13>
			<id1.13>6d08c15bd40c6226093a29183b3b81fd19aab7d8</id1.13>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/qDbaafRfWkOuQAhOOeI4kcFJl3a.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/bJLGy57UQyJk0D0V9vYPXLcMQdx.jpg</fanart.2>
			<ep2.1>1 EL AÑO DEL TIGRE (PRIMERA PARTE)</ep2.1>
			<id2.1>2d63301db4366607bc6e2df68ab6256042abed71</id2.1>
			<ep2.2>2 EL AÑO DEL TIGRE (SEGUNDA PARTE)</ep2.2>
			<id2.2>0768680022026af4f65d803658a6d14a13816402</id2.2>
			<ep2.3>3 LA CAMPANA</ep2.3>
			<id2.3>aca633d50a497a2ae67cbfd889b21daaa3765eae</id2.3>
			<ep2.4>4 CLEMENTINA</ep2.4>
			<id2.4>4d1688eb59eac84413246ca3139893e04437a32f</id2.4>
			<ep2.5>5 REUNION</ep2.5>
			<id2.5>a242c38f79f080d0474b2260869cb222fd6090f2</id2.5>
			<ep2.6>6 JYU SA</ep2.6>
			<id2.6>d33751ddcb1f9444f45cc2973b1e99e6b90f7c68</id2.6>
			<ep2.7>7 EL ALQUIMISTA</ep2.7>
			<id2.7>0cc703953ed38864612fdb45d2c4bfc0535323ea</id2.7>
			<ep2.8>8 REVELACION</ep2.8>
			<id2.8>310a29eb8909c4893d57328cc95f8cfe07346c2a</id2.8>
			<ep2.9>9 EL ENCLAVE</ep2.9>
			<id2.9>701a81f0e51f9f84e78ae96f6c39582673e6d5e8</id2.9>
			<ep2.10>10 DESTRUCCION</ep2.10>
			<id2.10>4f83ee2e22c1b8b1d642222346cf4663d0720bce</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA ASISTENTA</title>
		<info>(2021) 10 episodios. Una madre soltera hace trabajos domésticos para llegar a fin de mes mientras lucha contra la indigencia y la burocracia.</info>
		<year>2021</year>
		<genre>Drama. Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/4brWcSXdH31BZUTtRTHj2BYFe6M.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/u4ydZotyPdOxSGAVUBiQnKLVwmz.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/4brWcSXdH31BZUTtRTHj2BYFe6M.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/u4ydZotyPdOxSGAVUBiQnKLVwmz.jpg</fanart.1>
			<ep1.1>1 TODO A CIEN</ep1.1>
			<id1.1>6f3d55f8f75f3323a80816a2621b43aa4268936f</id1.1>
			<ep1.2>2 AL 4</ep1.2>
			<id1.2>f8603bba94124a243ae09910330a1be6ea42069c</id1.2>
			<ep1.3>5 AL 7</ep1.3>
			<id1.3>0a254f2ebfd5fd984a2e70c18ab0b8d24640bc7d</id1.3>
			<ep1.4>8 AL 10</ep1.4>
			<id1.4>85b1a901650a0ea6abe9833f7a821017e7e8b1d3</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CASA DE PAPEL</title>
		<info>(2017)5 temporadas. 41 episodios. La Casa de Papel narra lo que se espera que sea el atraco perfecto al Museo de la Fábrica Nacional de Moneda y Timbre. La mente que idea este plan es El Profesor, un hombre que recluta a siete personas para llevar a cabo el gran golpe. Tokio es una joven atracadora muy buscada por la policía, Berlín asume el papel de "el cabecilla", Moscú es el experto en perforaciones, Río es "el informático", Nairobi es la falsificadora, Denver es el hijo de Moscú y, como siempre, falta la fuerza bruta: Helsinki y Oslo. La banda planea cada paso durante cinco meses, valoran todos los inconvenientes, todas las posibilidades y cuando llega el día, se encierran durante once días en la Fábrica Nacional de Moneda y Timbre con 67 rehenes. Su objetivo es salir de allí con su propio dinero de curso legal recién impreso y sin registrar, algo que será difícil ya que la policía ha sitiado el lugar. Tendrán que hacer todo el atraco con una condición: nada de relaciones entre los atracadores, algo que no llevan a rajatabla.</info>
		<year>2017</year>
		<genre>Thriller. Intriga. Acción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/s5QiIPNzND9pruA4tOAnB4cM06k.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dFiSWPCe3oBRpGeDQyez55WmglA.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gGqOtjBlcW1omTGSSdmnFRuyJUH.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/sqrQhxyVdZSA6jKDmR0K8jikyh6.jpg</fanart.1>
			<ep1.1>1 EFECTUAR LO ACORDADO</ep1.1>
			<id1.1>85d513d008a72214be939dc841c794a730e90f5c</id1.1>
			<ep1.2>2 IMPRUDENCIAS LETALES</ep1.2>
			<id1.2>9df6a1b715504dc8272f004ccb3180625b418556</id1.2>
			<ep1.3>3 ERRAR AL DISPARAR</ep1.3>
			<id1.3>0747c85a3493ac60431a61b9914156812674cf2e</id1.3>
			<ep1.4>4 CABALLO DE TROYA</ep1.4>
			<id1.4>9848233c5520106ea86feb699907848d0dee8e8c</id1.4>
			<ep1.5>5 EL DIA DE LA MARMOTA</ep1.5>
			<id1.5>cabeb25b008f2aaacf2b124555b230aae0505d25</id1.5>
			<ep1.6>6 LA CALIDA GUERRA FRIA</ep1.6>
			<id1.6>4e76091c565e3b1da3e5a4ebe49e36719a1ea988</id1.6>
			<ep1.7>7 REFRIGERADA INESTABILIDAD</ep1.7>
			<id1.7>73b3f0d264fa5af23093e94fa6c84becfe5d16b2</id1.7>
			<ep1.8>8 TU TE LO HAS BUSCADO</ep1.8>
			<id1.8>f0fc9c1cc66b1739ca6cc6f147eb4c1fd280903c</id1.8>
			<ep1.9>9 EL QUE LA SIGUE LA CONSIGUE</ep1.9>
			<id1.9>3b242a1a04e19b9460a234acf9594a40e7c7106b</id1.9>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/cUxzQXnUIdMqX2hM83UDRIUBdXC.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/llYYKjI1bfugli2kqFwDJXGkp3c.jpg</fanart.2>
			<ep2.1>1 SE ACABARON LAS MASCARAS</ep2.1>
			<id2.1>f70a73f68cffba2a707dbca13486eb56406238b3</id2.1>
			<ep2.2>2 LA CABEZA DEL PLAN</ep2.2>
			<id2.2>be020963dabec4326feaeb534fce8bc2fea7dde2</id2.2>
			<ep2.3>3 CUESTION DE EFICACIA</ep2.3>
			<id2.3>1a1e1ee01adb75bd8bc3a399f605f9e8468aa01f</id2.3>
			<ep2.4>4 QUE HEMOS HECHO</ep2.4>
			<id2.4>b49d5ff1c59fed3de2b4fc4b055c365f7f9d5d4f</id2.4>
			<ep2.5>5 A CONTRARELOJ</ep2.5>
			<id2.5>db2e4673411a8b4511997290d5c2e14bc89776bf</id2.5>
			<ep2.6>6 BELLA CIAO</ep2.6>
			<id2.6>8dcddd91a8c68d5a39bbd0d77e22b71bcc2f7d48</id2.6>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/blUCPGyD9mg0kNz2Oyp3PIgqD0N.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/tNlkKrypFs6QSUjFVQntFISoFv5.jpg</fanart.3>
			<ep3.1>1 AL 8 (1080)</ep3.1>
			<id3.1>9fe4b71e05ad96ca9aa410017dda26621f27b98f</id3.1>
			<ep3.2>1 HEMOS VUELTO</ep3.2>
			<id3.2>1b868f17c93cb6992dbab7abc7ec0afe03656903</id3.2>
			<ep3.3>2 AIKIDO</ep3.3>
			<id3.3>ec1597b17556c28c977b3b10b518bb007a6340b7</id3.3>
			<ep3.4>3 48 METROS BAJO EL SUELO</ep3.4>
			<id3.4>2c16135d015051a8d3d323c1b1440de89daded07</id3.4>
			<ep3.5>4 BUM BUM CIAO</ep3.5>
			<id3.5>9de02271872fdc172758b1c72bee13ad188029e1</id3.5>
			<ep3.6>5 LAS CAJAS ROJAS</ep3.6>
			<id3.6>eae6e58993320f6501eae27c7cbccca4d2c9bfa3</id3.6>
			<ep3.7>6 TODO PARECIO INSIGNIFICANTE</ep3.7>
			<id3.7>3bfdbc78c79622c3886c70622ea47a41d1f1ee5b</id3.7>
			<ep3.8>7 PEQUEÑAS VACACIONES</ep3.8>
			<id3.8>1aed7055737c308556418c5c355974795b784a7f</id3.8>
			<ep3.9>8 LA DERIVA</ep3.9>
			<id3.9>59c116778056fdff8dab9a583d9e43cf28b1e097</id3.9>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/95NhRroljS5dIP1FSzG03PGVoE7.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/xGexTKCJDkl12dTW4YCBDXWb1AD.jpg</fanart.4>
			<ep4.1>1 AL 8 (1080)</ep4.1>
			<id4.1>72b63faaa683a009c245c4acbb412417bab79603</id4.1>
			<ep4.2>1 GAME OVER</ep4.2>
			<id4.2>83c4eebc739ff4206e07c1ee712861c34ff27a1f</id4.2>
			<ep4.3>2 LA BODA DE BERLIN</ep4.3>
			<id4.3>e7408518b591d20ff9d2a4d3a16effd2611fe7d6</id4.3>
			<ep4.4>3 LECCION DE ANATOMIA</ep4.4>
			<id4.4>20b8400398e7e55d0961e5b561ee7ffbee2b9727</id4.4>
			<ep4.5>4 SUSPIROS DE ESPAÑA</ep4.5>
			<id4.5>1f40c3ba58a790cb86af2a318da1c6aa437449a8</id4.5>
			<ep4.6>5 CINCO MUNUTOS ANTES</ep4.6>
			<id4.6>d5c00c0b1ec041b5c85896114c0335e31a671d88</id4.6>
			<ep4.7>6 KO TECNICO</ep4.7>
			<id4.7>f7049bd8d1eacf123383455d352363a9c8a18e49</id4.7>
			<ep4.8>7 TUMBAR LA CARPA</ep4.8>
			<id4.8>3aa19bf4725cae2e2c28d2aff12692a357801585</id4.8>
			<ep4.9>8 PLAN PARIS</ep4.9>
			<id4.9>d98483ed016ef8b98e7bb2753b64eac57bd8ddc8</id4.9>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/wcijC3hrcahKOUTMQeoUWbMqnYA.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/AfX3K8oGdRl4nyGxtonwNIkRDuf.jpg</fanart.5>
			<ep5.1>1 AL 5 (1080)</ep5.1>
			<id5.1>bf108f1c50281f6f6d5d868e63f697c5415aecaf</id5.1>
			<ep5.2>6 AL 10 (1080)</ep5.2>
			<id5.2>ED39C4514E7D9A5E961F0D43AEAEAF327371AF98</id5.2>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CASA DE PAPEL: DE TOKIO A BERLIN</title>
		<info>(2021) 1 episodio. La Casa de Papel: De Tokio a Berlín es una serie documental donde los actores protagonistas, que dan vida al Profesor o Tokio, y los directores de los episodios hablan de cómo es el proceso artístico y emocional que supone rodar esta serie de gran éxito. Los episodios se centran sobre todo en la primera mitad de la temporada final.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/lIa5eVVFdVNQmfAlKRexoJCVVd1.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/bVUiZVA8iPwdEc0lX5nwEr6MwdM.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/lIa5eVVFdVNQmfAlKRexoJCVVd1.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/bVUiZVA8iPwdEc0lX5nwEr6MwdM.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>cf99dc5886a804582873eba257bea02176afadf3</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CASA DE PAPEL: EL FENOMENO</title>
		<info>(2020) 1 episodio. Este documental gira en torno al fenómeno social que ha causado La casa de papel en todo el mundo. A través de diferentes testimonios de directores y parte del elenco, se descubren el porqué de la elección de ciertos símbolos, como la máscara de Dalí; cómo un personaje detestable como el de Berlín se convierte en uno de los favoritos; o la razón por la que se escogió el tema Bella ciao. </info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/AboUXTrDWEi0PuZUqaft0iwBTm7.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/yEBBtk1eyZltGgJt8Z2zi3KIvvX.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/AboUXTrDWEi0PuZUqaft0iwBTm7.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/yEBBtk1eyZltGgJt8Z2zi3KIvvX.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>f081cd1eb783eede4b314f5edeb21bbb1533261c</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CATEDRAL DEL MAR</title>
		<info>(2018)  8 episodios. Barcelona, siglo XIV. Bajo la Corona de Aragón, la ciudad condal se encuentra en su momento de mayor prosperidad y los habitantes del humilde barrio de pescadores de la Ribera deciden construir, con el dinero de unos y el esfuerzo de otros, el mayor templo mariano jamás conocido: la catedral Santa María del Mar. Mientras se construye el edificio, Arnau Estanyol va creciendo y descubriendo Barcelona. Adaptación de la famosa novela de Ildefonso Falcones, publicada en 2006. </info>
		<year>2018</year>
		<genre>Drama. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/nEO1n2PVuKhY9BGl8KKNvyj0K7n.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fFFEOitoqZe46oqxlxTWI7EYfkh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/nEO1n2PVuKhY9BGl8KKNvyj0K7n.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fFFEOitoqZe46oqxlxTWI7EYfkh.jpg</fanart.1>
			<ep1.1>1 AL 8 (1080)</ep1.1>
			<id1.1>8954AED70E92FC57C823472FCF456ED09CDDDF84</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CAZA: MONTEPERDIDO</title>
		<info>(2019) 8 episodios. Hace cinco años, dos niñas de once años, Ana y Lucía, desaparecieron en Monteperdido, un pueblo del Pirineo. Tras una extensa búsqueda, el caso nunca se resolvió y los vecinos quedaron marcados por la tragedia. Ahora, una de las niñas, Ana, ha regresado. Las autoridades se volcarán con el caso para encontrar a Lucía, pero los habitantes de Monteperdido, incluso las familias de las niñas atrapadas en sus secretos y mentiras, no se lo pondrán nada fácil a los investigadores. Algo tienen claro: el monstruo está entre ellos y los agentes iniciarán una caza que pondrá en peligro sus propias vidas.</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bmU4ZWzGGGV0c4ebv7v1ThqQr9c.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5pYAtLLxVgkQRRrmkR6VvmhUuXP.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bmU4ZWzGGGV0c4ebv7v1ThqQr9c.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5pYAtLLxVgkQRRrmkR6VvmhUuXP.jpg</fanart.1>
			<ep1.1>1 EL DESHIELO</ep1.1>
			<id1.1>182644a59c1f1a3cc34c4728995bb893d66bd24a</id1.1>
			<ep1.2>2 TREMOLS</ep1.2>
			<id1.2>4b5c0c2959e06bc1c908a50225e54b27dc8dce14</id1.2>
			<ep1.3>3 EL BAILE DE LOS HOMBRES</ep1.3>
			<id1.3>25ed0f9da0a487dd67d35beaa054ee157735972d</id1.3>
			<ep1.4>4 OSCUROS DE LIESTRA</ep1.4>
			<id1.4>886adde2de6a1e0fc4aaf82387db47d9e2b3acdd</id1.4>
			<ep1.5>5 LAGO</ep1.5>
			<id1.5>4b0dc340baaf5311cc00e1bc2e3d13824f1ded67</id1.5>
			<ep1.6>6 LA GUARDIA</ep1.6>
			<id1.6>991d1be7680093c0e3181be2d796100c0e14639f</id1.6>
			<ep1.7>7 LA BATIDA</ep1.7>
			<id1.7>eda26f7160fd356fe53f6370aca03f25e76ebade</id1.7>
			<ep1.8>8 CIERVA BLANCA</ep1.8>
			<id1.8>b6447805cc2178f88785d3cd86ceea13d1747bc4</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CAZA: TRAMUNTANA</title>
		<info>(2021) 8 episodios. Ambientada en la sierra mallorquina, la sargento Sara Campos deberá investigar la muerte de Bernat Cervera, una de las personas más queridas del ficticio pueblo de Tramuntana, asesinado ante los ojos de tres adolescentes. Secuela de 'La caza. Monteperdido'</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bmU4ZWzGGGV0c4ebv7v1ThqQr9c.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5pYAtLLxVgkQRRrmkR6VvmhUuXP.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/aa9jV65UGGKuli8pIdgVunwHgNJ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7M0S10827RkcX6GeY8JEVcUpx8.jpg</fanart.1>
			<ep1.1>1 EL CANT DE LA SIBIL.LA</ep1.1>
			<id1.1>3d68f542cda28f8de484d9a409d7affcdb1dbf91</id1.1>
			<ep1.2>2 LA PLAGA</ep1.2>
			<id1.2>e139c6a8ff0ad27d4a4eaf6ceba4f2dafb0a4674</id1.2>
			<ep1.3>3 DONA D'AIGO</ep1.3>
			<id1.3>b542dcc06d77fefdb865a30659004772f7645e33</id1.3>
			<ep1.4>4 AVENC</ep1.4>
			<id1.4>c9b2d2522539499b5b49e36cabf13b35616c1136</id1.4>
			<ep1.5>5 NOCTURNO</ep1.5>
			<id1.5>8dc75e6a4d722c930352591129a26afd6387e28c</id1.5>
			<ep1.6>6 S'ILLA</ep1.6>
			<id1.6>265ba910d152a2fcb1e3c01683a4253007d365d5</id1.6>
			<ep1.7>7 CAN FALGUERES</ep1.7>
			<id1.7>75aa28ca16f72b004acb388c9f5d61644b91d1e5</id1.7>
			<ep1.8>8 ANS DEL JUDICI</ep1.8>
			<id1.8>66ec7d43079d15659c9ea1757e7501697901e340</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA CIUDAD ES NUESTRA</title>
		<info>(2022) 6 episodios. Baltimore, 2015. La muerte en circunstancias sospechosas de Freddie Gray, un joven negro bajo custodia policial, ha provocado una ola de disturbios. La ciudad alcanza un nuevo récord de asesinatos. Bajo presión por la oficina del alcalde y por una investigación federal sobre la muerte de Gray, la policía de Baltimore recurre al Sargento Wayne Jenkins y a su unidad de élite de agentes de paisano, para librar las calles de armas y drogas. Sin embargo, una conspiración criminal sin precedentes se desarrolla en el departamento de policía, mientras Jenkins decide explotar la crisis.</info>
		<year>2022</year>
		<genre>Drama. Thriller </genre>
		<thumb>https://www.themoviedb.org/t/p/original/gWlZRCE9ttL99UK1aIOq8GvKL0Y.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/sOSXMqtgB7YSOCVts3js2pxtvW5.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gWlZRCE9ttL99UK1aIOq8GvKL0Y.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/sOSXMqtgB7YSOCVts3js2pxtvW5.jpg</fanart.1>
			<ep1.1>1 PARTE UNA</ep1.1>
			<id1.1>b6fc13bbecc9e19abe802003f8413c70dc0582ff</id1.1>
			<ep1.2>PARTE DOS</ep1.2>
			<id1.2>6f32d93012a585f0d15eef78c574782f73f13487</id1.2>
			<ep1.3>3 PARTE TRES</ep1.3>
			<id1.3>605494918b8da2f59fc1d02806e3a0fce7841a77</id1.3>
			<ep1.4>4 PARTE CUATRO</ep1.4>
			<id1.4>72507af333a9426a78e167b02ef17c6430998921</id1.4>
			<ep1.5>5 PARTE QUINTA</ep1.5>
			<id1.5>2840be477337cc847a534b326cac9aa6fc36c5dc</id1.5>
			<ep1.6>6 PARTE SEXTA</ep1.6>
			<id1.6>baa45e79c5b37dda851774f87bb0e6b8e1f6420e</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA COCINERA DE CASTAMAR</title>
		<info>(2021).12 episodios. Adaptación de la novela homónima de Fernando J. Múñez, ambientada en el Madrid del siglo XVIII. La joven Clara Belmonte (Michelle Jenner) comienza a cocinar para Diego (Roberto Enríquez), Duque de Castamar. Esto cambiará la vida de ambos, que tendrán que luchar contra uno de los grandes obstáculos de la época para estar juntos: la diferencia de clases, además de hacer frente a la sed de venganza de Enrique de Arcona (Hugo Silva).</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/pf7u8l0NQnECwG06dC94s0ZJLpC.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/ojsBo6AnJ3NyonBYqfS0enn0JiD.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/pf7u8l0NQnECwG06dC94s0ZJLpC.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/ojsBo6AnJ3NyonBYqfS0enn0JiD.jpg</fanart.1>
			<ep1.1>1 EL INGREDIENTE ESENCIAL</ep1.1>
			<id1.1>f3d080fb57e26535dc5d8da7cf328c48b99117c8</id1.1>
			<ep1.2>2 LA NOCHE DEL REY</ep1.2>
			<id1.2>3ba0a326a358df7c2867273c024bd69d10330cac</id1.2>
			<ep1.3>3 CREDO UT INTELLIGAM</ep1.3>
			<id1.3>b4fd93b91a882388a71152c25adee1fa79281d8a</id1.3>
			<ep1.4>4 VUELA</ep1.4>
			<id1.4>b9182a1aa20e86e50f687ea862c2bc8e7c1a9e86</id1.4>
			<ep1.5>5 LA DECISION</ep1.5>
			<id1.5>5a619cbf9b2a279c0d2baf9b6b4c9980d8ce9fee</id1.5>
			<ep1.6>6 DONDE NO LLEGA LA LUZ</ep1.6>
			<id1.6>62252fedf82a71dd9d1a13841d15b23505ed6e77</id1.6>
			<ep1.7>7 PARA QUE NO NOS BORREN</ep1.7>
			<id1.7>ec44cc93fb08c48e5898c62a981a9323d015ad2d</id1.7>
			<ep1.8>8 LO QUE NO SERA</ep1.8>
			<id1.8>e7316aac840e1a730923b57c2cd31a58025dfcc8</id1.8>
			<ep1.9>9 LA VERDAD</ep1.9>
			<id1.9>2e42c662f5dcda64a1e11d5f7dea4468cdf360bf</id1.9>
			<ep1.10>10 LO QUE DE VERDAD IMPORTA</ep1.10>
			<id1.10>2f617eb47fcf68d0108d36c77ad6057be1bbe85f</id1.10>
			<ep1.11>11 LUB-DUB</ep1.11>
			<id1.11>a54fa49c3b84a9f07b70f7d97d29ff29bd9de56f</id1.11>
			<ep1.12>12 EL LUGAR DE CADA UNO</ep1.12>
			<id1.12>bccf84ea8bbfd93f67709c00b8905fc11053d005</id1.12>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA EDAD DE LA IRA</title>
		<info>(2022) 4 episodios. Arranca con el brutal asesinato de un hombre, presuntamente, a manos de su hijo Marcos, un adolescente sin problemas aparentes. El incidente cae como un jarro de agua fría en el día a día de un instituto en donde alumnos y profesores se preguntan qué ha fallado para un chico popular, casi perfecto, haya acabado cometiendo semejante crimen.</info>
		<year>2022</year>
		<genre>Drama. Thriller. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/z1ea0WQV1iT6oziHgSVaMmKIEEJ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/szjKI3xlt1HvmAGr21IUCXto46.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/z1ea0WQV1iT6oziHgSVaMmKIEEJ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/szjKI3xlt1HvmAGr21IUCXto46.jpg</fanart.1>
			<ep1.1>1 SANDRA</ep1.1>
			<id1.1>7fe97c7c7568abe9e4ebde43958f65e735f2929c</id1.1>
			<ep1.2>2 IGNACIO</ep1.2>
			<id1.2>b2579e7bc0ef5822c5c48579a3c4e6987c011e93</id1.2>
			<ep1.3>3 RAUL</ep1.3>
			<id1.3>3485ef9bee34a1c658842602a10c0503a2ad2146</id1.3>
			<ep1.4>4 MARCOS</ep1.4>
			<id1.4>0d5f5198b2a5cea72852b9a107cccb05972c1852</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA EDAD DORADA</title>
		<info>(2022) 9 episodios. La historia arranca en 1882 con la mudanza de la joven Marian Brook de la zona rural de Pensilvania a la ciudad de Nueva York después de la muerte de su padre para vivir con sus tías adineradas. Acompañada por una aspirante a escritora que busca un nuevo comienzo, Marian se ve envuelta de forma inesperada en una guerra social entre una de sus tías, hija de la vieja riqueza, y sus vecinos tremendamente ricos, un magnate ferroviario despiadado y su ambiciosa esposa. Expuesta a un mundo al borde de la Edad moderna, ¿Marian seguirá las reglas ya establecidas por la sociedad o forjará su propio camino?</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/583aS5oppLxr9p67sTZWDYT33KF.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/Qmpyftcb2LrdwWQqesEt2O0BMD.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/583aS5oppLxr9p67sTZWDYT33KF.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/Qmpyftcb2LrdwWQqesEt2O0BMD.jpg</fanart.1>
			<ep1.1>1 NUNCA LO NUEVO</ep1.1>
			<id1.1>FE679C88F56F8B4EC0239A7BF9EBE889A3D889AF</id1.1>
			<ep1.2>2 EL DINERO NO LO ES TODO</ep1.2>
			<id1.2>AF57F2BE1149111DF721239B820791707421ED8F</id1.2>
			<ep1.3>3 AFRONTAR LAS CONSECUENCIAS</ep1.3>
			<id1.3>4991CB7E53446273C22074728C7A69866220E665</id1.3>
			<ep1.4>4 UNA LARGA ESCALERA</ep1.4>
			<id1.4>A5B85D7C34C32CBD36C604690DD98CB15A1F32DB</id1.4>
			<ep1.5>5 LA CARIDAD TIENE DOS FUNCIONES</ep1.5>
			<id1.5>D587A2A862F10356555BE81C6BC179A030A36D2A</id1.5>
			<ep1.6>6 HAN RODADO CABEZAS POR MENOS</ep1.6>
			<id1.6>44211EEF5AB61F9A65CB9FCA500D7D0610920A85</id1.6>
			<ep1.7>7 CAMBIO IRRESISTIBLE</ep1.7>
			<id1.7>9775D2F3881B916A0D692BDB9B95113BCEEA0E51</id1.7>
			<ep1.8>8 ESCONDIDO EN NEWPORT</ep1.8>
			<id1.8>AF87D5CA4A2F99646DAEE85F149E54DAC68ED3C3</id1.8>
			<ep1.9>9 QUE COMIENCE EL TORNEO</ep1.9>
			<id1.9>F45A5197F76CF1E2775F0457EFD5B7541E734271</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA FORTUNA</title>
		<info>(2021) 6 episodios. Álex Ventura, un joven e inexperto diplomático, se ve convertido sin proponérselo en el líder de una misión que pondrá a prueba todas sus convicciones: recuperar el tesoro submarino robado por Frank Wild, un aventurero que recorre el mundo saqueando el patrimonio común de las profundidades del mar. Conformando un singular equipo con Lucía, una funcionaria de armas tomar, y Jonas Pierce, un brillante abogado norteamericano apasionado por las viejas historias de piratas, Álex emprenderá la aventura de su vida, descubriendo la importancia del amor, la amistad y el compromiso con aquello en lo que uno cree.</info>
		<year>2021</year>
		<genre> Aventuras. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/yljwJHcvA1GMQ249qgcvkk1cSv4.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/mTRaCPxVYBT58D5jO5oP4nvhhge.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/yljwJHcvA1GMQ249qgcvkk1cSv4.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/mTRaCPxVYBT58D5jO5oP4nvhhge.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>0e5633890acd76878db002ab1a3766dd5a30ba13</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>4e2ccade691d0831e35897197cd39a89cbaf6ea8</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>cc8668d6f801df73ccd63273bc335abc9e942a42</id1.3>
			<ep1.4>Episodio 4</ep1.4>
			<id1.4>63b756648578ebb9382922c6dd05a3c0c0417a95</id1.4>
			<ep1.5>Episodio 5</ep1.5>
			<id1.5>79d4536ca96d80008ff606c83452c5827ff25e71</id1.5>
			<ep1.6>Episodio 6</ep1.6>
			<id1.6>22e6787450c2a6caf1dd90eb89e670ee5ac4f280</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA FUERZA DEL GRUPO</title>
		<info>(2021) 2 episodios. Documental con imágenes inéditas de un torneo donde la Selección superó muchas adversidades hasta llegar a las semifinales. A través de ese material, se descubrirá cómo la selección se sobrepuso mediante la fuerza del grupo a todas las dificultades que se encontró para alcanzar las semifinales de la Euro 2020.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/xyVvHoPGIoyF1oDmcEcjsqWOIha.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/btwqimMX2a1Ky5QWiIDmuzCK3eb.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/xyVvHoPGIoyF1oDmcEcjsqWOIha.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/btwqimMX2a1Ky5QWiIDmuzCK3eb.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>1af0f06e6930c57dce55174715622c0eafab0944</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA GUERRA DE LOS MUNDOS</title>
		<info>(2019) 2 temporadas. 16 episodios. Ambientada en la actualidad. Cuando los astrónomos detectan una transmisión desde otra galaxia, es una prueba definitiva de la existencia de vida extraterrestre inteligente. La población mundial espera un futuro contacto con la respiración contenida. Sin embargo, no tendrán que esperar mucho. En cuestión de días, la humanidad queda prácticamente aniquilada en un ataque devastador; los supervivientes se encontrarán un mundo misteriosamente desierto. Mientras los extraterrestres cazan y matan a los que quedan vivos, los supervivientes se preguntan quiénes son estos atacantes y por qué están empeñados en nuestra destrucción.</info>
		<year>2019</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bxJ53tsvVtlNmtESf5n62NhzpG.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/Agit9xBKUzYiWTaos8W08kjnU3r.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bxJ53tsvVtlNmtESf5n62NhzpG.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/Agit9xBKUzYiWTaos8W08kjnU3r.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>e152ee49d4cdf505cf9dfb2d61200e9f8f7a52a2</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>ee89d9cce0196b9544d0ee6000563080fe3a765e</id1.2>
			<ep1.3>Episodio 3</ep1.3>
			<id1.3>a15af4698074498e6d108d59cba2d5d6b0211aa2</id1.3>
			<ep1.4>Episodio 4</ep1.4>
			<id1.4>de70316d0539fce4d4065d503f496a800d2da0e5</id1.4>
			<ep1.5>Episodio 5</ep1.5>
			<id1.5>88f8b45a6ab7663be8de4c97565a8bae0a90f955</id1.5>
			<ep1.6>Episodio 6</ep1.6>
			<id1.6>509dc0fa15f28abfb28038ade2015d69cc60d05c</id1.6>
			<ep1.7>Episodio 7</ep1.7>
			<id1.7>a9304bad22767f6fc66ce383ca9305801d9b1364</id1.7>
			<ep1.8>Episodio 8</ep1.8>
			<id1.8>d40fcdb4c3a1bf1db8f2fe2d939ba5f2c3712ed3</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/pDZ1oMOZ7N1UtRBjRrYwzPSCgGY.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/lpw6cyjQWGa6yWtudwHsd9Gg17c.jpg</fanart.2>
			<ep2.1>Episodio 1</ep2.1>
			<id2.1>6e7e2bd85ead411924667f08e9258fdadc6faf69</id2.1>
			<ep2.2>Episodio 2</ep2.2>
			<id2.2>056a319cc61a9aa2501d75bb76f6983cadd0f7bd</id2.2>
			<ep2.3>Episodio 3</ep2.3>
			<id2.3>0ec8dedf6ac6f73fc244467783054c6ee9987f5f</id2.3>
			<ep2.4>Episodio 4</ep2.4>
			<id2.4>efc8e4c57def4712cdb18297887a8db1d9fb8244</id2.4>
			<ep2.5>Episodio 5</ep2.5>
			<id2.5>a20d9d4b96f7b7d0b11eb4cd56abb894870e3b68</id2.5>
			<ep2.6>Episodio 6</ep2.6>
			<id2.6>dd9a7c9114b9a5c484cbfa33de591ff0dffd20e1</id2.6>
			<ep2.7>Episodio 7</ep2.7>
			<id2.7>b5474d2d8efb28e004c300cdc3cf5ff1f1e056d7</id2.7>
			<ep2.8>Episodio 8</ep2.8>
			<id2.8>15443e013099561cf69d4da110317c6a8e76999b</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA HISTORIA DE LISEY</title>
		<info>(2021) 8 episodios. Una viuda se convierte en objeto de un peligroso acosador, obsesionado con el trabajo de su marido. Lisey Landon (Julianne Moore) sigue con su vida dos años después de la muerte de su marido, el famoso novelista Scott Landon (Clive Owen). Una sucesión de hechos inquietantes hace que Lisey empiece a recordar episodios de su matrimonio con Scott que había suprimido deliberadamente de su memoria.</info>
		<year>2021</year>
		<genre>Drama. Terror. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/veaU2nMdTvNnzKnmfHnTX8Hhpaj.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/63zbegJkwsiAg4DLM0AeP4bkKkn.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/veaU2nMdTvNnzKnmfHnTX8Hhpaj.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/63zbegJkwsiAg4DLM0AeP4bkKkn.jpg</fanart.1>
			<ep1.1>1 CACERIA DE DALIVAS</ep1.1>
			<id1.1>7740f66a7e62ec53864a23f27ff3769e3878ae32</id1.1>
			<ep1.2>2 DALIVA SANGRIENTA</ep1.2>
			<id1.2>de44b47db191fd285dc9c81d58d8b810e2ae8c6d</id1.2>
			<ep1.3>3 BAJO EL ARBOL ÑAM ÑAM</ep1.3>
			<id1.3>d2749026ba55dab15511cbe6c813a01006c33bf9</id1.3>
			<ep1.4>4 JIM DANDY</ep1.4>
			<id1.4>6c84394bf134f571aea671113ca984a7bc2fb9f7</id1.4>
			<ep1.5>5 EL BUEN HERMANO</ep1.5>
			<id1.5>7ccd3b7424c63a292b7cbe2e512fa90ff61e1362</id1.5>
			<ep1.6>6 AHORA TIENES QUE GUARDAR SILENCIO</ep1.6>
			<id1.6>fd805ba64ac97191df46bd27d905e2dc1d002385</id1.6>
			<ep1.7>7 NI LLAMAS NI CHISPAS</ep1.7>
			<id1.7>6ad9393001e69761465bb609fd8e20c4590ce94f</id1.7>
			<ep1.8>8 LA HISTORIA DE LISEY</ep1.8>
			<id1.8>2d5a80b4b432bce1c34e36c17514065119f7accf</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA LINEA INVISIBLE</title>
		<info>(2020) 6 episodios. Miniserie sobre los comienzos de ETA. El 7 de junio de 1968, el líder de ETA, Txabi Etxebarrieta, cruzaba "la línea invisible" asesinando a la primera de las 853 víctimas de la organización terrorista, el guardia civil gallego José Antonio Pardines, de sólo 25 ańos de edad. Pocas horas después, el propio Txabi Etxebarrieta era abatido en un enfrentamiento con la guardia civil, convirtiéndose así en el primer terrorista en matar y el primero en morir en la historia de ETA. Tras la muerte de su líder, los compañeros de Txabi decidieron vengarle asesinando a su principal perseguidor, el inspector Melitón Manzanas. No eran conscientes de que estaban a punto de abrir un camino plagado de dolor y venganza, de miedo y terror, que marcaría los siguientes cincuenta años de la historia de España</info>
		<year>2020</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/8v3Z5gUpAg5nEgoqzsUjWvJCxcv.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/cDP3JBEAzRhfqF0yMybPA4HrXBb.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/8v3Z5gUpAg5nEgoqzsUjWvJCxcv.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/cDP3JBEAzRhfqF0yMybPA4HrXBb.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>c04bde6434c61c705376e06aeefe9ccf898a350d</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA LINEA: LA SOMBRA DEL NARCO</title>
		<info>(2020) 4 episodios. Entra en La Línea, la ciudad costera española convertida en el centro del tráfico de drogas de Europa, y conoce a los agentes del orden determinados a cambiar eso.</info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fdhp11lKBpCzPl6YRcZwvLsJT82.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/d7fYfsR6e59sEULkY0jKayeOAM5.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fdhp11lKBpCzPl6YRcZwvLsJT82.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/d7fYfsR6e59sEULkY0jKayeOAM5.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>e28e9e2bdd033a5bc18dd456401603c78ea58a7d</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA MALDICION DE BLY MANOR</title>
		<info>(2020) 9 episodios. Continuación de "La maldición de Hill House" al estilo de "American Horror Story" o "Channel Zero", es decir, contando con nuevos personajes y una historia diferente. En esta ocasión, se basa en el clásico de Henry James "Otra vuelta de tuerca" (The Turn of the Screw), cuya mejor adaptación al cine, "Suspense" (1961), es uno de los grandes clásicos del cine de terror.</info>
		<year>2020</year>
		<genre>Terror. Thriller. Intriga. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/deEQpv3rXUDUrDjw1pQ5vWU8Eri.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/isevY1UDcpaAYlYo7IqoSAU9s81.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/deEQpv3rXUDUrDjw1pQ5vWU8Eri.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/isevY1UDcpaAYlYo7IqoSAU9s81.jpg</fanart.1>
			<ep1.1>1 AL 9</ep1.1>
			<id1.1>fa6ef0d2597cd2f86c370ba185d6c8f3ace6c08d</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA MATERIA OSCURA</title>
		<info>(2019) 2 temporadas. 15 episodios. Lyra y Will son dos niños que viven una peligrosa aventura en mundos encantados y repletos de amenazas. Haciendo frente a osos, brujas, ángeles caídos e incluso a espectros devoraalmas, los dos muchachos pronto se darán cuenta de que el peso del futuro de la humanidad, tanto de los vivos como de aquellos que han fallecido, depende de ellos.</info>
		<year>2019</year>
		<genre>Fantástico. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/aF3UVTrSIhz3B0tgHU5yI3bPc5k.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/4y3ZgtdADnCue297sK1BXPAPH9I.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/aF3UVTrSIhz3B0tgHU5yI3bPc5k.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/9yKCJTOh9m3Lol2RY3kw99QPH6x.jpg</fanart.1>
			<ep1.1>1 EL JORDAN DE LYRA</ep1.1>
			<id1.1>6ba3e5de11977dd7f53377b93116fdbfb02e612d</id1.1>
			<ep1.2>2 LA IDEA DEL NORTE</ep1.2>
			<id1.2>f201218c0004e23d33702607cc1936bf8566d1e5</id1.2>
			<ep1.3>3 LOS ESPIAS</ep1.3>
			<id1.3>28c0d118ca2c0a00593fd008ff23fb9f25342373</id1.3>
			<ep1.4>4 ARMADURA</ep1.4>
			<id1.4>9fdcfec8071c539fff0d74b147b595b5ddfd0256</id1.4>
			<ep1.5>5 EL NIÑO PERDIDO</ep1.5>
			<id1.5>6c6981085186c9a3522f0f9d98f38df7d01a44f7</id1.5>
			<ep1.6>6 LA JAULA PARA DAIMONIONS</ep1.6>
			<id1.6>d614ca64b8e4a6dad1f8e96545bbbddda6a2a8be</id1.6>
			<ep1.7>7 LUCHA A MUERTE</ep1.7>
			<id1.7>2e24d0e77b3d67a421f3e003c939f1db7d67c776</id1.7>
			<ep1.8>8 TRAICION</ep1.8>
			<id1.8>1271d48c97145849e77c362a8bada79a28aa4f0a</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/mHyH2RDNqONW41c0VwzXzw24gau.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/t8KjRqfOqNx14cHLwARjR08bjeb.jpg</fanart.2>
			<ep2.1>1 LA CIUDAD DE LAS URRACAS</ep2.1>
			<id2.1>db089a9cbb46a920876ddaca5ffc5bd9c491f044</id2.1>
			<ep2.2>2 LA CUEVA</ep2.2>
			<id2.2>8d5e86e104fd39b8172703a419ade3d33a4e3ef1</id2.2>
			<ep2.3>3 EL ROBO</ep2.3>
			<id2.3>e08742e899a8d82422ccbc774d4498a4b4b62516</id2.3>
			<ep2.4>4 LA TORRE DE LOS ANGELES</ep2.4>
			<id2.4>b76a93e60176dcc0b11e072d8a5416e08e0fcb85</id2.4>
			<ep2.5>5 EL ERUDITO</ep2.5>
			<id2.5>c35c7d0db19b8d2e65be949fd8541dc2580e690a</id2.5>
			<ep2.6>6 MALICIA</ep2.6>
			<id2.6>8966ff83c7fcd63b7381857c27b5584fb2725e54</id2.6>
			<ep2.7>7 ÆSAHÆTTR</ep2.7>
			<id2.7>bd2570498105d85d898e60b4b82921fa2fc7228c</id2.7>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA PESTE</title>
		<info>(2017-2019) 2 temporadas. 12 episodios. En la segunda mitad el Siglo XVI, Sevilla era la metrópoli del mundo occidental. Puerta de acceso de América en Europa. Ciudad donde la riqueza florecía con facilidad gracias al comercio internacional, al oro, la plata; a la convivencia de nacionales y extranjeros: cristianos, judíos conversos, moriscos, esclavos, libertos, pícaros, ladrones, prostitutas, nobles y plebeyos. Pero también era una ciudad de sombras por la desigualdad, las hambrunas y epidemias. En medio de un brote de peste, varios miembros destacados de la sociedad sevillana aparecen asesinados. Mateo, condenado por la Inquisición, debe resolver esta serie de crímenes diabólicos para lograr el perdón del Santo Oficio y así salvar su vida. Una investigación a vida o muerte en un entorno de represión pública y hedonismo privado; de misticismo y caos; de conventos relajados y burdeles reglamentados; de cárceles como escondite; de hospitales como tumbas; de traiciones y lealtades.</info>
		<year>2017</year>
		<genre>Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/jFBSKRerBqcvGHnDQZykrgTlzRh.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1cDUGdFL6QObHkdPLES2DICUx6m.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/jFBSKRerBqcvGHnDQZykrgTlzRh.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/v1P38jNNb4DDvfHfHhujFoV1GB3.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>ca1973c0c1e9b0e88c2fc54bbfaef70ed23fbc4a</id1.1>
		</t1>
		<t2>
			<thumb.2>https://i.imgur.com/W7bdCnP.jpg</thumb.2>
			<fanart.2>https://i.imgur.com/yCcCaQ2.jpg</fanart.2>
			<ep2.1>1 EL NUEVO MUNDO</ep2.1>
			<id2.1>d6c5d1535589056837f763a15f28a8c6fdf72687</id2.1>
			<ep2.2>2 ESCALANTE</ep2.2>
			<id2.2>a102b46b8e601ad4267d9573c8bb67d4d87a3962</id2.2>
			<ep2.3>3 PONTOCORVO</ep2.3>
			<id2.3>7443884030b8bd994c973be293a996902b5769c3</id2.3>
			<ep2.4>4 EUGENIA</ep2.4>
			<id2.4>2d4a4f4085e9d15862eb85761091fdb87dd894aa</id2.4>
			<ep2.5>5 CONRADO</ep2.5>
			<id2.5>e5f674509783a3fed07be22fc09d0cd9e245fcc3</id2.5>
			<ep2.6>6 EL VIEJO MUNDO</ep2.6>
			<id2.6>64d0c419c7394a9181ec55f6ba7cc8298219cbe6</id2.6>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA PRIMERA MUERTE (FIRST KILL)</title>
		<info>(2022) 8 episodios. Cuando llega el momento de que la vampira adolescente Juliette haga su primera muerte para poder ocupar su lugar entre una poderosa familia de vampiros, pone su mirada en una nueva chica en la ciudad llamada Calliope.</info>
		<year>2022</year>
		<genre>Drama. Terror. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/70yK1hRyyQiwqstpMTHZCpcnP7.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/zmVWlrXNwEtCBuKxXBL7QBrh61n.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/70yK1hRyyQiwqstpMTHZCpcnP7.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/zmVWlrXNwEtCBuKxXBL7QBrh61n.jpg</fanart.1>
			<ep1.1>1 PRIMER BESO</ep1.1>
			<id1.1>80740ec01125434986f0e5951a2ebdbc26703b5c</id1.1>
			<ep1.2>2 PRIMERA SANGRE</ep1.2>
			<id1.2>1a37164a95d4413f92cf83cf1c5eb5ff10f378d3</id1.2>
			<ep1.3>3 PRIMERA VISTA</ep1.3>
			<id1.3>5a1b82e900bf78a6fb729aa57cfc4dc1cc94b524</id1.3>
			<ep1.4>4 PRIMERA FECHA</ep1.4>
			<id1.4>ad5cf331cdfc9be6f43b3885bdc39ab537bb8f93</id1.4>
			<ep1.5>5 PRIMER AMOR</ep1.5>
			<id1.5>80694932f20c76a9f7cc219fb924d28bea0e62cc</id1.5>
			<ep1.6>6 PRIMERA SEPARACION</ep1.6>
			<id1.6>092fb43fb3585dcc7ec2f7d7291dbf739f0a1585</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA REINA DEL FLOW</title>
		<info>(2018) 2 temporadas. 171 episodios.Después de diecisiete años injustamente condenada a prisión, Yeimy Montoya llega a un acuerdo para conseguir su libertad y así poder empezar una venganza contra el hombre que amaba. Para conseguir su objetivo Yeimy cambia de identidad y pasa a llamare Tammy Andrade, una productora musical que busca nuevos talentos dentro de la industria. Charly Flow, el hombre del que estaba enamorado Yeimy, es uno de los cantantes con más éxito de toda Colombia. Además de ser el responsable del encarcelamiento de Yeimy, el músico adinerado robó canciones a la joven. Ahora, casado y con una hija, su nueva vida gira en torno a la música y su trabajo con su tío Manií, uno de los narcotraficantes más importantes de Medellín.</info>
		<year>2018</year>
		<genre>Drama. Musical</genre>
		<thumb>https://www.themoviedb.org/t/p/original/iv6DfDOu6bCe616G33fR06As1a2.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/kfhvcdHZbsUUlf7wQfQremkQHYc.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/mq7QQnmEmLVIJq5ZMsbFBEj4udM.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/lKGi8u6l5PJVSAdOazHjt0n3wHZ.jpg</fanart.1>
			<ep1.1>1 AL 21</ep1.1>
			<id1.1>5b3b7b19dfa8fe73b98f7acc3bbfd5b2cc572512</id1.1>
			<ep1.2>22 AL 45</ep1.2>
			<id1.2>48b4dc9f4f778dc5fc3ec69cdff009551c924fa1</id1.2>
			<ep1.3>46 AL 67</ep1.3>
			<id1.3>11313542729602d7e444e4d8deb26b813a0c4a18</id1.3>
			<ep1.4>68 AL 82</ep1.4>
			<id1.4>2ae5d5c1af578f751f3f8d2b8baf04a97c01b459</id1.4>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/marMIUOlZdDKh7CuVeor7QlvAMe.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/69fke6HBMjueYde4Zn7dVVtImEj.jpg</fanart.2>
			<ep2.1>1 AL 10</ep2.1>
			<id2.1>089AA0511E37B0EEE782ED42013068CD44299C63</id2.1>
			<ep2.2>11 AL 20</ep2.2>
			<id2.2>C195C0B48FEE6C5923D4C66AF2C72AEF781EE19E</id2.2>
			<ep2.3>21 AL 30</ep2.3>
			<id2.3>33B33F5D74F49503FC22C4CA4B094CB494BAAE04</id2.3>
			<ep2.4>31 AL 40</ep2.4>
			<id2.4>E68C8143B349DFA878B45EE918143DF07075AEC3</id2.4>
			<ep2.5>41 AL 50</ep2.5>
			<id2.5>CCBDFB7E4B1D4AEC8A28D3EA43364867A39A1580</id2.5>
			<ep2.6>51 AL 60</ep2.6>
			<id2.6>B2719DD935DA77358801F5357AE47DF99FEDC5B0</id2.6>
			<ep2.7>61 AL 70</ep2.7>
			<id2.7>7C812A3E3EB540EE90BE34E4231F3750B58E302D</id2.7>
			<ep2.8>71 AL 80</ep2.8>
			<id2.8>B8CE997231E3DAE6377043AD1DFF52B2AC805DA6</id2.8>
			<ep2.9>81 AL 89</ep2.9>
			<id2.9>4F6C728EFCD1ED2AF605E147C169E83E5560F14F</id2.9>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA RUEDA DEL TIEMPO</title>
		<info>(2021) 1 temporada. 8 episodios. Serie basada en la popular serie literaria de fantasía de Robert Jordan, "La rueda del tiempo (The Wheel of Time), con más de 90 millones de libros vendidos. Ambientada en un extenso y épico mundo en el que existe la magia y únicamente algunas mujeres pueden acceder a ella, la historia sigue a Moiraine, miembro de una organización femenina increíblemente poderosa llamada 'Aes Sedai', cuando llega a la pequeña ciudad de Two Rivers. Allí se embarca en un peligroso viaje por todo el mundo con cinco jóvenes, uno de los cuales ha sido profetizado como "el dragón renacido", que salvará o destruirá a la humanidad. Confirmada una 2ª temporada.</info>
		<year>2021</year>
		<genre>Aventuras. Fantástico </genre>
		<thumb>https://www.themoviedb.org/t/p/original/7cVmgz7TzkZf64VD6Vc3AOZS1uA.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/1P3QtW1IkivqDrKbbwuR0zCYIf8.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7cVmgz7TzkZf64VD6Vc3AOZS1uA.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/1P3QtW1IkivqDrKbbwuR0zCYIf8.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>60F018D068A65F80A06C34875227958781AB4694</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA SANGRE HELADA</title>
		<info>(2021) 5 episodios. Siglo XIX. Patrick Sumner es un ex cirujano del ejército caído en desgracia que se inscribe como médico del barco en una expedición ballenera al Ártico. A bordo se encuentra con Henry Drax, el arponero, un brutal asesino cuya amoralidad ha sido moldeada para adaptarse a la dureza de su mundo. Tenía la esperanza de escapar de los horrores de su pasado, pero Sumner se encuentra con un viaje desafortunado y un psicópata asesino. En busca de la redención, su historia se convierte en una dura lucha por la supervivencia en el Ártico inclemente.</info>
		<year>2021</year>
		<genre>Aventuras. Drama. Intriga</genre>
		<thumb>https://i.imgur.com/lfmz2NQ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8IC1q0lHFwi5m8VtChLzIfmpaZH.jpg</fanart>
		<t1>
			<thumb.1>https://i.imgur.com/lfmz2NQ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8IC1q0lHFwi5m8VtChLzIfmpaZH.jpg</fanart.1>
			<ep1.1>1 OBSERVEN AL HOMBRE</ep1.1>
			<id1.1>60f0922197921cd5a598b96a72fb6cbb804627a6</id1.1>
			<ep1.2>2 LOS HOMBRES SOMOS SERES MISERABLES</ep1.2>
			<id1.2>10be6137b567ab7c13bd0e0b2857c3e5a495ba1d</id1.2>
			<ep1.3>3 HOMO HOMINI LUPUS</ep1.3>
			<id1.3>f4f2973bac98562b7b8670b168d2aa9e2736dcc3</id1.3>
			<ep1.4>4 LOS DEMONIOS DE LA TIERRA</ep1.4>
			<id1.4>1918c3694c62a42c3714d6afd7eb3ca531ff8e9c</id1.4>
			<ep1.5>5 VIVIR ES SUFRIR</ep1.5>
			<id1.5>d9f17acd278cd7f4fc42c40483ddc6ea6b85d1db</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA SERPIENTE DE ESSEX</title>
		<info>(2022) 6 episodios. Cora (Claire Danes) es una mujer recientemente viuda que, tras haberse liberado de un matrimonio abusivo, se muda del Londres victoriano al pequeño pueblo de Aldwinter en Essex. Allí se siente intrigada por una superstición local de que una criatura mítica conocida como la 'Serpiente de Essex' ha regresado a la zona.</info>
		<year>2022</year>
		<genre>Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/yz1BKQAh5mQfWlwmoPMRXjGPy41.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/wXPDaCGosh1BTHggpsoUQn2gBZi.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/yz1BKQAh5mQfWlwmoPMRXjGPy41.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/wXPDaCGosh1BTHggpsoUQn2gBZi.jpg</fanart.1>
			<ep1.1>1 EL BLACKWATER</ep1.1>
			<id1.1>12d59f55e37a5c57a045eca614700d76154ee04d</id1.1>
			<ep1.2>2 ASUNTOS DEL CORAZON</ep1.2>
			<id1.2>e990b507c343df94ab3ae073d9f4916ea41e5007</id1.2>
			<ep1.3>3 CAER</ep1.3>
			<id1.3>cbe66520d7dce290b0446fda19f699fed444c0c5</id1.3>
			<ep1.4>4 TODO ES AZUL</ep1.4>
			<id1.4>005ce6abdfff887179ba45ed8217f4743c749f53</id1.4>
			<ep1.5>5 ROMPO COSAS</ep1.5>
			<id1.5>f4e5364f1c9e472bbd90b37fead0d3bc8d566dd8</id1.5>
			<ep1.6>6 SUPERFICIE</ep1.6>
			<id1.6>4a6f922f68ccf8a7fd9d562e45fde0e8e8fc7d18</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA TEMPLANZA</title>
		<info>(2021) 10 epiosdios . Ambientada a finales del siglo XIX, narra la historia de Mauro Larrea y Soledad Montalvo, un hombre y una mujer hechos a sí mismos cuyos destinos están a punto de converger en un lugar y un tiempo fascinantes. Empezando por las tumultuosas comunidades mineras de México en el siglo XIX, pasando por los elegantes salones de la más exclusiva sociedad londinense, para llegar a la vibrante Cuba de la trata de esclavos y, de allí, a un Jerez glorioso en el que se encontraban las bodegas más importantes del mundo, la historia habla sobre la superación de la adversidad y sobre cómo encontrar nuestro lugar en el mundo.</info>
		<year>2021</year>
		<genre>Drama. Romance</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gGP0ShpdWQMqaW2yaCWG6T2UefG.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/ukGFBfAZz3Q3WnppbSszXKUtTLU.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gGP0ShpdWQMqaW2yaCWG6T2UefG.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/ukGFBfAZz3Q3WnppbSszXKUtTLU.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>c38e64ace9c5bb6ba563c9b373244339f78aa05d</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA TUMBA (THE GRAVE)</title>
		<info>(2019) 8 episodios. La Tumba es una serie thriller que sigue la historia de tres personas que supuestamente han sido encontradas muertas, pero que en realidad siguen vivas. Tras un terremoto en el norte de Israel, tres esqueletos humanos salen a la luz. Los resultados de las pruebas de ADN muestran que estas tres personas siguen vivas: Yoel, un trabajador de una reserva natural; Niko, un artista sensorial capaz de adivinar el pasado de las personas; y Aavigail, una reclusa que cumple condena en la cárcel.</info>
		<year>2019</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://i.imgur.com/pGFDHGd.jpg</thumb>
		<fanart>https://i.imgur.com/pGFDHGd.jpg</fanart>
		<t1>
			<thumb.1>https://i.imgur.com/pGFDHGd.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/pGFDHGd.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>a30d2acde3777b1dac29e58bd484f4424e022946</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>e00231254232d4c3bf20c8496e449032881811f3</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>fd5ffb4308b2f78056f140654ac46c2b619812fe</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>c7f44e1a3421302db2bebc04c058645ccac4ad3e</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>654bf654347b58f92fb8965692ddb06491de78ad</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>e3d53dbc0104e76dcb54553ca9b3256360c83fbd</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>ddde63cc9f754d2f610783f954265a6d2b47b323</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>802ed69807279e75b7a698c1f21d27ae2953d61e</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA UNIDAD</title>
		<info> (2020) 2 temporadas. 12 episodios. Thriller policíaco basado en el trabajo de una unidad de élite de la Policía Nacional especializada en terrorismo yihadista. La detención en España del líder terrorista más buscado del mundo convierte al país en el principal objetivo terrorista por parte de sus seguidores. Ha empezado una cuenta atrás sin que la ciudadanía lo sepa, y los miembros de la Unidad, liderados por la comisaria Carla Torres, se enfrentan a la misión secreta de intentar desarticular una célula yihadista a contrarreloj, mientras intentan resolver los conflictos de unas vidas personales que su oficio, en parte, les ha arrebatado.</info>
		<year>2020</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/7zFwsYtE7tHAIWkRMHedJHISRUL.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dOIbG31ozNRJ3MRfCg6gDh3e3cD.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7zFwsYtE7tHAIWkRMHedJHISRUL.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/dOIbG31ozNRJ3MRfCg6gDh3e3cD.jpg</fanart.1>
			<ep1.1>1 AL 6 (1080)</ep1.1>
			<id1.1>00C880C89BDA1DA2D3984C0140DA26DD220C8CA8</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/gCcR7YiIiBab2HP5lC5ZH6q4eDh.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/xs0J9Mz5s3dKSRO7aFfZDCi4UpF.jpg</fanart.2>
			<ep2.1>1 AL 6 (1080)</ep2.1>
			<id2.1>4F71DD877121D1DB0001CBCAC416FA1FDD8A9F2D</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA VERDAD SOBRE EL CASO HARRY QUEBERT</title>
		<info>(2018) 10 episodios. Un célebre escritor norteamericano, Harry Quebert (Patrick Dempsey), es acusado del asesinato de una adolescente después de que el cadáver de ésta aparezca enterrado en su jardín. Marcus Goldman (Ben Schnetzer), un joven y exitoso novelista y antiguo pupilo de Quebert, tratará de esclarecer el caso.</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/kgUogaLrvHjpbI80MPmNhGyBf1R.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/y7DxRMRIiqToFtj9yxSXyoMWO3c.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/kgUogaLrvHjpbI80MPmNhGyBf1R.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/y7DxRMRIiqToFtj9yxSXyoMWO3c.jpg</fanart.1>
			<ep1.1>1 COMO CRECE EL JARDIN</ep1.1>
			<id1.1>a3fc9035ef4e9b10979cce72c87a676522162776</id1.1>
			<ep1.2>2 EL COMBATE DE BOXEO</ep1.2>
			<id1.2>bd6632c0a51c1885faa374444fe4bf5746c372d2</id1.2>
			<ep1.3>3 EL 4 DE JULIO</ep1.3>
			<id1.3>6c856e62aedd743fd62adbdced7269512476ad95</id1.3>
			<ep1.4>4 LA FAMILIA IMPORTA</ep1.4>
			<id1.4>f127e2a5ec271d9274b01a6d6929feb2de3ec569</id1.4>
			<ep1.5>5 ESPEJITO, ESPEJITO</ep1.5>
			<id1.5>f88de3c170b1113e883c2852824b9941f4501e1f</id1.5>
			<ep1.6>6 SIN ANGEL</ep1.6>
			<id1.6>0eb3512afe6d5347157f1a05067b7cdfd191cd89</id1.6>
			<ep1.7>7 PERSONA NON GRATA</ep1.7>
			<id1.7>a357176a92f5287dfd9e0f5d664678b95df36d98</id1.7>
			<ep1.8>8 TODO MAL</ep1.8>
			<id1.8>ae26c445df027bb4608848532d5befb1cbf74744</id1.8>
			<ep1.9>9 PIROMANO</ep1.9>
			<id1.9>58558c24f0e638cac2fd141c4ff7c8ef939123fa</id1.9>
			<ep1.10>10 FIN</ep1.10>
			<id1.10>2a6775634c14e57e5628b5574724f52838218391</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA VICTIMA NUMERO 8</title>
		<info>(2018) 8 episodios. Un atentado de naturaleza yihadista en pleno casco viejo de Bilbao acaba con la vida de siete personas y deja malheridas a más de una treintena, muchas de ellas de gravedad. Víctimas y verdugos serán a partes iguales los protagonistas de una trama en clave personal. Narrada a modo de thriller, la historia trata cómo esos personajes viven las consecuencias de la barbarie. La investigación policial se centrará en atrapar a los responsables de la matanza. Una cacería en la que nuestros personajes se verán inmersos en una vertiginosa espiral repleta de giros insospechados.</info>
		<year>2018</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/l1LLEnNQJcTsHlmGK4qzp8WpNNZ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/xPFrqATs5zBgWxEWOV8BdoeuaH7.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/l1LLEnNQJcTsHlmGK4qzp8WpNNZ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/xPFrqATs5zBgWxEWOV8BdoeuaH7.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>2ae1d6543ab7b2db08dbf42cbe23b25c29a69de1</id1.1>
			<ep1.2>1 OMAR</ep1.2>
			<id1.2>1351847ec1cc890c9ae822ae856bc0dc95e323eb</id1.2>
			<ep1.3>2 LOS MILAGROS NO EXISTEN</ep1.3>
			<id1.3>86901e01b6b39cdb20cdfc34aa7f90152f7acaf2</id1.3>
			<ep1.4>3 EMPEZAR A CREER</ep1.4>
			<id1.4>d9942c05c0b42f2a31448497acb57cf50a421683</id1.4>
			<ep1.5>4 EL TIRO DE GRACIA</ep1.5>
			<id1.5>c5dddefc1cf265fd13af003305567d20fa317f12</id1.5>
			<ep1.6>5 UNA TEORIA DE MIERDA</ep1.6>
			<id1.6>c38a5c155c4cdf050b9247fff180cbd39171ea81</id1.6>
			<ep1.7>6 UN TIBURON QUE NOS HAGA AUN MAS RICOS A TODOS</ep1.7>
			<id1.7>3f0dfa82fed0527c5f2ed53f51b5f38036bb1d53</id1.7>
			<ep1.8>7 EL SINDROME DEL NIDO</ep1.8>
			<id1.8>7ea41391fce7b60882487e79c8acb779e08660c3</id1.8>
			<ep1.9>8 MUY FACIL O MUY DIFICIL</ep1.9>
			<id1.9>b7a3fe0be3f84889d5b21f97ba0120edd60141ee</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LA VOZ MAS ALTA</title>
		<info>(2019) 7 episodios. Para poder entender los sucesos que llevaron al nacimiento del Partido Republicano tal y como se conoce hoy en día, es imprescindible entender el ascenso de Roger Ailes, el fundador de Fox News. Esta serie de siete episodios indaga en la vida social del magnate y en algunos aspectos de su parte más personal, como en las acusaciones de acoso sexual que llevarían a su canal de televisión, otrora el rey de las noticias en los Estados Unidos, a caer en el olvido.</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/zEWNNN2JEKylPSUhH1JN3hNiJ92.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/7yGEbWaBa909mei8lzQ65mCFVu7.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/zEWNNN2JEKylPSUhH1JN3hNiJ92.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/7yGEbWaBa909mei8lzQ65mCFVu7.jpg</fanart.1>
			<ep1.1>1 1995</ep1.1>
			<id1.1>039061f8e45467078c2175c1c6e4dab20dc37b37</id1.1>
			<ep1.2>2 2001</ep1.2>
			<id1.2>98f286067e1fd5796cc13db510c364586a282af8</id1.2>
			<ep1.3>3 2008</ep1.3>
			<id1.3>82dd50e29f7aaa9237691a88a855f75b8412706d</id1.3>
			<ep1.4>4 2009</ep1.4>
			<id1.4>5fc6678dba5d57441ed4711661d133458dcfb109</id1.4>
			<ep1.5>5 2012</ep1.5>
			<id1.5>b13c50dc1f3d90732d7e1e17901c083410126457</id1.5>
			<ep1.6>6 2015</ep1.6>
			<id1.6>92bffa7783edabfc18a1131f7d4159d60069eab2</id1.6>
			<ep1.7>7 2016</ep1.7>
			<id1.7>ae0f204f9b95a83dd23726e456ab311a0cc1a5e5</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LAS CHICAS DEL CABLE</title>
		<info>(2017-2020) 5 temporadas. 42 episodios.  Madrid, año 1928. Las operadoras de la recién nacida Telefónica viven sus romances y envidias dentro de una empresa moderna, reflejo del cambio social de la época. Lidia, Marga, Ángeles y Carlota comienzan a trabajar como telefonistas en el edificio más moderno de toda la ciudad. Para ellas empieza la lucha por una independencia que tanto su entorno como la sociedad de entonces les niega. Su amistad será clave para conseguir sus sueños y juntas irán descubriendo lo que significa la verdadera libertad.</info>
		<year>2017</year>
		<genre>Drama. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/tVzN7TJvUc4B0dxoZPukXtVNsQm.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/aKDA2CMvyJdFf6ztVjZmW2T9OcV.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/tVzN7TJvUc4B0dxoZPukXtVNsQm.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/aKDA2CMvyJdFf6ztVjZmW2T9OcV.jpg</fanart.1>
			<ep1.1>1 LOS SUEÑOS</ep1.1>
			<id1.1>f0fa8f31f0ecd54ed7bfe733abce75eb09e00923</id1.1>
			<ep1.2>2 LOS RECUERDOS</ep1.2>
			<id1.2>b57db334cf8cec35633d5da41867754e940413c3</id1.2>
			<ep1.3>3 LAS MENTIRAS</ep1.3>
			<id1.3>5304e440f1eb5da03879741ae8653b804a80bcce</id1.3>
			<ep1.4>4 LOS SENTIMIENTOS</ep1.4>
			<id1.4>e207e65ab054d7924f1160cefc9b5a411a88a025</id1.4>
			<ep1.5>5 EL PASADO</ep1.5>
			<id1.5>46fefc44d4c997140d91775c808d6929693dd36a</id1.5>
			<ep1.6>6 LA FAMILIA</ep1.6>
			<id1.6>fc9357c6fde1de16474359fc419563c7532ea61d</id1.6>
			<ep1.7>7 LA PERDIDA</ep1.7>
			<id1.7>fdae73e4aae271c510e851d47dfebc63d4593e0c</id1.7>
			<ep1.8>8 EL AMOR</ep1.8>
			<id1.8>b8ae07c999a41aabf29f870606d9be0a2cd2cb74</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/oJtJ3cUMliSFrCgr9wmY15PG644.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/gzU6TQ70WYTa52yIuxBKFk875hx.jpg</fanart.2>
			<ep2.1>1 AL 8 (720)</ep2.1>
			<id2.1>cf3ed604144cf4ae7dd461f02a3de7768fdcce07</id2.1>
			<ep2.2>1 AL 8 (HDTV)</ep2.2>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/iMf2LpEsVWXIuYcvb1VwvXMx9bZ.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/bPtCJtfdt9hsabwEvEiiwYSzNi9.jpg</fanart.3>
			<ep3.1>1 AL 8 (720)</ep3.1>
			<id3.1>ae9fffacfe51b191fa1eedc3ec7cb0b648347f00</id3.1>
			<ep3.2>1 AL 8 (HDTV)</ep3.2>
			<id3.2>8380b34e33893d87d2f36cff526dfe464a5c5498</id3.2>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/keaY0N4oJI1P2GnxuksSWOpPYYw.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/oioWjm3C9waR3fMEkGwRJ6lnIO2.jpg</fanart.4>
			<ep4.1>1 AL 8</ep4.1>
			<id4.1>34aad9b9a15153ff4de28b3c427c9b464a01dc1c</id4.1>
			<ep4.2>1 LA IGUALDAD</ep4.2>
			<id4.2>5f98bef5833caa655e7040d655b65ba1266f21c9</id4.2>
			<ep4.3>2 LA LIBERTAD</ep4.3>
			<id4.3>1e4594324277a23bee86fed71e084cae32ae846c</id4.3>
			<ep4.4>3 LA JUSTICIA</ep4.4>
			<id4.4>aa46b5c87ea2c35d20460ed6da657ad25f2b1224</id4.4>
			<ep4.5>4 EL MIEDO</ep4.5>
			<id4.5>b1ffa00f8322adb9af514bdae4463dea80db06d3</id4.5>
			<ep4.6>5 LA VIDA</ep4.6>
			<id4.6>8d930c3710b0cc72bf5a5b3c4876a75ad888884b</id4.6>
			<ep4.7>6 LA DUDA</ep4.7>
			<id4.7>cecef8433165b32e6fbbf6cf714a3ea3bb8d7479</id4.7>
			<ep4.8>7 LA FELICIDAD</ep4.8>
			<id4.8>54fe5691c14427d9c7b39ddb87adbf2b3a7bd9df</id4.8>
			<ep4.9>8 LA SUERTE</ep4.9>
			<id4.9>a2647f8d4e41400e079e4babd4bd633173ea5bae</id4.9>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/vOaiWhBzT6PPC9AXhBq8VOOzOF1.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/iHLexEHCET03oGwe22gbHw0BfLL.jpg</fanart.5>
			<ep5.1>1 AL 5</ep5.1>
			<id5.1>086fae9c067b0f64cb899a47e0e9d6f5e8c327ab</id5.1>
			<ep5.2>6 AL 10</ep5.2>
			<id5.2>09116145d41ff7d4a9cb41074aef151eb10dfb14</id5.2>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LAS LUMINOSAS</title>
		<info>(2022) 8 episodios. Años 30. Kirby Mazrachi (Moss) es una archivista de periódicos de Chicago cuyo sueño de convertirse en periodista quedó en suspenso tras sobrevivir a un brutal ataque que la dejó habitando una realidad incierta. Cuando Kirby se entera de que un asesinato reciente está relacionado con su asalto, se une al veterano y atormentado reportero Dan Velazquez (Wagner Moura) para intentar descubrir la identidad de su atacante. A medida que se dan cuenta de que varios casos sin resolver están inextricablemente vinculados, la realidad borrosa de Kirby y sus traumas personales permiten que su agresor permanezca siempre un paso por delante de ellos.</info>
		<year>2022</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/j5KRj1junA9L6fg02L01uMGpGPB.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/1cEOLsPv7I1Dzo6i24u7PnR6TsF.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/j5KRj1junA9L6fg02L01uMGpGPB.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/1cEOLsPv7I1Dzo6i24u7PnR6TsF.jpg</fanart.1>
			<ep1.1>1 LINEA DE CORTE</ep1.1>
			<id1.1>89ce989fc9c8f1b946c245f9e804d1458b40f40a</id1.1>
			<ep1.2>2 HOJAS PERENNES</ep1.2>
			<id1.2>353749b4c129190dac1ab0f63642ca976ee58037</id1.2>
			<ep1.3>3 DURANTE LA NOCHE</ep1.3>
			<id1.3>cdf3a027a55c560d76676eab34ebd564e3abecfb</id1.3>
			<ep1.4>4 RECONOCIMIENTO</ep1.4>
			<id1.4>58be07f92bf7b491aae231750762b33ede5e1fd2</id1.4>
			<ep1.5>5 CHILLON</ep1.5>
			<id1.5>7543a0e6ffe05b065e35ca03502ddb6bdf394e13</id1.5>
			<ep1.6>6 BRILLANTE</ep1.6>
			<id1.6>387a198bbcacff6f80d74ca94667ac86b1bd9126</id1.6>
			<ep1.7>7 COMPENSAR</ep1.7>
			<id1.7>87273d7a09cfbd1e8af20731ca9142d039109f5a</id1.7>
			<ep1.8>8 30</ep1.8>
			<id1.8>c95e0b472e9b3178580d91fc8440d0126cadc232</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LEONARDO</title>
		<info>(2021) 8 episodios. Superproducción europea que explora los secretos y dramas detrás del genial artista italiano del Renacimiento. A lo largo de ocho capítulos y ocho de sus principales obras, desde la ‘Mona Lisa’ y su enigmática sonrisa al simbolismo de ‘La última cena’, la serie cuenta la historia del hombre y del genio, a través de una desconocida historia de misterio y pasión</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bsQNmTepBE5Iqcc0FMERs91XLO8.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/eu7Bicrl7M3T6MlTbov3IgOZzRh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bsQNmTepBE5Iqcc0FMERs91XLO8.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/eu7Bicrl7M3T6MlTbov3IgOZzRh.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>a1b973224e44307e658e4d2efd7b734b4eac17fe</id1.1>
			<ep1.2>2 AL 8</ep1.2>
			<id1.2>5240979adf8fbd89331580eb55e00c9e6e770ef9</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LIBERTAD</title>
		<info>(2021) 5 episodios. Una mujer apodada La Llanera sale de prisión tras 17 años. Durante su encierro, ha sido sentada todos los años en el garrote vil e indultada en el último momento. Su hijo Juan nació en la celda y no conoce a su padre, el célebre bandolero Lagartijo. Madre e hijo intentarán vivir la libertad que tantos años les ha sido negada mientras son perseguidos por cuadrillas de bandoleros y escopeteros del Gobernador. Lagartijo, Aceituno y el Gobernador buscan a La Llanera, pero también se buscan entre sí. En la España del siglo XIX no hay sitio para todos, y mucho menos para traidores.</info>
		<year>2021</year>
		<genre>Drama. Western</genre>
		<thumb>https://www.themoviedb.org/t/p/original/tB8UVEZYr5VRZKgt8mCTKsLveza.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8tpaM2xoOAX7yxsK6exvJpy1lic.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/tB8UVEZYr5VRZKgt8mCTKsLveza.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8tpaM2xoOAX7yxsK6exvJpy1lic.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>88d73cb65ad9ecc9c4ba98ba87ee0c502970b052</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LITTLE BIRDS</title>
		<info>(2020) 6 epsodios. Little Birds está ambientada en Tánger en 1955, en la emblemática "zona internacional", uno de los últimos resquicios de la decadencia colonial. Allí, Lucy Savage vive un 'shock' cultural al llegar a la ciudad marroquí. Lucy desea una vida poco convencional, libre de las ataduras de la sociedad en la que se encuentra, pero sabe que el camino hacia la libertad no será fácil. Adaptación de los relatos eróticos escritos por Anaïs Nin.</info>
		<year>2020</year>
		<genre>Drama. Romance</genre>
		<thumb>https://www.themoviedb.org/t/p/original/d0qM3ABz3gdTyUViSm7zOjfsAqz.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/iS7ZhboZegUrUcbFeaoZ9ZSBFGQ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/d0qM3ABz3gdTyUViSm7zOjfsAqz.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/iS7ZhboZegUrUcbFeaoZ9ZSBFGQ.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>d9ec7a4588039250889e8786d63e67d5ca071fdf</id1.1>
			<ep1.2>Episodio 2</ep1.2>
			<id1.2>1d0eebe57fc13cb2f45de60f85b91efa1be5897b</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>0dc76b16a56f92902e199cd947e2191bb493ce9f</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>e2ea6645907bcd5ec24124f1c5776e113d99f332</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>bc0a0a73877a387baf16d72f4dc94b3f48c7262b</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>9b39c78b6eb4a599cd002357a1c63771d626042b</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOCKE AND KEY</title>
		<info>(2020) 10 episodios. Después de la muerte de su padre, los hijos de Locke regresan a su casa ancestral en Nueva Inglaterra para descubrir que están rodeados de magia, que sólo ellos pueden ver y contra la que están destinados a pelear para terminar con los horrores que pueblan la ciudad de Lovecraft, Massachusetts.</info>
		<year>2020</year>
		<genre>Fantástico. Terror. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/OPL8TSMru7vqbxk8TwfBWCxByz.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1OTITGQ8yqerNEiwnHqpA7pD8Aq.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/OPL8TSMru7vqbxk8TwfBWCxByz.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/1OTITGQ8yqerNEiwnHqpA7pD8Aq.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>f4561b42a0dd1c1f4d8c7904a0e06b58717baa97</id1.1>
			<ep1.2>6 AL 10</ep1.2>
			<id1.2>8a0d071366358425b2eefaa7c39ee24b9fde2b46</id1.2>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/zuxGfRKziGHPogipnEXXykdDmyT.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/ptxU0BUnMxpIhVtcUWuUHscfQ1R.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>97fa0cfa2316f32c36da6a1c3dd75a498c4589cf</id2.1>
			<ep2.2>5 AL 7</ep2.2>
			<id2.2>0d2b745faa50fe1757c07d05d69f6840c24070b9</id2.2>
			<ep2.3>8 AL 10</ep2.3>
			<id2.3>b35969183700ecdb601708ecefd09ebba42067a2</id2.3>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOKI</title>
		<info>(2021) 6 episodios. Serie de Disney+ protagonizada por Loki, el personaje de las películas de Marvel al que Tom Hiddleston ha dado vida en films como Thor o Los Vengadores.</info>
		<year>2021</year>
		<genre>Ciencia ficción. Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/kAHPDqUUciuObEoCgYtHttt6L2Q.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/71kjcn5PYnNz3bMOZnGyuR4RH1V.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/kAHPDqUUciuObEoCgYtHttt6L2Q.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/71kjcn5PYnNz3bMOZnGyuR4RH1V.jpg</fanart.1>
			<ep1.1>1 AL 6 (1080)</ep1.1>
			<id1.1>63ba3df5907ce73ee613584d9a5e3886fb2aa4e6</id1.1>
			<ep1.2>1 AL 6 (4K)</ep1.2>
			<id1.2>8a0dd31921bca6f6e8f32d76ab9a8c353c924290</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOLA</title>
		<info>(2021) 4 episodios. Esta serie de no ficción sobre la vida y la carrera de Lola Flores se construye a partir del testimonio que nos han dejado sus hijas, su hermana, sus amigos, sus investigadores y muchos de los artistas actuales de diversa índole que se han visto influenciados por su arte como Rosalía, Miguel Poveda, C. Tangana o Ara Malikian (entre otros muchos).</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/nDHRI6pDMdxzZ1JDRnepKRYhmbv.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/ojFImfdk0ZpNphXXMeWn2YOW2JB.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/nDHRI6pDMdxzZ1JDRnepKRYhmbv.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/ojFImfdk0ZpNphXXMeWn2YOW2JB.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>6eb2e688b5d857956e82f1dfd82ff0c51c6de522</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS BORBONES : UNA FAMILIA REAL</title>
		<info>(2022) 6 episodios. ‘Los Borbones: una familia real’ muestra una mirada única y sin precedentes sobre la Familia Real Española, con imágenes y testimonios de gran impacto e interés público. El relato de ‘Los Borbones: una familia real’ se construye a partir del archivo histórico, imágenes y documentos inéditos, recreaciones y los testimonios de historiadores, escritores, periodistas, colaboradores y personas cercanas a la Casa Real.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/qISSENUWqr5S0ZVacTMOMpeVVTZ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dTZsoI9nreHwc1pV83D1h2igK4D.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qISSENUWqr5S0ZVacTMOMpeVVTZ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/dTZsoI9nreHwc1pV83D1h2igK4D.jpg</fanart.1>
			<ep1.1>1 UNA FAMILIA REAL</ep1.1>
			<id1.1>1DF7B88AA6D3F03C958BE495D4DEA63D60496E15</id1.1>
			<ep1.2>2 LOS BORBONES Y EL DINERO</ep1.2>
			<id1.2>2AFF9887D833216F495FD1A1D6390BD3BD1F8423</id1.2>
			<ep1.3>3 LOS BORBONES Y LAS MUJERES</ep1.3>
			<id1.3>A3869D4F78F1766C22B6F198583E82AF71C90BCA</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS BRIDGERTON</title>
		<info>(2020) 2 temporadas. 16 episodios. Los Bridgerton comienza en 1813 en Londres con Daphne Bridgerton (Phoebe Dynevor), la hija mayor de la poderosa familia Bridgerton, en su debut en el competitivo mercado matrimonial de la Regencia londinense. Con el ánimo de seguir los pasos de sus padres y encontrar el verdadero amor, las perspectivas de Daphne parecen inicialmente esperanzadoras. Pero todo empieza a desmoronarse cuando sale a la luz un diario repleto de escándalos sobre la alta sociedad escrito por la misteriosa Lady Whistledown que lanza calumnias sobre Daphne. La entrada en escena del rebelde Duque de Hastings (Regé-Jean Page), el soltero más deseado de la temporada, supone una válvula de escape para ambos, cuando deciden aliarse en una creciente batalla de ingenio para eludir las expectativas sociales sobre su futuro.</info>
		<year>2021</year>
		<genre>Drama. Romance</genre>
		<thumb>https://www.themoviedb.org/t/p/original/qaewZKBKmXjb4ZfFBb1LCug6BE8.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/m7FqiUOvsSk7Ulg2oRMfFGcLeT9.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qaewZKBKmXjb4ZfFBb1LCug6BE8.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/m7FqiUOvsSk7Ulg2oRMfFGcLeT9.jpg</fanart.1>
			<ep1.1>1 AL 8 (1080)</ep1.1>
			<id1.1>FB58087FE59C6C61262C23534932855C3895EA60</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/luoKpgVwi1E5nQsi7W0UuKHu2Rq.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/frH4DKCUK5RhpUahuEaTMt6PvoO.jpg</fanart.2>
			<ep2.1>1 AL 8 (1080)</ep2.1>
			<id2.1>18D8BC0D0499663B2C55687FD9F3AEF133993329</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS FAVORITOS DE MIDAS</title>
		<info>(2020) 6 episodios. Ambientada en el Madrid actual. La historia está inspirada en el relato corto de Jack London 'The Minions of Midas', publicado en 1901, en el que un gran empresario sufre un extraño chantaje: si no accede a pagar una elevada suma de dinero, los autodenominados "Favoritos de Midas" matarán a una persona al azar en la fecha señalada y añadirán una nueva víctima periódicamente hasta conseguir su objetivo.</info>
		<year>2020</year>
		<genre>Thriller. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/AjGZmFI6ykWRlfagcN2BrSYXTEQ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/fG34740e9GKhIDqMTlFSswZ97ik.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/AjGZmFI6ykWRlfagcN2BrSYXTEQ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/fG34740e9GKhIDqMTlFSswZ97ik.jpg</fanart.1>
			<ep1.1>1 al 6</ep1.1>
			<id1.1>34237d1217d74e1a3ffb2907c06fac7716f5086b</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS HEREDEROS DE LA TIERRA</title>
		<info>(2022) 8 episodios. El relato nos traslada a la Barcelona tardomedieval del año 1387, y que narra la historia de Hugo Llor, un muchacho que pasa la mayor parte de su tiempo en las calles. Su día también transcurre en las atarazanas y su sueño es convertirse en un artesano constructor de barcos, aunque su destino es incierto. La vida de Hugo no es fácil; es un chico muy solitario y su madre se ve obligada a alejarse de él, pero cuenta con el apoyo y la protección de un respetado anciano: Arnau Estanyol. Secuela de "La catedral del mar".</info>
		<year>2022</year>
		<genre>Drama. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/pUVJkvHYSNwNhlqo1obqZGCisms.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/9KjiHBfeQAoRGhk6ts5p4mFUM5T.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/pUVJkvHYSNwNhlqo1obqZGCisms.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/9KjiHBfeQAoRGhk6ts5p4mFUM5T.jpg</fanart.1>
			<ep1.1>1 DESTINO</ep1.1>
			<id1.1>24a34571d7e6c5cb4c747a7faf5d3e453fb68d36</id1.1>
			<ep1.2>2 PENITENCIA</ep1.2>
			<id1.2>f53ecf88e0537d314ee4532ae9efe92653dc473a</id1.2>
			<ep1.3>3 TALION</ep1.3>
			<id1.3>c5f9bb129932c6c6309c5a3d0fe1a69e660741a8</id1.3>
			<ep1.4>4 HERIDA</ep1.4>
			<id1.4>9fd54ce51d737494d69a7d3995d5ae9f2d384ccc</id1.4>
			<ep1.5>5 ESCLAVOS</ep1.5>
			<id1.5>cc8b806a6ef3a6fcfc578e3277f5c533da2cf1ed</id1.5>
			<ep1.6>6 BEATRIZ</ep1.6>
			<id1.6>934677d2e89ceb7d2679308bb049faaad1a578bc</id1.6>
			<ep1.7>7 VENENO</ep1.7>
			<id1.7>00ad56400f10a3902b565ad2bd461562b124fe63</id1.7>
			<ep1.8>8 REDENCION</ep1.8>
			<id1.8>da636b957379409f3262f5496b2cd24649b46e7c</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS IRREGULARES</title>
		<info>(2021)8 episodios. Un heterogéneo grupo de inadaptados investiga una serie de crímenes sobrenaturales en el Londres victoriano para el Dr. Watson y su misterioso socio, Sherlock Holmes.</info>
		<year>2021</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/qN6HCKR3ZRqbpGBWFctVlSTKR2k.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/edlaSNkxcpNy36WiGTO9AR1XtnB.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qN6HCKR3ZRqbpGBWFctVlSTKR2k.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/edlaSNkxcpNy36WiGTO9AR1XtnB.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>a2d0d731782c862dd54b6fd7b50491770cbb44b4</id1.1>
			<ep1.2>4 AL 8</ep1.2>
			<id1.2>42adce31d1c38c84788a286b6c7f76fe37971e72</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS RELOJES DEL DIABLO</title>
		<info>(2020) 5 episodios. Cuenta la historia real de Marco Merani (nombre ficticio), un mecánico especializado en motores de embarcaciones náuticas, algo que lo vincula sin saberlo a una banda de narcotráfico. Se acabará convirtiéndo en un infiltrado de la policía en la organización criminal.</info>
		<year>2020</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/k5Gt4wf0SOlZDPNDpt0Yj3vU2mm.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/Rh9mKoN786vNvohp1jjqwSeI11.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/k5Gt4wf0SOlZDPNDpt0Yj3vU2mm.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/Rh9mKoN786vNvohp1jjqwSeI11.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>fb86efbb9eb5c4ae4419a95149bb1aa95b720f31</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>475d056bd1cbee80e4e9d81d8e8334046cce83dd</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>e2ff0a0b06e27f390c0b4c6fe7391a953807c026</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>bc55b603d41d313befc950f0915621d5f14360b7</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>76aaedbe047d37c42737471619fc80acdfd86f4e</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS SECRETOS DE SULPHUR SPRINGS</title>
		<info>(2021) 2 temporadas. 19 episodios. Griffin Campbell, de 12 años, y su familia se trasladan a la pequeña ciudad de Sulphur Springs y se hacen con la propiedad de un hotel abandonado del que se rumorea que está embrujado por el fantasma de una niña que desapareció hace décadas. Griffin se hace amigo de Harper, una compañera de clase de ojos brillantes y obsesionada con el misterio, y juntos descubren un portal secreto que les permite viajar en el tiempo. En el pasado, intentarán descubrir la clave para resolver este misterio sin resolver, un misterio que afecta a todos sus allegados.</info>
		<year>2021</year>
		<genre>Aventuras. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/30MqitTOgIfeOhvwdMHkrM9p5mD.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5zTeE4HqzzhN3w6FgyfVMPgtkqc.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/30MqitTOgIfeOhvwdMHkrM9p5mD.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5zTeE4HqzzhN3w6FgyfVMPgtkqc.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>d8ee8611bb9d2b2989a36b7aa3e4bc5c4580233a</id1.1>
			<ep1.2>7 AL 11</ep1.2>
			<id1.2>fd40431fce3974c990b2e5272ecb8cb6726903ad</id1.2>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/2huc4Ef44MBNlbYMa2qjqFvyW2q.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/naudQkDg4MY2OFlYLBFU8Gq18Yt.jpg</fanart.2>
			<ep2.1>1 Episodio</ep2.1>
			<id2.1>d404d766d5e10d566732109b339bd92a158f1e56</id2.1>
			<ep2.2>2 Episodio</ep2.2>
			<id2.2>4e1406b255b412b72c96d3f7058d414e67f482d4</id2.2>
			<ep2.3>3 Episodio</ep2.3>
			<id2.3>af26cbcaaedf36d8ca52a18ef021926bc678db89</id2.3>
			<ep2.4>4 Episodio</ep2.4>
			<id2.4>d3c03fbdd5266874d63e4a55fb5a11af47207ee1</id2.4>
			<ep2.5>5 Episodio</ep2.5>
			<id2.5>e33b178244fa50acc1bd37f3d9b6ae60b17bc963</id2.5>
			<ep2.6>6 Episodio</ep2.6>
			<id2.6>cb7b1f1ec9283092f944504bc57efea5dd07da72</id2.6>
			<ep2.7>7 Episodio</ep2.7>
			<id2.7>ebfe495b62ae0d13813ef2a3f7ea873e9ba3b65e</id2.7>
			<ep2.8>8 Epsisodio</ep2.8>
			<id2.8>bfe3408d168a59703d4dbb246654dbe9af68662d</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOS ULTIMOS DIAS DE PTOLEMY GREY</title>
		<info>(2022) 6 episodios. Ptolemy Grey (Samuel L. Jackson) es un hombre de 91 años enfermo olvidado por su familia, por sus amigos e incluso por sí mismo. Cuando repentinamente se queda sin su cuidador de confianza y al borde de hundirse aún más en la demencia y en la soledad, Ptolemy es puesto al cuidado de la adolescente huérfana Robyn (Dominique Fishback). Cuando descubren un tratamiento que puede restaurar los recuerdos afectados por la demencia de Ptolemy, se embarcan un viaje hacia impactante verdades sobre el pasado, el presente y el futuro. </info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/dOMsBBhIRFb3KywESaKioqxvg2Z.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/bbVsHMmKt7FSFPiXqSnKpjYmdyh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/dOMsBBhIRFb3KywESaKioqxvg2Z.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/bbVsHMmKt7FSFPiXqSnKpjYmdyh.jpg</fanart.1>
			<ep1.1>1 REGGIE</ep1.1>
			<id1.1>7e0502d278c78070bb48e421cfc03adb977d8641</id1.1>
			<ep1.2>2 ROBYN</ep1.2>
			<id1.2>17457307d600a0eaec1c218ffac825803f6a1f79</id1.2>
			<ep1.3>3 SENSIA</ep1.3>
			<id1.3>fb74c4090be49c738064b639c2fc0e5b8b589f58</id1.3>
			<ep1.4>4 COYDOG</ep1.4>
			<id1.4>96cd803ec965da6b5f45cde21b505d89db5a6685</id1.4>
			<ep1.5>5 NINA</ep1.5>
			<id1.5>670139e6e8918a4a4bcaf037484c9c596166ee43</id1.5>
			<ep1.6>6 PTOLEMY</ep1.6>
			<id1.6>6519f575d3732a673abcb0917641bddc9031dc78</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LOVE DEATH AND ROBOTS</title>
		<info>(2019) 3 temporadas. 35  episodios. Love, Death &amp; Robots es una serie antológica animada para adultos que no tiene un género único. Esta ficción cuenta con géneros como la ciencia ficción, la fantasía, el terror o la comedia negra. Con un enfoque narrativo atrevido, las  historias que componen esta serie están llenas de energía, de acción y tienen un toque de humor negro.</info>
		<year>2019</year>
		<genre>Animación. Ciencia ficción. Fantástico. Terror. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/rlrRI2b6RvM9I9xOBTKqcTaehkE.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/e7VzDMrYKXVrVon04Uqsrcgnf1k.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/rlrRI2b6RvM9I9xOBTKqcTaehkE.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/e7VzDMrYKXVrVon04Uqsrcgnf1k.jpg</fanart.1>
			<ep1.1>1 AL 18</ep1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/od6z5tMT8BON1iakDdyulXiRKhG.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/daXzoOWNBwSoG03RFh5tEqzl1sH.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>cb796f05b25cbc57445ce95bf01c978d6e244040</id2.1>
			<ep2.2>5 AL 8</ep2.2>
			<id2.2>de588ccc44ef2abdd05e7261163a642eeb1a8fe4</id2.2>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/cRiDlzzZC5lL7fvImuSjs04SUIJ.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/78NtUwwo3lhH7QGh4vG3U1qK1mc.jpg</fanart.3>
			<ep3.1>1 Episodio</ep3.1>
			<id3.1>0e2c17b137f2a3184eb2f3b149ace763bf5653f1</id3.1>
			<ep3.2>2 Episodio</ep3.2>
			<id3.2>954bc7e27de2fb9ff5f4415865c44c2f2069e062</id3.2>
			<ep3.3>3 Episodio</ep3.3>
			<id3.3>745700794952928d04b1d7d881c04f47cd8e7850</id3.3>
			<ep3.4>4 Episodio</ep3.4>
			<id3.4>f33e61c224ca2672bbf2853a5ccc96fa04ac5472</id3.4>
			<ep3.5>5 Episodio</ep3.5>
			<id3.5>fc781beabdb3478fb9227d972544d0703875b99f</id3.5>
			<ep3.6>6 Episodio</ep3.6>
			<id3.6>ddaef21b9a8260019356369e6a7a2b4ba9ca1135</id3.6>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LUCIFER</title>
		<info>(2015) 6 temporadas. 93 episodios. La serie se centra en Lucifer (Tom Ellis) quien, aburrido e infeliz como el Señor del Infierno, dimite de su trono y abandona su reino para trasladarse a la ciudad de Los Ángeles y abrir un lujoso piano-bar llamado Lux. Una vez allí ayudará a la policía a castigar a los más peligrosos criminales de la ciudad.</info>
		<year>2015</year>
		<genre>Fantástico</genre>
		<thumb>https://image.tmdb.org/t/p/original/vkGnVBSNpayJ1oCfyVaMvhrim95.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/k9o7phlrza9YHqc6ByECYh7TcdZ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/58RBABIaWJLMxdYw8ToImTJsFaL.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/aqXNyPyymYFzbnR3yuD8laWwmzo.jpg</fanart.1>
			<ep1.1>1 AL 13</ep1.1>
			<id1.1>596DB29134843235A93C9FD9C6FFA42815A7C739</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/s6oLPnsa2OTmX7BtCv8YVIRxumD.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/ta5oblpMlEcIPIS2YGcq9XEkWK2.jpg</fanart.2>
			<ep2.1>1 AL 18</ep2.1>
			<id2.1>0E6B4C1898AD326609BDC7E9C21A09C65FC29541</id2.1>
			<id2.4>a8e38a8a67c49340cb89b6e5d17722f70c4a4307</id2.4>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/wmOuYyqVaczBLXxQNFSaRfAUgPz.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/vMQ0g1k4R7kgPf7LLMB28O4MNWP.jpg</fanart.3>
			<ep3.1>1 AL 26</ep3.1>
			<id3.1>1D23EE9857609B1E639EF527EBB76CFD2E1CD4D4</id3.1>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/1sBx2Ew4WFsa1YY32vlHt079O03.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/1JGDaB0rYcLXU17fbThEpsEJdYT.jpg</fanart.4>
			<ep4.1>1 AL 10</ep4.1>
			<id4.1>e0697907c2c397793a19e3f688d1a084f56a8818</id4.1>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/4EYPN5mVIhKLfxGruy7Dy41dTVn.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/mAXOCbZzvmDa6PCh5dcIPOB51Qc.jpg</fanart.5>
			<ep5.1>1 AL 4</ep5.1>
			<id5.1>51b532bd938f3fce250bf6c14811f9f5de00007c</id5.1>
			<ep5.2>LUCIFER LUCIFER LUCIFER</ep5.2>
			<id5.2>00e394f18f74839fe248b2008057ef9929b96197</id5.2>
			<ep5.3>5 AL 8</ep5.3>
			<id5.3>afec99e4d26432b9ec489cf8dace8b4ea3471094</id5.3>
			<ep5.4>9 AL 16</ep5.4>
			<id5.4>151cbe6c1d2f003ce2b755f88f4515f53ce6c916</id5.4>
		</t5>
		<t6>
			<thumb.6>https://www.themoviedb.org/t/p/original/wQh2ytX0f8IfC3b2mKpDGOpGTXS.jpg</thumb.6>
			<fanart.6>https://www.themoviedb.org/t/p/original/ccaZ3yyyC6rcMAQrlLZ51FpahNO.jpg</fanart.6>
			<ep6.1>1 AL 4</ep6.1>
			<id6.1>0709d0393590b0701f4663138c994748c72d1f59</id6.1>
			<ep6.2>5 AL 7</ep6.2>
			<id6.2>5fcaf0f4b35003e8c7ce8eacbd7b21920d52fcf1</id6.2>
			<ep6.3>8 AL 10</ep6.3>
			<id6.3>a6cff59bc0f6bbcf135da3788a9bb108e835c3c6</id6.3>
		</t6>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>LUPIN</title>
		<info>(2021) 2 temporadas. 20 episodios. Versión contemporánea del clásico francés, Omar Sy da vida a Assane Diop, ladrón de guante blanco y aficionado a las aventuras de Arsene Lupin que busca vengar la muerte de su padre.</info>
		<year>2021</year>
		<genre>Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/5Hke4jkFgD0loC6QBY7ym235Hq.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/lbWUCXIeoVXowAtLlAVR3aeHjkT.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/5Hke4jkFgD0loC6QBY7ym235Hq.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/lbWUCXIeoVXowAtLlAVR3aeHjkT.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>79d113a320a271bbc074cb87ac82a7f2f20e285e</id1.1>
		</t1>
		<t2>
			<thumb.2>https://i.imgur.com/46hSvzE.jpg</thumb.2>
			<fanart.2>https://i.imgur.com/t8cKhjM.jpg</fanart.2>
			<ep2.1>EMPEZAR POR EL CAPITULO 5 DESPUES YA ORDEN NORMAL</ep2.1>
			<ep2.2>1 AL 3</ep2.2>
			<id2.2>1685c62cf4f18a441f6dad69dbcdf3f67c9a52a5</id2.2>
			<ep2.3>4 AL 5</ep2.3>
			<id2.3>1288a4dc8c28ccb35571580689c30a40040ee4fb</id2.3>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MADRES: AMOR Y VIDA</title>
		<info>(2020) 4 temporadas. 42 episodios. Explora el universo femenino en un momento crítico para cualquier mujer que sea madre: la enfermedad de sus hijos.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/mfESZ1AmbWkcoBW1VYmLvk01DKq.jpg</thumb>
		<fanart>https://i.imgur.com/2haFqdD.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/sfrlBj2uBF5YHlGMaEKb5xmG7pH.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/2haFqdD.jpg</fanart.1>
			<ep1.1>1 AL 13</ep1.1>
			<id1.1>48bbf92a40cd3268343bcf5268a86284ec95036f</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/mpCLF6PKvBlH5ZVkrkn96EVgKKg.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/uBvBvoHVvF3H4UaffPh1JuysUZq.jpg</fanart.2>
			<ep2.1>1 AL 13</ep2.1>
			<id2.1>f84aeaae679659b5010e79475725533b8ff0d5ce</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/2CDEunrP0qFJDOqqlybvd6m32sO.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/gYASpmBed2KoeoDsykPCRC0pnUP.jpg</fanart.3>
			<ep3.1>1 BROTES</ep3.1>
			<id3.1>8e3c338b76288f55a54084471f71d0610f20dfcc</id3.1>
			<ep3.2>2 AL 5</ep3.2>
			<id3.2>76bc210bdf3212045bcccd0605d7aae53a6eefe4</id3.2>
			<ep3.3>6 AL 8</ep3.3>
			<id3.3>7e0602ca6e167c41ca5bb671fe301bab8952e06a</id3.3>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/fNgMPuYL0vklkJLjZruFflCXzk7.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/kF8H8HXUeTT42RJ0Kbx40eBhJ1W.jpg</fanart.4>
			<ep4.1>1 Episodio</ep4.1>
			<id4.1>002f0942537f4a2f1b48d6223de3e577c2199027</id4.1>
			<ep4.2>2 Episodio</ep4.2>
			<id4.2>1b7f48aa5651d56bd979545f858f33b2532a76b5</id4.2>
			<ep4.3>3 Episodio</ep4.3>
			<id4.3>948d41a592034a242924de4d388b387a315242c2</id4.3>
			<ep4.4>4 Episodio</ep4.4>
			<id4.4>a5152e8db027a1b9a7f52f095b2511e213a0d364</id4.4>
			<ep4.5>5 Episodio</ep4.5>
			<id4.5>ca6d1512d399c090fb5aa8ff9af99a2f5f67b7fd</id4.5>
			<ep4.6>6 Episodio</ep4.6>
			<id4.6>b0ffdde75d32dbbea64cee3bdbce23dca2fd08c5</id4.6>
			<ep4.7>7 Episodio</ep4.7>
			<id4.7>4c83e8e04e3c736164b1a9b7b274c2100dd4f728</id4.7>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MALDITA</title>
		<info>(2020) 10 episodios. Fantasía medieval que reescribe la leyenda del Rey Arturo desde un punto de vista completamente nuevo: a través de los ojos de una hechicera adolescente con un misterioso y poderoso don. Tras la muerte de su madre, Nimue emprende una expedición para encontrar a Merlín y entregarle una antigua espada, y halla un compañero inesperado en Arturo, un humilde mercenario. A lo largo de su viaje, Nimue se convertirá en símbolo de coraje y rebeldía contra los aterradores Paladines Rojos y su cómplice, el rey Uter.</info>
		<year>2020</year>
		<genre>Thriller. Fantástico</genre>
		<thumb>https://image.tmdb.org/t/p/original/VNWWEwDQqZFPzwY17doboxBKRK.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/jIZTOHGp5jUlzOh7Uaculusrpkc.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/VNWWEwDQqZFPzwY17doboxBKRK.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/jIZTOHGp5jUlzOh7Uaculusrpkc.jpg</fanart.1>
			<ep1.1>1 NIMUE</ep1.1>
			<id1.1>86fd4e5d7a9abf46adc43ee876c14f5c8e7fd71c</id1.1>
			<ep1.2>2 MALDITA</ep1.2>
			<id1.2>c4b19c22ab9d1426c8f328c4bca67c9060651fd5</id1.2>
			<ep1.3>3 SOLEDAD</ep1.3>
			<id1.3>c068c79d8db77e9fc742473f0a4cfb276c65bbb9</id1.3>
			<ep1.4>4 EL LAGO ROJO</ep1.4>
			<id1.4>e002c0d1164dda2fa6226bae07bcb1b56ee7ebd5</id1.4>
			<ep1.5>5 LA UNION</ep1.5>
			<id1.5>ec2201bcb5409614c88a2f98889659538476f585</id1.5>
			<ep1.6>6 FESTA Y MOREII</ep1.6>
			<id1.6>edc484af97b908cba0d6ac8f57c3821543e5afeb</id1.6>
			<ep1.7>7 TRAENOS BUENA CERVEZA</ep1.7>
			<id1.7>199714de6b902957f7dc7aa732f1ff1c2305dd86</id1.7>
			<ep1.8>8 LA REINA FEY</ep1.8>
			<id1.8>6b1d0c93eb65a84476acce3016ab3a8011059c3d</id1.8>
			<ep1.9>9 VENENOS</ep1.9>
			<id1.9>7a4bb91afac0f80015b6cb95cf6e200cece33d34</id1.9>
			<ep1.10>10 EL SACRICIO</ep1.10>
			<id1.10>f3ad289014806758b7058418d04649bd99b7b62c</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MANIFEST</title>
		<info>(2018) 3 temporadas. 42 episodios. Cuando el vuelo 828 de Montego Air aterriza de manera segura después de un vuelo turbulento pero de rutina, la tripulación y los pasajeros se sienten aliviados. Sin embargo, aunque para ellos han pasado pocas horas, el mundo ha envejecido cinco años y sus amigos, familias y colegas, después de llorar su pérdida, han perdido la esperanza y están intentando seguir adelante. Ahora, frente a lo imposible, a todos se les da una segunda oportunidad. Pero, a medida que se aclaran sus nuevas realidades, se enfrentan a un misterio más profundo y algunos de los pasajeros pronto se dan cuenta de que pueden estar destinados a algo más grande de lo que alguna vez creyeron posible.</info>
		<year>2018</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/moaCMoZYVifaQnNJGDr3M6rBglB.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/gvQL4qXWDD50FC8rXwblReMWHBP.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/moaCMoZYVifaQnNJGDr3M6rBglB.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/gvQL4qXWDD50FC8rXwblReMWHBP.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>a170e2c15e8a5724283844cc7c19c1e0865bc65e</id1.1>
			<ep1.2>2 REENTRADA</ep1.2>
			<id1.2>62de7f6d4625ec5295f7044b2168b8bf8e76ea30</id1.2>
			<ep1.3>3 TURBULENCIAS</ep1.3>
			<id1.3>0421d633976d869659c053ee3f2dbc258b2b81d9</id1.3>
			<ep1.4>4 EQUIPAJE SIN RECLAMAR</ep1.4>
			<id1.4>6122b36bbd5d7ad907aabbad5a5c4a93e8517809</id1.4>
			<ep1.5>5 VUELOS SIN CONEXION</ep1.5>
			<id1.5>659f95d5ea0e2e368ec6f31e3ba8fcd080ab81df</id1.5>
			<ep1.6>6 FUERA DE RADAR</ep1.6>
			<id1.6>68a8f79f1795255c1b2bbff6ecf530ff6de63722</id1.6>
			<ep1.7>7 SITUACION COMPLICADA</ep1.7>
			<id1.7>81a9ef24b3f3c4e7239e8635cc85c2fc6578a75e</id1.7>
			<ep1.8>8 PUNTO DE NO RETORNO</ep1.8>
			<id1.8>f02cb9aa02b167a458a2d62ea42bdf4842d6691e</id1.8>
			<ep1.9>9 NAVEGACION POR ESTIMA</ep1.9>
			<id1.9>1944bbb12b820b7e1982db31dc3b1b3bab9129a1</id1.9>
			<ep1.10>10 VIENTO DE COSTADO</ep1.10>
			<id1.10>60f9803cc3b64d65ffa4ffc5971dc6b3706f62c6</id1.10>
			<ep1.11>11 ESTELAS</ep1.11>
			<id1.11>3e1fbb836043d996ed22e9f0a5d00cbbe5bab271</id1.11>
			<ep1.12>12 PUNTO DE FUGA</ep1.12>
			<id1.12>ae9623d2f20a5e91263dfdb07f134a59456e8816</id1.12>
			<ep1.13>13 AUTORIZACION PARA LA APROXIMACION</ep1.13>
			<id1.13>19d059ce677596b0086d87e10d98c38e5862c51e</id1.13>
			<ep1.14>14 MEJORAR</ep1.14>
			<id1.14>15f2de7593bf9903a34f881b78b73e23ce2f3918</id1.14>
			<ep1.15>15 ATERRIJACE FORZOSO</ep1.15>
			<id1.15>6264c62cdcabf96c5f89cb8b0871464066d69fe4</id1.15>
			<ep1.16>16 TIEMPO ESTIMADO DE DESPEGUE</ep1.16>
			<id1.16>ecfc74f42add142504b2d4b3dbb53da1a263c5c5</id1.16>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/1xeiUxShzNn8TNdMqy3Hvo9o2R.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/3ib0uov9Qq9JtTIEGL39irTa3vZ.jpg</fanart.2>
			<ep2.1>1 ABROCHENSE LOS CINTURONES</ep2.1>
			<id2.1>9824f6a62b06d6fb3ca297444c4a93069b772059</id2.1>
			<ep2.2>2 EN TIERRA</ep2.2>
			<id2.2>9a2d5e76e59784cc5d17c9bcd44dce9015245b98</id2.2>
			<ep2.3>3 FALSO HORIZONTE</ep2.3>
			<id2.3>d674090d2a155cc18194a33434f2031d885c4bd3</id2.3>
			<ep2.4>4 CAJA NEGRA</ep2.4>
			<id2.4>368744be6a823815edd3a43ebab56e837f89d93b</id2.4>
			<ep2.5>5 VUELO COORDINADO</ep2.5>
			<id2.5>6ea08b86f651fdc18c9be08c15840cb782665ff6</id2.5>
			<ep2.6>6 VIAJE DE VUELTA</ep2.6>
			<id2.6>cffa75c42a1790ad5fd0806b165ff038482a2f73</id2.6>
			<ep2.7>7 SALIDA DE EMERGENCIA</ep2.7>
			<id2.7>7ef5ddfc4e60b43748e7b50b3944cb18ecd9f0c2</id2.7>
			<ep2.8>8 SIGUE ADELANTE</ep2.8>
			<id2.8>fb826092775c0b11981b57b6f2406ad528a78ec7</id2.8>
			<ep2.9>9 BOTELLAS DE AVION</ep2.9>
			<id2.9>450332a3c43dac44647df2b1962d64148fc7e104</id2.9>
			<ep2.10>10 DESVIACION DEL CURSO</ep2.10>
			<id2.10>525fc137864689302fb70061b9ac59fa017681e3</id2.10>
			<ep2.11>11 AL 13</ep2.11>
			<id2.11>232be2dcde511d1e1aabea8f26538988343db648</id2.11>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/nSY1JXE5ObgCZMbp7v6U1dVCbHp.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/p68WrxXPgmhjuaYetTiVKLUb65s.jpg</fanart.3>
			<ep3.1>1 TAILFIN</ep3.1>
			<id3.1>e627bd41bdd3f0f39826645746961fc23bec4919</id3.1>
			<ep3.2>2 POLIZON</ep3.2>
			<id3.2>a563b07d8536b55187161c949142be3082265d6d</id3.2>
			<ep3.3>3 WINGMAN</ep3.3>
			<id3.3>416d13a9e873b860fd3e87171f3ed102d9db0632</id3.3>
			<ep3.4>4 TAILSPIN</ep3.4>
			<id3.4>95d7f3f67e1697fc63c29d12566867501ef5d64f</id3.4>
			<ep3.5>5 AL 6</ep3.5>
			<id3.5>ca1c90def199fcb5c5f492ef84405577cbf3d92f</id3.5>
			<ep3.6>7 AL 9</ep3.6>
			<id3.6>7a21a85997bae6ee8b6451c6914edd8a0b69f890</id3.6>
			<ep3.7>10 CALIBRACION DE BRUJULA</ep3.7>
			<id3.7>2ee97c31b9e98323ad4176949d446bd4640a268b</id3.7>
			<ep3.8>11 LIBRE DE DERECHOS</ep3.8>
			<id3.8>a7ee7feea0343cc99795ff84125d7f8ef2548710</id3.8>
			<ep3.9>12 MAYDAY (1)</ep3.9>
			<id3.9>47b6775f29ef09cab5bfa7d8d0eece578d835e0e</id3.9>
			<ep3.10>13 MAYDAY (2)</ep3.10>
			<id3.10>b936af94726923f1c52dc3ce524eefc105d7a0be</id3.10>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MAR DE LA TRANQUILIDAD</title>
		<info>(2021) 8 episodios. Una científica se une a una misión para recuperar unas muestras de una estación lunar, escenario de un accidente que acabó con todos, incluida su hermana.</info>
		<year>2021</year>
		<genre>Ciencia ficción. Terror. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/rxnfJEOwWlPaUdALv5YowGTD3Hd.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/9hNJ3fvIVd4WE3rU1Us2awoTpgM.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/rxnfJEOwWlPaUdALv5YowGTD3Hd.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/9hNJ3fvIVd4WE3rU1Us2awoTpgM.jpg</fanart.1>
			<ep1.1>1 ESTACION LUNAR DE INVESTIGACION BALHAE</ep1.1>
			<id1.1>e9886d61036c9a7bd4737f9ad57f2c7effb76d2b</id1.1>
			<ep1.2>2 TRES DEPOSITOS</ep1.2>
			<id1.2>f3bf7cea93148be41d90b3b650f1988892de8955</id1.2>
			<ep1.3>3 CAUSA DE LA MUERTE</ep1.3>
			<id1.3>017bae115cc5c789e0b3b33dd035f1539cf0849f</id1.3>
			<ep1.4>4 LA VERDAD SALE A LA LUZ</ep1.4>
			<id1.4>dbd5a8f38a91d1917fc34290894ec8fb862fcdbb</id1.4>
			<ep1.5>5 DEPOSITO SECRETO</ep1.5>
			<id1.5>c46fde2c815f2cee6ee4f8cb07b58caec3e680cd</id1.5>
			<ep1.6>6 CLAVE PARA LA SALVACION</ep1.6>
			<id1.6>b968d7a178029cc0c39ae8aa73cdefa8fc7f2a66</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MARADONA SUEÑO BENDITO</title>
		<info>(2021) 10 episodios. Serie de ficción en la que tres diferentes actores interpretan al argentino Diego Armando Maradona (1960-2020) a lo largo de su vida y prolífica carrera; desde sus humildes comienzos en el barrio bonaerense de Fiorito, partido de Lomas de Zamora, y pasando por su carrera como futbolista en el Barcelona y el Napoli, entre otros acontecimientos destacados de su vida. Además, la serie mostrará su papel clave en la victoria de su selección nacional en el Mundial de Fútbol en México ‘86.</info>
		<year>2021</year>
		<genre>Drama. Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/mGPdWEEmfzP7VQBQsXrFt1b1ikQ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/auuN4uAvn6KiqyYX5r7BsXrMPNx.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/mGPdWEEmfzP7VQBQsXrFt1b1ikQ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/auuN4uAvn6KiqyYX5r7BsXrMPNx.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>1de2fea634183258924dd77ed58442d632905df1</id1.1>
			<ep1.2>4 AL 7</ep1.2>
			<id1.2>4f7e09ed67f13e6d8759cf2a4d5f77d982c1bb95</id1.2>
			<ep1.3>8 CRISTIANA</ep1.3>
			<id1.3>2c61de8d203cc78fd58a7e904c2c96c7e5469576</id1.3>
			<ep1.4>9 CAPITAN</ep1.4>
			<id1.4>243c99629f3204471d1081555f201b0afce83b19</id1.4>
			<ep1.5>10 DIOS</ep1.5>
			<id1.5>06e59b65d09e6da98e8bb0fa57420095fd7e07fe</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MARE OF EASTTOWN</title>
		<info>(2021) 7 episodios. Una detective de un pequeño pueblo de Pennsylvania investiga un asesinato local mientras intenta que su vida no se desmorone.</info>
		<year>2021</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/78aK4Msbr22A5PGa6PZV0pAvdwf.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/gxvm7iyWSpaXLQZIpThBloSsJnW.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/78aK4Msbr22A5PGa6PZV0pAvdwf.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/gxvm7iyWSpaXLQZIpThBloSsJnW.jpg</fanart.1>
			<ep1.1>1 LA SEÑORITA LADY HAWK EN PERSONA</ep1.1>
			<id1.1>7eba5c1f652550d0d67a39762b8dcc4a9727ca00</id1.1>
			<ep1.2>2 PADRES</ep1.2>
			<id1.2>23f219b332f38bfe371175725368d2d672b111d4</id1.2>
			<ep1.3>3 INTRODUCIR NUMERO DOS</ep1.3>
			<id1.3>36691b7092c9fe2516739bcb69632e04732867b9</id1.3>
			<ep1.4>4 POBRE SISIFO</ep1.4>
			<id1.4>5851bf582178e51bc735942addd1a8e24d7cf1c7</id1.4>
			<ep1.5>5 ILUSIONES</ep1.5>
			<id1.5>cba94b2f291d66f925095b63c8df82f1ced13018</id1.5>
			<ep1.6>6 EL DOLOR DEBE SE TORMENTA</ep1.6>
			<id1.6>8ce7cf4846df0a01daecf99f21e19b7192bcaea5</id1.6>
			<ep1.7>7 SACRAMENTO</ep1.7>
			<id1.7>0659176b89f6a7b14d6b2c1f14b26feff58e62e8</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MARIANNE</title>
		<info>(2019) 8 episodios. Una novelista se da cuenta de que sus historias de terror se hacen realidad y decide volver a su ciudad natal para enfrentarse a los demonios del pasado que la inspiran.</info>
		<year>2019</year>
		<genre>Terror</genre>
		<thumb>https://image.tmdb.org/t/p/original/vORXihb6o0UvzHZfikBSD7H13Qk.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/yD01qWyOqc6J2lRutzOt3Wu8Wnz.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/vORXihb6o0UvzHZfikBSD7H13Qk.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/yD01qWyOqc6J2lRutzOt3Wu8Wnz.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>7a8afaf8d20d541754c8a6d9165e26ee8e4ad010</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MARICON PERDIDO</title>
		<info>(2021) 6 episodios. “Roberto, ¿tú eres marica?” Bea, su mejor y única amiga, es la que pregunta a un Roberto de 12 años, desencadenando una búsqueda de identidad que se alargará hasta diez años más tarde, cuando en Chueca y con vocación de ser escritor, intentará reformular su vida intentando responderse a la pregunta de quién es y cómo ser feliz en un mundo tan hostil. Bob Pop realiza su primera incursión en la creación de series de ficción, inspirándose en episodios de su propia vida, pero con buenas dosis de ficción y fantasía.</info>
		<year>2021</year>
		<genre>Comedia. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/6HMt6D4ZWMxfmGlUXc7cx7jnSqo.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/yaD13p8VCh7n39HIwPt8lWmqquN.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/6HMt6D4ZWMxfmGlUXc7cx7jnSqo.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/yaD13p8VCh7n39HIwPt8lWmqquN.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>4e045d568e7e4ce9fbb12d833117b7f20e30c613</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>17b4a949eff9c07b684c453808bf9a90b08e602e</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MARVEL'S HIT-MONKEY</title>
		<info>(2021) 10 episodios. Un mono japonés, que es un experto tirador y tiene una gran agilidad y reflejos, se entrena para hacer un viaje de venganza oscuro y sangriento por los lugares más peligrosos de Tokyo. Para este entrenamiento cuenta con la gran ayuda del fantasma de un asesino americano.</info>
		<year>2021</year>
		<genre> Animación. Acción. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/YS1JG9T2mPLA0uXpXU3MzvcIyu.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/uJQbAp63BzgoQYFG9fsLYQxdvnE.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/YS1JG9T2mPLA0uXpXU3MzvcIyu.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/uJQbAp63BzgoQYFG9fsLYQxdvnE.jpg</fanart.1>
			<ep1.1>1 AL 10 (1080)</ep1.1>
			<id1.1>07216BE6B4DDDF823F70CE022C857F0F64FB34E2</id1.1>
			<ep1.2>1 AL 4</ep1.2>
			<id1.2>5db288759944c1865523f59e29e193182c18f497</id1.2>
			<ep1.3>5 AL 7</ep1.3>
			<id1.3>a7bcd8a3fcd765b07def8ec9ea3138db1f1f2d2e</id1.3>
			<ep1.4>8 AL 10</ep1.4>
			<id1.4>795f064ce4e16f87c43e8309b7e8a7edc9b8cb60</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MAYANS M C</title>
		<info>(2018) 4 temporadas. 37 episodios. "Mayans M.C." se desarrolla dos años y medio después de los sucesos de "Sons of Anarchy" y se centra en los problemas de Ezekiel "EZ" Reyes. Su búsqueda del sueño americano se vio truncada por la violencia de los narcos y ahora sus ansias de venganza le llevan a un estilo de vida que nunca quiso y del que no puede escapar.</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/y8PYGJ7TGjtMxwPA5Mhwa9svTRJ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/43yEwj8aJigrDMmBlF6JQrCUrXY.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/y8PYGJ7TGjtMxwPA5Mhwa9svTRJ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/43yEwj8aJigrDMmBlF6JQrCUrXY.jpg</fanart.1>
			<ep1.1>1 PERRO/OC</ep1.1>
			<id1.1>27ecb35691df007109ec87c7af4cafe3dd366971</id1.1>
			<ep1.2>2 ESCORPION/DZEC</ep1.2>
			<id1.2>405eacb7fb6f36105e02e71f4ba55d138612efd8</id1.2>
			<ep1.3>3 BUHO/MUWAN</ep1.3>
			<id1.3>63ace32687822b1e9a0c1c7e15270ccc49382902</id1.3>
			<ep1.4>4 MURCIELAGO/ZOTZ</ep1.4>
			<id1.4>cb4c8d91876551e2fc590384082b6a9760437ce1</id1.4>
			<ep1.5>5 UCH/OPOSSUM</ep1.5>
			<id1.5>0b82a8e35342ab60293afc8be8b11f99eb6bd547</id1.5>
			<ep1.6>6 GATO/MIS</ep1.6>
			<id1.6>a3ba5f8bcb0dae0e601638aedf2d6e8c0ee8b899</id1.6>
			<ep1.7>7 CUCARACHA/K'URUCH</ep1.7>
			<id1.7>e6afdc5bb9bcd7bdf21684f2152b0c5712db6b7b</id1.7>
			<ep1.8>8 RATA/CH’O</ep1.8>
			<id1.8>d28e4178710d62389e762378eb1fd6e0106fa8b3</id1.8>
			<ep1.9>9 SERPIENTE/CHIKCHAN</ep1.9>
			<id1.9>27fd5504b1470f673e974cce2878f92d15627828</id1.9>
			<ep1.10>10 CUERVO/TZ'IKB'UUL</ep1.10>
			<id1.10>99dee1dee24aba8d36c3de4f14304ddf3555d37d</id1.10>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/i749BmjbQHH1JKlOG3IiCNFZYVH.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/75RhgfnujnwmJJDilhDMg1EURm4.jpg</fanart.2>
			<ep2.1>1 XBALANQUE</ep2.1>
			<id2.1>79ab595cded96dd9265e48fd832e162c0e1d607b</id2.1>
			<ep2.2>2 XAMAN-EK</ep2.2>
			<id2.2>1e4ebb911dafba47d211f096a1d40e7f7a640ae1</id2.2>
			<ep2.3>3 CAMAZOTZ</ep2.3>
			<id2.3>1d079390d85af49df7f388e43c2c3164e3d3e5b5</id2.3>
			<ep2.4>4 LAHUN CHAN</ep2.4>
			<id2.4>c42649e519d74c50e30d84488f6cc9f0b7b4bf86</id2.4>
			<ep2.5>5 XQUIC</ep2.5>
			<id2.5>b4764aea241c758d88e69787542efda781ccb141</id2.5>
			<ep2.6>6 MULUC</ep2.6>
			<id2.6>1e62979b463d138d4695a33b23bbff15a2188576</id2.6>
			<ep2.7>7 TOHIL</ep2.7>
			<id2.7>9d6ceb49ce055492dc18cd77f462e97f1558c2a2</id2.7>
			<ep2.8>8 KUKULKAN</ep2.8>
			<id2.8>b973513b6959c9f5cdf777330a72bff685183086</id2.8>
			<ep2.9>9 ITZAM-YE</ep2.9>
			<id2.9>620c5cc5252f17c85a80b31df2516ad18a073760</id2.9>
			<ep2.10>10 HUNAHPU</ep2.10>
			<id2.10>ac159d1aefb0a1bb7b6601cbec0140c5d2c517c3</id2.10>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/gaKhfksFK24N19bjlFpJxamYZ02.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/9XUIsRUB7V3iEoLSKxZCqPvbKKW.jpg</fanart.3>
			<ep3.1>1 PAP LUCHA CON EL ANGEL DE LA MUERTE</ep3.1>
			<id3.1>4815898b0cc0e79d4c6e4ab7f0c21e7ee2cc4593</id3.1>
			<ep3.2>2 LA MALDAD DE LOS REYES</ep3.2>
			<id3.2>894253a5922bfc73747b7ba21a9225b8a91fe6ce</id3.2>
			<ep3.3>3 LA EXAGERACION NO RINDE</ep3.3>
			<id3.3>f91cc411c03752138bd9b50b196152ee5bef3d9b</id3.3>
			<ep3.4>4 EL OSCURO JURAMENTO DE NUESTRA BANDA</ep3.4>
			<id3.4>dbde5ee087759e3360ed7f92cde60b0d0d37f7ff</id3.4>
			<ep3.5>5 PLANES OSCUROS Y PROFUNDOS</ep3.5>
			<id3.5>da44c9abab4484f16034649804b61e88da1bb79e</id3.5>
			<ep3.6>6 NO PUEDES REZAR UNA MENTIRA</ep3.6>
			<id3.6>127402c3ac44366c85ee61407b3e241de818e6de</id3.6>
			<ep3.7>7 LAS CONSECUENCIAS DE MANIPULAR PIEL DE SERPIENTE</ep3.7>
			<id3.7>b5737ecc07bf0ce774f12ee3b0848f42c4f2f48b</id3.7>
			<ep3.8>8 UN RESCATE MIXTO Y ESPLENDIDO</ep3.8>
			<id3.8>b1a29036a6d266d5546302ed524a082f7591954d</id3.8>
			<ep3.9>9 LA CASA DE LA MUERTE PASA FLOTANDO</ep3.9>
			<id3.9>8d4913a2f99e6cd0660c89f5406914ec8bfd1b55</id3.9>
			<ep3.10>10 ULTIMO CAPITULO, NADA MAS QUE ESCRIBIR</ep3.10>
			<id3.10>3b3f9e330ca5c76d87806bf0d39f5d25fd91ab58</id3.10>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/pneaBHGQ5BqGJ3afxlPezL5czZB.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/vLX3GUUAYQJimuHUeliYMN5y4pn.jpg</fanart.4>
			<ep4.1>1 LIMPIEZA DEL TEMPLO</ep4.1>
			<id4.1>5602ad6f3af8143f1d5115db0561333cd692e51f</id4.1>
			<ep4.2>2 HIMNO ENTRE RUINAS</ep4.2>
			<id4.2>039466d794f1db33eb067add341e6e9bae387096</id4.2>
			<ep4.3>3 AUTORRETRATO EN UN BAÑO AZUL</ep4.3>
			<id4.3>fc6046d1cd6abd7c158e437b8d945e8bd433017a</id4.3>
			<ep4.4>4 UN CUERVO PASO VOLANDO</ep4.4>
			<id4.4>d37bb9d8bc0735ce1d32c6d0c087212b634e1f9c</id4.4>
			<ep4.5>5 MUERTE DE LA VIRGEN</ep4.5>
			<id4.5>80e27bd9ca95395489f1d6a1cab791460b470083</id4.5>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MINDHUNTER</title>
		<info>(2017-2019) 2 temporadas. 19 episodios. Año 1977. Dos agentes del FBI (Jonathan Groff y Holt McCallany) revolucionan las técnicas de investigación para encontrar las respuestas a cómo atrapar a asesinos en serie y mentes psicópatas. Adaptación del libro "Mind Hunter: Inside FBI’s Elite Serial Crime Unit", escrito por Mark Olshaker y John E. Douglas.</info>
		<year>2017</year>
		<genre>Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/qqD3GUTC6F1Y81oqTQBv3RdUJlz.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/a906PH7CDmSOdS7kmnAgdWk5mhv.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/qqD3GUTC6F1Y81oqTQBv3RdUJlz.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/a906PH7CDmSOdS7kmnAgdWk5mhv.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>8862f5be371eb000ebc5f45ccd798bd11222a3d8</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/hOO7LBgN7AKJuV3LbLuIKrtHtnZ.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/5hn1nitcqaWU6Hm06yDHA9Bdcly.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>03a83d9cbceab0022e1fc7974f5ef447fe839583</id2.1>
			<ep2.2>5 AL 8</ep2.2>
			<id2.2>388921d0cccbf3d3f0c3a8eff270199b6489e7d1</id2.2>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MONSTRUOS A LA OBRA</title>
		<info>(2021) 10 episodios. Continuación de la franquicia "Monstruos S.A.", en este caso en formato serie y para la plataforma Disney+.</info>
		<year>2021</year>
		<genre>Animación. Comedia. Fantástico. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/yeJDavUZnIJYTIZ9BB6H4YjePCT.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/2O3ZRECju00Jod6LrVB3uRgZMXK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/yeJDavUZnIJYTIZ9BB6H4YjePCT.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/2O3ZRECju00Jod6LrVB3uRgZMXK.jpg</fanart.1>
			<ep1.1>1 BIENVENIDO A MONSTERS, INCORPORATED</ep1.1>
			<id1.1>922f7c27b5bdab3d00529268f21f5ea6f02f72c7</id1.1>
			<ep1.2>2 CONOCE A LA MIBT</ep1.2>
			<id1.2>b0fb9f81dac6a0d4dcaeb530e11f6dde6ba48ba0</id1.2>
			<ep1.3>3 DESPERFECTOS EN EL CUARTO</ep1.3>
			<id1.3>fded805729437f222ff9c30e546978bdcdf6be01</id1.3>
			<ep1.4>4 LOS "GRAN WAZOWSKIS"</ep1.4>
			<id1.4>45114ca93edfde465a0d9701344fdeadb12a4562</id1.4>
			<ep1.5>5 ENCUBRIMIENTO</ep1.5>
			<id1.5>5c0f8728fe417f889ee0df6b84c8bc3efd936786</id1.5>
			<ep1.6>6 LA MAQUINA EXPENDEDORA</ep1.6>
			<id1.6>808c81df88b38a6a36f6f46ccc4a620648d33beb</id1.6>
			<ep1.7>7 ADORABLES REGRESOS</ep1.7>
			<id1.7>d448276e0cab8f745eaa6269762410257031f002</id1.7>
			<ep1.8>8 MONSTRUITOS</ep1.8>
			<id1.8>9ca6b2b05dcf01a769f0e26d47abbc9bdaccfd9b</id1.8>
			<ep1.9>9 POR LOS PELOS</ep1.9>
			<id1.9>63ddbc26ab9b492d847bb04c8a42541281eb2b92</id1.9>
			<ep1.10>10 EL HUMOR ES SU LABOR</ep1.10>
			<id1.10>cfffd6ca44fdff0187eab642c89f0a65e0f606b4</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MS. MARVEL</title>
		<info>(2022) 6 episodios. Una gran estudiante, ávida jugadora y voraz escritora de fan-fic, Kamala Khan tiene una afinidad especial por los superhéroes, en particular por la Capitana Marvel. Sin embargo, lucha por encajar en casa y en la escuela, es decir, hasta que obtiene superpoderes como los héroes que siempre ha admirado. La vida es más fácil con superpoderes, ¿verdad?</info>
		<year>2022</year>
		<genre>Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/woZiAKaWDb3mm5JwNXcQ3MExWU0.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/mVBBJ56u8gQlfYHvTsCXKhTJIfU.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/woZiAKaWDb3mm5JwNXcQ3MExWU0.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/mVBBJ56u8gQlfYHvTsCXKhTJIfU.jpg</fanart.1>
			<ep1.1>1 GENERACION QUE</ep1.1>
			<id1.1>8555DC18BC810053DCD100BE3C83765B2783F7B2</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>MUNECA RUSA</title>
		<info>(2019) 8 episodios. Serie que sigue las aventuras de una joven, de nombre Nadia Vulvokov, que repite una y otra vez la misma noche en la ciudad de Nueva York. La mujer se ve atrapada en un misterioso bucle en el que asiste a una fiesta y muere esa misma noche, para despertar al día siguiente totalmente ilesa.</info>
		<year>2019</year>
		<genre>Ciencia ficción. Fantástico. Drama. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/4WijEAbnGMJifP6uepGALci3Jf.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/h13MfhInnUzLMTrrulVEBrhqe9s.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/4WijEAbnGMJifP6uepGALci3Jf.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/h13MfhInnUzLMTrrulVEBrhqe9s.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>37c881bc9ec3cf1c6fef37e30104d690b8e3e379</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NARCOS</title>
		<info>(2015-2017) 3 temporadas. 30 episodios. Serie que narra los esfuerzos de Estados Unidos, a través principalmente de la DEA, y de las autoridades y policía de Colombia, para luchar en la década de los 80 contra el narcotraficante Pablo Escobar y el cartel de Medellín, una de las organizaciones criminales más ricas y despiadadas en la historia de la delincuencia moderna. En la tercera temporada el objetivo de la DEA y la policía colombiana es intentar acabar con el cartel de Cali</info>
		<year>2015</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/uwXAfcxp4r6iM5y89J5bfiFKB6S.jpg</thumb>
		<fanart>https://i.imgur.com/TVV5HH7.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/qEfVfmR6KFuwjAV6Ew5RObj73xS.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/TVV5HH7.jpg</fanart.1>
			<ep1.1>1 AL 10 (1080)</ep1.1>
			<id1.1>95f337370375f0606c3b2c41774005d03aa96553</id1.1>
			<ep1.2>1 DESCENSO</ep1.2>
			<id1.2>707f33ef49f665c824ddba508803c524a4300680</id1.2>
			<ep1.3>2 LA ESPADA DE SIMON BOLIVAR</ep1.3>
			<id1.3>4fcaf054ac1481a06e17803275c8a21834fd7607</id1.3>
			<ep1.4>3 LOS HOMBRES DE SIEMPRE</ep1.4>
			<id1.4>f77d218d59b9846d7505d449f1ea9e9e64e832fb</id1.4>
			<ep1.5>4 EL PALACIO EN LLAMAS</ep1.5>
			<id1.5>8550cd22b125843f461fc3a4299a27a6a21d5e0f</id1.5>
			<ep1.6>5 HABRA UN FUTURO</ep1.6>
			<id1.6>a7faec46d1cc9e3b4a38361f9c6e9d00e23681ef</id1.6>
			<ep1.7>6 EXPLOSIVOS</ep1.7>
			<id1.7>12e86801e55b6405a54d81f26b68fbafb49bac62</id1.7>
			<ep1.8>7 VAS A LLORAR SANGRE</ep1.8>
			<id1.8>f0224e10dfaa1aaacd39498eb23f949a28e9a9be</id1.8>
			<ep1.9>8 LA GRAN MENTIRA</ep1.9>
			<id1.9>356d3b860d2de93a1c0c6cf97a0d05ba0a83bf34</id1.9>
			<ep1.10>9 LA CATEDRAL</ep1.10>
			<id1.10>3578f1dec80fabb2fb00538023bc193fe5f50c0c</id1.10>
			<ep1.11>10 DESPEGUE</ep1.11>
			<id1.11>b48813c48dad414682631b6bee296a58a06bff8a</id1.11>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/ajGtNNR17aXCGrOHpNxhacbS5cZ.jpg</thumb.2>
			<fanart.2>https://i.imgur.com/TVV5HH7.jpg</fanart.2>
			<ep2.1>1 AL 10 (1080)</ep2.1>
			<id2.1>0b214d662f9194efa621c18de34f5cbc88b1dc0f</id2.1>
			<ep2.2>1 POR FIN LIBRE</ep2.2>
			<id2.2>2e5a22821e4fa629989a7705eee4ecfb476d59ac</id2.2>
			<ep2.3>2 CAMBALACHE</ep2.3>
			<id2.3>5b1b88877f75fd443ea052c087f955fd3446be69</id2.3>
			<ep2.4>3 NUESTRO HOMBRE EN MADRID</ep2.4>
			<id2.4>e3ee707f6c198045fdd8cd5593fa2d84c7ffc8f9</id2.4>
			<ep2.5>4 EL BUENO EL MALO Y EL MUERTO</ep2.5>
			<id2.5>1600906317072f8f673c50c8da323985b1223ece</id2.5>
			<ep2.6>5 LOS ENEMIGOS DE MI ENEMIGO</ep2.6>
			<id2.6>e8e38cd90afa026ce2982985334a4ab784a4dd7b</id2.6>
			<ep2.7>6 LOS PEPES</ep2.7>
			<id2.7>20eb747015c218003eb79c07902394056bbbd01b</id2.7>
			<ep2.8>7 ALEMANIA 1993</ep2.8>
			<id2.8>b2d1c81581996dd0835dbc118162183ab68a8cac</id2.8>
			<ep2.9>8 EL PATRON SE QUEDA SOLO</ep2.9>
			<id2.9>2604b2c9244eb9cdbbe1074bb0adbfa7900b9934</id2.9>
			<ep2.10>9 NUESTRA FINCA</ep2.10>
			<id2.10>e191095e1ea3c53732f6306347a367dd6ade71a3</id2.10>
			<ep2.11>10 AL FIN CAYO</ep2.11>
			<id2.11>7318bf4f924a978c157daab64b597ac0dffd0d19</id2.11>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/3s9P3DazdH1jaamXqVBdMAcuF0E.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/pYTIMgcP3wXgYMXuIK9bFnnJWNd.jpg</fanart.3>
			<ep3.1>1 AL 10</ep3.1>
			<id3.1>f9c595857d84eff6584c3b7550af134cd572ccf4</id3.1>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NARCOS MEXICO</title>
		<info>(2018-2021) 3 temporadas. 30 episodios. Cuenta la historia real del ascenso al poder del cártel de Guadalajara, liderado por Miguel Ángel Félix Gallardo (Diego Luna), y el inicio de las guerras de la droga en el México de los años 80. Por su parte Kiki Camarena (Michael Peña) es un agente de la DEA norteamericana al que trasladan desde California a Guadalajara para incorporarse a la investigación del recién nacido cartel mexicano. Gallardo comenzó su imperio traficando con marihuana y uniendo a todos los narcos del país con un propósito común, pero pronto su ambición le llevó a ver México como el mejor sitio para transportar la cocaína colombiana...</info>
		<year>2018</year>
		<genre>Thriller. Acción. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/yCid4sQkaVzfa2y4yQWs5E0TES9.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/rs6D9K1fQOllLEHNlQQ0g84ALg5.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/yCid4sQkaVzfa2y4yQWs5E0TES9.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/rs6D9K1fQOllLEHNlQQ0g84ALg5.jpg</fanart.1>
			<ep1.1>1 AL 10 (1080)</ep1.1>
			<id1.1>a8abee1ea52464ba8fbaf0c801d30a625129d881</id1.1>
			<ep1.2>1 CAMELOT</ep1.2>
			<id1.2>5a375bec6b4a0704aa73bb7926ffdabb92c1690b</id1.2>
			<ep1.3>2 EL SISTEMA PLAZA</ep1.3>
			<id1.3>cda51ef4a8fa396e689f69524b4f7ccc448f3229</id1.3>
			<ep1.4>3 EL PADRINO</ep1.4>
			<id1.4>413f95e9ed5c336eed51d03b4fe7f6e8291b6522</id1.4>
			<ep1.5>4 AL 6</ep1.5>
			<id1.5>2668aa5d4aeab6ebeae620c51f18f3d098401aea</id1.5>
			<ep1.6>7 AL 10</ep1.6>
			<id1.6>c164df95fe8a574192b96b66f014329e15de87c4</id1.6>
		</t1>
		<t2/>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/uYcZMiIIUv8NPebpqKbUGhf2M3y.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/cxg6CzZl7D0oJrn2425WmgtJnva.jpg</fanart.3>
			<ep3.1>1 AL 4</ep3.1>
			<id3.1>b002639dd5dbfb317652beeb79a3ee4deff94d9e</id3.1>
			<ep3.2>5 AL 7</ep3.2>
			<id3.2>e0b3c020d2d58d40fec2f51a5d0b59bcef73d4e2</id3.2>
			<ep3.3>8 AL 10</ep3.3>
			<id3.3>a604862f8794a5fcefbf8ed5725cd76f9f50704c</id3.3>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NEW AMSTERDAM</title>
		<info>(2018) 4 temporadas. 63 episodios.El New Amsterdam se encuentra en una mala situación tanto económica como de reputación. Esto cambiará con la llegada del Dr. Max Goodwin, el nuevo director médico que se propone romper con la burocracia y brindar una atención excepcional. Él no acepta un "no" por respuesta y demostrará que no se detendrá ante nada para dar una nueva vida al hospital, el único en el mundo capaz de tratar a pacientes con ébola y aceptar bajo el mismo techo tanto a presidiarios como al Presidente de Estados Unidos.</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bg556WSKtyAzmVTe0kK35lelsIc.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/sH9yAC2cVqNj36wEwscFqC9U3jy.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bg556WSKtyAzmVTe0kK35lelsIc.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/sH9yAC2cVqNj36wEwscFqC9U3jy.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>e93a1d1c30410c5e4095758024ac66b76112140b</id1.1>
			<ep1.2>2 RITUALES</ep1.2>
			<id1.2>10c3868f7e9fde8c8d4c2c0212fb825b83b414ce</id1.2>
			<ep1.3>3 CADA MINUTO</ep1.3>
			<id1.3>4c929b83ed9d82f48a77a756b6cc8dafaa494bb8</id1.3>
			<ep1.4>4 LIMITES</ep1.4>
			<id1.4>614b25ab520af10cc1d9a32fc661f3709b2a6488</id1.4>
			<ep1.5>5 CAVITACION</ep1.5>
			<id1.5>b225187516e934f265805c6db2e61196f4cb336a</id1.5>
			<ep1.6>6 AL 10</ep1.6>
			<id1.6>9a96a30b0eb2466c275ad8f404668ad4202e1485</id1.6>
			<ep1.7>11 AL 15</ep1.7>
			<id1.7>152dd5fde044e05cfefacc4ad8bc0e06f9734156</id1.7>
			<ep1.8>16 AL 19</ep1.8>
			<id1.8>c2b8e00f5ecdf20d79e5debf33ae8392d4b86c93</id1.8>
			<ep1.9>20 AL 22</ep1.9>
			<id1.9>81c208e7fbce1373b653b9aaeb8236a517a5f316</id1.9>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/vXDupgsWKkFizWp8yPDCmAbMV4l.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/ukq1UyAdiIqytZ1eEbQG19LIRrv.jpg</fanart.2>
			<ep2.1>1 AL 6</ep2.1>
			<id2.1>906b5b9c88050539a3204e56d982b4ac0faee2af</id2.1>
			<ep2.2>7 AL 18</ep2.2>
			<id2.2>1fb0257a27e81b8086ccd289e8cbbafa48c7f91e</id2.2>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/wKTAz8fkoXJoHqPpi4ArAUGDtco.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/kXdfhKazlnRhnyc687DDdZJPaoJ.jpg</fanart.3>
			<ep3.1>1 LA NUEVA NORMALIDAD</ep3.1>
			<id3.1>34569e9bbf4a587007a0ab7efdbf1a2c9dd97934</id3.1>
			<ep3.2>2 TRABAJADORES ESENCIALES</ep3.2>
			<id3.2>76226079e63a6381896122063552bb7dbe471b65</id3.2>
			<ep3.3>3 SUFICIENTEMENTE SEGURO</ep3.3>
			<id3.3>5073489384c87bf469f3a28dab3fa55c3ae98767</id3.3>
			<ep3.4>4 ESTO ES TODO LO QUE NECESITO</ep3.4>
			<id3.4>dc04f5bc234cd6041d87d976fb3d7e1f01f42ab2</id3.4>
			<ep3.5>5 SANGRE, SUDOR Y LAGRIMAS</ep3.5>
			<id3.5>1da5715a824b93946fab6a6cfd3eecc44bb50f4d</id3.5>
			<ep3.6>6 POR QUE NO AYER</ep3.6>
			<id3.6>e28b23c50b938da27595e1e09be5a725a754f0f8</id3.6>
			<ep3.7>7 LA LEYENDA DE HOWIE COURNEMEYER</ep3.7>
			<id3.7>1d493514f48929d4cb40826bfb60bdeafe76358e</id3.7>
			<ep3.8>8 PILLADO</ep3.8>
			<id3.8>04cdbfc95e96196050a2764af4c64a0cf3ee2e46</id3.8>
			<ep3.9>9 DESCONECTADO</ep3.9>
			<id3.9>1e73df51e3ed34c19151cb4d0d47ae5401650686</id3.9>
			<ep3.10>10 RADICAL</ep3.10>
			<id3.10>f26f416da0d147232810b9b5cc964c4021b7fef8</id3.10>
			<ep3.11>11 BAJADA DE PRESION</ep3.11>
			<id3.11>59418685ec4164a792d781ac059603adf681a088</id3.11>
			<ep3.12>12 LAS COSAS SE DERRUMBAN</ep3.12>
			<id3.12>4792489ce2a205a1ae61535056f39e303809c58d</id3.12>
			<ep3.13>13 TIEMPO DE LUCHA</ep3.13>
			<id3.13>9ba31ec65d48daaa25c5e8cc83b10ee754615ad5</id3.13>
			<ep3.14>14 LA MUERTE EMPIEZA EN RADIOLOGIA</ep3.14>
			<id3.14>8babe875053d5cbfcec70f226f70b817ed8e8854</id3.14>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/50zBc18T7wK1EQYDhkpau3YJIPy.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/4Ol8Z8c1EMa6ThTcq3juUVbgrpA.jpg</fanart.4>
			<ep4.1>1 MAS ALEGRIA</ep4.1>
			<id4.1>c881647a5e550fbddc492c2d55cedc2cdf81f3db</id4.1>
			<ep4.2>2 ESTAMOS JUNTOS EN ESTO</ep4.2>
			<id4.2>17a4207e3ac332e64a4f11f3c2ac0ca38c3197cb</id4.2>
			<ep4.3>3 LO MISMO DE SIEMPRE</ep4.3>
			<id4.3>f7d620a2dba34b4f905cb7e4dbdc8e83b2a87ddb</id4.3>
			<ep4.4>4 PLANTAR PARA RECOGER</ep4.4>
			<id4.4>36f094495f29f0bb317eaca9c6471953561843bd</id4.4>
			<ep4.5>5 DE PADRES A HIJOS</ep4.5>
			<id4.5>3355ff8425001a62ab92b0e58056ed7d66585c36</id4.5>
			<ep4.6>6 RISAS Y ESPERANZA Y UN PUÑETAZO</ep4.6>
			<id4.6>ccbed205abc68104625a823bb5bab6fa135ccfd3</id4.6>
			<ep4.7>7 ARMONIA</ep4.7>
			<id4.7>0501c63e405f99a7472af5525cd8e26ab3c165f0</id4.7>
			<ep4.8>8 CUENTA SALDADA</ep4.8>
			<id4.8>f4a4990756c47d84f3d3ba086df268517c99f96d</id4.8>
			<ep4.9>9 EN TIERRA EXTRAÑA</ep4.9>
			<id4.9>10d20ad7ce60512d0c33e6d1b5ff00a97bab4c82</id4.9>
			<ep4.10>10 LA MUERTE ES LA NORMA LA VIDA LA EXCEPCION</ep4.10>
			<id4.10>b2c02164f17eacbec4cc406d2d98114bdeb8f7bc</id4.10>
			<ep4.11>11 VIVA LA REVOLUCION</ep4.11>
			<id4.11>36a9158ae26b8305e56aef49e8d1be67da3073b8</id4.11>
			<ep4.12>12 EL CRUCE</ep4.12>
			<id4.12>f112bdd12de9faf36f249ef1ad7021f81605e790</id4.12>
			<ep4.13>13 FAMILIA</ep4.13>
			<id4.13>f112bdd12de9faf36f249ef1ad7021f81605e790</id4.13>
			<ep4.14>14 INTENTEMOSLO OTRA VEZ</ep4.14>
			<id4.14>0a08bc72480d786adebef4f1663df9f0c2bedc11</id4.14>
			<ep4.15>15 DOS OPCIONES</ep4.15>
			<id4.15>59f11800d1b21773e3cd3ade97f284c77e500c1a</id4.15>
			<ep4.16>16 TODA LA NOCHE</ep4.16>
			<id4.16>430a1cb01dfd94cf7e3c6606707ce93cf25ab529</id4.16>
			<ep4.17>17 ASUNTOS PENDIENTES</ep4.17>
			<id4.17>0235b1b2c26c73bdf68109d235de0ff6e8df089d</id4.17>
			<ep4.18>18 NADA DE EXCUSAS</ep4.18>
			<id4.18>1821bd9f77f6f118aefd4e61ecea00e9a2ce4188</id4.18>
			<ep4.19>19 LA VERDAD SEA DICHA</ep4.19>
			<id4.19>bb145b6a01af6dffc0d94d2604d327a9621be482</id4.19>
			<ep4.20>20 LEVANTAMIENTO</ep4.20>
			<id4.20>ee8dda71be761ab21490fe87db92534bd3254e76</id4.20>
			<ep4.21>21 CASTILLOS DE ARENA</ep4.21>
			<id4.21>2bba95802ef4c18c571d3c2598d4619115f561ef</id4.21>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NEYMAR EL CAOS PERFECTO</title>
		<info>(2022) 3 episodios. Una visión inédita de uno de los deportistas más famosos y mejor pagados del mundo: Neymar, un héroe sobre el terreno de juego y un personaje controvertido fuera de él. Esta docuserie en tres partes, dirigida por David Charles Rodrigues, esboza un retrato personal y cercano del futbolista Neymar da Silva Santos Júnior y muestra su ascenso a la fama en el Santos Fútbol Club, sus días de gloria en el Barcelona y los altibajos con la selección nacional brasileña y el Paris Saint-Germain. Al mismo tiempo, el documental descubre lo que hay detrás de la maquinaria de marketing de Neymar, dirigida con mano de hierro por su padre. Ofrece entrevistas con Beckham, Messi, Mbappé y muchos otros iconos del fútbol que reflexionan sobre el lugar de Neymar en la historia del deporte.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/aMKNSwbkuCE3VoT3BfwVjQgrdmX.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/icX0fw1wfRx6mSRLTEQovXThWK2.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/aMKNSwbkuCE3VoT3BfwVjQgrdmX.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/icX0fw1wfRx6mSRLTEQovXThWK2.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>7e0bb6fdbc732aa03ed112d957a0c47cca3672e7</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NI UNA PALABRA</title>
		<info>(2022) 6 episodios. En una urbanización rica, donde la comunidad local lleva una vida tranquila, todo cambia cuando Adam, de 18 años, desaparece sin dejar rastro.</info>
		<year>2022</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/p0aD6sXMMpEIjmrsRhVuhn8YV3p.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/oVN5bh3nl0vdXXgoar2EUlEUId.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/p0aD6sXMMpEIjmrsRhVuhn8YV3p.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/oVN5bh3nl0vdXXgoar2EUlEUId.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>fe6f5b185cbc6532ab7c2a48defaac82acc78b8c</id1.1>
			<ep1.2>4 Episodio</ep1.2>
			<id1.2>6c43a31ace8110e1aeead1981a9ac6d38cf5afc8</id1.2>
			<ep1.3>5 Episodio</ep1.3>
			<id1.3>99aba9dec41b80619efda8d8de11800b985b4afa</id1.3>
			<ep1.4>6 Episodio</ep1.4>
			<id1.4>c5d0f3bc9d47a30a864493db1d1d5ce1bf89e499</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NINE PERFECT STRANGERS</title>
		<info>(2021) 8 episodios. Nueve australianos, en diferentes momentos de su vida, asisten a un costoso "retiro de transformación total de mente y cuerpo" de 10 días en un lugar llamado Tranquillum House, dirigido por una misteriosa mujer rusa llamada Masha (Nicole Kidman). Miniserie que adapta la novela de la australiana Liane Moriarty.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/oPcqsZIFHrv7HoGgFQnsGDiPulD.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/5JuWMvOxVUM7dt1VSlUUUX1yGhh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/oPcqsZIFHrv7HoGgFQnsGDiPulD.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/5JuWMvOxVUM7dt1VSlUUUX1yGhh.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>a57b675df48e151b4e8177c4c7ed6595c6d1bf79</id1.1>
			<ep1.2>4 UN MUNDO FELIZ</ep1.2>
			<id1.2>6ce89b6523c41ed9b2d0321d9197156921c1acaf</id1.2>
			<ep1.3>5 DULCE RENDICION</ep1.3>
			<id1.3>3d29cb3d0115e7b96f386d4749302d4cc3ceb4bf</id1.3>
			<ep1.4>6 VETA MADRE</ep1.4>
			<id1.4>1a724e0941ba68333d69cf07a3786092adecc605</id1.4>
			<ep1.5>7 LAS RUEDAS DEL AUTOBUS</ep1.5>
			<id1.5>098a26fb8f6074f651ba1bcaf6362126a9d3c36e</id1.5>
			<ep1.6>8 PARA SIEMPRE</ep1.6>
			<id1.6>3f3d1add9e4eede98dd796e02a0a87c82580e957</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NOW AND THEN</title>
		<info>(2022) 8 episodios. Thriller que explora las diferencias entre las aspiraciones de juventud y la realidad de la edad adulta. Las vidas de un grupo de amigos de la universidad cambian para siempre cuando uno de ellos muere después de un fin de semana de fiesta. Ahora, 20 años después, los cinco restantes se reúnen por una amenaza que pone en riesgo sus mundos aparentemente perfectos.</info>
		<year>2022</year>
		<genre>Intriga. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/2z4ePy6Z6cURDvNnEmR36nidBNU.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/amdcpzNaOfa2TaehImbQlrMlfpW.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/2z4ePy6Z6cURDvNnEmR36nidBNU.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/amdcpzNaOfa2TaehImbQlrMlfpW.jpg</fanart.1>
			<ep1.1>1 20 AÑOS</ep1.1>
			<id1.1>4fe24d2d957679467c0f6021c243b1da4d5f8c1c</id1.1>
			<ep1.2>2 ENTERRADO</ep1.2>
			<id1.2>7aab0eb5501f25076c876036a914e0194a843e9c</id1.2>
			<ep1.3>3 UNA MALA DECISION</ep1.3>
			<id1.3>56c63480653e6d42eccb219a2d153dc4183e5e59</id1.3>
			<ep1.4>4 CRUZAR EL LIMITE</ep1.4>
			<id1.4>c7ee2da0b6e4d551cdee8a746c0917aa929aa6c3</id1.4>
			<ep1.5>5 CARA A CARA</ep1.5>
			<id1.5>ba3ea6ae5a6164e9acc173d4e43f986ac1e70727</id1.5>
			<ep1.6>6 UN BUEN DIA PARA DAR GRACIAS</ep1.6>
			<id1.6>c50087ac414db87d208c059535b0052a53c88a0a</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>NUESTRA BANDERA SIGNIFICA MUERTE</title>
		<info>(2022) 10 episodios. Stede Bonnet deja su vida acomodada para ser un intrépido bucanero y se convierte en el capitán del barco pirata “Venganza”. Mientras intenta ganarse el respeto de su tripulación, su suerte cambia tras encontrarse con el capitán Barbanegra.</info>
		<year>2022</year>
		<genre>Aventuras. Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/qkTcWWgmjyytSchUqDzCpRrmhiz.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nBxz9MzZMIRgA9Ohnw9gE94HQz4.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qkTcWWgmjyytSchUqDzCpRrmhiz.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nBxz9MzZMIRgA9Ohnw9gE94HQz4.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>80740ec01125434986f0e5951a2ebdbc26703b5c</id1.1>
			<ep1.2>2 UN HOMBRE CONDENADO</ep1.2>
			<id1.2>1a03fd6e75eef07476646362d0ec59580988712b</id1.2>
			<ep1.3>3 EL CABALLERO PIRATA</ep1.3>
			<id1.3>8363e50bd8e5e526004fe6274ced42fb6ba79470</id1.3>
			<ep1.4>4 AHOGADO EN EL MATRIMONIO</ep1.4>
			<id1.4>ad5cf331cdfc9be6f43b3885bdc39ab537bb8f93</id1.4>
			<ep1.5>5 LA MEJOR VENGANZA ES VESTIR CON ELEGANCIA</ep1.5>
			<id1.5>9a30fc5d6b49b10dfc71da8d94e0f29f98e6554d</id1.5>
			<ep1.6>6 EL ARTE DEL ACOJONE</ep1.6>
			<id1.6>4e62ace0492d249afa7a08d8f009696b3f744332</id1.6>
			<ep1.7>7 ESTO ESTA OCURRIENDO</ep1.7>
			<id1.7>65583bc69c500124b9cc10d487359d52dea36c28</id1.7>
			<ep1.8>8 LAS GAVIOTAS CAMINAN DE REGRESO</ep1.8>
			<id1.8>d11c391328bebdb865762dbf90792fe452baac27</id1.8>
			<ep1.9>9 AMNISTIA</ep1.9>
			<id1.9>4ad6f58b2a184b215b082f8e0309c5b9dffb7ac9</id1.9>
			<ep1.10>10 DONDE QUIERA QUE VAYA AHI ESTAS</ep1.10>
			<id1.10>df7df229581e743d06f6f19eb94c2851623d4847</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OBI-WAN KENOBI</title>
		<info>(2022) 6 episodios. Se centra en Obi-Wan Kenobi 10 años después del final de las Guerras Clon, donde enfrentó su mayor derrota; la caída y corrupción de su mejor amigo y aprendiz de Jedi, Anakin Skywalker se convirtió en el malvado Lord Sith Darth Vader.</info>
		<year>2022</year>
		<genre>Ciencia ficción. Aventuras </genre>
		<thumb>https://www.themoviedb.org/t/p/original/qJRB789ceLryrLvOKrZqLKr2CGf.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/oJ1KKAKiIuBA3s1tvtEa7NlQ2XM.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qJRB789ceLryrLvOKrZqLKr2CGf.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/oJ1KKAKiIuBA3s1tvtEa7NlQ2XM.jpg</fanart.1>
			<ep1.1>1 PARTE I</ep1.1>
			<id1.1>803E82A5DAC3E59946B5572D168B0C1F5BF19FBB</id1.1>
			<ep1.2>2 PARTE II</ep1.2>
			<id1.2>E5BD61D4E4B0DF9374071F995A60042BB6A2C8AE</id1.2>
			<ep1.3>PARTE III</ep1.3>
			<id1.3>A271E9F6183D657224A7C62C5A6AD45B255420F9</id1.3>
			<ep1.4>PARTE IV</ep1.4>
			<id1.4>4BCB321C078B2BCA5F6832645969DFC5BF8C83DD</id1.4>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OJO DE HALCON</title>
		<info>(2021) 6 episodios. Ojo de Halcón es una serie de Marvel que se centra en la historia del superhéroe Clint Barton que se encuentra viviendo en Nueva York tras los acontecimientos de Vengadores: Endgame, justo después del  trágico Lapso. Ahora como un ex Vengador se encuentra en una misión simple: la de reunirse con su familia por Navidad. Pero no lo tendrá tan fácil cuando alguien del pasado de Barton amenaza con echar a perder mucho más que el espíritu navideño. Pero no se encontrará solo, ya que tendrá la ayuda de Kate Bishop, una arquera de 22 años que sueña con un día en convertirse en una super heroína que siempre ha soñado llegar a ser.</info>
		<year>2021</year>
		<genre>Acción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/3ZcJetFxd8fwJ1I5xsLHCMTAKQo.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/1R68vl3d5s86JsS2NPjl8UoMqIS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/3ZcJetFxd8fwJ1I5xsLHCMTAKQo.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/1R68vl3d5s86JsS2NPjl8UoMqIS.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>25DD8BDD3F2635E6409AE892B8DCC1B1901DC09D</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OPERACION MAREA NEGRA</title>
		<info>(2022)  4 episodios. Noviembre 2019. Un semisumergible de construcción artesanal atraviesa el océano Atlántico con tres toneladas de cocaína en su interior. Dentro, tres hombres sobreviven a tormentas, corrientes, averías, hambre, peleas y un constante acoso policial. Al frente de los mandos va Nando, un joven gallego campeón de España de boxeo amateur y marino experto sin recursos económicos, que se ve obligado a buscar otros medios para ganarse la vida... Inspirada en la historia real de la operación policial homónima que persiguió al primer narcosubmarino en Europa dedicado al tráfico de estupefacientes.</info>
		<year>2022</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/13C2yjnFIIZwQzkf3WvPP0mnoPC.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fXs8FCiVqJsmLENvaKl9r3RYUNz.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/13C2yjnFIIZwQzkf3WvPP0mnoPC.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fXs8FCiVqJsmLENvaKl9r3RYUNz.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>77ea76464c52c1d2e894bf8c4c6ad95810582a14</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>84bcb52d589db639b2f2b159025316694e920d93</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OUTER BANKS</title>
		<info>(2020) 2 temporadas. 20 episodios. Un grupo de adolescentes de Outer Banks, en Carolina del Norte, descubre un secreto que desencadena una serie de turbios acontecimientos, y que embarcará a todos en una aventura inolvidable. Amores prohibidos, la desaparición del padre del líder del grupo, la búsqueda de un tesoro y el creciente conflicto entre los amigos y sus rivales darán lugar a un verano inolvidable de misterios y aventuras.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/ovDgO2LPfwdVRfvScAqo9aMiIW.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/nQLINyozCxcL7mlAz8OzWGYPaJS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/ovDgO2LPfwdVRfvScAqo9aMiIW.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/nQLINyozCxcL7mlAz8OzWGYPaJS.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>0312544338678f356b04f0ef731833e753d81b1e</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/oPxi7n5w4GRMi67MfLBxXwuaotz.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/qvEyo02cFEf6boAfKFtqD3HpWcc.jpg</fanart.2>
			<ep2.1>1 ORO</ep2.1>
			<id2.1>9ef20181747d8f15aa7afb74e2c323c793ab682b</id2.1>
			<ep2.2>2 AL 4</ep2.2>
			<id2.2>1bbb8885beae004ed2a96cd7c1c84c19d8192cd4</id2.2>
			<ep2.3>5 AL 7</ep2.3>
			<id2.3>7598c8e0bcfe2656842e0bbcb95c10f405976171</id2.3>
			<ep2.4>8 AL 10</ep2.4>
			<id2.4>945d317bfa89cee5287fdb88ce277ac0605a0676</id2.4>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OUTER RANGE</title>
		<info>(2022) 8 episodios. Royal Abbott (Josh Brolin), un ranchero de Wyoming que lucha por sacar adelante su ganado y mantener unida a su familia, descubre un día un misterioso agujero en sus tierras. El inexplicable descubrimiento coincide con una demanda de la familia Tillerson, los ambiciosos propietarios del rancho vecino que quieren apoderarse de sus tierras. Una desaparición y una muerte en la comunidad desencadenarán una serie de acontecimientos llenos de tensión en los que Royal solo contará con la singular complicidad de Autumn (Imogen Poots), una joven que ha acampado en sus tierras.</info>
		<year>2022</year>
		<genre>Western. Intriga. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/nO2qkja0cYBlx59fJ89TnrzDvHe.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/wI4aGtreUx7vFtZGcyiFoOX3qf3.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/nO2qkja0cYBlx59fJ89TnrzDvHe.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/wI4aGtreUx7vFtZGcyiFoOX3qf3.jpg</fanart.1>
			<ep1.1>1 EL VACIO</ep1.1>
			<id1.1>531da6b086cebf5f82dbe1fed3d6c049d618521d</id1.1>
			<ep1.2>2 LA TIERRA</ep1.2>
			<id1.2>41c9c95d2da2f11391dfe857bd8ae6f30956d4e8</id1.2>
			<ep1.3>3 EL TIEMPO</ep1.3>
			<id1.3>512cebc60e69f5b24da35d416a890ab6a3753c61</id1.3>
			<ep1.4>4 LA PERDIDA</ep1.4>
			<id1.4>2c5f2daf2bd22cd490208517ed2f91ec1eea38d8</id1.4>
			<ep1.5>5 LA TIERRA</ep1.5>
			<id1.5>7547420d51af610349b745a7120d7910ae0a805c</id1.5>
			<ep1.6>6 LA FAMILIA</ep1.6>
			<id1.6>c5626205016809f0df06b065e5efe28a54baae73</id1.6>
			<ep1.7>7 LA DESCONOCIDA</ep1.7>
			<id1.7>d89d4ec1ea7617f111059f234d3a2771613ad918</id1.7>
			<ep1.8>8 EL OESTE</ep1.8>
			<id1.8>badf4627ca936e39e86883b1ae6a9b4bc57723b3</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OUTLANDER</title>
		<info>(2014) 6 temporadas. 75 episodios. Sigue la historia de Claire Randall, una enfermera de combate casada en los años 40, que misteriosamente es arrastrada atrás en el tiempo hasta 1743, donde se lanza de inmediato a un mundo desconocido, viéndose amenazada su propia vida. Cuando se ve obligada a casarse con Jamie Fraser, un joven guerrero escocés caballeroso y romántico, Claire comienza un pasional triángulo entre dos hombres muy diferentes con dos vidas irreconciliables.</info>
		<year>2014</year>
		<genre>Drama. Romance. Ciencia ficción</genre>
		<thumb>https://image.tmdb.org/t/p/original/wwxKbrMlwMrC06B7aCIkDy3SAo6.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/qutOe29dCGmT2JUlLPCqlzaQIo.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/yE9EvhZh8mOPmPh4ZqApea4JHpn.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/qutOe29dCGmT2JUlLPCqlzaQIo.jpg</fanart.1>
			<ep1.1>1 SASSENCACH</ep1.1>
			<id1.1>2633a7400e1e1a652ebbea3045b17fa70ca803cf</id1.1>
			<ep1.2>2 CASTILLO LEOCH</ep1.2>
			<id1.2>fe62b4fafa6d2c030c65eeb2872db46a82e4bfdd</id1.2>
			<ep1.3>3 LA HUIDA</ep1.3>
			<id1.3>1f9fb7c96e3fba037734979385f134691f7baf8b</id1.3>
			<ep1.4>4 LA REUNION</ep1.4>
			<id1.4>0bb08b89f99f362e7f9896fc7277cadf986ebb3c</id1.4>
			<ep1.5>5 LA RECAUDACION</ep1.5>
			<id1.5>45a7e108e993ebc5dd9cae820aa8028c7627c914</id1.5>
			<ep1.6>6 EL COMANDANTE DE LA GUARNICION</ep1.6>
			<id1.6>a9ce63f615e280f03d5f9bd7a913e6757feb3a0d</id1.6>
			<ep1.7>7 LA BODA</ep1.7>
			<id1.7>14364e61c10c1cbe5ea60b3ccdd00bf5ab39660f</id1.7>
			<ep1.8>8 A AMBOS LADOS</ep1.8>
			<id1.8>d8c5eb87eba5d9b689c78215a1fd3398bff96d2b</id1.8>
			<ep1.9>9 AJUSTE DE CUENTAS</ep1.9>
			<id1.9>6d3ad6139ab29a43267dc81073f2a2b3d2169792</id1.9>
			<ep1.10>10 UN CASO DE BRUJERIA</ep1.10>
			<id1.10>77411ef2a11c756a0cc7cbe0a3d417a4dee790f4</id1.10>
			<ep1.11>11 LA MARCA DEL DIABLO</ep1.11>
			<id1.11>48018547167484fa810fa37ba800413fb0962d5b</id1.11>
			<ep1.12>12 LALLYBROCH</ep1.12>
			<id1.12>7b72dbef6329f4b32d1d5beb9a7a577d2e29cc22</id1.12>
			<ep1.13>13 LA GUARDIA</ep1.13>
			<id1.13>dae8a7935496e14f5854412a9c831b3a9b443ded</id1.13>
			<ep1.14>14 LA BUSQUEDA</ep1.14>
			<id1.14>a3b8188a82468634a26f44dba6c77ff9921cb6c2</id1.14>
			<ep1.15>15 LA CARCEL DE WENTWORTH</ep1.15>
			<id1.15>fcf05659d3795993f348b27f012da8b0f004a975</id1.15>
			<ep1.16>16 LA REDENCION DEL ALMA DE UN HOMBRE</ep1.16>
			<id1.16>f41f4bd6d5e4df992e3615d80774008b5f047219</id1.16>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/5cGhnwh6d0IvLvOg5Ko5Ybl7trV.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/pfRLVNeAlCjvLRuxLBEdxBMbUJl.jpg</fanart.2>
			<ep2.1>1 A TRAVES DEL ESPEJO A OSCURAS</ep2.1>
			<id2.1>da8567f17c0c6e66d5b6b0d825b35df47870cac8</id2.1>
			<ep2.2>2 NO MAS ESCOCIA</ep2.2>
			<id2.2>19e3d04bd96d2066ba817742240ba38df1c5f4c1</id2.2>
			<ep2.3>3 ACTIVIDADES UTILES Y ENGANOS</ep2.3>
			<id2.3>3e82c8fa8e68cd202bd86133dfe008d70f163998</id2.3>
			<ep2.4>4 LA DAMA BLANCA</ep2.4>
			<id2.4>eae431e67bccfa6d4dc0478ef5fd387807a357e9</id2.4>
			<ep2.5>5 RESURRECION INOPORTUNA</ep2.5>
			<id2.5>ec2d3dfe409f8abd834b1a1a7a0b0a20244cf738</id2.5>
			<ep2.6>6 LOS PLANES MEJOR TRAZADOS</ep2.6>
			<id2.6>fceabeb69b08c490c31b8e047fc7810699c26e17</id2.6>
			<ep2.7>7 FE</ep2.7>
			<id2.7>3bbe863b92000683825f3ff021f83cd4f40fbc74</id2.7>
			<ep2.8>8 LA GUARIDA DEL ZORRO</ep2.8>
			<id2.8>187b95bd6a94a743fafaaae51cf891a8f1e2b667</id2.8>
			<ep2.9>9 JE SUIS PREST</ep2.9>
			<id2.9>ce28cbcb959758bff8d258b23977480d581d56a4</id2.9>
			<ep2.10>10 PRESTONPANS</ep2.10>
			<id2.10>7b18beac9e6ab4205e782c5ec2f8aad57fbbc90f</id2.10>
			<ep2.11>11 LA VENGANZA ES MIA</ep2.11>
			<id2.11>64c07ecb761c890e00de197bcc1fcfcc2f68fa8b</id2.11>
			<ep2.12>12 EL AVEMARIA</ep2.12>
			<id2.12>e3a0fd812f69e9305f8cac5575c437cac0d7b70e</id2.12>
			<ep2.13>13 LIBELULA EN AMBAR</ep2.13>
			<id2.13>c209c53f44757468dbc60c8572f3a21d2f936b37</id2.13>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/9wINUBHicsC9HpJFUKwQ2SnQmrf.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/voWkHVZxwh34QHNqSelJNsWBI9H.jpg</fanart.3>
			<ep3.1>1 LA BATALLA CONTINUA</ep3.1>
			<id3.1>5a2fbcefee0bf7b6f568ecce0cb3f21d5ad9230c</id3.1>
			<ep3.2>2 RENDICION</ep3.2>
			<id3.2>4c95f4c0d42360170f2d48a8bda7030a6e1d634a</id3.2>
			<ep3.3>3 TODAS LAS DEUDAS PAGADAS</ep3.3>
			<id3.3>55278e909e55723ffd1ec0721f2d104ad4ef0fd2</id3.3>
			<ep3.4>4 DE LAS COSAS PERDIDAS</ep3.4>
			<id3.4>15cb582327447003b323a352033372f8f6f46570</id3.4>
			<ep3.5>5 LIBERTAD Y WHISKY</ep3.5>
			<id3.5>b52895290b850940f66c9f4710f3fa48c29e9a92</id3.5>
			<ep3.6>6 ALEXANDER MALCOM</ep3.6>
			<id3.6>beddd1c85f33faa40622d9b6e0069138049de77a</id3.6>
			<ep3.7>7 CREME DE MENTHE</ep3.7>
			<id3.7>d5e187be66a8795fa5c5c2735f237e8b4e45bb10</id3.7>
			<ep3.8>8 PRIMERA ESPOSA</ep3.8>
			<id3.8>4d950eacafcb522460403895e7151dab6e032947</id3.8>
			<ep3.9>9 MAR EN CALMA</ep3.9>
			<id3.9>8abeff51ac49072fc8bc039881180d3ec8324b95</id3.9>
			<ep3.10>10 CIELO Y TIERRA</ep3.10>
			<id3.10>825c57cf1ce42a12db464a8b2678d91be7d7c8a3</id3.10>
			<ep3.11>11 INEXPLORADO</ep3.11>
			<id3.11>24345f47c7870f12e3ab18847455c86f1d4ffb01</id3.11>
			<ep3.12>12 LA BAKRA</ep3.12>
			<id3.12>88faacfc374b8abac83b53123aa8d0bded0807b3</id3.12>
			<ep3.13>13 EN EL OJO DE LA TORMENTA</ep3.13>
			<id3.13>c2f3421860642e13db3deed2ce05ef8d63446718</id3.13>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/kRkV1k2cJYTUVQsTPJQnPvICa47.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/fa9Ls633phl4NQzw3JyAuwdYgPV.jpg</fanart.4>
			<ep4.1>1 PRECIOSO NUEVO  MUNDO</ep4.1>
			<id4.1>1bb2a60670fe14c39121a60d5711c3f0759717ba</id4.1>
			<ep4.2>2 NO HACER DAÑO</ep4.2>
			<id4.2>de7c25fb7ee78f85042ec513d0ad0ee654149c9f</id4.2>
			<ep4.3>3 LA NOVIA FALSA</ep4.3>
			<id4.3>0623865e9d2d3f107d8b5592b03f1dfc5de4c810</id4.3>
			<ep4.4>4 TERRENO COMUN</ep4.4>
			<id4.4>eac960522041c99a0da8024383fb1d69c8ce1dce</id4.4>
			<ep4.5>5 SALVAJES</ep4.5>
			<id4.5>86fc78d66f6e0eb1d9266bfb9b4ce42f9f247e5f</id4.5>
			<ep4.6>6 SANGRE DE MI SANGRE</ep4.6>
			<id4.6>b18275452ccaf486daf3de7ab74e8f1ab8c56835</id4.6>
			<ep4.7>7 DENTRO DEL AGUJERO DEL CONEJO</ep4.7>
			<id4.7>fae82e269515fe936f37c3de5e72d02e31231073</id4.7>
			<ep4.8>8 WILMINGTON</ep4.8>
			<id4.8>40de98264fb285609247e5b8b962f03052efd530</id4.8>
			<ep4.9>9 LAS AVES Y LAS ABEJAS</ep4.9>
			<id4.9>728a31fc296872bb9c654de4120702533a70fd3f</id4.9>
			<ep4.10>10 LA PROFUNDIDAD DEL CORAZON</ep4.10>
			<id4.10>e99fcc1c90428a6649791ed82a26452a0ca6cb27</id4.10>
			<ep4.11>11 SI NO ES POR LA ESPERANZA</ep4.11>
			<id4.11>11985acf776696dc5be56416040a6536eb9969b2</id4.11>
			<ep4.12>12 PROVIDENCIA</ep4.12>
			<id4.12>7a0e0e41e0f63bee8f72619d215088f048fbf6df</id4.12>
			<ep4.13>13 HOMBRE DE VALOR</ep4.13>
			<id4.13>58022759aec590440799dca9dec04699056ff4a2</id4.13>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/vjxxroFkSe7OODdzb8nzsY80zCu.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/piWI87vyNn4FtWYzsIL5Ee2OE7r.jpg</fanart.5>
			<ep5.1>1 LA CRUZ ARDIENTE</ep5.1>
			<id5.1>70aaabc72ca55e7a091c84f2a1a54dfbeabcc753</id5.1>
			<ep5.2>2 ENTRE DOS FUEGOS</ep5.2>
			<id5.2>1609226f5cac02c959719ed2d56233afa02cbbfd</id5.2>
			<ep5.3>3 LIBRE LIBERTAD</ep5.3>
			<id5.3>a75399ef36aae66a8d0da7c390b64dd4c57a7622</id5.3>
			<ep5.4>4 NUESTRAS COMPANIAS</ep5.4>
			<id5.4>92916de9e6a45239292b68fbad60e2674a1eb8af</id5.4>
			<ep5.5>5 ADORACION PERPETUA</ep5.5>
			<id5.5>461ed1fbba74af5f37bdd9e180efec59eb3a4d91</id5.5>
			<ep5.6>6 MEJOR CASARSE QUE ARDER</ep5.6>
			<id5.6>fe8e2aac1fd24f2950d28b327a4bee912d67758f</id5.6>
			<ep5.7>7 LA BALADA DE ROGER MARC</ep5.7>
			<id5.7>1a61a84a7e231c46ff5693a4a00cf3e2be4ac846</id5.7>
			<ep5.8>8 ULTIMAS PALABRAS CELEBRES</ep5.8>
			<id5.8>7fa9a32b96301ed8d3eac1bbda01295cfc4c9d43</id5.8>
			<ep5.9>9 MONSTRUOS Y HEROES</ep5.9>
			<id5.9>72924ba3d2999cda351c2831e96649dc7f45ad02</id5.9>
			<ep5.10>10 ME SEGUIRA LA CLEMENCIA</ep5.10>
			<id5.10>308f94bc378f24c4c7c32e1e93443b60d0abcda8</id5.10>
			<ep5.11>11 JOURNEYCAKE</ep5.11>
			<id5.11>61b69006211d4e5d777570c3b28bd206b3adcf74</id5.11>
			<ep5.12>12 NUNCA MI AMOR</ep5.12>
			<id5.12>28e51a006cc84baa2fd9ad8e5a755032089ca996</id5.12>
		</t5>
		<t6>
			<thumb.6>https://www.themoviedb.org/t/p/original/oR0PM7hFDqY3kO12cLLQMKiJt8I.jpg</thumb.6>
			<fanart.6>https://www.themoviedb.org/t/p/original/lnEU5KKrpQNsDaxxjDHuzRRh8Zl.jpg</fanart.6>
			<ep6.1>1 ECOS</ep6.1>
			<id6.1>B9BB8FFD09C5F6E59760BCC741FBFEC27879B3AB</id6.1>
			<ep6.2>2 ARMAS DE GUERRA</ep6.2>
			<id6.2>63DDD5771B874195D5892A4ADDFF3931ADEA5854</id6.2>
			<ep6.3>3 TEMPLANZA</ep6.3>
			<id6.3>205E33811792EAC30FFAB263F21EC8FD8C7FA2F2</id6.3>
			<ep6.4>4 LA HORA DEL LOBO</ep6.4>
			<id6.4>A13B7BA57D994696CEA174AA4A0D2C28FC8F490A</id6.4>
			<ep6.5>5 LIBERADME</ep6.5>
			<id6.5>501958EC547CA4A8049E3BF93E48117FABC8F3F7</id6.5>
			<ep6.6>6 EL MUNDO AL REVES</ep6.6>
			<id6.6>164282871B350552ACC1AD8D6E71A7B139AA0721</id6.6>
			<ep6.7>7 A PALABRAS NECIAS</ep6.7>
			<id6.7>B15956B4BD9E76DAF1750910396BFFD6A58AAB66</id6.7>
			<ep6.8>8 NO ESTOY SOLA</ep6.8>
			<id6.8>842F7760BF136D2CB382150A4090000C8B02E548</id6.8>
		</t6>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>OZARK</title>
		<info>(2017) 4 temporadas. 37 episodios. Marty Byrde (Jason Bateman) es un asesor financiero con una aparente vida normal en familia. Casado con Wendy (Laura Linney) y con dos hijos, Charlotte (Sofia Hublitz) y Jonah (Skylar Gaertner), todos llevan una vida apacible y ordinaria. Pero bajo esa apariencia la vida de Marty esconde un gran secreto: es el encargado de blanquear el dinero de uno de los cárteles de droga más importantes de México. Todo parece ir bien hasta que algo inesperado sucede y Marty debe llevarse a toda su familia desde Chicago a Ozark, en Missouri.</info>
		<year>2017</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/vbiy84IkwuUpoXGC9cGXaXykc45.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/hNaBXLiLTxMhtj7IFjOdJngXxxr.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/pCGyPVrI9Fzw6rE1Pvi4BIXF6ET.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/99JLwYsKg9ReNtr2h9ZgPfCdoYZ.jpg</fanart.1>
			<ep1.1>1 TEMPORADA A 3 TEMPORADA (1080)</ep1.1>
			<id1.1>0bd49a5dcb879d57231de3c3e5c36f09a859a7c7</id1.1>
			<ep1.2>2 AL 10</ep1.2>
			<id1.2>6d65334ea47f8fb8044175cc8cf0b095a78ce19f</id1.2>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/oTTKc6FTBUzNx8GPvC6JCPSFD5g.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/kaYsYb2V1mTkoIDwnOQIlU5Fkpo.jpg</fanart.2>
			<ep2.1>1 AL 10</ep2.1>
			<id2.1>746acb2fa5e2be6204216f01ef960b6104c4b4ea</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/oy7Peo5iFIt9sNM59lN6csbJeX2.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/dA4yGq7cXPG3YhDLGztMslPuW9w.jpg</fanart.3>
			<ep3.1>1 TIEMPOS DE GUERRA</ep3.1>
			<id3.1>516997289f1a246016dacb81e41153543e6f30c8</id3.1>
			<ep3.2>2 ENLACE CIVIL</ep3.2>
			<id3.2>df0731922320eff8c077132af23eea21536e8883</id3.2>
			<ep3.3>3 KEVIN CRONIN ESTUVO AQUI</ep3.3>
			<id3.3>d4c1e1c10733b74d6c8aa1e6634d0f2ca177bc67</id3.3>
			<ep3.4>4 PELEA ENTRE JEFES</ep3.4>
			<id3.4>70e0977da5c2688f7cf6665df3efa4d8ea1ae552</id3.4>
			<ep3.5>5 VINO DE MICHOACAN</ep3.5>
			<id3.5>bcdc8055cb89f4b08baf0f3e1048d7e562f02046</id3.5>
			<ep3.6>6 SU CASA ES MI CASA</ep3.6>
			<id3.6>19804e9356a32004a0fa8d182f4a186648b7a5e1</id3.6>
			<ep3.7>7 EN CASO DE EMERGENCIA</ep3.7>
			<id3.7>dab37fa12c1b55f8c3fdd1cbc0d2111251785186</id3.7>
			<ep3.8>8 UNA AMISTAD INQUEBRANTABLE</ep3.8>
			<id3.8>0aff3128b838659cdaaef5f388eaf9a0dd74ac1a</id3.8>
			<ep3.9>9 FIRE PINK</ep3.9>
			<id3.9>b45e7b1d1cafa2a29eb8a9ed08dec2b0f09c955f</id3.9>
			<ep3.10>10 A POR TODAS</ep3.10>
			<id3.10>fb9a241a3385c98867265d842631682632c467a9</id3.10>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/m73bD8VjibSKuTWg597GQVyVhSb.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/eUcV1rkUnqULehogmSjyPGu9cd.jpg</fanart.4>
			<ep4.1>1 EL PRINCIPIO DEL FIN</ep4.1>
			<id4.1>f105b2ddf57227f1974be61452aa5c9b2f8b62a1</id4.1>
			<ep4.2>2 QUE EL GRAN MUNDO GIRE</ep4.2>
			<id4.2>78619c81e26d8fdd77c0396e8c32c316980e16af</id4.2>
			<ep4.3>3 UNA CIUDAD AMBICIOSA</ep4.3>
			<id4.3>b4f10c4a730d9542c182358ab8a9674baf804ff0</id4.3>
			<ep4.4>4 UNA MALA JUGADA</ep4.4>
			<id4.4>8421800200a60287aa06a2ce403b0c454e774d4a</id4.4>
			<ep4.5>5 UNA POSIBLE EXPLICACION</ep4.5>
			<id4.5>22cc3a50a4cf35b3f48f16508211d35cc364ec3a</id4.5>
			<ep4.6>6 SANGRE SOBRE TODO</ep4.6>
			<id4.6>9c50e48dd578a9425f4c123238ed65226613aff6</id4.6>
			<ep4.7>7 SANTIFICADO</ep4.7>
			<id4.7>7e666088cbe6b849c9e0a7b4501690e1fa591a2f</id4.7>
			<ep4.8>8 EL SUEÑO Y LA MUERTE</ep4.8>
			<id4.8>066acff0f0b6138d7834ff2df096f3ad15a75803</id4.8>
			<ep4.9>9 ELIGE UN DIOS Y PONTE A REZAR</ep4.9>
			<id4.9>1d2cd9b39fa2f53a2de8be2f17b1cf4a34511bb5</id4.9>
			<ep4.10>1O TU ERES EL JEFE</ep4.10>
			<id4.10>dc631b0f2a3100ebedc64828d7e8d5546f7ae85c</id4.10>
			<ep4.11>11 TODAVIA EN PIE</ep4.11>
			<id4.11>3a727cb32a1149ebf57c609c8956bbf564b9a245</id4.11>
			<ep4.12>12 AGUAS REVUELTAS</ep4.12>
			<id4.12>1c5cab22d525db47d27860191e3b8ae0cea1591f</id4.12>
			<ep4.13>13 BARRO</ep4.13>
			<id4.13>ede5faf9a69b4af360e925ac065371134cf71e96</id4.13>
			<ep4.14>14 UN FINAL SIN RETORNO</ep4.14>
			<id4.14>40d026f90e7cbb76e6243a339b097ecdaccb233f</id4.14>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PACHINKO</title>
		<info>(2022) 8 episodios. Narra la historia épica e íntima que comienza con un amor prohibido y va creciendo hasta convertirse en una saga que viaja entre Corea, Japón y los Estados Unidos para contar un relato de guerra y paz, amor y pérdida, triunfo y ajuste de cuentas.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/wUTXdmL6oNjhiStGveOaPeuFOYQ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/vVKlL4HyrQYAcJuaaUW49FrRqY5.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/wUTXdmL6oNjhiStGveOaPeuFOYQ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/vVKlL4HyrQYAcJuaaUW49FrRqY5.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>b76f131c240f119ce73544243a1dc0845d0c7bb0</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>99713e74c19ba5b5e13297e43f707462e5c29eca</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>14338ac10e91c2490d4212c61361e54566cf5800</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>eaa40d79542811275eb4f845170e2f7501dfcb52</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>4d6c80b4132bc6b3c71d122fafad05c51c256708</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>f0d83bd0f8bba055448dc1286aed4324df3ed949</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>44be8fa5bdf7337fd425f9c1a041c799cae2cf08</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>1399cd29bfc659c20664f393007a4e2fa9e8433a</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PARA TODA LA HUMANIDAD</title>
		<info>(2019) 3 temporadas. 30 episodios. La serie explora lo que habría ocurrido si la carrera espacial mundial nunca hubiera terminado y la Unión Soviética hubiera conseguido superar a Estados Unidos como el primer país en poner a un hombre en la Luna.</info>
		<year>2019</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gcRmVGO0M7lbFYE3TwXs9GirM76.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/gEZvi3moCRLFQl0KGgClCtaEa1Q.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gcRmVGO0M7lbFYE3TwXs9GirM76.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/gEZvi3moCRLFQl0KGgClCtaEa1Q.jpg</fanart.1>
			<ep1.1>1 LUNA ROJA</ep1.1>
			<id1.1>7c662b795ac3d712405d5cbb0cf40986c56dbbab</id1.1>
			<ep1.2>2 EL CONSTRUYO EL SATURNO</ep1.2>
			<id1.2>aa3c986387dc38b94d1c5c2748d24aa946872bd1</id1.2>
			<ep1.3>3 LAS MUJERES DE NIXON</ep1.3>
			<id1.3>85e7c11a5aa4d73ef02b1b09addebbedd1fda476</id1.3>
			<ep1.4>4 LA TRIPULACION PRINCIPAL</ep1.4>
			<id1.4>1fbd64f1229df93eb3426348d732f216d3ad7db8</id1.4>
			<ep1.5>5 HACIA EL ABISMO</ep1.5>
			<id1.5>dc7fd18621b712ea8d96b98445a7686d23ed4799</id1.5>
			<ep1.6>6 DE VUELTA A CASA</ep1.6>
			<id1.6>03ca9f8d0cd66f2712bcad1d91985d7ab9141316</id1.6>
			<ep1.7>7 HOLA BOB</ep1.7>
			<id1.7>3c956043080da8491fe14ef3110e4a3a55bccd42</id1.7>
			<ep1.8>8 RUPTURA</ep1.8>
			<id1.8>aa84b8a61dd9e55cf7e5d430e5197d69e1d1dfdf</id1.8>
			<ep1.9>9 PAJARO HERIDO</ep1.9>
			<id1.9>a9d56e4a865ac1072a72e20277dedd273ae5a1ed</id1.9>
			<ep1.10>0 UNA CIUDAD EN LA COLINA</ep1.10>
			<id1.10>cee9df7e06d1d90ab816c34b190a95fddf729bfe</id1.10>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/q5vmnIJvXJgstbjCW9Jxhd4GoRJ.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/5sEV5v8vEGnNp9s06YjFI0RLkM4.jpg</fanart.2>
			<ep2.1>1 ABSOLUTAMENTE TODO</ep2.1>
			<id2.1>6c398a8771289a932af09923401e2aa9a4836094</id2.1>
			<ep2.2>2 LA HOJA ENSANGRENTADA</ep2.2>
			<id2.2>2bb43f37f95d1e09f44d4ab30453027015f22c0a</id2.2>
			<ep2.3>3 REGLAS DE ENFRENTAMIENTO</ep2.3>
			<id2.3>e0470e925f39348e93536d6019b3b0e30abd247d</id2.3>
			<ep2.4>4 PATHFINDER</ep2.4>
			<id2.4>eb737ba05205ed6a5356efdf401d76af62614fd5</id2.4>
			<ep2.5>5 EL PESO</ep2.5>
			<id2.5>d245482215d1c5d44f5223b16962e3ef3ec7b5e3</id2.5>
			<ep2.6>6 HASTA EN LAS MEJORES FAMILIAS</ep2.6>
			<id2.6>2fa0829cda81705455244038242cb1a5c9979846</id2.6>
			<ep2.7>7 AL 8</ep2.7>
			<id2.7>3df036fce0542460d45793aa2fa284c268358f4b</id2.7>
			<ep2.8>9 TRIAGE</ep2.8>
			<id2.8>3bb4f86780b795f4ced9845228fa26d0ba12e87b</id2.8>
			<ep2.9>10 EL GRIS</ep2.9>
			<id2.9>a13d1d8a7c3a32fc02f00e13ac111864cc485f80</id2.9>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/5Y6VNHILuyw5Dc9o7xyC5u6arG1.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/wufW6p9oRC38MnYW7lmUlsTUak8.jpg</fanart.3>
			<ep3.1>1 POLARIS</ep3.1>
			<id3.1>47734a71198d8244dab7858a59dad54f37c3ea45</id3.1>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PARAISO</title>
		<info>(2021) 7 episodios. Levante, 1992, final del verano en un pueblo de la costa. Sandra, Eva y Malena, de 15 años, desaparecen en una discoteca sin dejar rastro. La policía no parece estar buscando en la dirección correcta, y por eso Javi, el hermano pequeño de Sandra, comienza una investigación junto a Quino y Álvaro, sus mejores amigos, y Zeta, el matón de la clase. Juntos descubren que quienes tienen a su hermana no son de este mundo.</info>
		<year>2021</year>
		<genre>Fantástico. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/6KkM6gLFloyybv6qFZs6t5taWws.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/q6vO4jguYHeAcZcaGtZ21wi3tGj.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/6KkM6gLFloyybv6qFZs6t5taWws.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/q6vO4jguYHeAcZcaGtZ21wi3tGj.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>9e83c9d6f2837386e131530029e4a20fdff79a76</id1.1>
			<ep1.2>4 LA FERIA</ep1.2>
			<id1.2>78899a2e46122d542d27b1972430fdeac31f16e3</id1.2>
			<ep1.3>5 PRESA</ep1.3>
			<id1.3>f7090bd515b8ae140f76b0e83e7f7c7fe6ff52d2</id1.3>
			<ep1.4>6 TRES CHICAS CADA TRES AÑOS</ep1.4>
			<id1.4>14a26fc3d2bd65987cd2c1b523439a7a74653f89</id1.4>
			<ep1.5>7 EL BALNEARIO</ep1.5>
			<id1.5>53c837ef5d5ba95c8a987481662a890d0ab0cbf9</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PARALELOS DESCONOCIDOS</title>
		<info>(2022) 6 episodios.La vida de cuatro adolescentes, Bilal, Romane, Sam y Victor, se ve gravemente alterada cuando un misterioso suceso los separa y los envía a dimensiones paralelas. Ellos intentan entender qué ha pasado y hacen lo posible para volver atrás en el tiempo para recuperar su vida anterior.</info>
		<year>2022</year>
		<genre>Aventuras. Fantastico. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gVbpn70pkhb5kjcFubz5GAhFkpA.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/sIdedEB5hZNQhDCVY3pgRXny89z.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gVbpn70pkhb5kjcFubz5GAhFkpA.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/sIdedEB5hZNQhDCVY3pgRXny89z.jpg</fanart.1>
			<ep1.1>1 EL MUNDO EN TU CARA</ep1.1>
			<id1.1>7f58f5daafdc0cd3e26ae068cbdea56af32845ab</id1.1>
			<ep1.2>2 CONTRA TODO ATENTADO</ep1.2>
			<id1.2>c3679b20b032da86b60f31f855cc8d33ffd71cad</id1.2>
			<ep1.3>3 TIEMPO PERDIDO</ep1.3>
			<id1.3>3c321535de7b9e626d581cd2e72b7fa0ee55db5b</id1.3>
			<ep1.4>4 INOCENCIA PASADA</ep1.4>
			<id1.4>4ad16afef8c076249cfc22a28cff0eeb86466dfa</id1.4>
			<ep1.5>5 UN PLAN SENCILLO</ep1.5>
			<id1.5>48e343eb3cf12a5bdc5414d19913b2cc6ce7559a</id1.5>
			<ep1.6>6 4 HORAS</ep1.6>
			<id1.6>5fed8597ed5d7ffeb8268f4535d17570ea5b0c88</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PAROT</title>
		<info>82021) 10 episodios. Thriller de ficción ambientado en España en el año 2013. Tras la derogación de la doctrina judicial “Parot” por el Tribunal Europeo de Estrasburgo, decenas de presos han sido puestos en libertad. Pocos días después, algunos excarcelados empiezan a aparecer asesinados de la misma forma en que lo fueron sus víctimas. Isabel Mora (Adriana Ugarte), es una policía íntegra y perseverante que tendrá que asumir la investigación de los asesinatos mientras se enfrenta al intento de venganza por parte de uno de los excarcelados.</info>
		<year>2021</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/tFKjXfDJFdRkK2RTaCHPPj9W1U5.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3kHNfamyaBydtUK9AjnxdwE1kUS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/tFKjXfDJFdRkK2RTaCHPPj9W1U5.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3kHNfamyaBydtUK9AjnxdwE1kUS.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>32280b23b252401e87d7b3ce24d619b51d8f4f2f</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PASAPORTE A LA LIBERTAD</title>
		<info>(2021) 8 episodios. Finales de los años 30 en la Alemania Nazi. Aracy es una joven empleada del Consulado de Brasil en Hamburgo. A pesar de las estrictas normas de inmigración de su Brasil natal y de su falta de inmunidad diplomática, Aracy expidió en secreto pasaportes a judíos para ayudarles a huir de los nazis. Su colega, João Guimarães Rosa, cónsul adjunto, se convertirá en su mejor aliado. Basada en la historial real de Aracy de Carvalho.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/vlko98oAN0vWeJWmm1kePPj0Foa.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/2PCXDdPd5vUY4gStEUGslG8RiPm.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/vlko98oAN0vWeJWmm1kePPj0Foa.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/2PCXDdPd5vUY4gStEUGslG8RiPm.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>84051e92980f9a732e212c8593fdda8a654f743a</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>6219e382269439f16da493d7e8a4705bb107b1a2</id1.2>
			<ep1.3>5 Episodio</ep1.3>
			<id1.3>b59963757b3157772b9a5f6a011a1f181c66ac44</id1.3>
			<ep1.4>6 Episodio</ep1.4>
			<id1.4>45516ca7c8ecd5951784ab2f1ef008892bb78177</id1.4>
			<ep1.5>7 Episodio</ep1.5>
			<id1.5>1e1dd210d034344347f1203495410404f808f0a6</id1.5>
			<ep1.6>8 Episodio</ep1.6>
			<id1.6>dad0cb9425d677faf47c48677938f2f67a12090f</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PATRIA</title>
		<info>(2020) 8 episodios. Basada en la novela de Fernando Aramburu, que abarca 30 años del conflicto vasco y estudia el impacto del mismo sobre la gente común, como la viuda de un hombre asesinado a tiros por la banda terrorista ETA, que vuelve a su pueblo natal tras el alto el fuego de 2011, o la madre de un etarra encarcelado.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/yTvhNt0QxXG7ww5hrUQwFtQoKIw.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/7Ker8fiaSLYJvgyUPIQyrPpgbUO.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/yTvhNt0QxXG7ww5hrUQwFtQoKIw.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/7Ker8fiaSLYJvgyUPIQyrPpgbUO.jpg</fanart.1>
			<ep1.1>1 AL 8 (1080)</ep1.1>
			<id1.1>2a21b1b9b80d70a8176bb904c901871ebd794de2</id1.1>
			<ep1.2>1 OCTUBRE BENIGNO</ep1.2>
			<id1.2>f07b4d74edc4a0c5760a1ee663c11ef92bc9fe3a</id1.2>
			<ep1.3>2 ENCUENTROS</ep1.3>
			<id1.3>f749170f316d4ef28af9ece167cf8c1ae1565e2c</id1.3>
			<ep1.4>3 ULTIMAS MERIENDAS</ep1.4>
			<id1.4>a32fb95606d8551f0768055e473699d2b105382b</id1.4>
			<ep1.5>4 TXATO, ENTZUN, PIM, PAM, PUM</ep1.5>
			<id1.5>8227df3bd756ed8cfb01796adabfeaac5a89d924</id1.5>
			<ep1.6>5 EL PAIS DE LOS CALLADOS</ep1.6>
			<id1.6>137db4e7350df89b408b5807dc3ef6eae74921a5</id1.6>
			<ep1.7>6 PATRIAS Y MANDANGAS</ep1.7>
			<id1.7>bf2a38b5ea46675a7d5553bfe5c6ad0573446fba</id1.7>
			<ep1.8>7 PAN ENSANGRENTADO</ep1.8>
			<id1.8>3786764f2ca146231f7d877a8e946c36186b8053</id1.8>
			<ep1.9>8 MAÑANA DE DOMINGO</ep1.9>
			<id1.9>8b5517229bea957c1c0ced5f0b809c162df51ec6</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PAU GASOL LO IMPORTANTE ES EL VIAJE</title>
		<info>(2021) 4 episodios. Lo más difícil de dedicarte al deporte que amas es despedirte de él. Este documental muestra el final de la carrera de leyenda de uno de los mejores deportistas españoles de todos los tiempos. Podremos ver la lucha y el esfuerzo para volver a las pistas y poder poner fin a su carrera jugando a baloncesto. Además, también veremos la manera de afrontar el futuro y los nuevos retos que le deparan a Pau una vez colgadas las zapatillas.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/gptizswdiBvQsLPc7Soye94VHvs.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/ar3XSye5wuctW8egQnc6jqe2SVG.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/gptizswdiBvQsLPc7Soye94VHvs.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/ar3XSye5wuctW8egQnc6jqe2SVG.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>76540c063e45884071312c1dd06fd7f6215c7889</id1.1>
			<ep1.2>2 AL 4</ep1.2>
			<id1.2>9835db87cd02bcf471e3a96d20e1cfd60b3a2a24</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PEAKY BLINDERS</title>
		<info>(2013) 6 temporadas. 36 episodios. En Gran Bretaña, Reino Unido se recuperan de la desesperación de la Gran Guerra, las personas sobreviven a como pueden, y las bandas criminales proliferan en una nación sacudida económicamente. Es justamente aquí donde una familia de gánsteres irlandeses de origen nómada (a veces llamados gitanos o chatarreros) asentada en Birmingham (los Peaky Blinders) justo después de la Primera Guerra Mundial, dirigen un local de apuestas hípicas en la ciudad. Las acciones del ambicioso, respetado, temerario y peligroso jefe de la banda, Thomas Shelby, llaman la atención del Inspector jefe Chester Campbell, un detective de la Real Policía Irlandesa que es enviado por el mismo Winston Churchill desde Belfast donde había sido enviado a limpiar la ciudad del Ejército Republicano Irlandés (IRA), comunistas, pandillas y delincuentes comunes.</info>
		<year>2013</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/AtJ3p0kbD6gcCggmPdjh2ltNzQm.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/dzq83RHwQcnP6WGJ6YkenIqeaa5.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/ky4IIpGFT1BHID7oQdwUK0Lg0lZ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/xqwY1TqwKECESoMnSpitrNGN5OO.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>2ee0e0182d91116af8aad238e8593cf26438c805</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>6cad560d2b883ca1ea9d295318989fa5be8c0e6e</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>9293b25ce0d47a7c09ce9bc57ed0f2a22ffda15a</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>98263c030926417585a28775070fbe934c196292</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>1b9adcbc47c08a516cfb3c687e8ac65212cbdff8</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>936fcda4702c83c530eee4b658d4ecd4fa067f14</id1.6>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/q1aC54M1hQISniMrwPJvxKdiHOC.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/kfUWupX3phYp7AleZA2U1dmVcjX.jpg</fanart.2>
			<ep2.1>1 Episodio</ep2.1>
			<id2.1>18ab6dcbaf519613ef17c82dd79b84369460f3c0</id2.1>
			<ep2.2>2 Episodio</ep2.2>
			<id2.2>83a10cc333335f0fcf84b3b1a78c238bab2f0f66</id2.2>
			<ep2.3>3 Episodio</ep2.3>
			<id2.3>f2f5dbcec6982ab284c7ad0e009f6c7cd927e8bf</id2.3>
			<ep2.4>4 Episodio</ep2.4>
			<id2.4>c078c277acc1759d59d2501787e650e81f8872d3</id2.4>
			<ep2.5>5 Episodio</ep2.5>
			<id2.5>682e313e72cd9719682a49c5e331006d5955dc7f</id2.5>
			<ep2.6>6 Episodio</ep2.6>
			<id2.6>60218fbeab6a99cdd48ecf71b939807756de6a3f</id2.6>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/vFwUHEgjPovlpXER2AZsMckShFh.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/6n63aBdgPH22PzNgd1kcDZlmwkM.jpg</fanart.3>
			<ep3.1>1 Episodio</ep3.1>
			<id3.1>fd6106b9f1a5006b35fdd44b4a1bdb9c137058a6</id3.1>
			<ep3.2>2 Episodio</ep3.2>
			<id3.2>9d8f560e710579d37e9d29f7dc15756189a4f21c</id3.2>
			<ep3.3>3 Episodio</ep3.3>
			<id3.3>7f211a45483f0c5bf2610be9bbd6e447ce226318</id3.3>
			<ep3.4>4 Episodio</ep3.4>
			<id3.4>96da8ddc1cce384d18020e571d3ea16e9a749ea7</id3.4>
			<ep3.5>5 Episodio</ep3.5>
			<id3.5>5b3661391c62b5fa8ebc7fa47170c4b307240645</id3.5>
			<ep3.6>6 Episodio</ep3.6>
			<id3.6>58fca6de4951622282743dc1a3718306f7217b35</id3.6>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/se86cWSwdSftjJH8OStW7Yu3ZPC.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/wiE9doxiLwq3WCGamDIOb2PqBqc.jpg</fanart.4>
			<ep4.1>1 LA HORCA</ep4.1>
			<id4.1>1cfc291103e15fe46f3cc46defc5cb77459ea976</id4.1>
			<ep4.2>2 PAGANOS</ep4.2>
			<id4.2>40768f33986ab94dbcfa54f7fd86d027143705c1</id4.2>
			<ep4.3>3 MIRLO</ep4.3>
			<id4.3>e55312708e41187534c03f17e14d1227ecbc4c8c</id4.3>
			<ep4.4>4 PELIGROSA</ep4.4>
			<id4.4>a9a01f139f92159d0f464fedf851395bcad8d4ac</id4.4>
			<ep4.5>5 EL DUELO</ep4.5>
			<id4.5>0965b5509f3c3900d75146641337bb5147ec68de</id4.5>
			<ep4.6>6 LA COMPAÑIA</ep4.6>
			<id4.6>98a643629492d086dba70774a5da2e8305b3ffc9</id4.6>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/arlFd2cGgP9kvEnL9FwXlcTm28D.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/lPhtB5KxpNAR1SOhj7eydgrAaXX.jpg</fanart.5>
			<ep5.1>1 MARTES NEGRO</ep5.1>
			<id5.1>1d0bdfd50317b89b64966c72335a698afb3e1b3f</id5.1>
			<ep5.2>2 AL 5</ep5.2>
			<id5.2>c2f031b9ed33c1868861d68c0d910b8aa97230a3</id5.2>
			<ep5.3>5 CONMOCION</ep5.3>
			<id5.3>a7c80841c2c6c3b9b9defb26faa6e3e73cbd9cf0</id5.3>
			<ep5.4>6 SR JONES</ep5.4>
			<id5.4>c5ab1b5371ceba89a290adb839503d3c9291a5c6</id5.4>
		</t5>
		<t6>
			<thumb.6>https://www.themoviedb.org/t/p/original/vUUqzWa2LnHIVqkaKVlVGkVcZIW.jpg</thumb.6>
			<fanart.6>https://www.themoviedb.org/t/p/original/hI5h8o3bbZlZwnySEs6rL7pXH32.jpg</fanart.6>
			<ep6.1>1 AL 6</ep6.1>
			<id6.1>F869EC3314384B4CE9311FA5FFDBA254F47A0D65</id6.1>
		</t6>
		<t7>
			<thumb.7>https://www.themoviedb.org/t/p/original/bGZn5RVzMMXju4ev7xbl1aLdXqq.jpg</thumb.7>
			<fanart.7>https://www.themoviedb.org/t/p/original/1c5wA3y9K8WRwQRcU8oGFRNJ7Yn.jpg</fanart.7>
			<ep7.1>TEMPORADAS 1 A 5</ep7.1>
			<id7.1>49be1cc5e1bd5849fc90099b4b1899556ba0a3a6</id7.1>
		</t7>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PENNY DREADFUL</title>
		<info>(2014-2016) 3 temporadas. 27 episodios. En la Inglaterra victoriana, con el telón de fondo de famosas creaciones literarias como Frankenstein, Drácula o Dorian Gray, un antiguo explorador, Sir Malcolm Murray, una vidente con secretos, Vanessa Ives, un pistolero americano, Ethan Chandler, y un prometedor cirujano se unen en Londres para combatir una amenaza sobrenatural que habita en el inframundo e intentar rescatar a una joven desaparecida.</info>
		<year>2014</year>
		<genre>Terror. Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/j7BgVwXNYRfePzRUPrC4DdLVBAy.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/97ngHA11iPd5Z6rw9pwKUAKB4OS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/j7BgVwXNYRfePzRUPrC4DdLVBAy.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/97ngHA11iPd5Z6rw9pwKUAKB4OS.jpg</fanart.1>
			<ep1.1>1 TRABAJO NOCTURNO</ep1.1>
			<id1.1>8b6a9e937571094eb9024f566694af99d12f3d14</id1.1>
			<ep1.2>2 SESION ESPIRITISTA</ep1.2>
			<id1.2>20d0d3006a4107c40cc611f76d494063b4e0d1bd</id1.2>
			<ep1.3>3 RESURRECION</ep1.3>
			<id1.3>a674edb98ba75655d17acff2f5f84ab271ff076e</id1.3>
			<ep1.4>4 LOS MARGINALES</ep1.4>
			<id1.4>811068695affb4c42b7f273ad84d40068e56fa6e</id1.4>
			<ep1.5>5 MAS QUE HERMANAS</ep1.5>
			<id1.5>ee078f4efe208b269153d2683460d6e69a2c256e</id1.5>
			<ep1.6>6 LO QUE LA MUERTE PUEDE JUNTAR</ep1.6>
			<id1.6>856e6f97b072e860e505a950419b130f068f81a0</id1.6>
			<ep1.7>7 POSESION</ep1.7>
			<id1.7>d0b5a4e3e4fec28b3111c853cf04945719d9be34</id1.7>
			<ep1.8>8 EL GRAN GUIÑOL</ep1.8>
			<id1.8>ed08ba26899c46f2694ae099c85a20c960d407dd</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/uNcmVroHdW835vGD0XdKr25ZNyY.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/tQa9JVK5PlbO0Bxuj4YMe0A9Cal.jpg</fanart.2>
			<ep2.1>1 AL 10 (1080)</ep2.1>
			<id2.1>62d26f802045ea0d39941cf3755888bf023cad6b</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/hQSdrXBYTbLGHYDIseHkBOPXTgL.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/32iacqVElB91YZKl9Ky3QEEkXtH.jpg</fanart.3>
			<ep3.1>1 AL 9 (1080)</ep3.1>
			<id3.1>a6e86b9df1b02e23153bb0e4c10c3a9223324213</id3.1>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PENNYWORTH</title>
		<info>(2019) 2 temporadas. 20 episodios. La historia de Alfred Pennyworth, un antiguo miembro de las fuerzas especiales londinenses que se encarga de trabajar para el padre de Bruce Wayne.</info>
		<year>2020</year>
		<genre>Acción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/czVjj5W113Aggz8fmtiW5bY1Vsz.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fOdbXYcLTthRYlOA77LkKI9fxu3.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/czVjj5W113Aggz8fmtiW5bY1Vsz.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fOdbXYcLTthRYlOA77LkKI9fxu3.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>17c22af44ef74714a1e2aed094e55ba5b2e1c001</id1.1>
			<ep1.2>2 LA HIJA DEL PROPIETARIO</ep1.2>
			<id1.2>ee3d639cf491555efd48676c0162da98a2c685dd</id1.2>
			<ep1.3>3 AL 4</ep1.3>
			<id1.3>3709c6cacd1787d4b45327ff371784a9c53cfe97</id1.3>
			<ep1.4>5 SHIRLEY BASSEY</ep1.4>
			<id1.4>f540302486c83bbfcdee977ac1d1b394434aaab6</id1.4>
			<ep1.5>6 AL 7</ep1.5>
			<id1.5>d5d59e8fbb1edd094b30bf1383aeb0240b4a5ef8</id1.5>
			<ep1.6>8 AL 10</ep1.6>
			<id1.6>21bc3b653fa276f95b74c1b397c8fab489e1d5ff</id1.6>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/sF14a0UUhdLmDMv5vQ3QudoM31w.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/uaKaTGxIcjGb1mb1gY9W0LiCX4b.jpg</fanart.2>
			<ep2.1>1 LA CORONA PESADA</ep2.1>
			<id2.1>0de76783cbf40df8670d014d66854e848776814f</id2.1>
			<ep2.2>2 EL PUENTE EN LLAMAS</ep2.2>
			<id2.2>0da64d06a460546940a8f3c90287bc54c1ea6198</id2.2>
			<ep2.3>3 EL CINTURON Y EL VERDUGON</ep2.3>
			<id2.3>c5aa7396e60e6251eca54bf2b3cce53d88ed3b2f</id2.3>
			<ep2.4>4 EL ZORRO CAZADO</ep2.4>
			<id2.4>d6fabd005d6d6d71b95d2cb418151741adf1b50f</id2.4>
			<ep2.5>5 CORAZON SANGRANTE</ep2.5>
			<id2.5>7ff6f9646a690012ca69319babe1a888b3b485e2</id2.5>
			<ep2.6>6 LA ROSA Y LA ESPINA</ep2.6>
			<id2.6>25f5873fb43378c6e0b062a8a3faf8d064351ae3</id2.6>
			<ep2.7>7 THE BLOODY MARY</ep2.7>
			<id2.7>64cc58ef62da75062ce490b275ed55fae1edcd94</id2.7>
			<ep2.8>8 LA SOGA DEL VERDUGO</ep2.8>
			<id2.8>4928085fb646fdce2f599fbdbfd02f1dda6068c6</id2.8>
			<ep2.9>9 PARADISE LOST</ep2.9>
			<id2.9>cab9ff661226371ce13ae8b8dcac6b94c600bc8c</id2.9>
			<ep2.10>10 EL LEON Y EL CORDERO</ep2.10>
			<id2.10>c9d581ae6286460371463f7e03968edce010a87e</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PLANETA PREHISTORICO</title>
		<info>(2022) 5 episodios. Nos traslada a 66 millones de años al pasado para descubrir cómo era nuestro mundo y los dinosaurios que lo habitaban. Con banda sonora del gran Hans Zimmer.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/u7Ulem8hT8nKVPvYlTeNLoWXfvk.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/6342n1ojL6RDUg7kgIBlrq200zZ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/u7Ulem8hT8nKVPvYlTeNLoWXfvk.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/6342n1ojL6RDUg7kgIBlrq200zZ.jpg</fanart.1>
			<ep1.1>1 COSTAS</ep1.1>
			<id1.1>cb6944c7015c9bf7529731d5e88f59a77cc0542d</id1.1>
			<ep1.2>2 DESIERTOS</ep1.2>
			<id1.2>b22aff50a44871a900965b9aceee4588047d112c</id1.2>
			<ep1.3>3 AGUA DULCE</ep1.3>
			<id1.3>c0cc45c41c7640a34fff3ab1e0971d9cd5c14b1a</id1.3>
			<ep1.4>4 MUNDOS DE HIELO</ep1.4>
			<id1.4>954e4e2e998c3420a90071bb66a1d7e1955d6b91</id1.4>
			<ep1.5>5 BOSQUES</ep1.5>
			<id1.5>d1ab1d09e5b3a2c538b5d7386238ea551841f441</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PLANETA VERDE</title>
		<info>(2022) 5 episodios. Esta serie es el primer retrato inmersivo de un mundo invisible e interconectado, lleno de nuevos y notables comportamientos, historias emocionales y seres vivos sorprendentes. Para su nueva superproducción, BBC Earth se ha valido de los nuevos desarrollos en robótica, "time-lapse" en movimiento, cámaras térmicas súper detalladas, enfoque profundo 'frame-stacking' y ultra alta velocidad para viajar más allá del poder del ojo humano y hacer visible la increíble y oculta vida de este Planeta Verde. Sir David Attenborough viaja a los Estados Unidos, Costa Rica, Croacia y el norte de Europa entre otras localizaciones.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/1P189avV6tFiJuFyqu1WDiOLaLB.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/LdsK8teOviNOCvGABfuxlbttcH.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/1P189avV6tFiJuFyqu1WDiOLaLB.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/LdsK8teOviNOCvGABfuxlbttcH.jpg</fanart.1>
			<ep1.1>1 MUNDOS TROPICALES</ep1.1>
			<id1.1>13fc0e818322d9bd3ca76aa99dfc2409e2665022</id1.1>
			<ep1.2>2 MUNDOS ACUATICOS</ep1.2>
			<id1.2>5b0c7b1f0df810da9003e29b9feccc3a32797f56</id1.2>
			<ep1.3>3 MUNDOS ESTACIONALES</ep1.3>
			<id1.3>91cc5a0cf97cd6789d9cfe160efcfb4d548d1c91</id1.3>
			<ep1.4>4 MUNDOS DEL DESIERTO</ep1.4>
			<id1.4>951e4ae235519950613004c86c23114aae571a31</id1.4>
			<ep1.5>5 MUNDOS DEL DESIERTO</ep1.5>
			<id1.5>dd84872eaae3636814f816625171463651f78585</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PODRIA DESTRUIRTE</title>
		<info>(2020) 12 episodios. Arabella Essiuedu (Michaela Coel) es una joven escritora fácilmente distraída, descomprometida y despreocupada que, tras escribir un exitoso texto que atrajo mucha atención en internet, se encuentra proclamada como la ‘voz de su generación’. Ahora cuenta con un agente, un contrato literario y una inmensa presión. Después de ser agredida sexualmente en un club nocturno, su vida cambia irreversiblemente y Arabella se ve obligada a reevaluar todo: su carrera, sus amigos, y hasta su familia. Mientras Arabella lucha por aceptar lo que sucedió, también comienza un viaje de autodescubrimiento.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/9awmdcJsukBEY5Er8t4Vi1Ji7YX.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/kroGoBecq43uCFIwfUkvyr0GKI4.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/9awmdcJsukBEY5Er8t4Vi1Ji7YX.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/kroGoBecq43uCFIwfUkvyr0GKI4.jpg</fanart.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>PONGAMOS QUE HABLO DE SABINA</title>
		<info>(2020) 3 episodios. Miniserie documental  sobre Joaquín Sabina, que repasa su carrera y su vida tanto encima como fuera de los escenarios, con entrevistas al artista y a gente de su entorno que aporta una mirada personal al autor.</info>
		<year>2020</year>
		<genre>Documental</genre>
		<thumb>https://image.tmdb.org/t/p/original/fxq4ItdkMRMwuniGSFjYcwZ1azH.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/xNpEJTvXFb1NO21OSGmPGhQznv3.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/fxq4ItdkMRMwuniGSFjYcwZ1azH.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/xNpEJTvXFb1NO21OSGmPGhQznv3.jpg</fanart.1>
			<ep1.1>1 LOS PECADOS</ep1.1>
			<id1.1>102726b18f1fba92814dac78edfb30a28ee2a3b1</id1.1>
			<ep1.2>2 LOS AMORES</ep1.2>
			<id1.2>6edd91748cebbbc9aeb98a3a967918ef31cf3eb6</id1.2>
			<ep1.3>3 LAS PASIONES</ep1.3>
			<id1.3>f59ed6e6cb37d8dc1b364c14e12dc5737e684d41</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>POR H O POR B</title>
		<info>(2020) 10 episodios. Con el madrileño barrio de Malasaña convertido en un personaje más, contará la historia de Hache y Belén, dos amigas de Parla que, tras años sin hablarse, se reencuentran en su nueva vida en el centro de la ciudad. El inevitable choque cultural entre los dos mundos dará lugar a una catástrofe de divertidas proporciones.</info>
		<year>2020</year>
		<genre>Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/yyYBZRfTYcKNfvyqlxRX2NUmYei.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/xHMivTetE2L9F80YhD20KfL63v7.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/yyYBZRfTYcKNfvyqlxRX2NUmYei.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/xHMivTetE2L9F80YhD20KfL63v7.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>c12e467683d9a65748349622bdd52f715b208189</id1.1>
			<ep1.2>3 AL 5</ep1.2>
			<id1.2>cd2ae7cbb9d4db9c0feb27a07aa73f9db4259104</id1.2>
			<ep1.3>6 LOS PADRES DE BELEN</ep1.3>
			<id1.3>747083df051c9dad89db887b7b9c6fde111a7455</id1.3>
			<ep1.4>7 SOPA DE LETRAS</ep1.4>
			<id1.4>a8a02f96a579c6dcaa9a7db0390d92b8113b7ca4</id1.4>
			<ep1.5>8 AL 10</ep1.5>
			<id1.5>af88685eecfe703ba10926920468c6bca5777df0</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>POSE</title>
		<info>(2018) 3 temporadas. 26 episodios. Pose se desarrolla en 1986 y analiza la yuxtaposición de varios segmentos de la vida y la sociedad en Nueva York: el auge del universo de lujo de la era Trump, la escena social y literaria del centro y el mundo de la cultura.</info>
		<year>2018</year>
		<genre>Drama. Musical</genre>
		<thumb>https://image.tmdb.org/t/p/original/1cBGu1KEP17Dexg4wYfR9bLF3kc.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/7JxNzXx9h3HvOWkWZ4gJnF2Onjg.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/1cBGu1KEP17Dexg4wYfR9bLF3kc.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/7JxNzXx9h3HvOWkWZ4gJnF2Onjg.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>6828e395a61584518f8d1a0fe002d908ae391a9a</id1.1>
			<ep1.2>2 ACCESO</ep1.2>
			<id1.2>6bbfa751fd38144654931ab7e80fc4b3879e051a</id1.2>
			<ep1.3>3 DAR Y RECIBIR</ep1.3>
			<id1.3>ae1a4e0bd9b4c7446c5d81c750b69b1dd2adb74a</id1.3>
			<ep1.4>4 AL 6</ep1.4>
			<id1.4>7d571c118ba56c06241fa82326af6b398ddc44b0</id1.4>
			<ep1.5>7 PINK SLIP</ep1.5>
			<id1.5>72e7b7e0fff46f60af95195ca6e380694dab1d37</id1.5>
			<ep1.6>8 LA MADRE DEL AÑO</ep1.6>
			<id1.6>e6686c14d31712ddd9ef413602b41334a0b313bc</id1.6>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/4HglUsPNrhivcLALVg7WYQBFnQD.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/64aOdaNmHnFHIKK0X7gldI9zPno.jpg</fanart.2>
			<ep2.1>1 RESPONDIENDO</ep2.1>
			<id2.1>6828e395a61584518f8d1a0fe002d908ae391a9a</id2.1>
			<ep2.2>2 VALE LA PENA</ep2.2>
			<id2.2>83687e71af4a1aae02be0d991e384ac36bdd4ba9</id2.2>
			<ep2.3>3 MARIPOSA / CAPULLO</ep2.3>
			<id2.3>8629ec0bf5a395af853ea87caa1d3a436227e546</id2.3>
			<ep2.4>4 NUNCA CONOCI UN AMOR COMO ESTE</ep2.4>
			<id2.4>25a596293da67b195aa5d7b12e67b33185901b06</id2.4>
			<ep2.5>5 ¿QUE HARIA AHORA CANDY?</ep2.5>
			<id2.5>35d57c6fcb23f5f7f44e40f5c78804f4a0483437</id2.5>
			<ep2.6>6 HOY EL AMOR NECESITA AMOR</ep2.6>
			<id2.6>59dd98bf1d4fe187bb4abbe6ead7940b91f08021</id2.6>
			<ep2.7>7 COCA</ep2.7>
			<id2.7>264569c5c591ec813d9a8d01b478b52c4f3dce87</id2.7>
			<ep2.8>8 REVELACIONES</ep2.8>
			<id2.8>634f35e2e4da56956dd5f3a298c1c1a6257b9aa1</id2.8>
			<ep2.9>9  LA VIDA ES UNA PLAYA</ep2.9>
			<id2.9>E5F9A914C4D9AFB93A55309B1E5275FD41B978A0</id2.9>
			<ep2.10>10 CON MIS TACONES</ep2.10>
			<id2.10>8f0b21e0f4fdf82e5eee5a70b445adb3ef87c20f</id2.10>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/1cBGu1KEP17Dexg4wYfR9bLF3kc.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/4Mig4AxB0uARFfQviu6WsVirYtM.jpg</fanart.3>
			<ep3.1>1 EN LA CARRERA</ep3.1>
			<id3.1>d479214010c25d35acbdc552c83766b8891633f9</id3.1>
			<ep3.2>2 INTERVENCION</ep3.2>
			<id3.2>982b3f76049fd899ac77683c0c64a435f239047e</id3.2>
			<ep3.3>3 EL BAUL</ep3.3>
			<id3.3>9b23c7b97ea89bd03236129bd34f7ad32f5d28c1</id3.3>
			<ep3.4>4 LLEVAME A LA IGLESIA</ep3.4>
			<id3.4>f9fc26817c0a65c83f345efcbfb95ddb61930736</id3.4>
			<ep3.5>5 ALGO PRESTADO, ALGO AZUL</ep3.5>
			<id3.5>5651180ec6d0246a1d953251f6e4f905b3abab5f</id3.5>
			<ep3.6>6 ALGO VIEJO, ALGO NUEVO</ep3.6>
			<id3.6>0e0f368fae4c5de2e6f3329841617c3f2d76ff3e</id3.6>
			<ep3.7>7 FINAL SERIE</ep3.7>
			<id3.7>27ec0cb7aa91775b280475a68ec599f398a5e846</id3.7>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RAGNAROK</title>
		<info>(2020) 12 episodios. Una serie de extraños sucesos comienzan a suceder en la pequeña localidad noruega de Edda, donde todos sus habitantes no son lo que parecen ser.</info>
		<year>2020</year>
		<genre>Fantástico. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/1LRLLWGvs5sZdTzuMqLEahb88Pc.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/jM7LHr811U4A6EnY9iMyKhUVsMN.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/1LRLLWGvs5sZdTzuMqLEahb88Pc.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/jM7LHr811U4A6EnY9iMyKhUVsMN.jpg</fanart.1>
			<ep1.1>1 AL 6</ep1.1>
			<id1.1>ffa8436c4f1073c11108daf6e7a6e130aa125c7b</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/wXfVqjVG9EZ1tgMciKktitwjvHv.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/wu444tM9YBllq9UcBv5TeidO3j3.jpg</fanart.2>
			<ep2.1>1 AL 2</ep2.1>
			<id2.1>b33fac33ff6fef0a4c4087d50f2088206f755a4a</id2.1>
			<ep2.2>3 AL 6</ep2.2>
			<id2.2>b2f91dc5862e2884c780232aca3ab2b94fdfa454</id2.2>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RAISED BY WOLVES</title>
		<info>(2020) 2 temporadas. 20 episodios. Dos androides, denominados "Padre" y "Madre", reciben la tarea de educar a un grupo de niños humanos en un misterioso y desértico planeta. A medida que las distintas colonias humanas comienzan a tener problemas a causa de sus diferencias religiosas, los androides descubren lo difícil que resulta controlar o incluso cambiar la opinión y las creencias de los seres humanos.</info>
		<year>2020</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/mTvSVKMn2Npf6zvYNbGMJnYLtvp.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/na2xUduK8HviOFT97TiFG2MkJmY.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/mTvSVKMn2Npf6zvYNbGMJnYLtvp.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/na2xUduK8HviOFT97TiFG2MkJmY.jpg</fanart.1>
			<ep1.1>1 CRIADO POR LOBOS</ep1.1>
			<id1.1>55140e49cf1e4f7533aa9cd56fbc3020dc939d06</id1.1>
			<ep1.2>2 PENTAGRAMA</ep1.2>
			<id1.2>7005ae4e1fc31410a0eaaff616a9faa11dc8e510</id1.2>
			<ep1.3>3 FE VIRTUAL</ep1.3>
			<id1.3>08785ab5242dcd70369c0489283beec18d4377f7</id1.3>
			<ep1.4>4 EL CURSO DE LA NATURALEZA</ep1.4>
			<id1.4>a9d649d9df4c6771d125106ffcc086cdee430a33</id1.4>
			<ep1.5>5 MEMORIA INFECTADA</ep1.5>
			<id1.5>ea05d6ac492985d7a50e3e3d327cea22287dce49</id1.5>
			<ep1.6>6 PARAISO PERDIDO</ep1.6>
			<id1.6>47d8321adfd12364dcbb89dbd4041ada8df40af3</id1.6>
			<ep1.7>7 CARAS</ep1.7>
			<id1.7>0ae2c831d95f62729b7a057fd112e774282ddc39</id1.7>
			<ep1.8>8 MASA</ep1.8>
			<id1.8>4bf8c09fcb82a05a9b3598516e1b09b47963a50e</id1.8>
			<ep1.9>9 UMBILICAL</ep1.9>
			<id1.9>6cf8f5290b4e30cc05fce93306bbbcc0dad0c3c6</id1.9>
			<ep1.10>10 EL PRINCIPIO</ep1.10>
			<id1.10>b593055ea83d47728b941c7500ae308896e13237</id1.10>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/hzREDWxAmEUOnKjxbh1X5MaF21I.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/na2xUduK8HviOFT97TiFG2MkJmY.jpg</fanart.2>
			<ep2.1>1 EL COLECTIVO</ep2.1>
			<id2.1>65492dcf235d9661e4bb94c01c0ef8a96423a358</id2.1>
			<ep2.2>2 SIETE</ep2.2>
			<id2.2>0193c281cd4878e7aaeb639724495c2946453077</id2.2>
			<ep2.3>3 BUENAS CRIATURAS</ep2.3>
			<id2.3>13eb0a198e92d8e6b3f6d8d3f981564dc36bd5e8</id2.3>
			<ep2.4>4 CONTROL</ep2.4>
			<id2.4>0d5b878d08aacae3555b03ae39c4be4051bf68bc</id2.4>
			<ep2.5>5 REY</ep2.5>
			<id2.5>19ee8de280aab705acfd0b740097918e621bd9ee</id2.5>
			<ep2.6>6 EL ARBOL</ep2.6>
			<id2.6>XJV7UXKXBG5N6FOVFFYMRV5VAL2VTQWB</id2.6>
			<ep2.7>7 ALIMENTO</ep2.7>
			<id2.7>10ddce5bf3f87a8c0be2f89ba8db3471fe3e9d6b</id2.7>
			<ep2.8>8 FELICIDAD</ep2.8>
			<id2.8>df95744a97531e56042434ada9e46ed4e2d73dcd</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RAPA</title>
		<info>(2022) 6 episodios. A Capelada. Una tierra antigua, de altos acantilados sobre el mar, donde los caballos viven libres. Salvo un día: El de la "rapa das bestas". La rapa es una tradición que sintetiza lo bello y lo salvaje de un territorio por lo general tranquilo, pero que va a ser escenario de un crimen. Buscar al asesino de Amparo Seoane, la alcaldesa de la localidad, será el objetivo común de Tomás, un profesor frustrado, y de Maite, una sargento de la guardia civil.</info>
		<year>2022</year>
		<genre>Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/cy49qWBbivYsP8WbFtaPra90JFw.jpg</thumb>
		<fanart>https://i.imgur.com/MHO7KNq.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/cy49qWBbivYsP8WbFtaPra90JFw.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/MHO7KNq.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>fa0dd400c2012c7b10751ed74afa4cf01c72cbba</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>2dd2aa523feaa64d94542499b7ef66e371a5eaaf</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>8fa59656345e2980113dae0028865467e0fddbc9</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>0e0148d3c399a4e40f90594ba9adab5f9390bc8d</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>56485130881cd52b137a5c5a25f8e0a326c770db</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>d275780fcd0f8da571c9d4078d32af0060588451</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RAPHAELISMO</title>
		<info>(2022) 4 episodios. Un retrato intimo que ahonda en los exitos, secretos, miedos y suenos del artista, con material audiovisual inedito. Raphael, el artista incombustible, el artista de uranio, el jefe indie, el ultimo artista hecho a mano, segun Francisco Umbral, el que triunfa en Eurovisión cuando todavia no habia llegado el color, el que siempre está en el manana y nunca se rinde, el que renace tras un trasplante de higado in extremis, el que despierta controversias y pasiones... Pero por encima de todo, el unico artista espanol en activo, tras más de 60 anos de carrera, que ha conseguido crear un termino propio: Raphaelismo. Esta serie documental repasa nuestra historia y la suya a traves de imágenes ineditas, recreaciones, archivo nunca visto y entrevistas intergeneracionales, con el deseo de desvelar todas las aristas de este fenómeno escenico arrollador.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/AuGDMnF58KyMtt6iSSBaF39yO83.jpg</thumb>
		<fanart>https://i.imgur.com/zRTJPBK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/AuGDMnF58KyMtt6iSSBaF39yO83.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/zRTJPBK.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>59508224DED40811278D2C4203E5E73E4E4C5A7D</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RATCHED</title>
		<info>(2020) 8 episodios. Una enfermera de una institución mental se vuelve hastiada, irascible y un verdadero monstruo para sus pacientes. En los años 40, Mildred Ratched se traslada al norte de California para conseguir un trabajo en un hospital psiquiátrico pionero en la aplicación de nuevos e inquietantes experimentos con la mente humana. Mildred se presenta como la imagen perfecta de lo que debería ser una enfermera pero, a medida que empieza a trabajar en el sistema de salud mental y con aquellos que están dentro de él, se descubrirá que su elegante exterior oculta una oscuridad que va creciendo en su interior y que lleva ardiendo durante mucho tiempo, revelando que los verdaderos monstruos se hacen, no nacen.</info>
		<year>2020</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/cDNxOIm6K5D2W21QyJWZ95sJzQt.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/zx3m4iW6CsM7yuMEUyvSkvbwaip.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/cDNxOIm6K5D2W21QyJWZ95sJzQt.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/zx3m4iW6CsM7yuMEUyvSkvbwaip.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>50d17b9ad508aabfb6afa64a2d1967b4661d4ec6</id1.1>
			<ep1.2>5 AL 8</ep1.2>
			<id1.2>8eaca03aa6e2b1da11c5e81e9b26fbf258d2f49c</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>REAL MADRID LA LEYENDA BLANCA</title>
		<info>(2022) 6 episodios. El Real Madrid es un club centenario que ha sabido formar un equipo ganador en todas las épocas y generaciones. Ningún otro club ha igualado el récord de este eterno campeón. Es un modelo a seguir en el mundo del deporte. Los jugadores y aficionados del club reviven los hitos y las pruebas históricas que convirtieron al Real Madrid en el mejor equipo del mundo.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://i.imgur.com/eXQdsY5.jpg</thumb>
		<fanart>https://i.imgur.com/75goNgd.jpg</fanart>
		<t1>
			<thumb.1>https://i.imgur.com/eXQdsY5.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/75goNgd.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>d7185e0e9b22a004da15f62b748bd1176993fb24</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>95b0498da0905cb36ef44df8af3037b49ed44fe7</id1.2>
			<ep1.3>5 AL 6</ep1.3>
			<id1.3>ce168c632e969cb1863d9eaf66ca4bb47db2a01e</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>REPUBLICA SALVAJE</title>
		<info>(2021) 8 episodios. Un grupo de jóvenes delincuentes ha perdido la fe y la confianza en el estado alemán. Deben ser rehabilitados y enviados en expedición a los Alpes. Pero hay un terrible accidente y uno de los cuidadores aparece muerto.</info>
		<year>2021</year>
		<genre>Aventuras. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/zv0kjLWPK8dM52ADDrQXjGIykyF.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/rK6TlN3nXvRqTEwc3JPjPN8h4KP.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/zv0kjLWPK8dM52ADDrQXjGIykyF.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/rK6TlN3nXvRqTEwc3JPjPN8h4KP.jpg</fanart.1>
			<ep1.1>1 KIM</ep1.1>
			<id1.1>4ca69c358338613b74de161500eabbe9b1b30fd2</id1.1>
			<ep1.2>2 RON</ep1.2>
			<id1.2>65c2fb6f49635fc3276242fee20b5f3103a43a4c</id1.2>
			<ep1.3>MARVIN Y JESSICA</ep1.3>
			<id1.3>1f99f5911faea6f99643558b954b89965d235808</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>REYES DE LA NOCHE</title>
		<info>(2021) 6 episodios. Francisco Javier Maldonado, alias Paco el Cóndor, es el periodista deportivo más importante de la radio española en los 80. Los oyentes le admiran, los jugadores le temen, los presidentes de clubes (y el gobierno) le odian. Cada medianoche reúne a millones de oyentes junto al transistor. Desde hace años, el Cóndor no tiene rival. Hasta ahora.</info>
		<year>2021</year>
		<genre>Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/eFGYVLxFr0agTM2JkZze2a02KvY.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/iE88SiiQShZt03NJaB0foUwBeVx.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/eFGYVLxFr0agTM2JkZze2a02KvY.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/iE88SiiQShZt03NJaB0foUwBeVx.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>d2142a1629d5dc97e5624bf4e8244ead1e72bc68</id1.1>
			<ep1.2>2 LA CONVOCATORIA</ep1.2>
			<id1.2>d30d012e0add17dca5eb248452b0c930e62da827</id1.2>
			<ep1.3>3 GIL</ep1.3>
			<id1.3>e9d24ef0e8724f31a32a6ccf84a6b0ca4f6dc124</id1.3>
			<ep1.4>4 PITO REGALAO</ep1.4>
			<id1.4>478a94c683c3fc7b26e1c8a5822355b443784ed5</id1.4>
			<ep1.5>5 LA GRAN FINAL</ep1.5>
			<id1.5>a7d8cb4af3f5ad30f37285dc552eb1800755f2aa</id1.5>
			<ep1.6>6 MINUTO Y RESULTADO</ep1.6>
			<id1.6>480ab1eead23fe8284d0e1fc29ad941d3f02c1a4</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RIVERDALE</title>
		<info>(2017) 6  temporadas. 95 episodios. Al mismo tiempo que un nuevo año escolar da comienzo, la ciudad de Riverdale aún está reponiéndose de la trágica muerte reciente de Jason Blossom. Por otro lado, durante el verano Archie Andrews (K.J. Apa) se ha dado cuenta de que quiere ser músico. Además a Archie aún le pesa haber roto su amistad con Jughead Jones (Cole Sprouse). Mientras tanto, Betty Cooper (Lili Reinhart) está ansiosa de ver a Archie, de quien está enamorada, pero aún no está lista para revelarle sus sentimientos. Cuando una nueva estudiante, Veronica Lodge (Camila Mendes), llega a Riverdale desde Nueva York junto a su madre, nace la chispa entre ella y Archie. Pero Veronica no quiere poner su nueva amistad con Betty en riesgo. Cheryl Blossom (Madelaine Petsch), la chica más popular de Riverdale, es feliz removiento las situaciones alrededor de Archie, Betty y Veronica para causar problemas. Pero Cheryl es una chica que guarda muchos secretos.</info>
		<year>2017</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/eNqIlDBeDQ7XjjefIicGPrfd8YS.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/ive5FO8ey0EOXaJ72ygLwV8Uvqd.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/6zBWSuYW3Ps1nTfeMS8siS4KUaA.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/p3FCnFi4QSxeZcqynzcQzx14mOs.jpg</fanart.1>
			<ep1.1>1 EN EL MARGEN DEL RIO</ep1.1>
			<id1.1>01b707bbb80d15c074ad0d6eff254999624a5633</id1.1>
			<ep1.2>2 UN TOQUE DE MALDAD</ep1.2>
			<id1.2>cb8b6f411938fc3f0bb24a4027ca1b2c7baef51b</id1.2>
			<ep1.3>3 DOBLE CUERPO</ep1.3>
			<id1.3>0a2100f3f9bf358a3922ddd9bf281e604c974434</id1.3>
			<ep1.4>4 LA ULTIMA PELICULA</ep1.4>
			<id1.4>8b8998ea9427e4a886f6bea410084983517090e9</id1.4>
			<ep1.5>5 CORAZON OSCURO</ep1.5>
			<id1.5>3348f98c4bfd3e87565ee331a13164cd464656d6</id1.5>
			<ep1.6>6 MAS RAPIDO, PUSSYCATS</ep1.6>
			<id1.6>855c02099ea60ad48a96488ffc618d4e17a005be</id1.6>
			<ep1.7>7 EN UN LUGAR SOLITARIO</ep1.7>
			<id1.7>a7ca8fc53969d7cf8778fcc8db8133bd4983c277</id1.7>
			<ep1.8>8 LOS MARGINADOS</ep1.8>
			<id1.8>e7214d91495389eed68a73ef537f140f664157f6</id1.8>
			<ep1.9>9 LA GRAN ILUSION</ep1.9>
			<id1.9>c935fb5cb5272cd3419b93e1fe46a98e94525b7d</id1.9>
			<ep1.10>10 EL FIN DE SEMANA PERDIDO</ep1.10>
			<id1.10>f49b4c5742464c1e37ef2ec1bcbdbc7e9a553042</id1.10>
			<ep1.11>11 VIAJE A RIVERDALE DE IDA Y VUELTA</ep1.11>
			<id1.11>3141d62e028b71687a2a80ae2fdb86a1e38530b4</id1.11>
			<ep1.12>12 ANATOMIA DE UN ASESINATO</ep1.12>
			<id1.12>ce3090f220f0e73bffc9f126d09d895b63584a5d</id1.12>
			<ep1.13>13 EL DULCE PORVENIR</ep1.13>
			<id1.13>44695eaa9274e4dc9a46fb967d459ff2b71aa283</id1.13>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/1TsbOTztAJtzTRXAhoLsX9a83XX.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/g4VfyYxLYWgZbQScctCkdqWZQLn.jpg</fanart.2>
			<ep2.1>1 UN BESO ANTES DE MORIR</ep2.1>
			<id2.1>69f263fef2cb9db79a56f54d0ceb52470895a1ec</id2.1>
			<ep2.2>2 HALCONES DE LA NOCHE</ep2.2>
			<id2.2>723335e7785dfee6c7ef9ce9de613bb7f1d2716b</id2.2>
			<ep2.3>3 LOS OJOS DEL BOSQUE</ep2.3>
			<id2.3>940128ebdd727c0dc05762155c200c4408d6bfb2</id2.3>
			<ep2.4>4 TERROR AL ANOCHECER</ep2.4>
			<id2.4>afddda40ff1965cffca4d2b5a942a47560e260f3</id2.4>
			<ep2.5>5 LLAMA UN EXTRAÑO</ep2.5>
			<id2.5>df64f4a8a39747be1c68b92e21378631fa6f11c0</id2.5>
			<ep2.6>6 A PRUEBA DE MUERTE</ep2.6>
			<id2.6>00f2e9e3add56cdcd4e30bd3f9e7fc9c91c06679</id2.6>
			<ep2.7>7 CUENTOS DESDE LA OSCURIDAD</ep2.7>
			<id2.7>a6d44e32802359e5236cf338d1490b76535d2d4e</id2.7>
			<ep2.8>8 LA CASA DEL DIABLO</ep2.8>
			<id2.8>ec014156e1d33684627b738c98b09bfce186de95</id2.8>
			<ep2.9>9 NOCHE DE PAZ, NOCHE DE MUERTE</ep2.9>
			<id2.9>a3fb056308a232421f65ac1bc33380006b5ba490</id2.9>
			<ep2.10>10 SEMILLA DE MALDAD</ep2.10>
			<id2.10>c821127f79ebe0a99bd2e684e7ae0622edca2c50</id2.10>
			<ep2.11>11 EL LUCHADOR</ep2.11>
			<id2.11>9ca230bcc8062b60a3f4bebfb49997f74664cadd</id2.11>
			<ep2.12>12 LO PERVERSO Y LO DIVINO</ep2.12>
			<id2.12>34393116349b7ea733db6dd1a1ddef5be2931b07</id2.12>
			<ep2.13>13 EL CORAZON DELATOR</ep2.13>
			<id2.13>3eac8a3876852f4ed31667f418b8a5b73b458b32</id2.13>
			<ep2.14>14 LAS COLINAS TIENEN OJOS</ep2.14>
			<id2.14>a245025bca615441ac9f2056c7a02d0ea3d71529</id2.14>
			<ep2.15>15 POZOS DE AMBICION</ep2.15>
			<id2.15>c97d330544e5dc2beedbab8689df87a708465a12</id2.15>
			<ep2.16>16 COLORES PRIMARIOS</ep2.16>
			<id2.16>8eeeafdf4548a46f76c0cc5f9b65ffb743490155</id2.16>
			<ep2.17>17 LAZOS MUY ESTRECHOS</ep2.17>
			<id2.17>b2d09741dbcc4ffe82c0431b7e237f448ee20ead</id2.17>
			<ep2.18>18 QUE NOCHE AQUELLA</ep2.18>
			<id2.18>29124c6c5ff414f2532153f408e3f1d159e2fee1</id2.18>
			<ep2.19>19 PRISIONEROS</ep2.19>
			<id2.19>a3ecf78a011d75bb7fbd4ab2cad8c6975eab1822</id2.19>
			<ep2.20>20 LA SOMBRA DE UNA DUDA</ep2.20>
			<id2.20>1c90c8fb4a4bb2a63ef9dfdcdccb4f0b17ccc57f</id2.20>
			<ep2.21>21 EL SACRIFICIO DE UN CIERVO SAGRADO</ep2.21>
			<id2.21>48c6e0f748ff1003585371735c5eb2686fa7790c</id2.21>
			<ep2.22>22 UN MUNDO FELIZ</ep2.22>
			<id2.22>547cfc8ec18a669a2132b92ad58cfd1b77de58d4</id2.22>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/gskv297rlbyzLaTU1XZf8UBbxp0.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/7LdzPorclA4XzRwnTU303vOd0fs.jpg</fanart.3>
			<ep3.1>1 UNA VIDA EN TRES DIAS</ep3.1>
			<id3.1>da73eb146d15ecdb8e46360d0f738bfde169e75a</id3.1>
			<ep3.2>2 LOS OJOS DE LA CARCEL</ep3.2>
			<id3.2>d78af8be5faebc0b98ab174e198411e06744ad1d</id3.2>
			<ep3.3>3 ASI EN LA TIERRA COMO EN EL INFIERNO</ep3.3>
			<id3.3>ddd2a7f040df9fc8eb7039cafcb92a6908887936</id3.3>
			<ep3.4>4 EL CLUB DE MEDIANOCHE</ep3.4>
			<id3.4>e4c0725f6c4383ef903412133c63b749edd0f434</id3.4>
			<ep3.5>5 LA GRAN EVASION</ep3.5>
			<id3.5>9a271d619a8b23cea28ebefcbd340a72662bbda0</id3.5>
			<ep3.6>6 CAZADOR DE HOMBRES</ep3.6>
			<id3.6>78d13e4c025f5fae07597929df841718255d707d</id3.6>
			<ep3.7>7 EL HOMBRE DE NEGRO</ep3.7>
			<id3.7>e473859a83f6a67ce35cbe0240423efc40477e55</id3.7>
			<ep3.8>8 ESTALLIDO</ep3.8>
			<id3.8>b294c59afa9051fd7ac7de79c9d6a854be4519f1</id3.8>
			<ep3.9>9 CLAUSTROFOBIA</ep3.9>
			<id3.9>2664da8a2c3994ee0a29c43822143c75c8e44eb2</id3.9>
			<ep3.10>10 EL EXTRAÑO</ep3.10>
			<id3.10>cc74cf201902c58a16812e3d9ca7a4a95ceffc49</id3.10>
			<ep3.11>11 LA DALIA ROJA</ep3.11>
			<id3.11>8d920c4946ff068327c0df1d0aa54b7fc3c05b04</id3.11>
			<ep3.12>12 CUIDAD EXTRAÑA</ep3.12>
			<id3.12>d27098ee8b11c2e01cf6b06ba484552486f0f0ca</id3.12>
			<ep3.13>13 REQUIEM POR UN PESO WELTER</ep3.13>
			<id3.13>d61562f54b4c78a53b1ab0475c172b2239184af3</id3.13>
			<ep3.14>14 FUEGO, CAMINA CONMIGO</ep3.14>
			<id3.14>3e509072f76863ca5e42abf461339f2b759848a7</id3.14>
			<ep3.15>15 EL SUEÑO AMERICANO</ep3.15>
			<id3.15>18d7f34e5a6356a29015ec11f3e5286fd83bd0bf</id3.15>
			<ep3.16>16 PASANDOLO BIEN</ep3.16>
			<id3.16>feab9f1d16ad88142b5ce70f5fb395fbb42e1fed</id3.16>
			<ep3.17>17 EL ASALTO</ep3.17>
			<id3.17>f78be8ac36fe7bb4e06471412a8b4e4e65496b47</id3.17>
			<ep3.18>18 CARAMELO ASESINO</ep3.18>
			<id3.18>8717fa40af7ce54f7b07d4bb6e90c53cdf175030</id3.18>
			<ep3.19>19 TEME A LA PERCA</ep3.19>
			<id3.19>d9838a2e124dab5fe81655d865751cc64a459788</id3.19>
			<ep3.20>20 REINA DEL BAILE</ep3.20>
			<id3.20>73989a2dcf4c83df31122aff91a3976e93f346b8</id3.20>
			<ep3.21>21 EL OSCURO SECRETO DE LA CASA DE LA COSECHA</ep3.21>
			<id3.21>ec8951e6b3406bfcaf2bc2351a4bbf04c2733166</id3.21>
			<ep3.22>22 APOCALYPTO</ep3.22>
			<id3.22>df5054f6202592ebb6c9d7162ff158afb136879e</id3.22>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/4X7o1ssOEvp4BFLim1AZmPNcYbU.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/yVc41GMQw4yujjk6fuuAMMFTt8H.jpg</fanart.4>
			<ep4.1>1 IN MEMORIAM</ep4.1>
			<id4.1>c1141ad60b531ca536688aa834af28c8fe3947ac</id4.1>
			<ep4.2>2 AQUEL EXCITANTE CURSO EN RIVERDALE</ep4.2>
			<id4.2>5789d4d9593d10b15d3db91c714539e37b4c314e</id4.2>
			<ep4.3>3 TARDE DE PERROS</ep4.3>
			<id4.3>bee118d32859f9d134c424625610bb1c7d89ba23</id4.3>
			<ep4.4>4 HALLOWEEN</ep4.4>
			<id4.4>194995241fe32ed72ae519cfc98c0acbdcc7b601</id4.4>
			<ep4.5>5 TESTIGO DE CARGO</ep4.5>
			<id4.5>328ba400703b838d5be68e6ea133a5a9951b37db</id4.5>
			<ep4.6>6 HEREDITARIO</ep4.6>
			<id4.6>76ff3cba5c2f87abfeccc608e17af21561da43b4</id4.6>
			<ep4.7>7 LA TORMENTA DE HIELO</ep4.7>
			<id4.7>60d23baef5ffca9d338f801adc3024a74678fc49</id4.7>
			<ep4.8>8 EN TRATAMIENTO</ep4.8>
			<id4.8>c0d67d860b431109f491ea93f5a965aebd57a796</id4.8>
			<ep4.9>9 MANDARINA</ep4.9>
			<id4.9>e330fcae0720de79921647ef802358ed4862c21f</id4.9>
			<ep4.10>10 JUEGO DE CAMPEONES</ep4.10>
			<id4.10>7cac4d38387d64d0580baa3a1f389467bd15ca1e</id4.10>
			<ep4.11>11 EL DILEMA</ep4.11>
			<id4.11>a7dc041084e47269d050ca7dbcf155e10021f31c</id4.11>
			<ep4.12>12 HOMBRES DE HONOR</ep4.12>
			<id4.12>a69aba0714092f0e04dfb0d023ef4fd5e0cbd0e3</id4.12>
			<ep4.13>13 LOS IDUS DE MARZO</ep4.13>
			<id4.13>14389bc3f349e6cdcd3957c0e01e0a780522a6c0</id4.13>
			<ep4.14>14 COMO DEFENDER A UN ASESINO</ep4.14>
			<id4.14>f1abb20d306335e5d315ec025e8118b70e255b33</id4.14>
			<ep4.15>15 TODO POR UN SUEÑO</ep4.15>
			<id4.15>5ef2dfdabb3a06a24abc27903a4bb017957a636d</id4.15>
			<ep4.16>16 LA HABITACION CERRADA</ep4.16>
			<id4.16>b37af752a12135e072f8c1aa0545d21a488d8dd2</id4.16>
			<ep4.17>17 ESTA CUIDAD RETORCIDA</ep4.17>
			<id4.17>b993ac9adc5507880e7eea4a351b74b3568824e1</id4.17>
			<ep4.18>18 LYNCHIANO</ep4.18>
			<id4.18>775f4e66f9a2afff1219ae799ff9137ea0c64935</id4.18>
			<ep4.19>19 LECCION AL SR. HONEY</ep4.19>
			<id4.19>f665a1fe3875dd984dbcebdf0074965ef00430a5</id4.19>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/cEmpGjZZu3JSlkKm8NUuCzrUscR.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/9hvhGtcsGhQY58pukw8w55UEUbL.jpg</fanart.5>
			<ep5.1>1 CLIMAX</ep5.1>
			<id5.1>6d4c1ca9750e448c1cf2588fbaf485b7d09968bc</id5.1>
			<ep5.2>2 ASESINATOS CON BUEN GUSTO</ep5.2>
			<id5.2>3cbbb24fa4efcfc99bc29fd69bfdc46cf1d2f614</id5.2>
			<ep5.3>3 GRADUACION</ep5.3>
			<id5.3>a4d3476627ea2c83578da4ef8503eb3aee9e0e41</id5.3>
			<ep5.4>4 PURGATORIO</ep5.4>
			<id5.4>abcd51d65d3148e3ad8ad1bc21d82a7b363870aa</id5.4>
			<ep5.5>5 REGRESO A CASA</ep5.5>
			<id5.5>8f31ed0ff6b7be0140e0925968703916bae03c98</id5.5>
			<ep5.6>6 VUELTA AL COLE</ep5.6>
			<id5.6>d0cb06dd6eb7ad73cb724092365e79daebca1be9</id5.6>
			<ep5.7>7 FUEGO EN EL CIELO</ep5.7>
			<id5.7>9d42b46e89b37f3003560f346c42f1c0ebed1f6c</id5.7>
			<ep5.8>8 BAJO LLAVE</ep5.8>
			<id5.8>f281e5fd07a540265d551ae1145f91d1510ef583</id5.8>
			<ep5.9>9 DESTRUCTOR</ep5.9>
			<id5.9>6f1a283f5483c664780d8003a7c670ae50eced92</id5.9>
			<ep5.10>10 EL HOMBRE DE LOS ALFILERES</ep5.10>
			<id5.10>b030860548a1ca3dc8cdb881b394307625b2b10a</id5.10>
			<ep5.11>11 SOCIOS EXTRAÑOS</ep5.11>
			<id5.11>6f584ba59f81b2290aad895bd1b62ab8f3a6c6ee</id5.11>
			<ep5.12>12 CIUDADANO LODGE</ep5.12>
			<id5.12>c31befd06069fe7994b5501408a06a003ac002aa</id5.12>
			<ep5.13>13 RESERVOIR DOGS</ep5.13>
			<id5.13>2d4570203500f977bf300702a117356f21efecc7</id5.13>
			<ep5.14>14 LA GALERIA NOCTURNA</ep5.14>
			<id5.14>3b65e602f30f123a5c29bc815e1dd7c0e3ba1798</id5.14>
			<ep5.15>15 EL REGRESO DE PUSSYCATS</ep5.15>
			<id5.15>8eaf4df9ee8d2abe0d8a13fc6089b611eb58a221</id5.15>
			<ep5.16>16 HERMANOS DE SANGRE</ep5.16>
			<id5.16>6ae6a1c009e84c3bf2904dadc126229fbd604d00</id5.16>
			<ep5.17>17 EL BAILE DE LA MUERTE</ep5.17>
			<id5.17>b789590c587ea6d2ff2d72b4e222e9302a66baae</id5.17>
			<ep5.18>CASI NORMALES</ep5.18>
			<id5.18>a308cba21897fdca9893b088baa2cc29406f275f</id5.18>
		</t5>
		<t6>
			<thumb.6>https://www.themoviedb.org/t/p/original/c1ROxK1Afo6YuGi3j6ClXmrNcHi.jpg</thumb.6>
			<fanart.6>https://www.themoviedb.org/t/p/original/2AyySFf6MSoR7bavBvoBLck2gpo.jpg</fanart.6>
			<ep6.1>1 BIENVENIDOS A RIVERDALE</ep6.1>
			<id6.1>a73d43c4c11ae6b038e87be1ba120a9caec2c63d</id6.1>
			<ep6.2>2 HISTORIAS DE FANTASMAS</ep6.2>
			<id6.2>70d0b01005a68440b2b3b80c65f312888f2eef3d</id6.2>
			<ep6.3>3 SR. CYPHER</ep6.3>
			<id6.3>49722ee0e8ff75ac7a7e3d65ba643cf011eab578</id6.3>
			<ep6.4>4 LA HORA DE LAS BRUJAS</ep6.4>
			<id6.4>82608caba5cef14bc4f65d36ca9d7b4a47d145a3</id6.4>
			<ep6.5>5 LA PARADOJA JUGHEAD</ep6.5>
			<id6.5>0bf093cb49dc9da45166c54e956c320e7825e8de</id6.5>
			<ep6.6>6 DIFICIL DE CREER</ep6.6>
			<id6.6>deb0e0d01e61ea2d47a6af2089dc699a5a9c2576</id6.6>
			<ep6.7>7 UN FUNERAL DE MUERTE</ep6.7>
			<id6.7>d166b2b2925555c0cdef7347ec255742b35bb074</id6.7>
			<ep6.8>8 LA CIUDAD</ep6.8>
			<id6.8>04c2691565c35a28c370db4b18905d7545f4b235</id6.8>
			<ep6.9>9 GAMBITO DE SERPIENTE</ep6.9>
			<id6.9>071fd25f326a65a721a04b35afdd1390ad140025</id6.9>
			<ep6.10>10 HEROES LOCALES</ep6.10>
			<id6.10>14d82c8898d6f3a850074dc64f5e41a3c9988e5c</id6.10>
			<ep6.11>11 ANGELES EN AMERICA</ep6.11>
			<id6.11>5a43db89a4786410786d195987eb60adff6f92c1</id6.11>
			<ep6.12>12 LA NIEBLA</ep6.12>
			<id6.12>18d24f8a3e7eff62aabb56d35bbf1df8978366d2</id6.12>
			<ep6.13>13 EX-LIBRIS</ep6.13>
			<id6.13>59a3a2f7113629fdfe522272be98ff4ccc4523d9</id6.13>
			<ep6.14>14 VENENOSOS</ep6.14>
			<id6.14>7a4394c3043c82bd6d8d641a453ecd07b5282b86</id6.14>
			<ep6.15>15 RUIDOS EN LA NOCHE</ep6.15>
			<id6.15>b18c0cb84aec91a4519f9289e2941e80771b26de</id6.15>
			<ep6.16>16 COLLAR AZUL</ep6.16>
			<id6.16>fed3b0e3ce4fefc4b586aed2035ed553eb730bfc</id6.16>
		</t6>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>ROAR</title>
		<info>(2022) 8 episodos. Narra la historia de 8 mujeres que exploran una contradicción o un problema absurdo diferente en su vida.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/1LfTFs5NLbbqe9DlajYdfw8eHyD.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/9nI4P4kZKwe9eQD4ynUhcJcZWkP.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/1LfTFs5NLbbqe9DlajYdfw8eHyD.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/9nI4P4kZKwe9eQD4ynUhcJcZWkP.jpg</fanart.1>
			<ep1.1>1 LA MUJER QUE DESAPARECIO</ep1.1>
			<id1.1>d529ec2db90c23309fad14a10bd4f1e529f16cc1</id1.1>
			<ep1.2>2 LA MUJER QUE COMIA FOTOGRAFIAS</ep1.2>
			<id1.2>3a184da583ae71a95bdc1bae45b9d7c5e49c782d</id1.2>
			<ep1.3>3 LA MUJER QUE FUE MANTENIDA EN UN ESTANTE</ep1.3>
			<id1.3>5da399fe7f6514f2d88fede9c381100dc065589e</id1.3>
			<ep1.4>4 LA MUJER QUE ENCONTRO MARCAS DE MORDEDURAS EN SU PIEL</ep1.4>
			<id1.4>7a12005ff3e38723518ba26afe4651b872298bb2</id1.4>
			<ep1.5>5 LA MUJER QUE FUE ALIMENTADA POR UN PATO</ep1.5>
			<id1.5>82528a6c41208a01b979fa542eec56ecca716d29</id1.5>
			<ep1.6>6 LA MUJER QUE RESOLVIO SU PROPIO ASESINATO</ep1.6>
			<id1.6>2638ad608f5226ba7615d0875e3b1a9999355bc0</id1.6>
			<ep1.7>7 LA MUJER QUE DEVOLVIO A SU MARIDO</ep1.7>
			<id1.7>68a03377e670a4fb73829a5bf1eb016571b36053</id1.7>
			<ep1.8>8 LA CHICA QUE AMABA LOS CABALLOS</ep1.8>
			<id1.8>2c888e51054c9e71a1babebd17aa8c0afd866dc4</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>RUMBO AL INFIERNO</title>
		<info>(2021) 6 episodios. Hellbound, Rumbo al infierno en España, es una serie de fantasía oscura ambientada en un mundo donde a las personas se les puede decir que morirán y cuándo ocurrirá exactamente. Y cuando se agota el tiempo, unos seres sobrenaturales aparecen en la Tierra para arrastrarles directamente al infierno. En este mundo surge además una nueva religión conocida como Saejinrihwe, cuyo líder religioso describe este impactante fenómeno en el que los ángeles de la muerte emergen del infierno para proclamar las revelaciones de Dios.</info>
		<year>2021</year>
		<genre>Terror. Drama. Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/5NYdSAnDVIXePrSG2dznHdiibMk.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/ikN8ABD9pXHuW4JFqTEHr3ae8rd.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/5NYdSAnDVIXePrSG2dznHdiibMk.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/ikN8ABD9pXHuW4JFqTEHr3ae8rd.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>73ac4f4d17ef38ff80e38171d4c7d4bffd6b526a</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>f44de22df068b5f5b822b50736e5ab5151537381</id1.2>
			<ep1.3>5 AL 6</ep1.3>
			<id1.3>24a40f90d770bd66d1be558e1f2a4d9821a122ea</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SANDITON</title>
		<info>(2019) 8 episodios. Drama histórico basado en la última novela inacabada de Jane Austen. Sanditon es el nombre de un antiguo pueblo de pescadores que se está transformando en un balneario de moda. Allí se encuentra Charlotte Heywood, una mujer liberal y moderna para la época y también muy impulsiva que desea reinventarse. La trama da un giro cuando salen a la luz las agendas ocultas de los personajes, cuya fortuna dependen del éxito comercial de Sanditon.</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/iHavixVXjEHNy3fWyVtiE3bN4NK.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/5cL5vHy6feImMk2BicocLkR2uQt.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/iHavixVXjEHNy3fWyVtiE3bN4NK.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/5cL5vHy6feImMk2BicocLkR2uQt.jpg</fanart.1>
			<ep1.1>Episodio 1</ep1.1>
			<id1.1>22ad273fefc4488ffa29a93ad76276424057b95a</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>f35b79f8c52b3bece3059664ea6401b5615ecd17</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>fce7af2b26c0ee078d8cdc772af8ceae4890a0f1</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>2515cfd0e5e0550563e8b0ee96548f7f9871eed0</id1.4>
			<ep1.5>Episodio 5</ep1.5>
			<id1.5>d2365936f61f1da75676a1a01df0a8ba967718bc</id1.5>
			<ep1.6>6 al 8</ep1.6>
			<id1.6>27abbe6023875e44978bb08ffd83850f91891173</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SEE</title>
		<info>(2019) 2 temporadas. 16 episodios. En el futuro, la raza humana ha perdido la capacidad de ver, y la sociedad ha encontrado nuevas formas de interactuar entre sí, de construir, de cazar y, sobre todo, de sobrevivir. Sin embargo, todo cambia tras el nacimiento de una pareja de gemelos que recobra la vista.En la segunda temporada encontramos a Baba Voss luchando para reunir a su familia y escapar de las guerras políticas que le rodean. Pero cuanto más lejos se va, más problemas aparecen, y la irrupción de su hermano y némesis Edo Voss (Dave Bautista) aumenta la amenaza a su familia aún más.</info>
		<year>2019</year>
		<genre>Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/81kRhOQOirUHnEqQQd6OYE9w7vR.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8TOkxONO3TEeJRuZWb0hG7SboyV.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/g3JsScc7mQCfc3e5e5rXwu7xVVP.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/u5N8tXgGDY6jS7R8Xn42LDeL0Dk.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>a05237f4067da820d4c74663e51996efb9cd8f02</id1.1>
			<ep1.2>4 EL RIO</ep1.2>
			<id1.2>71cbcbad8db55e562c571d14f22c87cd27b49e28</id1.2>
			<ep1.3>5 PLASTICO</ep1.3>
			<id1.3>09ac459395aebb99baca3268f62916b267acd79f</id1.3>
			<ep1.4>6 SEDA</ep1.4>
			<id1.4>4ea38d03cf59266eca2db96ce197932d6a30b0c1</id1.4>
			<ep1.5>7 LA SENDA DE LAVANDA</ep1.5>
			<id1.5>d3dbf39860341b339d8304c6e1476612b31d6a02</id1.5>
			<ep1.6>8 CASA DE LA REVELACION</ep1.6>
			<id1.6>a63d4766e1cb58c5facda4c32feddece1f8d407d</id1.6>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/A6dnHWe8YYcoFBHzP7T6WPP4b6F.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/oKxrDdG0SfmWRtJYmreK4rjDMUo.jpg</fanart.2>
			<ep2.1>1 HERMANOS Y HERMANAS</ep2.1>
			<id2.1>6a2ea6fd6ba83b27bcf0341c1a711bdb18029e27</id2.1>
			<ep2.2>2 PARA SIEMPRE</ep2.2>
			<id2.2>81bebccba12b5ff76e303715b1518f3ffc8ba340</id2.2>
			<ep2.3>3 LA BRUJULA</ep2.3>
			<id2.3>fac3f77280be29c898232409b7fd3260f4ef0a59</id2.3>
			<ep2.4>4 EL BUSCABRUJOS</ep2.4>
			<id2.4>23f6fab079bdf3a01feb6919165b229a2b5f36ed</id2.4>
			<ep2.5>5 LA CENA</ep2.5>
			<id2.5>435ba8710a8634c0e2f8bec2538089cea081d896</id2.5>
			<ep2.6>6 LA VERDAD SOBRE LOS UNICORNIOS</ep2.6>
			<id2.6>fe3ed4e0bbfec558aaecabd43a0b022c9e993d5e</id2.6>
			<ep2.7>7 EL DISCURSO DE LA REINA</ep2.7>
			<id2.7>49da6d5cad47d16882e830b3b6b11e1a3e391fff</id2.7>
			<ep2.8>8 CANCION DE CUNA</ep2.8>
			<id2.8>3a79483638293ac2efc75eaebc650b4d547e01cd</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SENTIMOS LAS MOLESTIAS</title>
		<info>(2022) 6 episodios. Cuenta la historia de Rafael Müller (Antonio Resines), un aclamado director de orquesta, y Rafael Jiménez (Miguel Rellán), una vieja gloria del rock que se resiste a colgar la guitarra. Ambos, que mantienen una amistad de décadas, luchan por encajar en un mundo que insiste en retirarles del juego, a pesar de que ellos se sienten en plenas facultades. Los dos son músicos solo que en distintos ámbitos. Müller es una figura de la cultura española. Mientras que Jiménez es una vieja estrella del rock que ya está un poco fuera de onda pero que ha logrado mantenerse a flote a pesar del paso de los años.</info>
		<year>2022</year>
		<genre>Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/lbMqkOX2NifgdPYWH2uzozJ6gG6.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dUJqDIJzJZdLiHZ83QhSm2ZD5Hq.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/lbMqkOX2NifgdPYWH2uzozJ6gG6.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/dUJqDIJzJZdLiHZ83QhSm2ZD5Hq.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>33b86aaef85f0a0b3c293d2f9bc31383bb922bf0</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>9275a9677171404a3527ed3c73239482ffda7564</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>b69ca0af12ae857a55a9d14596078144a3420923</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>a3f2006b5ddb82a7b9c69c42e1ebf5eb1273c03b</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>01e3944719bacb71ad967b255cfdc2eef41927c9</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>4e5b122d551d7e52d18a3f85604e1fe2bd352766</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SEÑOR DAME PACIENCIA</title>
		<info>(2022) 13 episodios. Sigue las desventuras de Gregorio (Jordi Sánchez), un padre de familia ultraconservador, reaccionario y lleno de prejuicios cuya situación vital se complica cuando una demanda relacionada con el accidente de su mujer le hace perder dinero, trabajo y su piso en el barrio Salamanca quedando a merced de sus hijos.</info>
		<year>2022</year>
		<genre>Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/9oIcKDhNXpi6wVdLhQQApdp9YLz.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/lwcRxaeAYNox5dyoUYqEDS9NZYK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/9oIcKDhNXpi6wVdLhQQApdp9YLz.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/lwcRxaeAYNox5dyoUYqEDS9NZYK.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>5ef1304f0bf70ce1623f1ef8a6afa4e5aaa75b60</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>f0d85fdee1b2d26c184bf0df65fd3c4bbe239044</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>802056fcb76b28c81d1df38c214f76a26e46c73e</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>7f6e97193e461354dbedec7ae0235856ccbed71c</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>2a2ecb2992189c21ca57c003c3b02f1a96dca6f8</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>54c3de6ac6506728f32f9d066a4b097ec8cd9f4f</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>061a79331afdb511db115d5788dc37a4d950694b</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>3eba4e69fc9ab891722fb620c3c49e62eb7decaa</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SEPARACION</title>
		<info>(2022) 9 episodios. Mark Scout (Adam Scott) dirige un equipo en Lumon Industries, cuyos empleados se han sometido a un procedimiento quirúrgico que separa sus recuerdos entre su ámbito laboral y su vida personal. Este atrevido experimento de "equilibrio entre el trabajo y la vida personal" se pone en tela de juicio cuando Mark se encuentra en el centro de un misterio que lo obligará a confrontar la verdadera naturaleza de su trabajo... y de sí mismo.</info>
		<year>2022</year>
		<genre>Thriller. Drama. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/wrZjYKxObEaWZmjB7scQMYo40o8.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dsMQSCOC9ReOUx0w6E1GMBMeLKS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/wrZjYKxObEaWZmjB7scQMYo40o8.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/dsMQSCOC9ReOUx0w6E1GMBMeLKS.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>b56b0148f4c0495883b2e4e26912d64e5b8d2e42</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>77b78b572099e5e562fcf833d89c20c668167d64</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>ff0d400969d163a42b3dffd3aea003e087e29fad</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>9e88b29a385de0c9d18f70c8075843c6ddc61d75</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>8bc7efeba4b7dafd4dc2bdb3832e7e92a18ef2ca</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>b8f5168ab909f723d595069bf6973bd5be02870f</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>fe807d7214d84dee23926ca7665756b24576c363</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>57d1099b38f55e172b47200959a5ef13dc943910</id1.8>
			<ep1.9>9 Episodio</ep1.9>
			<id1.9>9c5944e6e2c95305f038e8912f91b01fc1845c63</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SEQUIA</title>
		<info>(2022) 8 episodios. A causa de una fuerte sequía, el pueblo de Campomediano emerge de las aguas en el fondo del pantano que también lleva su nombre. En una de las antiguas casas aparecen unos restos humanos que pertenecen a dos hombres. La inspectora de la policía Daniela Yanes se hace cargo del caso. Las víctimas, con orificios de balas, evidencian que se trata de un crimen que ha permanecido oculto durante muchos años. La identificación de los dos cuerpos hace que dos familias separadas por la frontera entre España y Portugal vayan hacia un destino común que arranca en los últimos años de la década de los 90 y a una trama en la que diferentes personajes entrecruzan sus caminos con consecuencias imprevisibles.</info>
		<year>2022</year>
		<genre>Intriga. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/4Sq0JyUxZDOxs1gkaLbFtuVPh31.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7iyxyl6DxaorQ4KdVDYFGdV1Sbe.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/4Sq0JyUxZDOxs1gkaLbFtuVPh31.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7iyxyl6DxaorQ4KdVDYFGdV1Sbe.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>beaa9fdd6f9accef63f63456b6a1e24c8af5f9c9</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>b4e672a4a133d19d314a737f19e95ad2603c7038</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>e8c68be458b28252607c8b3a61e8d40cf6fe5bbb</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>06780529566a2c2c1b616c3168585dfab30f264a</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>53c777ca9e18dddb640156a7d0d0c44e97de5a58</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>708274ae1c6e66af787f6d74dc46920c306babaa</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>f99a16c04bea476fc4989520d9d4049ad44f9141</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>02d27139132a182529a46199ff5e39392d2790e0</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SERVANT</title>
		<info>(2019) 3 temporadas. 30 episodios. Una pareja de Philadelphia, de luto tras suceder una horrible tragedia, crea una grieta en su matrimonio que abre la puerta a una misteriosa fuerza que entra en su hogar.</info>
		<year>2019</year>
		<genre>Intriga. Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/6IeZmBBLZ6umcpGa1bTZQDrwg1x.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/digxxEISFHSPOLpBFbZnQqm9nhD.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/6IeZmBBLZ6umcpGa1bTZQDrwg1x.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/digxxEISFHSPOLpBFbZnQqm9nhD.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>547db71347f039f68e655ce5cc8b2b9b97d6b909</id1.1>
			<ep1.2>4 OSO</ep1.2>
			<id1.2>590dc69c32ad8c03c2bda5fed85a38bf86d618f4</id1.2>
			<ep1.3>5 GRILLO</ep1.3>
			<id1.3>2a386094a6a24dcf4be2c9b00f5ab9c7febb0e17</id1.3>
			<ep1.4>6 LLUVIA</ep1.4>
			<id1.4>7b7835ef0fdd974ba48217edb20d3bb10b5ea3e1</id1.4>
			<ep1.5>7 HAGGIS</ep1.5>
			<id1.5>b9a83abc4e357414b90575319292d1dd9aa08dd4</id1.5>
			<ep1.6>8 BOBA</ep1.6>
			<id1.6>dd2011aa4aa426a0081c9a0d0053969368c943bb</id1.6>
			<ep1.7>9 JERICHO</ep1.7>
			<id1.7>c121335b2ab62c42b6111e547390c3108904177b</id1.7>
			<ep1.8>10 GLOBO</ep1.8>
			<id1.8>bbf75a3cc936c03f32743ca06d4691eb60793f55</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/8yfkkAeoI77opqAvB9fyf4knftS.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/jufxJaesnhfcYHDWoq8RFYeoBau.jpg</fanart.2>
			<ep2.1>1 MUÑECO</ep2.1>
			<id2.1>878933eea07c1755b3273f421f2bd059527ddefd</id2.1>
			<ep2.2>2 ASTRONAUTA</ep2.2>
			<id2.2>039acccdd5158567f32e78481bf651400f32206a</id2.2>
			<ep2.3>3 PIZZA</ep2.3>
			<id2.3>3190c4ccf8f8f197deb1e169e88d4b28364f41b8</id2.3>
			<ep2.4>4 2:00</ep2.4>
			<id2.4>37e280023288907a2d5f3d2587d3c6b8db9e4979</id2.4>
			<ep2.5>5 TARTA</ep2.5>
			<id2.5>31d7cffdff43d68c680497411b754a5abca2a54f</id2.5>
			<ep2.6>6 EXPRESO</ep2.6>
			<id2.6>1ca3eb3863a54fb8c64a19e4e9d86f69d5bfc7db</id2.6>
			<ep2.7>7 MARINO</ep2.7>
			<id2.7>cd4a0c31dfcc815913ea6f6724ad7a8b1554890d</id2.7>
			<ep2.8>8 LOVESHACK</ep2.8>
			<id2.8>175cf084f0aae46ed49c264e3adeb47af34b899f</id2.8>
			<ep2.9>9 GANSO</ep2.9>
			<id2.9>7e23022f4fb01ec67ba29f31c83fc0030e9511ff</id2.9>
			<ep2.10>10 JOSEPHINE</ep2.10>
			<id2.10>95ed4353920bd1f0ec52da8681f0870dc9c29422</id2.10>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/xcQLwQQ7YQipan4JZoXSz5LNUqq.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/7KyYzQGA94P4dO543F7T2Nf0MPx.jpg</fanart.3>
			<ep3.1>1 BURRO</ep3.1>
			<id3.1>75c0c8bdc4c173de2d4276f441bc2ccbfe73a628</id3.1>
			<ep3.2>COLMENA</ep3.2>
			<id3.2>c2fc836dcd155d0beefba6897f5d283f79ae04e6</id3.2>
			<ep3.3>3 PELO</ep3.3>
			<id3.3>8812954f5ae733ae0d5d2bfbda76f87794546a28</id3.3>
			<ep3.4>4 ANILLO</ep3.4>
			<id3.4>647be2237b68c6e2233d5a301a0e36ff838f24a6</id3.4>
			<ep3.5>5 TIGRE</ep3.5>
			<id3.5>6e3c6c25000c291e092af79d40fae29d0303ff77</id3.5>
			<ep3.6>6 PEZ</ep3.6>
			<id3.6>ff0f3de7dadc4f2b5fd40862d9579369bceed31a</id3.6>
			<ep3.7>7 ACAMPAR</ep3.7>
			<id3.7>f1f946b38e76ff20486c7033048afa79fae00f46</id3.7>
			<ep3.8>8 DONUT</ep3.8>
			<id3.8>456656315de2d6675e06fc2bd47bdf3876911ea2</id3.8>
			<ep3.9>9 COMPROMISO</ep3.9>
			<id3.9>27d0c43dd5a5e02af9a598c6a210571e67bdeece</id3.9>
			<ep3.10>10 MAMA</ep3.10>
			<id3.10>c212c7107f62438cb32c2293317c5962478f5add</id3.10>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SEX EDUCATION</title>
		<info>(2019) 3 temporadas. 23 episodios. Como el inseguro Otis (Asa Butterfield) tiene respuesta para cualquier duda sobre sexo gracias a que su madre (Gillian Anderson) es sexóloga, una compañera le anima a abrir una "consultoría sexual" en el instituto.</info>
		<year>2019</year>
		<genre>Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/h4l0hILb6Fr1vwoKwv9XOk4Fx5.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/u23G9KZregWHs1use6ir1fX27gl.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/h4l0hILb6Fr1vwoKwv9XOk4Fx5.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/u23G9KZregWHs1use6ir1fX27gl.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>9bf83ae2606a0992abb19616e089ebc4cd81d6db</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>2f7e337571f543539306af95df0e79b795613ece</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>213b0f1c1ae385c564d00acab517c04ee42ee238</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>74c91389460830675ff590094ba1151e68d0bca2</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>1579db89659aab65d8428490a0ccdf9d9125043f</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>d752724e2c4c7dd34a1e42da647f0350d977164c</id1.6>
			<ep1.7>7 Episodio</ep1.7>
			<id1.7>0f141e2e5356f811a5079302470329e55ec396c6</id1.7>
			<ep1.8>8 Episodio</ep1.8>
			<id1.8>8b97f18d7d960fb927300946949e0ef0bad97759</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/tgecOYPSMflcG1jz9zhz1vzcpn2.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/61m8HGEWwE4q8oSz3AY5vl5F3BS.jpg</fanart.2>
			<ep2.1>1 AL 4</ep2.1>
			<id2.1>12f5de00a20097473150b0940f958219f65a7d7a</id2.1>
			<ep2.2>5 AL 8</ep2.2>
			<id2.2>f5bd43f9468d35e36188a690728c0eb97c9290cc</id2.2>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/oOEOg8rawAtc1nRbgGkf5fYcMB4.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/1h8f3ehnDpcnlnzPjk0ydYvXNyA.jpg</fanart.3>
			<ep3.1>1 Episodio</ep3.1>
			<id3.1>40474db8d10e36b8f8117489f85c0f53e9b0825d</id3.1>
			<ep3.2>3 AL 5</ep3.2>
			<id3.2>3cc6551f135557dd4509b02e83de932c74ef7462</id3.2>
			<ep3.3>6 AL 8</ep3.3>
			<id3.3>186c66d5ca0b48c79890d867341a5bd922512269</id3.3>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SEXO/VIDA</title>
		<info>(20219) 8 episodios. La historia de un triángulo amoroso entre una mujer, su marido y su pasado, que da una nueva mirada a la identidad y el deseo femeninos.</info>
		<year>2021</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/7xKl8dPErKtu3wY41dWPnF2IhmC.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7xKl8dPErKtu3wY41dWPnF2IhmC.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7xKl8dPErKtu3wY41dWPnF2IhmC.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/9yKE30esy4AQYbNvLWoOTxjLi4S.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>1627e1b6a33e0eeacd45f9d3f3cf01d218265c6a</id1.1>
			<ep1.2>5 AL 8</ep1.2>
			<id1.2>dc221496a4d6801d36ba278d9d411c0c167986ce</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SHERLOCK</title>
		<info>(2012-2017) 3 temporadas. 10 episodios. Moderna actualización del mito de Sherlock Holmes, ambientado en el Londres del siglo XXI. Sus insuperables habilidades de deducción y su arrogante distanciamiento emocional le convierten en el perfecto detective junto a su inseparable compañero John Watson.</info>
		<year>2012</year>
		<genre>Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/xfYp3q2FWwFb7ayhAyQzR8xOYfk.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1RvrUN2i9hziV7xygHScPgc1lcy.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/xfYp3q2FWwFb7ayhAyQzR8xOYfk.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/1RvrUN2i9hziV7xygHScPgc1lcy.jpg</fanart.1>
			<ep1.1>1 ESCANDALO EN BELGRAVIA</ep1.1>
			<id1.1>594881bfe843e6823186dd8adf6417afd563a4ed</id1.1>
			<ep1.2>2 LOS PERROS DE BASKERVILLE</ep1.2>
			<id1.2>b50833096922328742afbf816db10b133d632a52</id1.2>
			<ep1.3>LA CAIDA DE REICHENBACH</ep1.3>
			<id1.3>021ce4cc6b089c227a27929862926a8a2480c753</id1.3>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/xfYp3q2FWwFb7ayhAyQzR8xOYfk.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/1RvrUN2i9hziV7xygHScPgc1lcy.jpg</fanart.2>
			<ep2.1>1 EL COCHE FUNERARIO VACIO</ep2.1>
			<id2.1>d6d3763cfd770a5fd7c3d977b744284c7002873e</id2.1>
			<ep2.2>SINTOMA DE SER TRES</ep2.2>
			<id2.2>1404dddb3ce56792c829189264b2af4050942a7f</id2.2>
			<ep2.3>SU ULTIMO VOTO</ep2.3>
			<id2.3>3ad80c6a5d3e64a5a98200a7ffedbe5907d0541d</id2.3>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/xfYp3q2FWwFb7ayhAyQzR8xOYfk.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/1RvrUN2i9hziV7xygHScPgc1lcy.jpg</fanart.3>
			<ep3.1>ESPECIAL NAVIDAD 2015</ep3.1>
			<id3.1>6b90003e01212a327689f7c753ee197f48df9567</id3.1>
			<ep3.2>LAS SEIS THATCHERS</ep3.2>
			<id3.2>fea865826b996d9b848fda89efae69fd693daf92</id3.2>
			<ep3.3>EL DETECTIVE MENTIROSO</ep3.3>
			<id3.3>427cebaf512e9edf0b39a6b751ac3d08acada1a2</id3.3>
			<ep3.4>EL PROBLEMA FINAL</ep3.4>
			<id3.4>ee4ed52d75496c0f656d2a7bf1f1a7e666b059a9</id3.4>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SHINING VALE</title>
		<info>(2022) 8 episodios. Tras una aventura extraconyugal que casi destruye su matrimonio, Pat y Terry Phelps se mudan con su familia de la ciudad a una antigua mansión en Connecticut. Pero Pat siente que algo anda mal. O está poseída o deprimida: los síntomas son los mismos.</info>
		<year>2022</year>
		<genre>Comedia. Terror</genre>
		<thumb>https://www.themoviedb.org/t/p/original/oNO8MI216Y4Mu7ogakv2oRPNJFv.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/pXxwTQfo05w32NQAbKh2eU3ozxG.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/oNO8MI216Y4Mu7ogakv2oRPNJFv.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/pXxwTQfo05w32NQAbKh2eU3ozxG.jpg</fanart.1>
			<ep1.1>1 CAPITULO UNO: BIENVENIDOS A LA CASA DE LOS PHELPS</ep1.1>
			<id1.1>3baa3e826a5d40fd07290258493223b356617f1c</id1.1>
			<ep1.2>2 CAPITULO DOS: ELLA VIENE DE NOCHE</ep1.2>
			<id1.2>14ce2f6aed6af5408ff1f5063634251134822f23</id1.2>
			<ep1.3>3 CAPITULO TRES: EL PAPEL PINTADO AMARILLO</ep1.3>
			<id1.3>fa51df2418886f8f6c14409c93fabf78f14fbe7f</id1.3>
			<ep1.4>4 CAPITULO CUATRO: MUCHA SANGRE</ep1.4>
			<id1.4>83940b4eb0cb5a2de0bb183947a0e9c09454167b</id1.4>
			<ep1.5>5 CAPITULO 5: LA ARDILLA LO SABIA</ep1.5>
			<id1.5>7caed6c760321deaa4dadfdcf5b0c513376abe77</id1.5>
			<ep1.6>6 CAPITULO 6: ESPERANZA SUSURRANTE</ep1.6>
			<id1.6>5410e4f7486ece6dc88c58f82513c2298fa67675</id1.6>
			<ep1.7>7 CAPITULO 7: PREGUNTAS IMPERTINENTES</ep1.7>
			<id1.7>2229d5b194208806719b3143a6d5add706f7198f</id1.7>
			<ep1.8>8 CAPITULO 8 : NOSOTRAS SOMOS PHELPS</ep1.8>
			<id1.8>502a4b0cb548037194d8e31e1398830456c85fd6</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SIMEONE VIVIR PARTIDO A PARTIDO</title>
		<info>(2022) 6 episodios. Diego Pablo Simeone ha creado a lo largo de casi tres décadas como jugador y entrenador, una manera única y mundialmente reconocible de entender el fútbol. El “cholismo” es más una filosofía de vida que una estrategia de juego: es un conjunto de valores y herramientas que Simeone ha reunido a lo largo de su carrera a base de observación y mucho trabajo para poder enfrentar las dificultades, aprendiendo de las victorias y, sobre todo, de las derrotas. La docuserie muestra una temporada única (2020/21) al lado de Diego Pablo Simeone y desvela el secreto de un éxito que ha empezado a escribir la historia de su legado.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fzEBdUc51cZXZIWpXEuyltCvozB.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3PfKn80lZ97VB5eC39k3wu0MRmj.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/3PfKn80lZ97VB5eC39k3wu0MRmj.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fzEBdUc51cZXZIWpXEuyltCvozB.jpg</fanart.1>
			<ep1.1>1 L 6 (1080)</ep1.1>
			<id1.1>4572F3A896E3A6873333B25030E3EF9A5F25F0CC</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SIN LIMITES</title>
		<info>(2022)  6 episodios. En agosto de 1519, capitaneados por el portugués Fernando de Magallanes, 239 marineros partieron desde Sevilla rumbo a las Indias. Tres años más tarde, solo 18 marinos famélicos y enfermos regresaron en la única nave que resistió el viaje, liderados por el marino español Juan Sebastián Elcano. Habían completado la vuelta al mundo; una misión casi imposible que pretendía encontrar una nueva ruta hacia las “islas de las especias” y que terminó cambiando la historia de la humanidad al demostrar que la Tierra es redonda. Una gesta que transformó para siempre el comercio, la economía, la astronomía y el conocimiento del planeta, y que está considerada como una de las mayores hazañas de la historia.</info>
		<year>2022</year>
		<genre>Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/8RAmN8pN40ZGDT8tYp3savTrqPs.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8j4jxGYejA93oXq063O0GlDMGiv.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8RAmN8pN40ZGDT8tYp3savTrqPs.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8j4jxGYejA93oXq063O0GlDMGiv.jpg</fanart.1>
			<ep1.1>1 MI REY NO HA QUERIDO VERME</ep1.1>
			<id1.1>0859bc12396e6fdac3621be8a2dcc4ebe4a33751</id1.1>
			<ep1.2>2 TE BUSCAN POR TRAICION</ep1.2>
			<id1.2>9e4440885adab77b50f1473fdbf9491ef1d353c1</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SIN NOVEDAD</title>
		<info>(2021) 6 episodios. Dos policías en una vigilancia nocturna, dos inspectoras que coordinan la operación desde la centralita de la comisaría y un par de delincuentes, están deseando "pasar a la acción". Sin embargo, cuando en realidad no sucede nada, empiezan a rellenar los silencios con anécdotas e historias sobre su intimidad para terminar exponiendo por el camino sus rarezas, secretos y verdaderas personalidades.</info>
		<year>2021</year>
		<genre>Comedia</genre>
		<thumb>https://www.themoviedb.org/t/p/original/jYuhnOapapB1C0rYVh0Q6nYqw3r.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/732s8VeJttwF6d34grQnpkBkFvK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/jYuhnOapapB1C0rYVh0Q6nYqw3r.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/732s8VeJttwF6d34grQnpkBkFvK.jpg</fanart.1>
			<ep1.1>1 EL UNICORNIO</ep1.1>
			<id1.1>d6a4c81b495076ff440b517dc9d7d4030b41c4fb</id1.1>
			<ep1.2>2 ABRA CADAVER</ep1.2>
			<id1.2>140e404c2adef1fbb99ca43d73a6d4a11ccf9f3e</id1.2>
			<ep1.3>3 LA CITA</ep1.3>
			<id1.3>373475175a867b8b3c2132be522f71782f1bc31e</id1.3>
			<ep1.4>4 LA TESTIGO</ep1.4>
			<id1.4>2e6718a2eb9fdd5cfdb00da3f38ad09883a01b91</id1.4>
			<ep1.5>5 INTERCAMBIOS</ep1.5>
			<id1.5>fc99955e8e0c7b7620070ee53989951142bbf241</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SISI</title>
		<info>(2021) 6 episodios. "Sisi" sigue la extraordinaria vida de la emperatriz Isabel de Austria. Moderno, honesto y auténtico. Contada desde la perspectiva de sus confidentes más cercanos, la serie da una nueva mirada a la vida de la emperatriz y revela una mujer de múltiples capas.</info>
		<year>2021</year>
		<genre>Drama. Romance</genre>
		<thumb>https://www.themoviedb.org/t/p/original/j8igK77NPOzEsy2D4yJCPDA3e6r.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/jdO8hYilP1Bc6wkaBiB2YNZ4bDa.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/j8igK77NPOzEsy2D4yJCPDA3e6r.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/jdO8hYilP1Bc6wkaBiB2YNZ4bDa.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>648d5dfc0303e8f7248af2169e672de51ddba009</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>605482ae9d43d2ce1cda22134e97c4e0ad6218b9</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>b7b74f05af55a3221ae777ede05ca6f6b0b0c2e0</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>eed9f41b6f8754402a15bab923ded5c4ce2cbacb</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>4971b609ddd9fe668cd9de885b18ef162fde3d08</id1.5>
			<ep1.6>6 Episodio</ep1.6>
			<id1.6>1bb432ea25b42df22906b958e6003a64af38f7c6</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SKAM ESPAÑA</title>
		<info>(2018-2020)  4 temporadas. 38 episodios. Skam sigue el día a día de un grupo de jóvenes de 16 años de un instituto y su relación con sus sentimientos, el amor, el sexo y el paso al mundo adulto. Remake de la popular serie noruega sobre la vida de los adolescentes actuales.</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/ueXNsr8XSyUoJPwT3g5v7YAhZIJ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/bW9M7di0lD6V5JKHT05d94yMTHX.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/sNqZzxDi7IaU3DBrsVud56iLn92.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/hrVF15fXneZcLwXO861j7Q32BgB.jpg</fanart.1>
			<ep1.1>1 LAS RARAS DEL INSTITUTO</ep1.1>
			<id1.1>fbb86773e19c53560f6d62755383e465cd4b16da</id1.1>
			<ep1.2>2 HAY LIO</ep1.2>
			<id1.2>99ee07099634f9e8c368e0783a040b82413eb292</id1.2>
			<ep1.3>3 ENSEÑAME TU MOVIL</ep1.3>
			<id1.3>c43082587c449b626151ef9f2230646fba600405</id1.3>
			<ep1.4>4 VERDAD O ATREVIMIENTO</ep1.4>
			<id1.4>d86d96998be7c11628227b23146238cd5a9c9208</id1.4>
			<ep1.5>5 ¿Y COMO SOY?</ep1.5>
			<id1.5>d3edd6153c608f7e2e6d241ccb8a1b687804a12b</id1.5>
			<ep1.6>6 SIEMPRE ENFADADOS</ep1.6>
			<id1.6>3089cfc769d79a000745f0ce674096cd86d417a3</id1.6>
			<ep1.7>7 HECHA UNA MIERDA</ep1.7>
			<id1.7>740edf3f76a7e5c7fdec1f0db85e310ea99d840b</id1.7>
			<ep1.8>8 LA VERDAD POR DELANTE</ep1.8>
			<id1.8>51a7f410e20e806213ca7a0100a5968198ca46ba</id1.8>
			<ep1.9>9 EVA LA ZORRA</ep1.9>
			<id1.9>25c656f122359e1bfd425f9acc478bfe2db66023</id1.9>
			<ep1.10>10 A NADIE LE IMPORTA</ep1.10>
			<id1.10>89ff62f0dbec400f2a0e12d681b4c79d7b96ba27</id1.10>
			<ep1.11>11 FIN</ep1.11>
			<id1.11>976a012ab8bdec1deab9bf016888c462bf56ec9d</id1.11>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/n9rBvsQztXVyoIEQUhaENOmmPnV.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/gRYQsfkNklwvCMpV9CbigbQBsFs.jpg</fanart.2>
			<ep2.1>1 EXTASIS</ep2.1>
			<id2.1>02f2978a663ae0d2e89047952e4b4c6030f3a123</id2.1>
			<ep2.2>2 OJOS DE SAPO</ep2.2>
			<id2.2>0f4f915a9e965e772336c370c5f687940bd06e49</id2.2>
			<ep2.3>3 PERDIDA</ep2.3>
			<id2.3>036434ec855ed8c47db770ef576196820a7de37b</id2.3>
			<ep2.4>4 DESEANDO NADAR</ep2.4>
			<id2.4>79581d92772e45a5cab6a4a98b9fb9dfa71e0b92</id2.4>
			<ep2.5>5 PUÑETAZO</ep2.5>
			<id2.5>d5489a0016112b00c0d61c34fd31d785e17cef35</id2.5>
			<ep2.6>6 ¿ERES LESBIANA?</ep2.6>
			<id2.6>eb495fb5f4db89527a42d76dcec22ca99df19357</id2.6>
			<ep2.7>7 DESCONOCIDA</ep2.7>
			<id2.7>3432f0f665cb2165aed07c070c3bb2c3278fac75</id2.7>
			<ep2.8>8 COBARDE</ep2.8>
			<id2.8>4e7a4101a41743e600bfdf584dc39d54ef24d8e6</id2.8>
			<ep2.9>9 NO PUEDO EVITARLO</ep2.9>
			<id2.9>82b51c4bc25395ef327f5dea44cde360fb28f01f</id2.9>
			<ep2.10>10 MINUTO A MINUTO</ep2.10>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/znrMH4JPLy7fc6JfnWnhQDaslf7.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/n2OJtmwd693x93lD8RwelwI8K0M.jpg</fanart.3>
			<ep3.1>1 FUCKBOY</ep3.1>
			<id3.1>1178cbaa050f664ade55e9b7393214a93951bdc5</id3.1>
			<ep3.2>2 NO SE LO CUENTES A NADIE</ep3.2>
			<id3.2>c425c9a3af2b4a6a65d09c404211e7409d9deffd</id3.2>
			<ep3.3>3 LO DE VIRI</ep3.3>
			<id3.3>975123de9d7c8f2c1ec041017cfe5ec4239c8da9</id3.3>
			<ep3.4>4 LA PERSONA ADECUADA, EL MOMENTO ADECUADO</ep3.4>
			<id3.4>7cd044cd47ddeb1722d0f2e7c6815fb110ede81b</id3.4>
			<ep3.5>5 EL CONCIERTO</ep3.5>
			<id3.5>429ad1c1cfbc8b399235d5436a3098bea82480fb</id3.5>
			<ep3.6>6 ¿QUE HE HECHO?</ep3.6>
			<id3.6>54f3a891ddbb94d234eef245ba5ef3a986d02de2</id3.6>
			<ep3.7>7 ¿EN SERIO NO TE ACUERDAS DE NADA?</ep3.7>
			<id3.7>bf23fc701b59f3454aaa49f1ef44c12548c6a6bc</id3.7>
			<ep3.8>8 LA NORA QUE ME GUSTABA</ep3.8>
			<id3.8>756ef656fe76f1652aee3755ceb0e1b785975c5b</id3.8>
			<ep3.9>9 ¿EXISTEN LOS FINALES FELICES?</ep3.9>
			<id3.9>682012231ed314d8bb8f9e8c43b9fdfc67d71817</id3.9>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/ueXNsr8XSyUoJPwT3g5v7YAhZIJ.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/bW9M7di0lD6V5JKHT05d94yMTHX.jpg</fanart.4>
			<ep4.1>1 ENTRE DOS MUNDOS</ep4.1>
			<id4.1>af3512754ad7c867039134a29207842313a71014</id4.1>
			<ep4.2>2 NO LLEVO EL HIJAB</ep4.2>
			<id4.2>280c2073086ded1abc201d6128cda607e4485857</id4.2>
			<ep4.3>3 SEGURO QUE ALA LO ENTIENDE</ep4.3>
			<id4.3>4112d27ba66293599b8bfa7ce26de989d6ab4c46</id4.3>
			<ep4.4>4 NUNCA ES POR AMIRA</ep4.4>
			<id4.4>f332645ff8d63f813b08a4930db2dc39a3a84f53</id4.4>
			<ep4.5>5 RAMADAN KAREEM</ep4.5>
			<id4.5>88055cb60363c2439157cfeb915be3ed909ac19e</id4.5>
			<ep4.6>6 LA MORA DEL INSTITUTO</ep4.6>
			<id4.6>22b6a55186bdd22ea98730fd5bfd867172157065</id4.6>
			<ep4.7>7 YA NO SE NADA</ep4.7>
			<id4.7>e04520d98f623c019a04699a081dfa99b2c0e5eb</id4.7>
			<ep4.8>LAS LOSERS</ep4.8>
			<id4.8>57af6aab01d4d5b6adcc4683b9bf3a1419e6b1be</id4.8>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SKY ROJO</title>
		<info>(2021) 2 temporadas 16 episodios. Coral, Wendy y Gina emprenden una huida en busca de su libertad mientras son perseguidas por Moisés y Christian, los secuaces de Romeo, el proxeneta y dueño del Club Las Novias. Juntas iniciarán una carrera desesperada en la se enfrentarán a todo tipo de peligros y cuyo único objetivo será seguir vivas cinco minutos más</info>
		<year>2021</year>
		<genre>Acción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/uTFX9V2dct1nKjG6zhNiThPm8Tp.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/aqrvHKPWe5VedvvJIz7FWrtcoQh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/uTFX9V2dct1nKjG6zhNiThPm8Tp.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/aqrvHKPWe5VedvvJIz7FWrtcoQh.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>84979994908db213b84562da927290c3e45d001a</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/nrwTcIXZAfQT3hqLjMEYjk01ItE.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/e8m25rmxcCtoNh3KQHtTFfoFSEF.jpg</fanart.2>
			<ep2.1>1 AL 3</ep2.1>
			<id2.1>400acc288c70c266e6c1194b4f8d9f5e4836cdec</id2.1>
			<ep2.2>4 AL 6</ep2.2>
			<id2.2>a92d2f55e368e56dc014db6dbc1e67e2d5dc4341</id2.2>
			<ep2.3>7 AL 8</ep2.3>
			<id2.3>c22116e068dff2114d65d747be940167d0dd27df</id2.3>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SLOW HORSES</title>
		<info>(2022) 6 episodios. Después de una misión de entrenamiento fallida y públicamente vergonzosa, el agente británico del MI5, River Cartwright, es exiliado a Slough House, un purgatorio administrativo para los marginados del servicio.</info>
		<year>2022</year>
		<genre>Intriga. Thriller </genre>
		<thumb>https://www.themoviedb.org/t/p/original/oBqlO6ZjTcVFiE0lBj89lAOi18s.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/mMwHOiL5H2dq3l3w0FRBRJbO5d7.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/oBqlO6ZjTcVFiE0lBj89lAOi18s.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/mMwHOiL5H2dq3l3w0FRBRJbO5d7.jpg</fanart.1>
			<ep1.1>1 LA INEPTITUD SE CONTAGIA</ep1.1>
			<id1.1>4355fd4cd2ba02519db481bbdf79bfdc42d11134</id1.1>
			<ep1.2>2 COPAS DESPUES DEL TRABAJO</ep1.2>
			<id1.2>e94bb23b450877a12b60643390d6dd13ca6e5cbe</id1.2>
			<ep1.3>3 IMPERDONABLE</ep1.3>
			<id1.3>ae83cc631d95478d7bb7b8bb4826513676868242</id1.3>
			<ep1.4>4 HORAS DE VISITA</ep1.4>
			<id1.4>c2f122d7b3da7f44a031c31df7fa8f226306d252</id1.4>
			<ep1.5>5 FIASCO</ep1.5>
			<id1.5>45f8c724c60b3cbbd434e7c29efd32769eb78614</id1.5>
			<ep1.6>6 LOCURAS</ep1.6>
			<id1.6>5a59b1050e05cc3874e2e8144504bc7bc0172d7f</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SNOWPIERCER: ROMPENIEVES</title>
		<info>(2020) 3 Temporadas. 30 episodios. Ambientada siete años después de que el planeta Tierra se haya convertido en un páramo helado, 'Snowpiercer' sigue a los restos de la humanidad, que habitan en un tren de grandes dimensiones en continuo movimiento. Clases sociales, justicia social y juegos políticos se dan dentro de lo que para la humanidad es su único hogar. Adaptación de la novela gráfica homónima.</info>
		<year>2020</year>
		<genre>Ciencia ficción. Acción. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/7AoS6Oez4C3z8wRnQVHYLwhY3FX.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/dJBKKbrQIqcQ9EUbdM48dM8mso6.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/7AoS6Oez4C3z8wRnQVHYLwhY3FX.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/dJBKKbrQIqcQ9EUbdM48dM8mso6.jpg</fanart.1>
			<ep1.1>1 PRIMERO CAMBIO EL CLIMA</ep1.1>
			<id1.1>c179bb61870ae5383476a15cbee81362f013d434</id1.1>
			<ep1.2>2 PREPARADOS PARA LO PEOR</ep1.2>
			<id1.2>97d16fd82b433153140141d432b663798f7433b1</id1.2>
			<ep1.3>3 EL ACCESO ES PODER</ep1.3>
			<id1.3>261a7920c59bac9a50d84678d51a4c38843acbce</id1.3>
			<ep1.4>4 SIN SU CREADOR</ep1.4>
			<id1.4>3113c0a8f9ba45f270165e546a47509579ea8101</id1.4>
			<ep1.5>5 JUSTICIA A BORDO</ep1.5>
			<id1.5>629ebb9929dbd0baa90fa1fa0c2cdfda485faf0e</id1.5>
			<ep1.6>6 EL PROBLEMA VIENE DE LADO</ep1.6>
			<id1.6>d7f709a8d87023e2ab9e984e8cf3f07372d39721</id1.6>
			<ep1.7>7 EL UNIVERSO ES INDIFERENTE</ep1.7>
			<id1.7>a8c2b341eeffdc085a10b265070fba52b6aa4d50</id1.7>
			<ep1.8>8 ESTAS SON SUS REVOLUCIONES</ep1.8>
			<id1.8>7e079548a9341820ec40be3249714a7bea0f97c1</id1.8>
			<ep1.9>9 EL TREN DEMANDABA SANGRE</ep1.9>
			<id1.9>a44a73886e890c820f1ef583529c53b536bf60e4</id1.9>
			<ep1.10>10 994 VAGONES</ep1.10>
			<id1.10>7de664c18151ea764e94a3a2e6c7279a2aae0a94</id1.10>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/95xQPSqwvQDRoB3rXUn4lRyJrBW.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/2fhU42I8pPdvEKfrZtuJ8KrzdeF.jpg</fanart.2>
			<ep2.1>1 HISTORIA DE DOS MAQUINAS</ep2.1>
			<id2.1>e1ec959481decfd8cba55ac2725ce839a68424cd</id2.1>
			<ep2.2>2 EL LATIDO DE LA VIDA</ep2.2>
			<id2.2>b95c39106b04287e94a76361d9043728756904e3</id2.2>
			<ep2.3>3 UNA GRAN ODISEA</ep2.3>
			<id2.3>63f73b08da5f14713d168db182f542914128118f</id2.3>
			<ep2.4>4 UNA SOLA TRANSACCION</ep2.4>
			<id2.4>d78d838918fdfd16801a29689f54d8a061be6bc5</id2.4>
			<ep2.5>5 MANTENER VIVA LA ESPERANZA</ep2.5>
			<id2.5>a74f4aa405a444a696f4aa28439699049ee6e0e6</id2.5>
			<ep2.6>6 LEJOS DE LA SNOWPIERCER</ep2.6>
			<id2.6>961c5c4069dc8d2fdca699c53a5a1d08b1b924bf</id2.6>
			<ep2.7>7 NUESTRA RESPUESTA PARA TODO</ep2.7>
			<id2.7>48b7b7b5a8085d6cb915855035fb83851a7e3b2b</id2.7>
			<ep2.8>8 INGENIERO ETERNO</ep2.8>
			<id2.8>c836e637b8733444a5ce244517a2024752c515cb</id2.8>
			<ep2.9>9 EL ESPECTACULO DEBE CONTINUAR</ep2.9>
			<id2.9>df0153a31fb89017c2d18a64f0df73de9dba532d</id2.9>
			<ep2.10>10 RUMBO A LO DESCONOCIDO</ep2.10>
			<id2.10>01cc78c4ab39f05c7b0ff78cdbe890c52b9aa70e</id2.10>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/3s6ZyZKurx6wDJZMXSsbUsgjWCI.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/h8dzXytAPFkAHv1kZAN1SshXI68.jpg</fanart.3>
			<ep3.1>1 LA TORTUGA Y LA NIEVE</ep3.1>
			<id3.1>ec61eff7aaabcffdc443c5124fbd2a241862f6b6</id3.1>
			<ep3.2>2 EL ULTIMO EN MORIR</ep3.2>
			<id3.2>a5311b35a4b121c6215245072de8a5671cd3f382</id3.2>
			<ep3.3>3 EL PRIMER GOLPE</ep3.3>
			<id3.3>d7a38e70bc9d0f94a09c7cd55b6e852890cbd6c4</id3.3>
			<ep3.4>3 EL PRIMER GOLPE</ep3.4>
			<id3.4>22cb2eeb5bc8b02dc7b3f98ef2aeca27c5930155</id3.4>
			<ep3.5>5 UNA NUEVA VIDA</ep3.5>
			<id3.5>4b03ffe1525b5400f5e6fc45d7fcf6994966414d</id3.5>
			<ep3.6>6 NACIDO PARA SANGRAR</ep3.6>
			<id3.6>ddc9c4c4c49dd32bcef4fdef081567ccae567156</id3.6>
			<ep3.7>7 UROBOROS</ep3.7>
			<id3.7>5c961150e17b33af979cdf984b4778fc96336d98</id3.7>
			<ep3.8>8 PONIENDOSE EN SU LUGAR</ep3.8>
			<id3.8>ba86af05d185ea764acb4cca02d63ab207d8ac8b</id3.8>
			<ep3.9>9 UN FARO PARA TODOS NOSOTROS</ep3.9>
			<id3.9>f5697ff643ac0f35d82fb86728b61089a7eaa9a8</id3.9>
			<ep3.10>10 LOS PECADORES ORIGINALES</ep3.10>
			<id3.10>d31d480812ddd58c505343bd1720bf01e1dbb95e</id3.10>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SOLO ASESINATOS EN EL EDIFICIO</title>
		<info>(2021) 10 Episodios. Tres extraños que comparten la obsesión por el "true crime" de repente se ven atrapados en uno.</info>
		<year>2021</year>
		<genre>Comedia. Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fOEiX00L6uw4GSYKD0JrxRr7Zrc.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fgYfch0MGfNEpgzPst49ThKUqA4.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fOEiX00L6uw4GSYKD0JrxRr7Zrc.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fgYfch0MGfNEpgzPst49ThKUqA4.jpg</fanart.1>
			<ep1.1>1 CRIMENES REALES</ep1.1>
			<id1.1>8d7e652afa6513434be7ab404b11bd80a4845588</id1.1>
			<ep1.2>2 ¿QUIEN ES TIM KONO?</ep1.2>
			<id1.2>95a2fd54dd8d4718b3ef1df068c7889586c1e4c6</id1.2>
			<ep1.3>3 ¿HASTA QUE PUNTO CONOCES A TUS VECINOS?</ep1.3>
			<id1.3>0f05babca919c5467c746d9c713f61a74fd55882</id1.3>
			<ep1.4>4 STING</ep1.4>
			<id1.4>0aaacb64855bbe563181ab51ceaf810808f5db4b</id1.4>
			<ep1.5>5 VUELTA DE TUERCA</ep1.5>
			<id1.5>ff599df55f555c11a7202106d6c02788efbaa12a</id1.5>
			<ep1.6>6 PARA PROTEGER Y SERVIR</ep1.6>
			<id1.6>8f4799cc90ec8a2489927200956fb9531c05dd21</id1.6>
			<ep1.7>7 EL CHICO DEL 6B</ep1.7>
			<id1.7>1df497eceed8922188aaa94b97de78ee854b645c</id1.7>
			<ep1.8>8 FANFICCION</ep1.8>
			<id1.8>b24efcc0d0499392e2bb4498437db0d57aaec061</id1.8>
			<ep1.9>9 ABIERTO Y CERRADO</ep1.9>
			<id1.9>0c0a06576315ba4bc72e52ed8787b423edd5bcab</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SOLOS</title>
		<info>(2021) 7  Episodios. Serie antológica de siete partes que explora el significado más profundo de la conexión humana a través de la visión individual de sus protagonistas.</info>
		<year>2021</year>
		<genre>Drama. Ciencia ficción. Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/szoC8f6wAXGmgn0aiSDroRx7Ngs.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/4DgUTuxK95CYfpOheUFzdvOveMt.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/szoC8f6wAXGmgn0aiSDroRx7Ngs.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/4DgUTuxK95CYfpOheUFzdvOveMt.jpg</fanart.1>
			<ep1.1>1 AL 7</ep1.1>
			<id1.1>b4f188e96af03caffafbb2c7005e05c8fa6</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SOMBRA Y HUESO</title>
		<info>(2021) 8 Episodios. Fuerzas siniestras conspiran contra una joven soldado cuando descubre un poder mágico que podría unir a su mundo.</info>
		<year>2021</year>
		<genre>Fantástico</genre>
		<thumb>https://www.themoviedb.org/t/p/original/1Ok36olOn9HXOUlFdlOYngItmxX.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/8z9qQkx7wA6FXpLV8Tiw9mfsRFK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/1Ok36olOn9HXOUlFdlOYngItmxX.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/8z9qQkx7wA6FXpLV8Tiw9mfsRFK.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>86b2550f531bd810abcd5a49e2bcfa3b90dbb61a</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SOMOS LOS MEJORES: UNA NUEVA ERA</title>
		<info>(2021) 10 Episodios. Cuando Evan, de 12 años, no es seleccionado para el equipo de hockey, su madre le anima a formar un nuevo equipo con Gordon Bombay, el entrenador original de los Ducks.</info>
		<year>2021</year>
		<genre>Comedia. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/cExVTHC240EX7n0AgiZFP4e5lva.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/qcKN95e2HJzfmypDqBUHW2Ti751.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/cExVTHC240EX7n0AgiZFP4e5lva.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/qcKN95e2HJzfmypDqBUHW2Ti751.jpg</fanart.1>
			<ep1.1>1 A JUGAR</ep1.1>
			<id1.1>4942112ab3e6bbe9e28632f0613c246065402de0</id1.1>
			<ep1.2>2 NEGADOS</ep1.2>
			<id1.2>4456897211c25c0df3e45b9911ad63b66b99cadc</id1.2>
			<ep1.3>3 ESCAPADA</ep1.3>
			<id1.3>a17e43d89255b2fe1c6bd1b1f7213afbd193ffeb</id1.3>
			<ep1.4>4 HOCKEY ENTRE MADRES</ep1.4>
			<id1.4>4afbed9cbd3c881035002c6f9600ba391bf28e45</id1.4>
			<ep1.5>5 LA LEY DEL MINIMO ESFUERZO</ep1.5>
			<id1.5>e5573dc2d57153082cd058d3ba207e2519b5087c</id1.5>
			<ep1.6>6 EL ESPIRITU DE LOS PATOS</ep1.6>
			<id1.6>1407bf454be4da4c6d821db650c2aca2b99e64fc</id1.6>
			<ep1.7>7 HOCKEY EN EL ESTANQUE</ep1.7>
			<id1.7>a663eec050c355cc638d10f03b28a4d9ff203a58</id1.7>
			<ep1.8>8 CAMBIO SOBRE LA MARCHA</ep1.8>
			<id1.8>18781ebace99d8d9dd894c8e113d422472c3842c</id1.8>
			<ep1.9>9 JUEGOS MENTALES</ep1.9>
			<id1.9>fd1243b94d247e8d88bf5e189868a9f9f3645381</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SOSPECHOSOS</title>
		<info>(2022) 8 episodios. Thriller sobre el secuestro del hijo de una importante empresaria estadounidense (Uma Thurman). El secuestro de Leo, de 21 años, en un gran hotel de lujo en el centro de Nueva York, se graba en video y se vuelve viral. Rápidamente, cuatro ciudadanos británicos que se alojan en el hotel se convierten en los principales sospechosos, pero ¿son quizá solo culpables de estar en el lugar equivocado en el momento equivocado? Remake de la serie israelí del 2015 "False Flag".</info>
		<year>2022</year>
		<genre>Thriller. Intriga </genre>
		<thumb>https://www.themoviedb.org/t/p/original/muLbLW8kWOSiEBLgmHeAw6gKpB0.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/yhIpzIfSjGX92ea7Ts8J3VHMBDt.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/muLbLW8kWOSiEBLgmHeAw6gKpB0.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/yhIpzIfSjGX92ea7Ts8J3VHMBDt.jpg</fanart.1>
			<ep1.1>1 SOSPECHOSOS</ep1.1>
			<id1.1>693b794283227d32fd668fc6859eb95010fa97fa</id1.1>
			<ep1.2>2 LUGAR A DUDAS</ep1.2>
			<id1.2>24062ff43747222dfc230974ba90ca19773edb91</id1.2>
			<ep1.3>3 DESCONOCIDOS</ep1.3>
			<id1.3>8a1bb8f6f2003ce2765ee5342cbc0052169fbc14</id1.3>
			<ep1.4>4 MAS VALE MALO CONOCIDO</ep1.4>
			<id1.4>3782cd3393704f519b3604dca5291d1233731827</id1.4>
			<ep1.5>5 ¿QUE ASPECTO TIENE UN SECUESTRADOR?</ep1.5>
			<id1.5>778e27d1729e96e6f792646cd47710bc36c2d45c</id1.5>
			<ep1.6>6 SE EL HOMBRE GRIS</ep1.6>
			<id1.6>c37d2d01ae0b18cb3637b95467f9c647aa43b695</id1.6>
			<ep1.7>7 CUESTION DE CONFIANZA</ep1.7>
			<id1.7>997813eee2936ef01312c03d369876a9a57d536d</id1.7>
			<ep1.8>8 DESENMASCARADOS</ep1.8>
			<id1.8>2bb1bf019e05e931a4f48c0a5deec2934ed9b6e3</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SOY GEORGINA</title>
		<info>(2022) 6 episodios. Esta es Georgina Rodríguez: madre, influencer, empresaria y pareja de Cristiano Ronaldo. Un retrato emotivo y exhaustivo de su vida cotidiana.</info>
		<year>2022</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/qv3PAKsQinkNrGZU6WlbyCrrQP6.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7LmTofaj0Jm7ZffxfnbEorpdwA3.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qv3PAKsQinkNrGZU6WlbyCrrQP6.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7LmTofaj0Jm7ZffxfnbEorpdwA3.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>7691978c985dd2ef3c00b44794e0fc08da3cc245</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>6789b936c93741df6a9343e3d60d8ac43ef7c822</id1.2>
			<ep1.3>5 AL 6</ep1.3>
			<id1.3>c6f4286138b1dc1a51b98ca77b2f4762c6e94300</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SPICE GIRLS EL PRECIO DEL EXITO</title>
		<info>(2021) 3 episodios. Miniserie documental de tres episodios que repasa la meteórica carrera del exitoso grupo pop femenino Spice Girls y analiza cómo lograron abrirse paso en un mundo de hombres y triunfar mundialmente.</info>
		<year>2021</year>
		<genre>Documental </genre>
		<thumb>https://www.themoviedb.org/t/p/original/zGwrKpIh7he1dOEfnFUexmuPp2O.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/bWiQpr1lht67ZtKl5ouSYlQUBNd.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/zGwrKpIh7he1dOEfnFUexmuPp2O.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/bWiQpr1lht67ZtKl5ouSYlQUBNd.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>64db94639d7f400af9f63a97d233291878735853</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>STAR TREK DISCOVERY</title>
		<info>(2017) 4 temporadas. 52 episodios: Ambientada aproximadamente diez años antes de la serie original de 'Star Trek' y desmarcándose de la línea temporal de los largometrajes, 'Star Trek: Discovery' sigue a la tripulación del USS Discovery según va descubriendo nuevos mundos y formas de vida.</info>
		<year>2017</year>
		<genre>Ciencia ficción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/ihvG9dCEnVU3gmMUftTkRICNdJf.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/h5jlI68Dutp5dq5dbFXRBXN79yE.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/1pNVxVyfz31C6w4tYFrkXKaQfBK.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/1pNVxVyfz31C6w4tYFrkXKaQfBK.jpg</fanart.1>
			<ep1.1>1 EL SALUDO VULCANO</ep1.1>
			<id1.1>95da869308204fb29e8631acf4d698b4a843928f</id1.1>
			<ep1.2>2 BATALLA EN LAS ESTRELLAS BINARIAS</ep1.2>
			<id1.2>0e4d93a9d08a7a90b5b91bacb3ec16164a2a3e89</id1.2>
			<ep1.3>3 EL CONTEXTO ES PARA LOS REYES</ep1.3>
			<id1.3>26ea3d96303d125eb4e094070676eb9b3af556cf</id1.3>
			<ep1.4>4 AL CUCHILLO DEL CARNICERO NO LE IMPORTAN LOS CHILLIDOS DEL CORDERO</ep1.4>
			<id1.4>5d409c2f8ab0269a70b9f099de3399d374d1e491</id1.4>
			<ep1.5>5 ELIGE QUIEN SUFRE</ep1.5>
			<id1.5>a2ed494ecbbbd720786baeb62b524406db5d5f3a</id1.5>
			<ep1.6>6 LETEO</ep1.6>
			<id1.6>e8ca6fe45240262c5622ac44e4ea51a85fe660f0</id1.6>
			<ep1.7>7 LA MAGIA QUE VUELVE LOCO AL HOMBRE CUERDO</ep1.7>
			<id1.7>bbbbae0a9951e79dbcbcbef0962b1ad2f771ae74</id1.7>
			<ep1.8>8 SI VIS PACEM, PARA BELLUM</ep1.8>
			<id1.8>82498fef64070d626a4394552e3c9b7fd3978fdf</id1.8>
			<ep1.9>9 EN EL BOSQUE ME ADENTRO</ep1.9>
			<id1.9>a977cb28130a3ea1ef67ddecfbb67fb761247bd2</id1.9>
			<ep1.10>0 A PESAR DE TI MISMO</ep1.10>
			<id1.10>aac1ad4801293987c9f16c219bbcc6dcd6b79c00</id1.10>
			<ep1.11>11 EL LOBO INFILTRADO</ep1.11>
			<id1.11>e75182003783902f308175b6a9c901428f20cfd6</id1.11>
			<ep1.12>12 LA AMBICION DEL SALTO</ep1.12>
			<id1.12>52ce4040d268f16317d8d8ba2222062f0ddf9c48</id1.12>
			<ep1.13>13 EL PASADO ES UN PROLOGO</ep1.13>
			<id1.13>89b6be447f1f3e0b8fd2047eebc504762482866b</id1.13>
			<ep1.14>14 LA GUERRA EXTERIOR, LA GUERRA INTERIOR</ep1.14>
			<id1.14>5ce80526b368ba70d7a10303879c6237e9fc6061</id1.14>
			<ep1.15>15 ¿ME COGES DE LA MANO?</ep1.15>
			<id1.15>3f8574cf36b1755b511bfd67d8b8ace9b6d71781</id1.15>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/aJTSeqG43514TewIuS9hiHvbero.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/5zXbVH4XGbUu8guBZSMyBHzDBLb.jpg</fanart.2>
			<ep2.1>1 HERMANO</ep2.1>
			<id2.1>44d9acf8b770ae8fef5a06eb6317ae684c024e10</id2.1>
			<ep2.2>2 NUEVO EDEN</ep2.2>
			<id2.2>ee9be37df0a8d3061f5bff78821ecd97b5ebe5cc</id2.2>
			<ep2.3>3 PUNTO DE LUZ</ep2.3>
			<id2.3>820c85c7e3565221ece5081e8e86636f4d8919b4</id2.3>
			<ep2.4>4 UN OBOLO PARA CARON</ep2.4>
			<id2.4>f96c4a754e6ecd8dcd1fb5afefad4c4e6662dd2e</id2.4>
			<ep2.5>5 SANTOS DE LA IMPERFECCION</ep2.5>
			<id2.5>6a5bf9ad425ae69a5374fc753e66d60f3d1bb8da</id2.5>
			<ep2.6>6 EL SONIDO DEL TRUENO</ep2.6>
			<id2.6>d94375f0dea61e4a3c45fcca4811ad8a63b61cb3</id2.6>
			<ep2.7>7 LUZ Y SOMBRAS</ep2.7>
			<id2.7>2897670d79d7777280ffca73a585fc96202982d2</id2.7>
			<ep2.8>8 SI LA MEMORIA NO FALLA</ep2.8>
			<id2.8>2f0ce1070ef11390dd500ca13f8618986e044baa</id2.8>
			<ep2.9>9 PROYECTO DEDALO</ep2.9>
			<id2.9>ab13dceebdbe7be56b60edb0bac958b8e85e1437</id2.9>
			<ep2.10>10 EL ANGEL ROJO</ep2.10>
			<id2.10>3f4068f10241204c0967a2cc7fccd20b0b1e9a30</id2.10>
			<ep2.11>11 INFINITO PERPETUO</ep2.11>
			<id2.11>19ab08c91a6c705a48e1bdafa9a25c316562a93d</id2.11>
			<ep2.12>12 POR EL VALLE DE LAS SOMBRAS</ep2.12>
			<id2.12>338127a4ad6c22197e89dcfe773b8cc652bbcb23</id2.12>
			<ep2.13>13 TAN DULCE TRISTEZA</ep2.13>
			<id2.13>1a209ab4de5e0d53a779da9bce46bc459c4172a1</id2.13>
			<ep2.14>14 TAN DULCE TRISTEZA, PARTE 2</ep2.14>
			<id2.14>7cafea1d5c18aed556fb5cebf22de7586d2c6819</id2.14>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/98RYSYsRNKWgrAAFBn0WfploUG7.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/p3McpsDNTNmpbkNBKdNxOFZJeKX.jpg</fanart.3>
			<ep3.1>1 ESA ESPERANZA ES USTED, PARTE 1</ep3.1>
			<id3.1>cd94dbad7612fb8ad2fad47544cfc3cb74a41b70</id3.1>
			<ep3.2>2 LEJOS DE CASA</ep3.2>
			<id3.2>af39ffbd55019da2b02ed3bdceb6faae54279080</id3.2>
			<ep3.3>3 HABITANTES DE LA TIERRA</ep3.3>
			<id3.3>21bc5627680a7602ac52aaca81bf9997f3a6794d</id3.3>
			<ep3.4>4 NO ME OLVIDES</ep3.4>
			<id3.4>e33736f342851f67dbd09dee3b0fe73beab172db</id3.4>
			<ep3.5>5 MORIR EN EL INTENTO</ep3.5>
			<id3.5>57a4eb4ec00646f4cae3d5f3a4e6a271f080a2b1</id3.5>
			<ep3.6>6 CHATARREROS</ep3.6>
			<id3.6>275cd602792e280a44277c72fe59ae075b22f83a</id3.6>
			<ep3.7>7 UNIFICACION III</ep3.7>
			<id3.7>43a194392bd4021f369c20c8ce97337607f17e21</id3.7>
			<ep3.8>8 EL SANTUARIO</ep3.8>
			<id3.8>7755ceaba4025884e0882c495adff269ada5068d</id3.8>
			<ep3.9>9 TERRA FIRMA, PARTE 1</ep3.9>
			<id3.9>c0d76a75de26347835772567b6638c7092e397f4</id3.9>
			<ep3.10>10 TERRA FIRMA, PARTE 2</ep3.10>
			<id3.10>1bec285dcb8d8e07499601de4cab426a8a1cbc38</id3.10>
			<ep3.11>11 SU'KAL</ep3.11>
			<id3.11>53120d0d454d6433694e1c81f6f13313eae36f5c</id3.11>
			<ep3.12>12 HAY UNA MAREA...</ep3.12>
			<id3.12>170476f8f5bb68fc0cf1567624c157f3195b2888</id3.12>
			<ep3.13>13 ESA ESPERANZA ES USTED, PARTE 2</ep3.13>
			<id3.13>b944415324e94646ded7a793028e3912bdb9c751</id3.13>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/zh7GLsorxecv0D8d7QAVkQUe1ju.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/kHwMIXsYNAg8eoJvj97F0LSlyDP.jpg</fanart.4>
			<ep4.1>1 KOBAYASHI MARU</ep4.1>
			<id4.1>6f632f4321fea393e675d9f4509b92bf46b4d13c</id4.1>
			<ep4.2>2 ANOMALIA</ep4.2>
			<id4.2>209cd5b671d8b0cf9d233628f118498f56c92ead</id4.2>
			<ep4.3>3 ELIGE VIVIR</ep4.3>
			<id4.3>07a0f7f5e431938b05a23483237c585b5389d9f7</id4.3>
			<ep4.4>4 TODO ES POSIBLE</ep4.4>
			<id4.4>c45f0b03adffb8de3a58eff4c2c31e572466a1fe</id4.4>
			<ep4.5>5 LOS EJEMPLOS</ep4.5>
			<id4.5>a8c07b8e4ef6060d1efd8adf65d8c5021b740a44</id4.5>
			<ep4.6>6 TIEMPO TORMENTOSO</ep4.6>
			<id4.6>2e78649eba45eba0ac1892c816ca7b6333a85fd4</id4.6>
			<ep4.7>7 …PERO PARA CONECTAR</ep4.7>
			<id4.7>8fa1149b696298d0a550232209f84e38d629c5c7</id4.7>
			<ep4.8>8 A POR TODAS</ep4.8>
			<id4.8>7c09e35458a1bc4bda1d9a41cb2bac53d2086e07</id4.8>
			<ep4.9>9 RUBICON</ep4.9>
			<id4.9>7c09e35458a1bc4bda1d9a41cb2bac53d2086e07</id4.9>
			<ep4.10>10 LA BARRA GALACTICA</ep4.10>
			<id4.10>4b8e46b88cfbef479d66cc3b829aebcef071b81e</id4.10>
			<ep4.11>11 ROSETTA</ep4.11>
			<id4.11>47d687386edc5d7ce839b88e6d961deab924819a</id4.11>
			<ep4.12>12 ESPECIE DIEZ-C</ep4.12>
			<id4.12>a03856c82449669957cbace4db7b100a49a37d6a</id4.12>
			<ep4.13>13 VOLVIENDO A CASA</ep4.13>
			<id4.13>fa78d8ef78b570cf1043b215b5bc087502a6e89f</id4.13>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>STAR TREK PICARD</title>
		<info>(2020) 2 temporadas. 20 episodios. Serie ambientada 18 años después de la última aparición de Jean-Luc Picard en "Star Trek: Nemesis", y encuentra al personaje profundamente afectado por la destrucción de Romulus como se muestra en la película "Star Trek"</info>
		<year>2020</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/nIlAKIrLKxOeoEnc0Urb65yNCp.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/saadmpo4DIap8XQxZpG8Gp3uViH.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/nIlAKIrLKxOeoEnc0Urb65yNCp.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/saadmpo4DIap8XQxZpG8Gp3uViH.jpg</fanart.1>
			<ep1.1>1 RECUERDO</ep1.1>
			<id1.1>895dfec281ceeaeff8217203a5677704e60210a4</id1.1>
			<ep1.2>2 MAPAS Y LEYENDAS</ep1.2>
			<id1.2>6bb8eebbcba1fee6dfa2f7372b0c9e7f9386faca</id1.2>
			<ep1.3>3 EL FIN ES EL COMIENZO</ep1.3>
			<id1.3>1ab51d1e50f58c97a5eed1afde07448d42ef1812</id1.3>
			<ep1.4>4 LA VERDAD ABSOLUTA</ep1.4>
			<id1.4>0f6352b8ed5c477f2480bfb1264351dba60a16c1</id1.4>
			<ep1.5>5 LA CHUSMA DE LA CIUDAD DEL POLVO ESTELAR</ep1.5>
			<id1.5>97b25d95c5566fd660ab4a4868e179172917f9cc</id1.5>
			<ep1.6>6 LA CAJA IMPOSIBLE</ep1.6>
			<id1.6>6c33cff56c4f64f71826472f1f44b8bfd309904f</id1.6>
			<ep1.7>7 NEPENTHE</ep1.7>
			<id1.7>afe67c414bd9664af2eac6eb810d350ecbcda34f</id1.7>
			<ep1.8>8 EN PEDAZOS</ep1.8>
			<id1.8>4b7a444f779b78fb51237265cd5b46a8f523ebf7</id1.8>
			<ep1.9>9 ET IN ARCADIA EGO 1 PARTE</ep1.9>
			<id1.9>014a6a1c71f29231c30e4a5e2ab20f90875f6b2f</id1.9>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/qGrHmHbMvodVTyxRDIxljyg4f4T.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/lodNd9ITHf32q8ybHArHqoWOcXf.jpg</fanart.2>
			<ep2.1>1 MIRA A LAS ESTRELLAS</ep2.1>
			<id2.1>A5A8D4BA7526E33CD3282455E36DC79F1F8DF6D8</id2.1>
			<ep2.2>2 PENITENCIA</ep2.2>
			<id2.2>0077AB6F5A07746D19C660E9BC70C3B320DDB6C2</id2.2>
			<ep2.3>3 ASIMILACION</ep2.3>
			<id2.3>CC61D038355AF5580D0D39E58414111D41D8DB43</id2.3>
			<ep2.4>4 EL OBSERVADOR</ep2.4>
			<id2.4>74B76B7563D72A83ADEDBCA992FB7C005A9ADECB</id2.4>
			<ep2.5>5 LLEVAME A LA LUNA</ep2.5>
			<id2.5>75C8D303221E32FCF817D1B8D5FE92376E97E0C5</id2.5>
			<ep2.6>6 DOS DE UNO</ep2.6>
			<id2.6>7C1EB180D4B2390ADE9CDE7A70650595CDF06A70</id2.6>
			<ep2.7>7 MONSTRUOS</ep2.7>
			<id2.7>FEAB859083DEDA56B59417AEAC6E05E65E2293CE</id2.7>
			<ep2.8>8 PIEDAD</ep2.8>
			<id2.8>9AE2EF8755BA80A04870B6499FE7B9794F8CDCA1</id2.8>
			<ep2.9>9 ESCONDITE</ep2.9>
			<id2.9>7CCA6B9DAE93AA52E897B573C7AC132E60D9151A</id2.9>
			<ep2.10>10 DESPEDIDA</ep2.10>
			<id2.10>F1F0C2F8CFA26B4509B90BE26EE9FFD2C4249A92</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>STAR WARS: LA REMESA MALA</title>
		<info>(2021) 16 episodios. Un "mala remesa" de clones experimentales de élite intentan abrirse camino a través de una galaxia en constante evolución justo cuando se empiezan a experimentar las primeras consecuencias de la Guerra de los Clones.</info>
		<year>2021</year>
		<genre>Animación. Aventuras. Ciencia ficción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fdgGfu6A3T8wMFrzzJgqI99J2Pu.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/2yZ97TAUKLN8iIaLa4m48BGqBSY.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fdgGfu6A3T8wMFrzzJgqI99J2Pu.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/2yZ97TAUKLN8iIaLa4m48BGqBSY.jpg</fanart.1>
			<ep1.1>1 POSGUERRA</ep1.1>
			<id1.1>70166da47b82971f3404d62153fb686ed0397dbc</id1.1>
			<ep1.2>2 CORRE, CUT</ep1.2>
			<id1.2>7701b6268bc30251e97385265a8e637a0304c4c5</id1.2>
			<ep1.3>3 RECAMBIOS</ep1.3>
			<id1.3>60fff7d541497dfe017d9037644a8dd785b05448</id1.3>
			<ep1.4>4 ACORRALADOS</ep1.4>
			<id1.4>3dd4c1ddac08da7140029a63f1656c9739ba2105</id1.4>
			<ep1.5>5 FURIA</ep1.5>
			<id1.5>e804d27d71f89a2bca2d5fd666efb6cfc00b3ba2</id1.5>
			<ep1.6>6 DESMANTELADO</ep1.6>
			<id1.6>a087f313edd59e4324463e1ccbbfa8be7fc73f6b</id1.6>
			<ep1.7>7 CICATRICES</ep1.7>
			<id1.7>226198570192b798707abdf69cb077f2e2daae0f</id1.7>
			<ep1.8>8 REUNION</ep1.8>
			<id1.8>0bbc6b13d51e5f6b2b0061e6c4b3bec5038dde73</id1.8>
			<ep1.9>9 RECOMPENSA PERDIDA</ep1.9>
			<id1.9>c1d14ffb6334149b5286d1e6cb1079762ce3e674</id1.9>
			<ep1.10>10 PUNTOS EN COMUN</ep1.10>
			<id1.10>3b99479d7ab115678c5f9eede8feccd0af61f616</id1.10>
			<ep1.11>11 PACTAR CON EL DIABLO</ep1.11>
			<id1.11>c0632c3d16965462369ae6bd3432e2f5145d1653</id1.11>
			<ep1.12>12 RESCATE EN RYLOTH</ep1.12>
			<id1.12>3a0de1ae4053c04740fc8cc8dd72b0c407dfb4e8</id1.12>
			<ep1.13>13 INFESTADO</ep1.13>
			<id1.13>8ba7b8420634034e96d6087f57c226e3d54e2845</id1.13>
			<ep1.14>14 MANTO DE GUERRA</ep1.14>
			<id1.14>7b083f1d83b60c3935ec7d686a27a693293c74c2</id1.14>
			<ep1.15>15 REGRESO A KAMINO</ep1.15>
			<id1.15>60d8ded9148977ac35b4f4169d853b08ef797423</id1.15>
			<ep1.16>16 REGRESO A KAMINO (PARTE 2 )</ep1.16>
			<id1.16>bfc2c90803b2f4f515827fb05e810fcb6dddbffb</id1.16>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>STARGIRL</title>
		<info>(2020) 2 Temporadas 26 episodios. Stargirl es una superheroína de DC Cómics que se oculta bajo la identidad de la estudiante de instituto Courtney Whitmore.</info>
		<year>2020</year>
		<genre>Ciencia ficción</genre>
		<thumb>https://image.tmdb.org/t/p/original/eD5QIAk9xfEcv7fCYEOhnbjItoJ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/b5lKppEquuQWm6fTjGcu2g8FJgs.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/JJxVsQPPiuLcArALotMHsCubeo.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/b5lKppEquuQWm6fTjGcu2g8FJgs.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>806b3c7128432de91c236b2f01f4386320977b4e</id1.1>
			<ep1.2>2 S.T.R.I.P.E.</ep1.2>
			<id1.2>5678aa478dd43ed49639a571366c42073f7133dd</id1.2>
			<ep1.3>3 ICICLE</ep1.3>
			<id1.3>2d352609110224be3592f7f744a36d4911eacc1e</id1.3>
			<ep1.4>4 WILDCAT</ep1.4>
			<id1.4>273634f6746a9468b98276000ca8879a9272050d</id1.4>
			<ep1.5>5 HOURMAN Y DR. MID-NITE</ep1.5>
			<id1.5>a51b28906f4db42d7e2851c3b5ff193e45b7d1ce</id1.5>
			<ep1.6>6 LA SOCIEDAD DE LA JUSTICIA</ep1.6>
			<id1.6>feb244ff3138f96dd58a02508ea531c09c2bfc44</id1.6>
			<ep1.7>7 SHIV PARTE 1</ep1.7>
			<id1.7>e480e2c238b1b687ad7008ec7dc133055ab4d71c</id1.7>
			<ep1.8>8 SHIV PARTE 2</ep1.8>
			<id1.8>33c94b7d57d1d0da1b161a2081a29b0843f77589</id1.8>
			<ep1.9>9 BRAINWAVE</ep1.9>
			<id1.9>48b079ecda089d4991da4e4841061db50a948556</id1.9>
			<ep1.10>10 BRAINWAVE JR</ep1.10>
			<id1.10>5dcc9d10d6616d40cb6736030c889e733502be19</id1.10>
			<ep1.11>11 EL CABALLERO BRILLANTE</ep1.11>
			<id1.11>baef47f22d79a0a79bac67861c923dcd02e83c63</id1.11>
			<ep1.12>12 BARRAS Y ESTRELLAS 1 PARTE</ep1.12>
			<id1.12>e463f3fb81a2e4e5e21d7497ebcb3961d82688c5</id1.12>
			<ep1.13>13 BARRAS Y ESTRELLAS 2 PARTE</ep1.13>
			<id1.13>6b8e8a2dfd3110fc51663ef000e8798d096e047f</id1.13>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/rbkGgrEHOPyAEZqPk609QN1Ird6.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/pXjpqrx65mlQskf9mfTWSszYODn.jpg</fanart.2>
			<ep2.1>ESCUELA DE VERANO : Episodio 1</ep2.1>
			<id2.1>1d07f9fd1c17ba0a1835de98fe98ac19dc693d6f</id2.1>
			<ep2.2>ESCUELA DE VERANO : Episodio 2</ep2.2>
			<id2.2>805dd4806ee6275f0a3b5ba16714da2a78ee964d</id2.2>
			<ep2.3>ESCUELA DE VERANO : Episodio 3</ep2.3>
			<id2.3>c2b65bc407e6fbe3c3a0ad683070fae61adfcffb</id2.3>
			<ep2.4>ESCUELA DE VERANO : Episodio 4</ep2.4>
			<id2.4>6f66e7926e16e5d82b38013d2cb62d4ab2f9d87e</id2.4>
			<ep2.5>ESCUELA DE VERANO : Episodio 5</ep2.5>
			<id2.5>82ead5e0f76f1636d1235659afaddcfb9a6e9738</id2.5>
			<ep2.6>ESCUELA DE VERANO : Episodio 6</ep2.6>
			<id2.6>3b43d7e14991aac2c6095650a4b54f50faae5180</id2.6>
			<ep2.7>ESCUELA DE VERANO : Episodio 7</ep2.7>
			<id2.7>ce50e0c88b1a19c7960fc75af296dad135f9d5ae</id2.7>
			<ep2.8>ESCUELA DE VERANO : Episodio 8</ep2.8>
			<id2.8>2c58db904e3af3cd9c83b60bdf6732295c9fc22d</id2.8>
			<ep2.9>ESCUELA DE VERANO : Episodio 9</ep2.9>
			<id2.9>375d313477f97ed2ff35892206861ed23c2cbb90</id2.9>
			<ep2.10>ESCUELA DE VERANO : Episodio 10</ep2.10>
			<id2.10>ff43c6a5c4ccd3a5a3209fa80a2d0649cc43d5c4</id2.10>
			<ep2.11>Episodio 11</ep2.11>
			<id2.11>ce17c32bae87a385d07fdbfb8adc2f6f8e6fe724</id2.11>
			<ep2.12>Episodio 12</ep2.12>
			<id2.12>3a270aa9f62370cdda381b385c9d543fb133887b</id2.12>
			<ep2.13>Episodio 13</ep2.13>
			<id2.13>9502f111835a9291aa01ebd24c7a0fbe1958bc8e</id2.13>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>STRANGER THINGS</title>
		<info>(2016) 4 temporadas. 34 episodios.  Homenaje a los clásicos misterios sobrenaturales de los años 80, "Stranger Things" es la historia de un niño que desaparece en el pequeño pueblo de Hawkins, Indiana, sin dejar rastro en 1983. En su búsqueda desesperada, tanto sus amigos y familiares como el sheriff local se ven envueltos en un enigma extraordinario: experimentos ultrasecretos, fuerzas paranormales terroríficas y una niña muy, muy rara..</info>
		<year>2016</year>
		<genre>Thriller. Terror. Fantástico. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/75zl1F4ACQXa5XYXzQaV9nWUeIh.jpg</thumb>
		<fanart>https://imgur.com/CAzwK8U.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/5tOTcMIpa9bYKQvzqu7anfSnM4f.jpg</thumb.1>
			<fanart.1>https://imgur.com/CAzwK8U.jpg</fanart.1>
			<ep1.1>1 LA DESAPARICION DE WILL BYERS</ep1.1>
			<id1.1>18f45444d8613019a3e1e383017e4c049d56aa36</id1.1>
			<ep1.2>2 LA LOCA DE LA CALLE MAPLE</ep1.2>
			<id1.2>67af34e41e7cd3dda09d88823de4c723414130ab</id1.2>
			<ep1.3>3 TODO ESTA BIEN</ep1.3>
			<id1.3>42f48c4b419c1741ca238fa8e6f6ca2de6d7e7c2</id1.3>
			<ep1.4>4 EL CUERPO</ep1.4>
			<id1.4>567c60935a5799dd1916a46bc31bec535b445a6a</id1.4>
			<ep1.5>5 LA PULGA Y EL ACROBATA</ep1.5>
			<id1.5>5880506971702f53f8518c613e0b0d135dd4ec49</id1.5>
			<ep1.6>6 EL MONSTRUO</ep1.6>
			<id1.6>886fb97cbfdcc39000666c6b5f52a3d806abac08</id1.6>
			<ep1.7>7 LA BAÑERA</ep1.7>
			<id1.7>d0cc1374a29ad057fc7f1a403385aa84341b25c9</id1.7>
			<ep1.8>8 EL OTRO LADO</ep1.8>
			<id1.8>367b8d2ada6aee6d85156c2fbf12264fa9b260e6</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/lXS60geme1LlEob5Wgvj3KilClA.jpg</thumb.2>
			<fanart.2>https://imgur.com/CAzwK8U.jpg</fanart.2>
			<ep2.1>1  AL 9 (1080)</ep2.1>
			<id2.1>343d937150bb78b040d24ea86d78bd0c09d68b63</id2.1>
			<ep2.2>1 MADMAX</ep2.2>
			<id2.2>acf6f9dc0942361b52aad1b6b3f7d706b9895079</id2.2>
			<ep2.3>2 TRUCO O TRATO</ep2.3>
			<id2.3>f93f5ce65d1f38432a03990c1ff1e094d48e0b95</id2.3>
			<ep2.4>3 EL RENACUAJO</ep2.4>
			<id2.4>a1f7a3e32df659bc93b54ff6681c4751acf8ea0a</id2.4>
			<ep2.5>4 WILL EL SABIO</ep2.5>
			<id2.5>66f7adbc4302133ba547f6b19cae297518be04d4</id2.5>
			<ep2.6>5 DIG DUG</ep2.6>
			<id2.6>4c4b9e27c50db8cd5457c30195b67143cf30ce7e</id2.6>
			<ep2.7>6 EL ESPIA</ep2.7>
			<id2.7>e5c622c09e0e3e152ae87e06a7b81460f25e2d71</id2.7>
			<ep2.8>7 LA HERMANA PERDIDA</ep2.8>
			<id2.8>28a7cc0825abc23f131ac2a3165fef18942d7342</id2.8>
			<ep2.9>8 EL AZOTAMENTES</ep2.9>
			<id2.9>993ffbf992d2556b8595408a30dd6d283dc6e37c</id2.9>
			<ep2.10>9 EL PORTAL</ep2.10>
			<id2.10>6d00b19b5d3f0e1f1fcdd558e96c08275c5d0390</id2.10>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/6lGapZA18mVfTE5M7QDKkVmVYEB.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/56v2KjBlU4XaOv9rVYEQypROD7P.jpg</fanart.3>
			<ep3.1>1 SUZIE ME RECIBES</ep3.1>
			<id3.1>303b7cc6644863d21a9f89360de83d85bddfbf27</id3.1>
			<ep3.2>2 RATAS DE CENTRO COMERCIAL</ep3.2>
			<id3.2>5907b2f93e3f907076d76666b2912df86af94ef5</id3.2>
			<ep3.3>3 EL CASO DE LA SOCORRISTA DESAPARECIDA</ep3.3>
			<id3.3>6c813cdacba2747f263d163cfa1b415bb9fa72b0</id3.3>
			<ep3.4>4 LA PRUEBA DE LA SAUNA</ep3.4>
			<id3.4>ba1fd8938ade538f120b7596882e10fd42c32c67</id3.4>
			<ep3.5>5 EL ORIGEN</ep3.5>
			<id3.5>5eb07aba680dc519125900641f76066ce1f85382</id3.5>
			<ep3.6>6 EL ANIVERSARIO</ep3.6>
			<id3.6>fa65c1c9ab459b09e01a2a322560b8024226e6ca</id3.6>
			<ep3.7>7 LA MORDEDURA</ep3.7>
			<id3.7>1098f283a19dbdb59a9e8188467dbf506ec30a39</id3.7>
			<ep3.8>8 LA BATALLA DE STARCOURT</ep3.8>
			<id3.8>431a3ef36fed7759f53133970a3c40b474b6fa69</id3.8>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/49WJfeN0moxb9IPfGn8AIqMGskD.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/rcA17r3hfHtRrk3Xs3hXrgGeSGT.jpg</fanart.4>
			<ep4.1>1 AL 7 (1080)</ep4.1>
			<id4.1>529F4CEFE94C1D454E5C547E85BE3E094B90ECAD</id4.1>
			<ep4.2>1 EL CLUB DEL FUEGO INFERNAL (1080)</ep4.2>
			<id4.2>444341e75473e7002f0e64703e96bad6693b929a</id4.2>
			<ep4.3>2 LA MALDICCION DE VEGNA(1080)</ep4.3>
			<id4.3>594d750e0b13dffacafc3432b29c534d258d7b5d</id4.3>
			<ep4.4>3 EL MONSTRUO Y LA SUPERHERONINA(1080)</ep4.4>
			<id4.4>54449a0f7d1335bf166abe889248d52b67c66bec</id4.4>
			<ep4.5>4 QUERIDO BILLY(1080)</ep4.5>
			<id4.5>86a2f31cb85123f0e613fc88e6eae57ae26f1847</id4.5>
			<ep4.6>5 EL PROYECTO NINA</ep4.6>
			<id4.6>4cf583b78e64fef01e5b5e0b4432e31468fac79f</id4.6>
			<ep4.7>6 LA INMERSION(1080)</ep4.7>
			<id4.7>6ded881ee2b519697760fe7957af57bf84424f87</id4.7>
			<ep4.8>7 LA MASACRE EN EL LABORATORIO DE HAWKINS</ep4.8>
			<id4.8>3a21e2bef7d5543c04e0adccda56067e80c73152</id4.8>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SUCCESSION</title>
		<info>(2018) 3 temporadas. 29 episodios.  Serie que sigue a la disfuncional familia de Logan Roy y sus cuatro hijos, que controlan una de las empresas de medios de comunicación y entretenimiento más importantes del mundo. Los problemas llegan cuando se plantea quién será el sucesor del patriarca.</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/fkAM5cLVK8EWXJVMBZBurg3Qiza.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/u12ZNqhfgxYkjP16KWostx75w9r.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/fkAM5cLVK8EWXJVMBZBurg3Qiza.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/u12ZNqhfgxYkjP16KWostx75w9r.jpg</fanart.1>
			<ep1.1>1 CELEBRACION</ep1.1>
			<id1.1>c785fd59b79839fcc462184f932379e7b5b945b0</id1.1>
			<ep1.2>2 PIFOSTIO EN LA CASA DE LAS PUTAS</ep1.2>
			<id1.2>9c7b6975389a5039e231602013b349bedc6535d2</id1.2>
			<ep1.3>3 SALVAVIDAS</ep1.3>
			<id1.3>e616c6873c0218cdaacf266526e5fd4208cd932a</id1.3>
			<ep1.4>4 LA TRAMPA PARA LOS RICOS Y DESFAVORECIDOS</ep1.4>
			<id1.4>a51990a44973887d79168661e680402cb23214f1</id1.4>
			<ep1.5>5 HE IDO AL MERCADO</ep1.5>
			<id1.5>610b8ba532e0edad4779933a8d2789213be176fc</id1.5>
			<ep1.6>6 ¿DE QUE LADO ESTAS?</ep1.6>
			<id1.6>5973fa2681a9ec4629c5364936d5d7abcb74129e</id1.6>
			<ep1.7>7 AUSTERLITZ</ep1.7>
			<id1.7>f5d67115baafe414c70dd33a2fb215e705ad99c5</id1.7>
			<ep1.8>8 PRAGA</ep1.8>
			<id1.8>88d02281ab1cd2ffa474960270407c84b95f0e80</id1.8>
			<ep1.9>9 PRENUPCIAL</ep1.9>
			<id1.9>48f23503e94411b0e33bfa15fcda8953ed633902</id1.9>
			<ep1.10>10 NADIE HA DESPARECIDO</ep1.10>
			<id1.10>de6668a9225e28be34d5a985010e4b7c3d67b669</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/hXuVhMxi3ZNzfBY8h7Hiiotmjiy.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/u12ZNqhfgxYkjP16KWostx75w9r.jpg</fanart.2>
			<ep2.1>1 EL PALACIO DE VERANO</ep2.1>
			<id2.1>496e85e1b751dd4a9ac7d79353080b7ebc3b3c2f</id2.1>
			<ep2.2>2 VAULTER</ep2.2>
			<id2.2>8e865ba08a8c94663a6d7b268381710f233f73a5</id2.2>
			<ep2.3>3 DE CAZA</ep2.3>
			<id2.3>3acc3eb73061c95dc2efd63356b3cc8854772cbf</id2.3>
			<ep2.4>4 LA HABITACION DEL PANICO</ep2.4>
			<id2.4>7758563d8e2ee751f0c268d516fd5e005e3e86fe</id2.4>
			<ep2.5>5 TERN HAVEN</ep2.5>
			<id2.5>5ffa75ee7ae2b441e58cc74bfa4551f4b16ff315</id2.5>
			<ep2.6>6 ARGESTES</ep2.6>
			<id2.6>2b418688361eda891de3cc9382e333cabef308ba</id2.6>
			<ep2.7>7 REGRESO</ep2.7>
			<id2.7>edad5a618da02d1bd118acb3c7e0a5f31481e731</id2.7>
			<ep2.8>8 DUNDEE</ep2.8>
			<id2.8>279ae7a78b2970b3d0e9bcceb6c192464f9f94a3</id2.8>
			<ep2.9>9 DC</ep2.9>
			<id2.9>b193b112e7795850af59ea361fd6569365a1081a</id2.9>
			<ep2.10>10 ESTO NO ES PARA LLORAR</ep2.10>
			<id2.10>5dacad4b868212c697acb590bdbc02561e09d5e2</id2.10>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/e2X32jUfJ2kb4QtNg3WCTnLyGxD.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/pvxlTSiM7VsbxdSYLrnKrN1OIUZ.jpg</fanart.3>
			<ep3.1>1 SECESION</ep3.1>
			<id3.1>4a3153972c3f6eaa1a1dccaa19d78a19462db72c</id3.1>
			<ep3.2>2 MISA EN TIEMPO DE GUERRA</ep3.2>
			<id3.2>ae493e46f22acc929be0c47c4d62f77303b84f70</id3.2>
			<ep3.3>3 LA DISRUPCION</ep3.3>
			<id3.3>e390c855a9d42a3ff9be45438d936fec9fa3f45b</id3.3>
			<ep3.4>4 LEON EN LA PRADERA</ep3.4>
			<id3.4>5ca865c3a1441e1cb34410c1cf7cafae6c6706d4</id3.4>
			<ep3.5>5 CONSERJES RETIRADOS DE IDAHO</ep3.5>
			<id3.5>4c426cd332e4c193d4ae43f4f7df7df6974fbb8a</id3.5>
			<ep3.6>6 LO QUE SE NECESITA</ep3.6>
			<id3.6>2647b23202e08d71fa5493bfa277b25bb07931b8</id3.6>
			<ep3.7>7 DEMASIADOS CUMPLEAÑOS</ep3.7>
			<id3.7>2ed7e13e4e88efb26cd8655b527916db1122b7d5</id3.7>
			<ep3.8>8 CHIANTISHIRE</ep3.8>
			<id3.8>1cd095509a29bbfb4f9132a4f5b2763951fcefb4</id3.8>
			<ep3.9>9 TODAS LAS CAMPANAS DICEN</ep3.9>
			<id3.9>14ce064a68c9bb0dbea350c18fd5fc2e5004936d</id3.9>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>SUPERMAN AND LOIS</title>
		<info>(2021) 2 temporadas. 23 episodios.  Sigue a la periodista y al superhéroe más famoso del mundo y los cómics, y cómo lidian él y Lois todo el estrés, las presiones y las complejidades que conlleva ser lo que son y además ser padres trabajadores en la sociedad actual.</info>
		<year>2021</year>
		<genre>Acción. Aventuras. Ciencia ficción</genre>
		<thumb>https://www.themoviedb.org/t/p/original/qhliL05pep73ZbHEljiA1PLjwD0.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/tsqgpCSb29MMmSgfKdwH7Iqu1cg.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/qhliL05pep73ZbHEljiA1PLjwD0.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/tsqgpCSb29MMmSgfKdwH7Iqu1cg.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>1d13b48d1456bad1ff2b80e2e9708f5ef30c797e</id1.1>
			<ep1.2>2 HERENCIA</ep1.2>
			<id1.2>e595839daa44d047a5bdb1a85566ec9c0829d96b</id1.2>
			<ep1.3>3 LAS VENTAJAS DE NO SER UN MARGINADO</ep1.3>
			<id1.3>bc67d9af4290ad0b097e29ae42022926bae9ff67</id1.3>
			<ep1.4>4 LOCO</ep1.4>
			<id1.4>a91efb3c03d308bcc2f77f46b0b9c4d6a4acca4c</id1.4>
			<ep1.5>5 LO MEJOR DE SMALLVILLE</ep1.5>
			<id1.5>d688e6bab343d9fb85982c0fcc6cc0c1a12e2c05</id1.5>
			<ep1.6>6 HUELE A ESPIRITU ADOLESCENTE</ep1.6>
			<id1.6>8713f8734f77df6ef7479c3e7e7e2d262f39f7a0</id1.6>
			<ep1.7>7 HOMBRE DE ACERO</ep1.7>
			<id1.7>f7d29c9465ad362e8e24c6196c1e30134e6515dc</id1.7>
			<ep1.8>8 VERDAD Y CONSECUENCIAS</ep1.8>
			<id1.8>8077a3a2347e6472c269cde3aa27945b912a9235</id1.8>
			<ep1.9>9 SUJETOS LEALES</ep1.9>
			<id1.9>f211fa42d390c2ab0f9c90c81e38cbdfd8d2c9c4</id1.9>
			<ep1.10>10 OH MADRE ¿DONDE ESTAS?</ep1.10>
			<id1.10>63ddb9993cb6536f9d3e66f456dd9887c30549bc</id1.10>
			<ep1.11>11 UNA BREVE REMINISCENCIA ENTRE EVENTOS CATACLISMICOS</ep1.11>
			<id1.11>ae8ba52a97a38750d50b53bdfb91bc82c76c34f5</id1.11>
			<ep1.12>12 POR EL VALLE DE LA MUERTE</ep1.12>
			<id1.12>47bf9e0ebce8d4ec728b75dd0a6a9ad6401ced5f</id1.12>
			<ep1.13>13 A PRUEBA DE ERRORES</ep1.13>
			<id1.13>b733cd7d3d673ddeeb6b4dbe9e89f20db415627a</id1.13>
			<ep1.14>14 EL ERRADICADOR</ep1.14>
			<id1.14>7051964f6f8a4b7937e937efb533e387b9ccc953</id1.14>
			<ep1.15>15 LOS ULTIMOS HIJOS DE KRYPTON</ep1.15>
			<id1.15>a2b7737d78cd1b2e7e41bcbbc9af1fa6d60fb54d</id1.15>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/utpXq61UIlfNUuviz2hRmCEYdYr.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/gmbsR4SvYhhj4SvLAlTKxIkFxp9.jpg</fanart.2>
			<ep2.1>1 LO QUE HAY DEBAJO</ep2.1>
			<id2.1>dba1416b60915703e6ab99e2307875c889142f6b</id2.1>
			<ep2.2>2 LAZOS QUE UNEN</ep2.2>
			<id2.2>7e94e75a12866b2426b4f78ba19948bc06501a5e</id2.2>
			<ep2.3>3 LA COSA EN LAS MINAS</ep2.3>
			<id2.3>f4ffd1f044518f39d7c7a6ae699f56ff5c992f99</id2.3>
			<ep2.4>4 EL METODO INVERSO</ep2.4>
			<id2.4>928f6d8b159a713ffbaf8f6387097ee8b0c6502f</id2.4>
			<ep2.5>5 NIÑA PRONTO TE HARAS MUJER</ep2.5>
			<id2.5>0e188e112ee4146a3b924c4e625d24c610007b57</id2.5>
			<ep2.6>6 DEMOSTRADO</ep2.6>
			<id2.6>53TVPENNWHUDOGWZAPLQZBTRMM7QUNP5</id2.6>
			<ep2.7>7 ANTI-HEROE</ep2.7>
			<id2.7>3a485286c2c1d1b6070712c5e62fd5a1cf3658f0</id2.7>
			<ep2.8>8 EN EL OLVIDO</ep2.8>
			<id2.8>301b0155bb5daa234921052361aa88280e26adec</id2.8>
			<ep2.9>9 30 DIAS Y 30 NOCHES</ep2.9>
			<id2.9>0973a52c26f3a554bffa16875b2848873b7eb2b2</id2.9>
			<ep2.10>10 BIZARROS EN UN MUNDO BIZARRO</ep2.10>
			<id2.10>47c205540a210793bd00bc2f0aba26f1bd324093</id2.10>
			<ep2.11>11 VERDAD Y CONSECUENCIAS</ep2.11>
			<id2.11>2d16c49eb0bc64777f112fb079e049295b003ed3</id2.11>
			<ep2.12>12 MENTIRAS QUE ATAN</ep2.12>
			<id2.12>896e11d95388f4996f50b226b0bba200cb7f66be</id2.12>
			<ep2.13>13 TODO ESTA PERDIDO</ep2.13>
			<id2.13>8943674a18a283ff2003f2ba0e05811db6ec7db9</id2.13>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TEHERAN</title>
		<info>(2020) 2 temporadas. 16 episodios. Tamar es un hacker-agente del Mossad que se infiltra en Teherán con una identidad falsa para ayudar a destruir el reactor nuclear de Irán. Pero cuando su misión falla y queda atrapada en una nueva vida, Tamar debe planificar una operación que pondrá en peligro a todos sus seres queridos.</info>
		<year>2020</year>
		<genre>Drama. Thriller </genre>
		<thumb>https://www.themoviedb.org/t/p/original/h3Ku2csmlRtoHy7UrDeYOdulFnD.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/vcaZRKoRJT67pNEQeiYD0irb28E.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/h3Ku2csmlRtoHy7UrDeYOdulFnD.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/vcaZRKoRJT67pNEQeiYD0irb28E.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>ce136c2dc8a5bc08d82e55be1de061571c96e002</id1.1>
			<ep1.2>4 SHAKIRA Y SICK-BOY</ep1.2>
			<id1.2>9e7bd99e2e994c29e05e28ea394fc71d86652819</id1.2>
			<ep1.3>5 EL OTRO IRAN</ep1.3>
			<id1.3>c31826d3ce06dfebee04a86711c7f9abb574d105</id1.3>
			<ep1.4>6 EL INGENIERO</ep1.4>
			<id1.4>c31826d3ce06dfebee04a86711c7f9abb574d105</id1.4>
			<ep1.5>7 EL PADRE DE TAMAR</ep1.5>
			<id1.5>39a805dbea7573c4e51c77541e7fdd0fdc5da4a9</id1.5>
			<ep1.6>8 5 HORAS PARA EL BOMBARDEO</ep1.6>
			<id1.6>06c750a9797fea8568730905452453bd7dd92853</id1.6>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/daeA95EXBB7AZP3JJWKpmMUAP1X.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/n1KezePqzeY5449V9HppWek2tpO.jpg</fanart.2>
			<ep2.1>1 13 000</ep2.1>
			<id2.1>ed95af66a98ca7b24b96cde3106255abacbaccc2</id2.1>
			<ep2.2>2 CAMBIO DE PLANES</ep2.2>
			<id2.2>873375ca8c5e397ccaea5719f1e374ecc6882d56</id2.2>
			<ep2.3>3 ESTRES PROSTRAUMATICO</ep2.3>
			<id2.3>cbd49fadc28df7edbe623fceb674f074b4df8446</id2.3>
			<ep2.4>4 LAS NIÑAS RICAS</ep2.4>
			<id2.4>d28c006e4c9f4ae60dc043de9183f11e5fae86c4</id2.4>
			<ep2.5>5 DOBLE FALTA</ep2.5>
			<id2.5>b2ece9711f7b55e0edf6b182a3bf36b5f9b103ea</id2.5>
			<ep2.6>6 LA ELECCION DE FARAZ</ep2.6>
			<id2.6>c18618e1d0c65f1fd16e1bdc0fe6e7453433ae6d</id2.6>
			<ep2.7>7 BETTY</ep2.7>
			<id2.7>54e5c3b7e697070f21094b5ee654a72eca0c3a9c</id2.7>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TERRITORIO LOVECRAFT</title>
		<info>(2020) 10 episodios. Atticus Black (Jonathan Majors) comienza un viaje por carretera en los años 50 junto a su amiga Letitia (Jurnee Smollett-Bell) y su tío George (Courtney B. Vance) en busca de su padre desaparecido (Michael Kenneth Williams). El viaje se convertirá en una lucha por la supervivencia, afrontando el racismo de la América blanca así como una serie de monstruos que podrían haber salido de un libro de Lovecraft... Basada en la novela de 2016 de Matt Ruff.</info>
		<year>2020</year>
		<genre>Thriller. Drama. Terror</genre>
		<thumb>https://image.tmdb.org/t/p/original/9Lz53Mt8TnB08d1IhuJuM7nYkXO.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/cpZeRsIRcOXXHAotqOuTgNdothK.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/9Lz53Mt8TnB08d1IhuJuM7nYkXO.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/cpZeRsIRcOXXHAotqOuTgNdothK.jpg</fanart.1>
			<ep1.1>1 CREPUSCULO</ep1.1>
			<id1.1>25854dc2ebdf8c189a3df4cb54a22fd8f92bb81b</id1.1>
			<ep1.2>2 EL BLANQUITO ESTA EN LA LUNA</ep1.2>
			<id1.2>c46e4dddab3e1332ff90b6d954763c639c9a67d2</id1.2>
			<ep1.3>3 ESPIRITU SANTO</ep1.3>
			<id1.3>ed5c166707df911e29c5f0bd2fa7422df9b1a382</id1.3>
			<ep1.4>4 UNA HISTORIA DE VIOLENCIA</ep1.4>
			<id1.4>23cc4fe4fb0d01a7296005647115554d0f5b2cea</id1.4>
			<ep1.5>5 CASO EXTRAÑO</ep1.5>
			<id1.5>85adaa6790ebf2119e5f04f3edc9c81fe1502cb2</id1.5>
			<ep1.6>6 NOS VEMOS EN DAEGU</ep1.6>
			<id1.6>119e9d938b65f2b1c32af145dc8d031cf72b1af8</id1.6>
			<ep1.7>7 SOY YO</ep1.7>
			<id1.7>4a24df920df3fc4d9c7436b274e4ad4bc97427a6</id1.7>
			<ep1.8>8 ADIOS A BOBO</ep1.8>
			<id1.8>eec53e6fe1d9922a70d79075020f2ba8a89d97ff</id1.8>
			<ep1.9>9 RETROCEDER A 1921</ep1.9>
			<id1.9>b5ce9dbe86df245c07b494a5b7c6d6d0b1decb67</id1.9>
			<ep1.10>10 CIRCULO COMPLETO</ep1.10>
			<id1.10>ac1309992811212751773864335196d1b498d835</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE ACT</title>
		<info>(2019) 8 episodios. Gypsy Blanchard (Joey King) es una joven que trata de escapar de la tóxica relación que tiene con su sobreprotectora madre, Dee Dee Blanchard (Patricia Arquette).</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/vb1sQLC2MqfCPOFqHd8SyVsyDVB.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1yhQGQH7pfKdIY2xJSYNdp8ReD6.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/vb1sQLC2MqfCPOFqHd8SyVsyDVB.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/1yhQGQH7pfKdIY2xJSYNdp8ReD6.jpg</fanart.1>
			<ep1.1>1 LA MAISON DU BON REVE</ep1.1>
			<id1.1>4e4bc5caa47132b1ad36309b36c88f2d1c0d3722</id1.1>
			<ep1.2>2 DIENTES</ep1.2>
			<id1.2>3a0297c606763e78dc0e4d86b684eca25aa0db99</id1.2>
			<ep1.3>3 DOS WOLVERINES</ep1.3>
			<id1.3>49a560165ad15595d45d4d6355e203e4e01cbbf2</id1.3>
			<ep1.4>4 LA TORRE</ep1.4>
			<id1.4>c9f84c3cb515f436d494f8522c45e544ceee4e48</id1.4>
			<ep1.5>5 PLAN B</ep1.5>
			<id1.5>bef6bf58e94cea4894af717051b589ae7c6d1e03</id1.5>
			<ep1.6>6 TODO UN MUNDO NUEVO</ep1.6>
			<id1.6>2810dfa2c6ee5954ca1c90812b7e518fb9350140</id1.6>
			<ep1.7>7 BONNIE AND CLYDE</ep1.7>
			<id1.7>00d5f4b1c1a4dfa9b8c5dc848fccda3749a2af61</id1.7>
			<ep1.8>8 LIBRE</ep1.8>
			<id1.8>85687bc27072503332735af4f60158626281b55c</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE BLACKLIST</title>
		<info>(2013) 9 temporadas. 175 episodios. El criminal más buscado del mundo, Thomas Raymond Reddington (James Spader), se entrega misteriosamente y se ofrece a delatar a todos los que alguna vez han colaborado con él. Su única condición: sólo colaborará con Elisabeth Keen (Megan Boone), una nueva agente del FBI, con quien parece tener alguna conexión que ella desconoce.</info>
		<year>2013</year>
		<genre>Intriga. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/i5VAFkJktNmMvBGB42sIRdJxSwc.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/ccrCgW1ukxgwBue9ptkpCEjXE6q.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>080cd7e8b04ac5ef36254e756e8fcdcf22638475</id1.1>
			<ep1.2>2 EL FREELANCER</ep1.2>
			<id1.2>b82d8cdec741e8da31b1209abcb73afafc875960</id1.2>
			<ep1.3>3 WUJING</ep1.3>
			<id1.3>666380434a37d45a0cdc3c2fc1aab6b28c2b419d</id1.3>
			<ep1.4>4 EL COCINERO</ep1.4>
			<id1.4>0e56e07d7e53c15a5caf6ce5d0bdd3369737547d</id1.4>
			<ep1.5>5 EL MENSAJERO</ep1.5>
			<id1.5>ee61990127ec83b0026be2c6c689cad01c8123e5</id1.5>
			<ep1.6>6 GINA ZENEKATOS</ep1.6>
			<id1.6>771da3bab0b5e9e902998283e51331be17704de7</id1.6>
			<ep1.7>7 FREDERICK BARNES</ep1.7>
			<id1.7>5360b17e768dba1181ea913c30e1de8880bb0977</id1.7>
			<ep1.8>8 GENERAL LUDD</ep1.8>
			<id1.8>27c668b5c2fb10921c984d357d0e45ceabaa5003</id1.8>
			<ep1.9>9 ANSLO GARRICK 1</ep1.9>
			<id1.9>9fddfb42bb7defd09a53a307fe2bec88dcaa8c8e</id1.9>
			<ep1.10>10 ANSLO GARRICK 2</ep1.10>
			<id1.10>ad76ea9143be6a1274909e47ad23ebbd382941f7</id1.10>
			<ep1.11>11 EL BUEN SAMARITANO</ep1.11>
			<id1.11>b0020624f937bf6af1a6f7c9576c593c50cd4589</id1.11>
			<ep1.12>12 EL ALQUIMISTA</ep1.12>
			<id1.12>fbbab657a2c50e84a7ce29306fc75bc0770750d1</id1.12>
			<ep1.13>13 LA AGENCIA CYPRUS</ep1.13>
			<id1.13>3ee90c96ddf017c534a99e857d36a7b74d568b86</id1.13>
			<ep1.14>14 MADELINE PRATT</ep1.14>
			<id1.14>71a59570a9db888a3db480de6cf79eb7ccb6613b</id1.14>
			<ep1.15>15 EL JUEZ</ep1.15>
			<id1.15>83bf7303c0f8c1278f8af220fbb555c29208bf00</id1.15>
			<ep1.16>16 MAKO TANIDA</ep1.16>
			<id1.16>0687ab931c139d529d459d497a87ed2916ee592f</id1.16>
			<ep1.17>17 IVAN</ep1.17>
			<id1.17>3908bd87734d060a2e34897d13d668ecea8a9cdb</id1.17>
			<ep1.18>18 MILTON BOBBIT</ep1.18>
			<id1.18>a5e2a0eaea7a6f86d3eda2f9176aa108b1f4f892</id1.18>
			<ep1.19>19 LOS HERMANOS PAVLOVITCH</ep1.19>
			<id1.19>cca3b4762bce2a287022f51e4d4a971049e31007</id1.19>
			<ep1.20>20 EL HACEDOR DE REYES</ep1.20>
			<id1.20>b74020114e66f401e6be66ed9350e7cfefa77e2e</id1.20>
			<ep1.21>21 BERLIN</ep1.21>
			<id1.21>1c1e2ac170ebbf66db37a4277412cde1a4937170</id1.21>
			<ep1.22>22 BERLIN: CONCLUSION</ep1.22>
			<id1.22>c2455ab4874684a59c11765ddd131631abc6a330</id1.22>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/r205xyjVlOyD4RXmjll5iOwKSOE.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.2>
			<ep2.1>1 LORD BALTIMORE</ep2.1>
			<id2.1>2d6e8b1a10dc437d040b6cdfcb21edc40e687b32</id2.1>
			<ep2.2>2 BANCO MONARCH DOUGLAS</ep2.2>
			<id2.2>a32816d722246789fef80c91e1f541d24593cdd4</id2.2>
			<ep2.3>3 DOCTOR JAMES COVINGTON</ep2.3>
			<id2.3>6f0bca08b29ef30e8fbfdae483053a9f382c4535</id2.3>
			<ep2.4>4 DOCTOR LINUS CREEL</ep2.4>
			<id2.4>2ba5da19ee7c8d4f7388cba6aede097b03ff6765</id2.4>
			<ep2.5>5 EL FRENTE</ep2.5>
			<id2.5>256e1b1edb847197d6b28fe3885da9ee29e37943</id2.5>
			<ep2.6>6 EL CARTEL DE MAMBASA</ep2.6>
			<id2.6>e15ec3402b293385af5f94c8e603577a4a9bb3e2</id2.6>
			<ep2.7>7 EL CIMITARRA</ep2.7>
			<id2.7>7475540be86bb238aa1c3ba96866947d64caae9c</id2.7>
			<ep2.8>8 EL DECEMBRISTA</ep2.8>
			<id2.8>fb5ff75f4854d9de086ddf516f7c4e97aa671c3c</id2.8>
			<ep2.9>9 LUTHER BRAXTON</ep2.9>
			<id2.9>3f69c0125b93171c82a51ec9e03e6e9f8189849f</id2.9>
			<ep2.10>10 LUTHER BRAXTON: CONCLUSION</ep2.10>
			<id2.10>3aefe902f4989e753daf4386ccd81b7d48837ad0</id2.10>
			<ep2.11>11 RUSLAN DENISOV</ep2.11>
			<id2.11>c8d9ab97ab980db149661f62d2749f049a456e8c</id2.11>
			<ep2.12>12 LA FAMILIA KENYON</ep2.12>
			<id2.12>77db384b61e5428de2cddadd659b0a9745812218</id2.12>
			<ep2.13>13 EL CAZADOR DE CIERVOS</ep2.13>
			<id2.13>eea0376cf94612cebcdb984f834e8765a0b5f279</id2.13>
			<ep2.14>14 T. EARL KING VI</ep2.14>
			<id2.14>075b931278ed9ef4e8fef50803c1eafc4a001351</id2.14>
			<ep2.15>15 EL MAYOR</ep2.15>
			<id2.15>502e29d68ddf8f5ed1aa403ee43f5bc99bfbf9f8</id2.15>
			<ep2.16>16 TOM KEEN</ep2.16>
			<id2.16>8683f1be725a356f995e484d7d406fb2d384e5bb</id2.16>
			<ep2.17>17 INICIATIVA LONGEVIDAD</ep2.17>
			<id2.17>1bfac5cf45d3294c776df5037c60aadca2dbd88a</id2.17>
			<ep2.18>18 VANESSA CRUZ</ep2.18>
			<id2.18>9f5d3355d8a0cffe4bf93111536518fdcf3a1d2b</id2.18>
			<ep2.19>19 LEONARD CAUL</ep2.19>
			<id2.19>01d0eefa157181cd19be2fb7dfa747b04bc3e6cc</id2.19>
			<ep2.20>20 QUON ZHANG</ep2.20>
			<id2.20>b931e0a43647062d5d27bda71fa13a535c188878</id2.20>
			<ep2.21>21 KARAKURT</ep2.21>
			<id2.21>459aa4f51f2d67a08ec82ac366ae3eab77beae65</id2.21>
			<ep2.22>22 TOM CONNOLLY</ep2.22>
			<id2.22>0ad1f7afaea15ccd8533f8d75cf212fff3683c49</id2.22>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/hWN5imHrpg8wjEBsa7v80sGsS8r.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.3>
			<ep3.1>1 EL GRANJERO TROLL</ep3.1>
			<id3.1>41fa8e1376f7831c8c81a64baef5a71bd6b9305b</id3.1>
			<ep3.2>2 MARVIN GERARD</ep3.2>
			<id3.2>fd2363b17e1d4e91166a3f0c3305b150c9b0cf9b</id3.2>
			<ep3.3>3 ELI MATCHETT</ep3.3>
			<id3.3>441006ce4b9e4e0d04ebf1ddaa26cb83c9acd223</id3.3>
			<ep3.4>4 EL YINN</ep3.4>
			<id3.4>4652cd4db2b9a632aac53aca85535ad3f3697bf6</id3.4>
			<ep3.5>5 ARIOCH CAIN</ep3.5>
			<id3.5>5bc572fe7b1190554ae793c8eb6b65a2ee51b7bb</id3.5>
			<ep3.6>6 SIR CRISPIN CRANDALL</ep3.6>
			<id3.6>d23f2976655d85dcddc273c63795d46aae83ebfd</id3.6>
			<ep3.7>7 ZAL BIN HASAAN</ep3.7>
			<id3.7>49b34553deb713e45f92b9456e2096487bf495ec</id3.7>
			<ep3.8>8 LOS REYES DE LA CARRETERA</ep3.8>
			<id3.8>6a45861fa5322f5bc300583abe8628db144e8155</id3.8>
			<ep3.9>9 EL DIRECTOR (PARTE 1)</ep3.9>
			<id3.9>76a81c1eeb3e162046b99d36bd4ea7748d7c5c41</id3.9>
			<ep3.10>10 EL DIRECTOR (PARTE 2)</ep3.10>
			<id3.10>dd0c9119731324d8bd5ea12a3668d5ff9e9aeede</id3.10>
			<ep3.11>11 SR. GREGORY DEVRY</ep3.11>
			<id3.11>f93f6aee44bbce55469bcd7f2deb043f36844fa1</id3.11>
			<ep3.12>12 EL VEHM</ep3.12>
			<id3.12>4fcf2697cdb807dfe178b6210fde2c3c96829e14</id3.12>
			<ep3.13>13 ALISTAIR PITT</ep3.13>
			<id3.13>4ca2260dae4484d58a578d73d44db0f8f8fbe861</id3.13>
			<ep3.14>14 LADY AMBROSIA</ep3.14>
			<id3.14>78d3ea5dde23460c76c95700068cc99b0d8049da</id3.14>
			<ep3.15>15 DREXEL</ep3.15>
			<id3.15>2aaf6cd546d8af4a5c9598754aa0f7db6a9f85bc</id3.15>
			<ep3.16>16 EL CUIDADOR</ep3.16>
			<id3.16>1927014b66370c1f0c7136a5446ad44ca49ca775</id3.16>
			<ep3.17>17 EL SEÑOR SOLOMON</ep3.17>
			<id3.17>a5d190b852696d9d1718ae6521a77d1213fdf66b</id3.17>
			<ep3.18>18 EL SEÑOR SOLOMON: CONCLUSION</ep3.18>
			<id3.18>2922b70778703819689d09da0e613bca7fe1e5dc</id3.18>
			<ep3.19>19 CABO DE MAYO</ep3.19>
			<id3.19>170644704e5f3b6b6f4c43e6dd4e9880769e5354</id3.19>
			<ep3.20>20 LA RED DE ARTAX</ep3.20>
			<id3.20>fd2959c5a377ebf5e31c176348b3375030c9a3b9</id3.20>
			<ep3.21>21 SUSAN HARGRAVE</ep3.21>
			<id3.21>202f9e76fbd5674d2b070fbb265cfd7c3875e5f8</id3.21>
			<ep3.22>22 ALEXANDER KIRK</ep3.22>
			<id3.22>193609c0abf48995e6c06cb86d094722b90cd0a2</id3.22>
			<ep3.23>23 ALEXANDER KIRK: CONCLUSION</ep3.23>
			<id3.23>829448cda6d58e3a46b49c9709b457c26585eb07</id3.23>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/d5AJOxPzGkAHiaCHcESVCim1vHu.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.4>
			<ep4.1>1 ESTEBAN</ep4.1>
			<id4.1>7011c73baacbf2c5d48d458e074660e8b474e9e9</id4.1>
			<ep4.2>2 MATO</ep4.2>
			<id4.2>b163715c3498e6922e6d9d9a4e5ffd4b4b422809</id4.2>
			<ep4.3>3 MILES MCGRATH</ep4.3>
			<id4.3>7ec1b781caf4368e86b68dfc6e46b50e3866a861</id4.3>
			<ep4.4>4 GAIA</ep4.4>
			<id4.4>4807e0c1f6fdac0019b1b8070d4a6251b3608075</id4.4>
			<ep4.5>5 LA COMPAÑÍA LINDQUIST</ep4.5>
			<id4.5>93537574c0e1d0d230c8b1c4af431d5a9618ae6d</id4.5>
			<ep4.6>6 LOS TORDOS</ep4.6>
			<id4.6>370b8cde2fea20a115fcfef8e21b7687d6e8d693</id4.6>
			<ep4.7>7 DR. ADRIAN SHAW (1)</ep4.7>
			<id4.7>0ac9a227d72cf1110f3954e5204b69cf10893c26</id4.7>
			<ep4.8>8 DR. ADRIAN SHAW: CONCLUSION (2)</ep4.8>
			<id4.8>0e6968027a030df47b6f4b5a9d0d05531b01d72d</id4.8>
			<ep4.9>9 PESCADOS Y MARISCOS LIPET</ep4.9>
			<id4.9>8f1e6ab05702ad17a3ff6c519989fe540399d1f5</id4.9>
			<ep4.10>10 EL ANALISTA</ep4.10>
			<id4.10>22d691ef78bff12e5220f9885901034b172af3f2</id4.10>
			<ep4.11>11 EL HAREN</ep4.11>
			<id4.11>68cbaaabbf4c196507fd241027d63639498c9d77</id4.11>
			<ep4.12>12 NATALIE LUCA</ep4.12>
			<id4.12>2379b724a45252223a302063ff8e4fb8357db0ed</id4.12>
			<ep4.13>13 ISABELLA STONE</ep4.13>
			<id4.13>eb65db59731f74db6bc07f3de76cf85d8c7ae2cc</id4.13>
			<ep4.14>14 EL ARQUITECTO</ep4.14>
			<id4.14>8aa3bb5d4d9016368d1e219eec59b8b83d151bf9</id4.14>
			<ep4.15>15 EL FARMACEUTICO</ep4.15>
			<id4.15>cf6b1a5168b39a587d21a2efc7791d073584bb52</id4.15>
			<ep4.16>16 DEMBE ZUMA</ep4.16>
			<id4.16>86393f20aebce7554ea93aa6ba284f4f6ad0f39e</id4.16>
			<ep4.17>17 REQUIEM</ep4.17>
			<id4.17>e7d7c461fa3cb73f03a3e7ec11ac5c864b50fa50</id4.17>
			<ep4.18>18 PHILOMENA</ep4.18>
			<id4.18>2f99858372f2c2605e90b35f0dc1a55ab2d762a3</id4.18>
			<ep4.19>19 DR. BOGDAN KRILOV</ep4.19>
			<id4.19>0b9e0c2ea54e36564975cb375830861d0bdad5f7</id4.19>
			<ep4.20>20 EL COBRADOR DE DEUDAS</ep4.20>
			<id4.20>a0f0490e2fd8a9720e6a511b6f2b44f0406d0c3f</id4.20>
			<ep4.21>21 SR. KAPLAN</ep4.21>
			<id4.21>84cbce874b1bab5860ceafd353f18ea36e6e2e6e</id4.21>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/544Xc9iV8B4hHvI3PT9C9CIle4o.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.5>
			<ep5.1>1 SMOKEY PUTNUM</ep5.1>
			<id5.1>30aec93380fafb8aa4b8df21272fb1a7d3f7dc2b</id5.1>
			<ep5.2>2 GREYSON BLAISE</ep5.2>
			<id5.2>c2213197edcc65e8c8cb1a92513a38e2205d7f61</id5.2>
			<ep5.3>3 MISS REBECCA THRALL</ep5.3>
			<id5.3>5e6385c10323edce5caf22b347ad3ade871eb7d3</id5.3>
			<ep5.4>4 EL FINALILLO</ep5.4>
			<id5.4>dd191d1cdfc5d6213a1ea064b3acefafad5bae9b</id5.4>
			<ep5.5>5 ILYAS SURKOV</ep5.5>
			<id5.5>e03ab66a3588f1789ccd061ff364af9db651cf0f</id5.5>
			<ep5.6>6 LA AGENCIA DE VIAJES</ep5.6>
			<id5.6>5c2d71edc0db90c33d959a19c9854b6ef76ca628</id5.6>
			<ep5.7>7 LA CORPORACIÓN KILGANNON</ep5.7>
			<id5.7>c314e7954690821c79d87c29cd4e7a247e4ebfcc</id5.7>
			<ep5.8>8 IAN GARVEY</ep5.8>
			<id5.8>7e32a68821913d5ab4056b387197939e416b88b8</id5.8>
			<ep5.9>9 RUINA</ep5.9>
			<id5.9>d07d20d18f4699edcc08c82a8bbd485111142225</id5.9>
			<ep5.10>10 EL CONFIDENTE</ep5.10>
			<id5.10>9ab3d95d7eac8b785cfad8dc903c5e6438f92aed</id5.10>
			<ep5.11>11 ABRAHAM STERN</ep5.11>
			<id5.11>73ff8c44402a73bacabbf3ea9054c3e4e1325b90</id5.11>
			<ep5.12>12 EL CHEF</ep5.12>
			<id5.12>e35024a7b8151f7bfe6240a5a96fbbffb693a69e</id5.12>
			<ep5.13>13 LA MANO INVISIBLE</ep5.13>
			<id5.13>99ba1104533a34119b0a611a4623ff692dd03258</id5.13>
			<ep5.14>14 MR. RALEIGH SINCLAIR III</ep5.14>
			<id5.14>afeafd470b24c62df528958682fdf7aa279548d8</id5.14>
			<ep5.15>15 PATTIE SUE EDWARDS</ep5.15>
			<id5.15>7aca55c77f816360d79ab54ea0f400b463504f62</id5.15>
			<ep5.16>16 EL ASESINO DE CAPRICORNIO</ep5.16>
			<id5.16>11cfbfbd054e7b580bade6642c15923ea9f30053</id5.16>
			<ep5.17>17 ANNA-GARCIA DUERTE</ep5.17>
			<id5.17>da380191bcf743537d796e264be093eb028bcd8f</id5.17>
			<ep5.18>18 ZARAK MOSADEK</ep5.18>
			<id5.18>34b8b5c8fa9ce5b492a747a0671ec87123128a34</id5.18>
			<ep5.19>19 IAN GARVEY: CONCLUSION</ep5.19>
			<id5.19>39e1e6b94d9d9bb565d0cb8883297ac861c4d993</id5.19>
			<ep5.20>20 NICHOLAS T. MOORE</ep5.20>
			<id5.20>180d4207b979be3edbf24d4d83b7fa00e696cd8e</id5.20>
			<ip5.21>21 LAWRENCE DEAN DEVLIN</ip5.21>
			<id5.21>3e62d25ba328ca7ad89eb3dbac34386c6ca074f7</id5.21>
			<ip5.22>22 SUTTON ROSS</ip5.22>
			<id5.22>44caad651d6af81dc43d9533187cc9a3e1aeba18</id5.22>
		</t5>
		<t6>
			<thumb.6>https://image.tmdb.org/t/p/original/cJZbQFPm2GuD2K4FvksOncRGNzm.jpg</thumb.6>
			<fanart.6>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.6>
			<ep6.1>1 DR. HANS KOEHLER</ep6.1>
			<id6.1>9376f4fd9d9bd7551ed9065db16c8a034e22ade5</id6.1>
			<ep6.2>2 EL CORSO</ep6.2>
			<id6.2>caa20bf93c1e1bf37cc555bec81be24f59e9509e</id6.2>
			<ep6.3>3 EL FARMACEUTICO</ep6.3>
			<id6.3>240e1216c49ec1c00277056ef266b2abf3ba0259</id6.3>
			<ep6.4>4 LOS PRESTAMISTAS</ep6.4>
			<id6.4>4cb95b3e79ecec298a021664b1ab160f2b4a3793</id6.4>
			<ep6.5>5 ALTER EGO</ep6.5>
			<id6.5>328ce3dfcff263acf41ad1e4886000603478a195</id6.5>
			<ep6.6>6 EL MORALISTA</ep6.6>
			<id6.6>b1ddc7b7af8680282b83e67bb9582d528e9857f0</id6.6>
			<ep6.7>7 GENERAL SHIRO</ep6.7>
			<id6.7>e6317a02d90e751f4e374e58407cfb3da812656e</id6.7>
			<ep6.8>8 MARKO JANKOWICS</ep6.8>
			<id6.8>9e15ec3882cc499db99f906d17cf11d0ce3a1205</id6.8>
			<ep6.9>9 MINISTRO D</ep6.9>
			<id6.9>333d080559c84081543e2c7faa8c27bb6a86a9e1</id6.9>
			<ep6.10>10 EL CRIPTOBANQUERO</ep6.10>
			<id6.10>f3fc80bb7bdf1218cd5c363bdbd14eeb866957f0</id6.10>
			<ep6.11>11 BASTIEN MOREAU</ep6.11>
			<id6.11>44fc6b3e277d0540685f12b82641e91de7cab369</id6.11>
			<ep6.12>12 BASTIEN MOREAU: CONCLUSION (2)</ep6.12>
			<id6.12>72d30adb39c51c832cd9389462630b94d5c4e02e</id6.12>
			<ep6.13>13 ROBERT VESCO</ep6.13>
			<id6.13>4015fc6f22ffc9c10a35d6a89bdd4cfd774bbc26</id6.13>
			<ep6.14>14 LA COMPAÑIA DE PARAGUAS OSTERMAN</ep6.14>
			<id6.14>209fa4e15f63ecba9805113341ffd1b2d514daf5</id6.14>
			<ep6.15>15 OLIVIA OLSON</ep6.15>
			<id6.15>c35b1e440a390b81a8a24fb6d3ca93125b1a7086</id6.15>
			<ep6.16>16 LA SEÑORA FORTUNA</ep6.16>
			<id6.16>2f91c84d061c44af6dd61c47e9979807976d766e</id6.16>
			<ep6.17>17 EL TERCER ESTADO</ep6.17>
			<id6.17>83d274345d501912c00e70ad9588b2bdf6e589db</id6.17>
			<ep6.18>18 EL ASESINO DE LA UNIVERSIDAD BROCKTON</ep6.18>
			<id6.18>caf5d526d1c820516fa530e20c787d18223794c3</id6.18>
			<ep6.19>19 RASSVET</ep6.19>
			<id6.19>e46c2a951539425f9217daeb62f933df97e800a4</id6.19>
			<ep6.20>20 GUILLERMO RIZAL</ep6.20>
			<id6.20>bfd68e6a5618b57c231db327d90fd1c51e814356</id6.20>
			<ep6.21>21 ANNA MCMAHON</ep6.21>
			<id6.21>80eb6593ca644625655649822c64beda73aefff8</id6.21>
			<ep6.22>22 ROBERT DIAZ</ep6.22>
			<id6.22>58dcf3798750180e57f954db542df0b59e4d5656</id6.22>
		</t6>
		<t7>
			<thumb.7>https://image.tmdb.org/t/p/original/zBnDzqCYOcvl8OmC53Hzd7W5hiZ.jpg</thumb.7>
			<fanart.7>https://image.tmdb.org/t/p/original/8YWQ1JrA4fihMwf9kYvGtd6mSe0.jpg</fanart.7>
			<ep7.1>1 LOUIS T. STEINHIL</ep7.1>
			<id7.1>b79cc66b0b8e0810aab407bf2e01f58fbd404e67</id7.1>
			<ep7.2>2 LOUIS T. STEINHIL: CONCLUSION</ep7.2>
			<id7.2>5a743fe3e25b735b7317fde586c41d7c4f3eaea0</id7.2>
			<ep7.3>3 LAS FLORES DEL MAL</ep7.3>
			<id7.3>5c933e9a90f096f7162d2d7d44565c952f123577</id7.3>
			<ep7.4>4 KUWAIT</ep7.4>
			<id7.4>89ab6d054f4d29b3a8767cf7f49b8daf0b52c758</id7.4>
			<ep7.5>5 NORMAN DEVANE</ep7.5>
			<id7.5>684e322e42249597e90afae05b938b63aff56b66</id7.5>
			<ep7.6>6 DR. LEWIS POWELL</ep7.6>
			<id7.6>0ffaf181ed87b60ada6989a98ca1600f62ae9256</id7.6>
			<ep7.7>7 HANNAH HAYES</ep7.7>
			<id7.7>735b7193f19db766836b5b834dcd30a5f2d7d227</id7.7>
			<ep7.8>8 EL HAWALADAR</ep7.8>
			<id7.8>f3e7f7b7184227c73a6f6a37586c222992656740</id7.8>
			<ep7.9>9 SERVICIO DE TRASLADOS ORION</ep7.9>
			<id7.9>a03b8b333135384f95a11fcbbb4d71963036d32d</id7.9>
			<ep7.10>10 KATARINA ROSTOVA</ep7.10>
			<id7.10>ac44ca95d87a99ac585478d6ea355e0fa6667c04</id7.10>
			<ep7.11>11 VICTORIA FENBERG</ep7.11>
			<id7.11>37a34419cf34958ec6a25aa3268b9636767d71c9</id7.11>
			<ep7.12>12 CORNELIUS RUCK</ep7.12>
			<id7.12>4c0fcc44169c10dee696f9ed4d8a07069f316cb7</id7.12>
			<ep7.13>13 NEWTON PURCELL</ep7.13>
			<id7.13>7ebc9d5bf26bcfb8ded34fff2261e48a6dba8d17</id7.13>
			<ep7.14>14 TWAMI ULLULAQ</ep7.14>
			<id7.14>55c66f7c693967b3c83d8be6d65077fe90f1d47a</id7.14>
			<ep7.15>15 GORDON KEMP</ep7.15>
			<id7.15>d97890138221922b3759a19cc1dc449ff799aec</id7.15>
			<ep7.16>16 NYLE HATCHER</ep7.16>
			<id7.16>bdc3e549da700b55ba4d0a68046a0d515a16cb59</id7.16>
			<ep7.17>17 HERMANOS</ep7.17>
			<id7.17>debadc2a4b3934b364d2b8a1a26cacc03adcf0d5</id7.17>
			<ep7.18>18 ROY CAIN</ep7.18>
			<id6.18>17d472d740378fd81c2df0f3ea3157fe78b29077</id6.18>
			<ep7.19>19 LOS HERMANOS KAZANJIAN</ep7.19>
			<id7.19>a51564edb03975b3ebb417a54e334b24683552f0</id7.19>
		</t7>
		<t8>
			<thumb.8>https://image.tmdb.org/t/p/original/htJzeRcYI2ewMm4PTrg98UMXShe.jpg</thumb.8>
			<fanart.8>https://image.tmdb.org/t/p/original/zXpSJLcczUt6EfbdULZanguzy87.jpg</fanart.8>
			<ep8.1>1 ROANOKE</ep8.1>
			<id8.1>5ae5b20564e7bf4cdf9e79855f62d5fe7d00a328</id8.1>
			<ep8.2>2 KATARINA ROSTOVA: CONCLUSION</ep8.2>
			<id8.2>3330c79d6732ee7b9be9f2fe61e2fa996648955b</id8.2>
			<ep8.3>3 16 ONZAS</ep8.3>
			<id8.3>f09799243ca6481fb0723e323a8f85f2ec91a181</id8.3>
			<ep8.4>4 ELIZABETH KEEN</ep8.4>
			<id8.4>aaa60cf0f7a28c2bf0651fa255784262c8406a74</id8.4>
			<ep8.5>5 LA CONFIANZA DE FRIBURGO</ep8.5>
			<id8.5>59ba983532f0d165c77087b7305bc4a0c5bc2519</id8.5>
			<ep8.6>6 LA AGENCIA WELLSTONE</ep8.6>
			<id8.6>408fa31dca18ecfb62fa31a0cd22c46847a82182</id8.6>
			<ep8.7>7 MARY, LA QUIMICA</ep8.7>
			<id8.7>b509ce1c8a9dedd323d3803c3111b1f32378800a</id8.7>
			<ep8.8>8 OGDEN GREELEY</ep8.8>
			<id8.8>2b42cae4b79d46803d0bc76d496cb0d236178d48</id8.8>
			<ep8.9>9 LA CIRANOIDE</ep8.9>
			<id8.9>e3a11c0f535b83849eb896961fcbf5f89dd8cf3f</id8.9>
			<ep8.10>10 DRA. LAKEN PERILLOS</ep8.10>
			<id8.10>a56fa1eb7adfd6212864c1696267ae363ed9c19e</id8.10>
			<ep8.11>11 CAPTAIN KIDD</ep8.11>
			<id8.11>5011b3342e5a1c1ccc226b5201a02708efeee825</id8.11>
			<ep8.12>12 RAKITIN</ep8.12>
			<id8.12>291fd55237a6ee8055e516edcc46255ef36cd936</id8.12>
			<ep8.13>13 ANNE</ep8.13>
			<id8.13>6eccdbfbad011f870c1ba3150b134ec97d64cf62</id8.13>
			<ep8.14>14 MISERE</ep8.14>
			<id8.14>43828c2c200aff09dc86a1ea2c8cacf620b8fbcd</id8.14>
			<ep8.15>15 EL NUDO RUSO</ep8.15>
			<id8.15>08d66046dfdb61ffdcdec61f0314e032c8fc75d9</id8.15>
			<ep8.16>16 NICHOLAS OBENRADER</ep8.16>
			<id8.16>07f7316f7065c0c6cb49a449742a38dec1a424ae</id8.16>
			<ep8.17>17 IVAN STEPANOV</ep8.17>
			<id8.17>a85c0ca7d604410b017b9c12a22bcce5fc82d7b6</id8.17>
			<ep8.18>18 EL PROTEICO</ep8.18>
			<id8.18>83e2c68344d02bf257ef6c1ce8a13959e91e963b</id8.18>
			<ep8.19>19 BALTHAZAR "BINO" BAKER</ep8.19>
			<id8.19>80ff666a62f0bd1cf063eed0b5421db53e50a670</id8.19>
			<ep8.20>20 PAGINA DE GODWIN</ep8.20>
			<id8.20>ddde3113c2929c2f74c2faaa9953c1e0662ac1d6</id8.20>
			<ep8.21>21 NACHALO</ep8.21>
			<id8.21>0359728309aed9f605aba04680f5336b01cd0042</id8.21>
			<ep8.22>22 KONETS</ep8.22>
			<id8.22>04e3e66c1282adfa6179489d7e3c36c02a5614e9</id8.22>
		</t8>
		<t9>
			<thumb.9>https://www.themoviedb.org/t/p/original/5P7zVf0TEyRShU8eYipYoicQ44n.jpg</thumb.9>
			<fanart.9>https://i.imgur.com/5WaYUir.jpg</fanart.9>
			<ep9.1>1 AL 6 (1080)</ep9.1>
			<id9.1>D1C413FC3CCBE7618E78704E0566495B01AF7045</id9.1>
			<ep9.2>1 EL DESOLLADOR</ep9.2>
			<id9.2>d89505d1ca325e1f6c174044724a06b52441e3e1</id9.2>
			<ep9.3>2 EL DESOLLADOR:CONCLUSION</ep9.3>
			<id9.3>5667537dce8563261e780e7143f1a43dc94564e7</id9.3>
			<ep9.4>3 LA OSP</ep9.4>
			<id9.4>072409ffe7dc14bd474e3f8ff6760c6790c82ccd</id9.4>
			<ep9.5>4 EL ANGEL VENGADOR</ep9.5>
			<id9.5>5a2c03c67cffc050e4ecdcb3a6d5588e35ed19ac</id9.5>
			<ep9.6>5 BENJAMIN T. OKARA</ep9.6>
			<id9.6>bf4f3c36bdbbdc7c3bbad1fa5759f4aebcd25f1e</id9.6>
			<ep9.7>6 DRA ROBERTA SAND</ep9.7>
			<id9.7>cee8e2badece5663131216e7ee40b78014dece8a</id9.7>
			<ep9.8>7 AL 8</ep9.8>
			<id9.8>DA5D4BD8B67A390C4B3F0D4B0EEADA69A8F3D76C</id9.8>
			<ep9.9>9 AL 10</ep9.9>
			<id9.9>9CF4545F2ABD306575796A39A7B39A006CACFEC4</id9.9>
			<ep9.10>11 EL CONGLOMERADO</ep9.10>
			<id9.10>876dd76ab0c019fc03c46d03a7e57f16cbfcbc76</id9.10>
			<ep9.11>12 EL PRESIDENTE</ep9.11>
			<id9.11>39645C4F15BAC7FD438E10DDC9DF606D3EF22CF5</id9.11>
			<ep9.12>13 MODELOS GENUINAS S.A.</ep9.12>
			<id9.12>77430300EE7277C2F39CC2654F5A4443A92CDF7B</id9.12>
			<ep9.13>14 EVA MASON</ep9.13>
			<id9.13>65CB4C2066F5501706E0B21447C3CADE7A6980C9</id9.13>
			<ep9.14>15 ANDREW KENNISON</ep9.14>
			<id9.14>114FC0166064F24FDE307B82B002FCCBF3EA7AB5</id9.14>
			<ep9.15>16 HELEN MAGHI</ep9.15>
			<id9.15>4DED75C80FB8321DD93294D099BA64945FD2B9CA</id9.15>
			<ep9.16>17 EL CONEJO</ep9.16>
			<id9.16>6edc286b2ff569365f1b4abf844f451ec5dda56b</id9.16>
			<ep9.17>18 LASZLO JANKOWICS</ep9.17>
			<id9.17>D6CF4A4572238B966DDB8D8F986A73C1E8EEC513</id9.17>
			<ep9.18>19 LA MASCARA DEL OSO</ep9.18>
			<id9.18>cd3811642c67949203fd64ba789ca21e821f3707</id9.18>
			<ep9.20>20 EL BANCO CAELUM</ep9.20>
			<id9.20>a7aa50e785b0561c71dff4dc97ee6c1b0f70a6e3</id9.20>
			<ep9.21>21 MARVIN GERARD: CONCLUSION (1)</ep9.21>
			<id9.21>a53bfc6d8df41094f10eebefedf732a81cf1845c</id9.21>
			<ep9.22>22 MARVIN GERARD: CONCLUSION (2)</ep9.22>
			<id9.22>9f36e756017a1b99ab42c3ddd34702d9760778e3</id9.22>
		</t9>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE BOYS</title>
		<info>(2019) 3 temporadas. 24 episodios. La serie tiene lugar en un mundo en el que los superhéroes representan el lado oscuro de la celebridad y la fama. Un grupo de vigilantes que se hacen llamar "The Boys" decide hacer todo lo posible por frenar a los superhéroes que están perjudicando a la sociedad, independientemente de los riesgos que ello conlleva.</info>
		<year>2019</year>
		<genre>Fantástico. Acción. Drama. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/hjO01XnizdVgwygBNzDwUEK9CDB.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/eub9xDaIfNwvg0oEv5tvkAlFqrO.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/dzOxNbbz1liFzHU1IPvdgUR647b.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/czffzEgaBko4mXW84B1J1EnQBzo.jpg</fanart.1>
			<ep1.1>1 LAS REGLAS DEL JUEGO</ep1.1>
			<id1.1>42f595b940a78adcd363387da1ad2957268ec903</id1.1>
			<ep1.2>2 CHERRY</ep1.2>
			<id1.2>32c56a2c3d4252de10be73c12471c85fffcbeb19</id1.2>
			<ep1.3>3 MOJA (EL COMPUESTO V)</ep1.3>
			<id1.3>9d321e40765377e3117370b4750e55443b481496</id1.3>
			<ep1.4>4 LA HEMBRA DE LA ESPECIE</ep1.4>
			<id1.4>59cf7e23305922976abc7b096d1ec3624f790c6e</id1.4>
			<ep1.5>5 BUENO PARA EL ALMA</ep1.5>
			<id1.5>6411c6320bab59abe6320a007f6bd1da0a097836</id1.5>
			<ep1.6>6 LOS INOCENTES</ep1.6>
			<id1.6>62ab0fc0563ebdc4ce02790f401a87bf3fb86980</id1.6>
			<ep1.7>7 LA SOCIEDAD DE LA AUTOPRESERVACION</ep1.7>
			<id1.7>643a1ebe57557b9825743ada1bdcdee6af31a9c8</id1.7>
			<ep1.8>8 ME HAS ENCONTRADO</ep1.8>
			<id1.8>6690cd7278f948cf4d18a94894127512062bb903</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/mY7SeH4HFFxW1hiI6cWuwCRKptN.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/bI37vIHSH7o4IVkq37P8cfxQGMx.jpg</fanart.2>
			<ep2.1>1 AL 3</ep2.1>
			<id2.1>0aae2158390c54bbb5ad2024df7dc7374be319ba</id2.1>
			<ep2.2>4 NADA IGUAL EN EL MUNDO</ep2.2>
			<id2.2>332724191f151581d4cc601873a18441fcf198d3</id2.2>
			<ep2.3>5 NOS DAMOS EL PIRO</ep2.3>
			<id2.3>d61052f070d201fcd938f832bea309e914800cb3</id2.3>
			<ep2.4>6 CARNICERO, PANADERO, FABRICANTE DE VELAS</ep2.4>
			<id2.4>959a7c0dbc90b8ff33d719ba4c212889bc429e5e</id2.4>
			<ep2.5>7 CARNICERO</ep2.5>
			<id2.5>8ba3e260e91769dd983b3c03532577ada2880aec</id2.5>
			<ep2.6>8 LO QUE YO SE</ep2.6>
			<id2.6>c9a756bdab8b34591d154858e474a4ac702c2674</id2.6>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/stTEycfG9928HYGEISBFaG1ngjM.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/3IkPwit8NPlmVL6k2IknL4SJqM4.jpg</fanart.3>
			<ep3.1>1 REVANCHA</ep3.1>
			<id3.1>db539cc30278d1001cf0a5edfdc7978ff415d047</id3.1>
			<ep3.2>2 EL UNICO HOMBRE EN EL CIELO</ep3.2>
			<id3.2>266bd05507bef5cacf286e26bbba5cf327d95409</id3.2>
			<ep3.3>3 COSTA BEREBER</ep3.3>
			<id3.3>6abc3a2df8b955123fc365fbffde501d5b0f0093</id3.3>
			<ep3.4>4 GLORIOSO PLAN DE CINCO AÑOS</ep3.4>
			<id3.4>af2878992a975f8c85cef112a718da2a57c3be26</id3.4>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE CROWN</title>
		<info>(2016) 4 temporadas. 40 episodios. Basada en la exitosa obra de teatro de Peter Morgan, "The Audience", cuenta la historia de la actual reina de Inglaterra, Isabel II, y de la relación entre dos de las direcciones más famosas del mundo: el Palacio de Buckingham y el número 10 de Downing Street, con las intrigas, amores y maquinaciones detrás de los eventos que forjaron la segunda mitad del siglo XX. Dos casas, dos cortes, una corona. Cada temporada trata las rivalidades políticas e intrigas personales durante una década del reinado de Isabel II y explora el delicado equilibrio entre su vida privada y la vida pública.</info>
		<year>2016</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/bFEFDqhxxon7CTe6k8lUgPE2qUO.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1exKX0EO9BWmHGxCOJ1rgWyFKze.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/6TKuHmdHdeeZkLdYxsIV099N8I6.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/8OXP0YM.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>0968f9182b1114c443f3a5b4d12059bee0f156c9</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/jkXkaZBg8GhFrnypBnVvGDuMj5c.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/pX05r6oecT0QpjMmTjr1awOshee.jpg</fanart.2>
			<ep2.1>1 AL 2</ep2.1>
			<id2.1>a11347a69aadcbf3c9bfc7275cefe070b8e7b741</id2.1>
			<ep2.2>3 AL 6</ep2.2>
			<id2.2>60b9357e6835f471ad4d56d4cae1400826decea7</id2.2>
			<ep2.3>7 AL 10</ep2.3>
			<id2.3>99adc9ede71f73112004ae4e980ed4fba87bd9f9</id2.3>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/uT4HL776Up8LgjqfmmGtGKSDgvT.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/p3qBwzzLe9aw1yA0s4hg0M2DvD0.jpg</fanart.3>
			<ep3.1>1 AL 10</ep3.1>
			<id3.1>354d83c61b6d5786a1958940904cf62ad526ca3c</id3.1>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/hWhUtIaieHMBqjOLxudgdnprWse.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/4InrdamBEM31unNiuEHGYTPX1e2.jpg</fanart.4>
			<ep4.1>1 AL 10</ep4.1>
			<id4.1>befb173f2c612cd8c87e37b5a549c359d77e1683</id4.1>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE DROPOUT: AUGE Y CAIDA DE ELIZABETH HOLMES</title>
		<info>(2022) 8 episodios. Serie de televisión que narra el intento de la fundadora de Theranos, Elizabeth Holmes, de revolucionar la industria de la salud después de abandonar la universidad y comenzar una empresa de tecnología.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/9SkOJF92FE0H1UXeOgLz7lAnoMb.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/969nC6k30OnMLCWyaa0d2FO0uTc.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/9SkOJF92FE0H1UXeOgLz7lAnoMb.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/969nC6k30OnMLCWyaa0d2FO0uTc.jpg</fanart.1>
			<ep1.1>1 TENGO PRISA</ep1.1>
			<id1.1>84c9f8808ea52cf36efff87c8c4f70109ea0aa38</id1.1>
			<ep1.2>2 QUIMIOLUMINISCENCIA</ep1.2>
			<id1.2>0f42f17de4d28288406a5f4f2e071fb2ba38d61d</id1.2>
			<ep1.3>3 ZUMO DE VERDURAS</ep1.3>
			<id1.3>d131309e80fc63597b385c464c8e1312f57d73ca</id1.3>
			<ep1.4>4 VIEJOS BLANCOS</ep1.4>
			<id1.4>70355f1c55d9795bc5959b19969e509d0d90cbff</id1.4>
			<ep1.5>5 LA FLOR DE LA VIDA</ep1.5>
			<id1.5>7a632267d69a020839d54a69d05cbf5c4b87b5d4</id1.5>
			<ep1.6>6 HERMANAS DE HIERRO</ep1.6>
			<id1.6>049ada333dd35a57c4b8b36592c1c518572d4c8f</id1.6>
			<ep1.7>7 HEROES</ep1.7>
			<id1.7>6a930228f89973fd9d3c77a91b5e222e80f921e2</id1.7>
			<ep1.8>8 LIZZY</ep1.8>
			<id1.8>e52c714f81c3678e27cf6e0c9c9c53efd2600435</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE EXPANSE</title>
		<info>(2015) 5 temporadas. 56 episodios En un futuro lejano en el que los humanos han colonizado el Sistema Solar, el detective de la policía Josephus Miller recibe el encargo de encontrar a una joven desaparecida. Para ello contará con la ayuda del oficial de un carguero y su tripulación, pero pronto se dan cuenta que el caso está relacionado con una conspiración que amenaza la paz del Sistema y la supervivencia de la humanidad.</info>
		<year>2015</year>
		<genre>Ciencia ficción. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/8djpxDeWpINnGhjpFXQjnBe6zbx.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/7yKtaRij2giAj0s09F6gmB8XIje.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8djpxDeWpINnGhjpFXQjnBe6zbx.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/7yKtaRij2giAj0s09F6gmB8XIje.jpg</fanart.1>
			<ep1.1>1 AL 10 LEVIATAN DESPIERTA</ep1.1>
			<id1.1>dc7521ad4397c348fdf81ceaadb31e668e38dbce</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/go2m0Cz5KqEwYFGiXVPNvCUZql3.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/hVrcSy5ca0xRjy9e9w5bguXSjAc.jpg</fanart.2>
			<ep2.1>1 AL 13 GUERRA DE CALIBAN</ep2.1>
			<id2.1>ee7fe6d48322c6ba523a9b941d780f9dfacf09be</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/irqtvxTFbjlIvgPNtG0EZwqTPrC.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/3OBpkhRPmEIat3MOhPtm3JJdKFQ.jpg</fanart.3>
			<ep3.1>1 AL 6 PUERTA DE ABADDON</ep3.1>
			<id3.1>6370ff2a91f29bc4f34204ad3c5847e0c69d766d</id3.1>
			<ep3.2>7 AL 13 PUERTA DE ABADDON</ep3.2>
			<id3.2>6745fffa50dbab00a800f4f55f2f293572ec6cfe</id3.2>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/wikmaI7OVqmq2O9HfknkzxX6Ygu.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/uUwnClwdMA12bpHgeKgkQrbu5Oe.jpg</fanart.4>
			<ep4.1>1 AL 10 QUEMADURA DE CIBOLA</ep4.1>
			<id4.1>68fd0c09b6f89476ca34bc05d309e85237321101</id4.1>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/5vQlVWkIMPhZ88OWchJsgwGEK9.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/fYX29Hf2effFV4bOhhiKMkKNRUG.jpg</fanart.5>
			<ep5.1>1 AL 3</ep5.1>
			<id5.1>fa2db38f10d72280d568bf7c5ea03df3e725db02</id5.1>
			<ep5.2>4 GUAGAMELA</ep5.2>
			<id5.2>83e55858c46fe3dde2c1fc99139b2831e7757b38</id5.2>
			<ep5.3>5 DESESPERADOS</ep5.3>
			<id5.3>c6b121d38f08bdf272b18af348be60ed41a0004d</id5.3>
			<ep5.4>6 TRIBUS</ep5.4>
			<id5.4>bbb44af1c10511c1e5ab7eff0b7c39cad5412d23</id5.4>
			<ep5.5>7 OYEDENG</ep5.5>
			<id5.5>665e64731521af6c4b697bd3e686321f197ad5f5</id5.5>
			<ep5.6>8 ALTO VACIO</ep5.6>
			<id5.6>1082eebeb8b28b018fe1607622dc0b9143aa20e4</id5.6>
			<ep5.7>9 WINNIPESAUKEE</ep5.7>
			<id5.7>b7ab1759e1538c44f703ffc895de26eefb9de37f</id5.7>
			<ep5.8>10 LOS JUEGOS DE NEMESIS</ep5.8>
			<id5.8>6ffa70aca7f3899e1cc6745700867ae3e93e064f</id5.8>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE FIRST LADY</title>
		<info>(2022) 10 episodios. En el ala este de la Casa Blanca, muchas de las decisiones más impactantes y que cambiaron el mundo han estado ocultas a la vista, tomadas por las carismáticas, complejas y dinámicas primeras damas de Estados Unidos.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/d4E8sayYfKqLfohq1gfXhNCbreC.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/jWBTRLWrUuqZWm7lcpAz7JndYzt.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/d4E8sayYfKqLfohq1gfXhNCbreC.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/jWBTRLWrUuqZWm7lcpAz7JndYzt.jpg</fanart.1>
			<ep1.1>1 ESA CASA BLANCA</ep1.1>
			<id1.1>a9a3a2ca2644ca6d9e4915125f49844d5d3a266b</id1.1>
			<ep1.2>2 LAS VOCES SE ESCUCHAN</ep1.2>
			<id1.2>712354e1ba36ff5eff84e18a3b5ab72fef21461e</id1.2>
			<ep1.3>3 POR FAVOR PERMITAME</ep1.3>
			<id1.3>7fc299c8e864274f026213f866d376e83050580f</id1.3>
			<ep1.4>4 OLLA ROTA</ep1.4>
			<id1.4>ee2ee7a7e0e23c2d379edae1d85f35df27e95667</id1.4>
			<ep1.5>5 EL BALANCIN</ep1.5>
			<id1.5>435610ce1c2ada7b339f3e23cd502d206a28aaa9</id1.5>
			<ep1.6>6 GRITAR</ep1.6>
			<id1.6>830f4a8af50240eee0306a18a8dae3f235c77de7</id1.6>
			<ep1.7>7 EXTRAÑO</ep1.7>
			<id1.7>eddc90e19fc6c8af2f5e16721ae479fbfea3c88e</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE FLIGHT ATTENDANT</title>
		<info>(2020)  2 temporadas. 16 episodios. La azafata de vuelo Cassandra Bowden se despierta en su habitación de hotel con resaca de la noche anterior con un cuerpo sin vida a su lado. Temerosa de llamar a la policía, continúa con su mañana como si nada hubiese ocurrido, reuniéndose con los demás operarios en el aeropuerto. En Nueva York recibe la visita de un grupo de agentes del FBI que la interrogan sobre sus acciones en Dubai. Incapaz de recordar exactamente lo que ha sucedido, comienza a preguntarse si es posible que sea ella la responsable del asesinato.</info>
		<year>2020</year>
		<genre>Thriller. Intriga. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/8azUzeNTqdHlSjkc12A5s6fpS6P.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/sAusLCtzzA0ZLnPlvm54eIS2FZj.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/8azUzeNTqdHlSjkc12A5s6fpS6P.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/sAusLCtzzA0ZLnPlvm54eIS2FZj.jpg</fanart.1>
			<ep1.1>1 UN CASO DE EMERGENCIA</ep1.1>
			<id1.1>c6f0de282860bb10a66dc4009e7f6144aaca9979</id1.1>
			<ep1.2>2 CONEJOS</ep1.2>
			<id1.2>0031F62F61832B7579801332E783987984612EC4</id1.2>
			<ep1.3>3 FUNERALIA</ep1.3>
			<id1.3>d6790da4d85e3f41a2044b6dc0f39172117ba043</id1.3>
			<ep1.4>4 AL 5</ep1.4>
			<id1.4>6aac25e39756d604b05e13a3393a22b0a34fc6d9</id1.4>
			<ep1.5>6 AL 7</ep1.5>
			<id1.5>b89ecd68840fa0eae7ae5a0bb48a922861967acb</id1.5>
			<ep1.6>8 LLEGADAS Y SALIDAS</ep1.6>
			<id1.6>d6191a28ea7252a231996bdf8ee4268b14ae2967</id1.6>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/oCK0o8CWV46Z0PUdh7CKXnjD2Po.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/l5yWgYnMoSN1X02k9MljpvfAps6.jpg</fanart.2>
			<ep2.1>1 VISION DOBLE</ep2.1>
			<id2.1>d316ff4ab680ce0215e8c2856878540e6149390d</id2.1>
			<ep2.2>2 ¡SETAS, TASER, Y OSOS, OH DIOS!</ep2.2>
			<id2.2>b82003c83955814f778e17946d325099f005c38b</id2.2>
			<ep2.3>3 EL FESTIVAL DE ESCULTURAS DE HIELO DE REIKIAVIK ES ENCANTADOR EN ESTA EPOCA DEL AÑO</ep2.3>
			<id2.3>e59a5ef90b9750bdbdd42b29282c07df82dc686f</id2.3>
			<ep2.4>4 LA REUNION DEL ATUN ROJO</ep2.4>
			<id2.4>0d1d02e7c9cc1827c405476d417a11b4ea60e69c</id2.4>
			<ep2.5>5 MUJERES AHOGADAS</ep2.5>
			<id2.5>52295a623b5d7e2b0b5af2fcc6092abaa27b9e08</id2.5>
			<ep2.6>6 HERMANOS Y HERMANAS</ep2.6>
			<id2.6>e1057bb2273b6db3497bf8ee48c65f65daae39c4</id2.6>
			<ep2.7>7 SIN SALIDA</ep2.7>
			<id2.7>22fb8821283dd9cc5ec6b491d3ebae02fb3664fd</id2.7>
			<ep2.8>8 IDAS Y VENIDAS</ep2.8>
			<id2.8>e4d1d4ada8955fdf365d9835bb5fba3cf29859f4</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE GOOD DOCTOR</title>
		<info>(2017) 5 temporadas. 76 episodios. Shaun Murphy (Freddie Highmore) es un joven cirujano residente que padece autismo y síndrome de Savant, conocido también como el “síndrome del sabio”, una enfermedad que le causa problemas a la hora de relacionarse con los demás, pero que a su vez le ha permitido desarrollar unas habilidades mentales prodigiosas, como una extraordinaria memoria. A pesar de que Shaun ha tenido una infancia muy complicada, se ha convertido en un médico con mucho talento y ha sido reclutado por el doctor Aaron Glassman (Richard Schiff) en la unidad de cirugía pediátrica del prestigioso San José St. Bonaventure Hospital. Sin embargo, no toda la junta del hospital se muestra conforme con la decisión de incorporar al equipo a un cirujano con autismo. Shaun tendrá que despejar las dudas y demostrar su valía, y aunque pueda encontrar su camino hacia la sala de operaciones, todavía hay muchos desafíos y prejuicios a los que debe hacer frente para cumplir su sueño de salvar vidas.</info>
		<year>2017</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/z1K4mJwISETia59rrnMdXxzoSrZ.jpg</thumb>
		<fanart>https://i.imgur.com/pNV9jmc.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/qYhUKf98Ut9yjqB515VgfvFezCQ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/tXSSPHvZQYHvdwrzLr2XPY6cRni.jpg</fanart.1>
			<ep1.1>1 COMIDA QUEMADA</ep1.1>
			<id1.1>a7e16961a9ac6e1d9e85b7269799c77743816ab7</id1.1>
			<ep1.2>2 MONTE RUSHMORE</ep1.2>
			<id1.2>24914ab571de3c2f7957e8bc3ddae76df425a796</id1.2>
			<ep1.3>3 OLIVER</ep1.3>
			<id1.3>502f726a5761560096db7e80e6006a3f24d67523</id1.3>
			<ep1.4>4 TUBERIAS</ep1.4>
			<id1.4>5ad0f0b2a34e691b14c0621a5b9839c3b387a7d2</id1.4>
			<ep1.5>5 0,3 POR CIENTO</ep1.5>
			<id1.5>fe11936a7802d61cdc5efb1e9895bd84fa2b7c8c</id1.5>
			<ep1.6>6 DE MENTIRA NO</ep1.6>
			<id1.6>9eee8b42f20c51852678d7359b39d84117f4a2b2</id1.6>
			<ep1.7>7 22 PASOS</ep1.7>
			<id1.7>eef458f97b32dc786231e482ae089dacf02f7a59</id1.7>
			<ep1.8>8 MANZANAS</ep1.8>
			<id1.8>abac15193e7e59b8165eac1f87e72f757e02e33c</id1.8>
			<ep1.9>9 INTANGIBLES</ep1.9>
			<id1.9>44611fc8803e06e8822eb5dc234f65a035bc4563</id1.9>
			<ep1.10>10 SACRIFICIO</ep1.10>
			<id1.10>4ea8814019291f8b9ab40c672ce009aef65a11cb</id1.10>
			<ep1.11>11 ISLAS PRIMERA PARTE</ep1.11>
			<id1.11>e8923c17d854b2d227e6e7cbe1c9b0ba17d43ef7</id1.11>
			<ep1.12>12 ISLAS SEGUNDA PARTE</ep1.12>
			<id1.12>5d09f0db9e23de86d8cfbbdf03eec8ab467631b6</id1.12>
			<ep1.13>13 7 RAZONES</ep1.13>
			<id1.13>29432e4a109f1222b891eddd7559443b03abe889</id1.13>
			<ep1.14>14 ELLA</ep1.14>
			<id1.14>ea44141e56e4ee94e8671abd562ecb602e2593a4</id1.14>
			<ep1.15>15 DE CORAZON</ep1.15>
			<id1.15>870290804e5554d496642b8783c7fa3330969c34</id1.15>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/zHdy8qTrKG0szzic3FCMkCrodnq.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/u1aDGhR1ENc8qzp6brbM29ViZfl.jpg</fanart.2>
			<ep2.1>1 HELLO</ep2.1>
			<id2.1>e8b13bf61137a69ec92db4c54361876754ded997</id2.1>
			<ep2.2>2 ZONA INTERMEDIA</ep2.2>
			<id2.2>c1e8c10eb3a2019d7452de7e7360e4d1d0fd847b</id2.2>
			<ep2.3>3 36 HORAS</ep2.3>
			<id2.3>15b531b0097d5bc34155f9d2155c616818cc596d</id2.3>
			<ep2.4>4 PLANTALE CARA</ep2.4>
			<id2.4>9d346a65d4402597f4f01594e170ed0cd118fdad</id2.4>
			<ep2.5>5 ZANAHORIAS</ep2.5>
			<id2.5>8486a2a6c3f33fe797c2b4c7e9d7192deba98c4e</id2.5>
			<ep2.6>6 DE MENTIRA,NO</ep2.6>
			<id2.6>02c86f25cc1b05ed3fe0b461f37746c172fd58ea</id2.6>
			<ep2.7>7 HUBERT</ep2.7>
			<id2.7>57fab905f88bd648a0ae91da4195194e072bf040</id2.7>
			<ep2.8>8 HISTORIAS</ep2.8>
			<id2.8>ff434873cd5c210d636107ac4ab53582d52be7a6</id2.8>
			<ep2.9>9 EMPATIA</ep2.9>
			<id2.9>cd95c238af17eab42868bb1fa64d95392fe312e3</id2.9>
			<ep2.10>10 CUARENTENA</ep2.10>
			<id2.10>1ad2b0d383cb7ebeb1c05a8505371d04e7b21234</id2.10>
			<ep2.11>11 CUARENTENA 2 PARTE</ep2.11>
			<id2.11>5f07b4840543d90ccbbb8181c35e04da0c63eeb7</id2.11>
			<ep2.12>12 SECUELAS</ep2.12>
			<id2.12>0e9f518958bac90c866409893ce6a7319758309f</id2.12>
			<ep2.13>13 XIN</ep2.13>
			<id2.13>4a2fa6b12230f00a73f90def1efaa6f5775ff8d9</id2.13>
			<ep2.14>14 CARAS</ep2.14>
			<id2.14>b78de8839ff05a7e0c334a9ae32478f57e242997</id2.14>
			<ep2.15>15 RIESGO Y CONOCIMIENTO</ep2.15>
			<id2.15>50deafe5ce0e37f6a61492aa33bf279c22fc963c</id2.15>
			<ep2.16>16 CREE</ep2.16>
			<id2.16>a24d5e43d4d03261a0e2db792e8c14731bbbdf48</id2.16>
			<ep2.17>17 CRISIS</ep2.17>
			<id2.17>70f65f253fb079fdb622d688c4afcd0df49e36a6</id2.17>
			<ep2.18>18 TRAMPOLINE</ep2.18>
			<id2.18>d48d3f40b19835b7c1df56ff92885c526e037d4f</id2.18>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/8QFssOwaWZ3eB60cGwDpfrZBscv.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/edLV34FXx1iFJA3hbZE7SYRSS4m.jpg</fanart.3>
			<ep3.1>1 DESASTRES</ep3.1>
			<id3.1>e160412fca22c2b9720d05b2542423bd45259e1e</id3.1>
			<ep3.2>2 DEUDAS</ep3.2>
			<id3.2>06170eb629367ffac01a1dda5c4ef4e0f8235e35</id3.2>
			<ep3.3>3 CLAIRE</ep3.3>
			<id3.3>bc183408be0ff20d08c5e699456ca603f18d4acf</id3.3>
			<ep3.4>4 COGE MI MANO</ep3.4>
			<id3.4>3efae768400e888f2c7c16061ee1a2f588f4fd6a</id3.4>
			<ep3.5>5 PRIMER PASO SEGUNDA BASE</ep3.5>
			<id3.5>21b8479432368985e24ec4428cc21bce6c0a5cbf</id3.5>
			<ep3.6>6 ANGULO DE 45 GRADOS</ep3.6>
			<id3.6>f0a85e92db27c8da0414d82fa3a42e14bdeda3f4</id3.6>
			<ep3.7>7 DAEF</ep3.7>
			<id3.7>268b7cafab90b85335e2502963d8cb23d4c4e450</id3.7>
			<ep3.8>8 DISPARO DE LUNA</ep3.8>
			<id3.8>fa8066d5e6c20ef0d972a6726e520dfc3bc0e193</id3.8>
			<ep3.9>9 INCOMPLETO</ep3.9>
			<id3.9>05d118fb7d6b8fa04a2178468e1973ce180fb90e</id3.9>
			<ep3.10>10 AMIGOS Y FAMILIA</ep3.10>
			<id3.10>2a9d05c055fdd4383fd17c785dd562a4e99ce212</id3.10>
			<ep3.11>11 FRACTURADO</ep3.11>
			<id3.11>9a73f1f8319c1d56d0ae293fd8a79567ca067b65</id3.11>
			<ep3.12>12 MUTACIONES</ep3.12>
			<id3.12>a8064ca61edde1bc388ae225cc083de509b00381</id3.12>
			<ep3.13>13 SEXO Y MUERTE</ep3.13>
			<id3.13>e28356225209a0d76efe15c225c068a51085e7f1</id3.13>
			<ep3.14>14 INFLUENCIA</ep3.14>
			<id3.14>7f368d7920b36e17326b48867df2bdd07775042e</id3.14>
			<ep3.15>15 LO QUE NO SE DICE</ep3.15>
			<id3.15>7a9806674610837a754e48fe6269e3f9634586fe</id3.15>
			<ep3.16>16 AUTOPSIA</ep3.16>
			<id3.16>662a083c6c6043580e0789e44b444a31ba32b08c</id3.16>
			<ep3.17>17 FIJACION</ep3.17>
			<id3.17>a4fae6a4d08dcb5b2eb8b49eef0004d9ceb33ac1</id3.17>
			<ep3.18>18 DESAMOR</ep3.18>
			<id3.18>9429bf376adaa6f102e41d9fe75984c54d3f0c24</id3.18>
			<ep3.19>19 RESCATE</ep3.19>
			<id3.19>c5e1784a995ddbe52aac03289ae35b4af749bc12</id3.19>
			<ep3.20>20 TE QUIERO</ep3.20>
			<id3.20>3ccf33d4359e148bd6f2f17dcea1ba25b33462d5</id3.20>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/6tfT03sGp9k4c0J3dypjrI8TSAI.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/zlXPNnnUlyg6KyvvjGd2ZxG6Tnw.jpg</fanart.4>
			<ep4.1>1 PRIMERA LINEA (1ª PARTE)</ep4.1>
			<id4.1>627605b9f64a19c99944c38701183b4582750732</id4.1>
			<ep4.2>1 PRIMERA LINEA (2ª PARTE)</ep4.2>
			<id4.2>2c23f02f117166d39d762513072f9af2bca2c416</id4.2>
			<ep4.3>3 NOVATOS</ep4.3>
			<id4.3>000d97e44e8a386403811e6b25cc52d1bda3b8fd</id4.3>
			<ep4.4>4 NO ES EL MISMO</ep4.4>
			<id4.4>593a7d8e0501c907fde6c444d5fef0badbacdaf4</id4.4>
			<ep4.5>5 CULPA</ep4.5>
			<id4.5>440327bf9c5aebb3f65130fa832afd62029bc80d</id4.5>
			<ep4.6>6 LIM</ep4.6>
			<id4.6>f0ca9dabe8c1a921d3b879748a1537507668531b</id4.6>
			<ep4.7>7 EL PRINCIPIO DE LA INCERTIDUMBRE</ep4.7>
			<id4.7>2c877a92b3acf69aa384a261178fe937ba14b933</id4.7>
			<ep4.8>8 SER PADRES</ep4.8>
			<id4.8>bdf6c693449b57607c50c459aac1bdbce5554087</id4.8>
			<ep4.9>9 HABITOS IRRESPONSABLES EN EL BUFE DE ENSALADAS</ep4.9>
			<id4.9>33ae8fb0f60f1facc5dfe719a5359a603d9209af</id4.9>
			<ep4.10>10 DESENCRIPTALO</ep4.10>
			<id4.10>6158e5e4fc2ca67b01f10f7fc640c584fd2a77f0</id4.10>
			<ep4.11>11 TODOS ESTAMOS LOCOS A VECES</ep4.11>
			<id4.11>fb4397455057bd147d3d9ea58cbb9bfd5a9bcddd</id4.11>
			<ep4.12>12 OJITOS AZULES</ep4.12>
			<id4.12>c8dd0de325f8a3c9261c9214dead491db8f96cba</id4.12>
			<ep4.13>13 HECHO ESTA</ep4.13>
			<id4.13>eb96df3abe3526cfb5c5201cbb63a77a4d544142</id4.13>
			<ep4.14>14 SABEMOS AL SEXO</ep4.14>
			<id4.14>bb231b8952ab8117e55706e034d301e2ca93d553</id4.14>
			<ep4.15>15 LA ESPERA</ep4.15>
			<id4.15>8dcb619f1dc88f50473385b11e9dc1901c7af0b4</id4.15>
			<ep4.16>16 DR. PELUCHE</ep4.16>
			<id4.16>0131a60f9a036438ce0918180d83803a09c0f9c9</id4.16>
			<ep4.17>17 ACEPTANDOLO</ep4.17>
			<id4.17>084f5b0e4e931964abc483c24157b6c2e6066d5d</id4.17>
			<ep4.18>18 PERDONA U OLVIDA</ep4.18>
			<id4.18>37101bd5f20bb50b0b6fc6463ef556fac05d9a7d</id4.18>
			<ep4.19>19 VENGA</ep4.19>
			<id4.19>76973e777aa5b6696da3900a302240fd7c972d68</id4.19>
			<ep4.20>20 VAMOS</ep4.20>
			<id4.20>fe9c07f6364a9a855258ab4c0a957337e1bc6f5e</id4.20>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/ubOaaQjDQ4lWtw1dkXhqsQWTsEY.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/9OYu6oDLIidSOocW3JTGtd2Oyqy.jpg</fanart.5>
			<ep5.1>1 NUEVOS COMIENZOS</ep5.1>
			<id5.1>0da4c5fb4965f4db2bf66d8cd64f6450a7deb9af</id5.1>
			<ep5.2>2 UN TROZO DE TARTA</ep5.2>
			<id5.2>5504c3f94815a1920122ee287750c774bd7a3ba6</id5.2>
			<ep5.3>3 MEDIDA DE INTELIGENCIA</ep5.3>
			<id5.3>64c506eec9c63fc99bf0a26bb4adddd55b71492e</id5.3>
			<ep5.4>4 RACIONALIDAD</ep5.4>
			<id5.4>3b4af2f5bbf21d92985a9dd2d5c9ed00cea96a36</id5.4>
			<ep5.5>5 OBSESION</ep5.5>
			<id5.5>d9c9319897fd5d50b575b46cfdbf9ff60ddb2013</id5.5>
			<ep5.6>6 UN CORAZON</ep5.6>
			<id5.6>88cfbaa512582122e68ca73bfa0090b6eaf623f7</id5.6>
			<ep5.7>7 CADUCADO</ep5.7>
			<id5.7>cc23f8bb2a210513c7267f7b54cacd185f30bb27</id5.7>
			<ep5.8>8 REBELION</ep5.8>
			<id5.8>299261A7BEAAAA18D05CF8A320299E587FC66FEC</id5.8>
			<ep5.9>9 YIPI KA YEI</ep5.9>
			<id5.9>11d4f521ced0bd4968cca0575b39c1cc68115139</id5.9>
			<ep5.10>10 HOY HAGO TRAMPA</ep5.10>
			<id5.10>fee6263a09ee0ccdd16862e732590d0b66424bfa</id5.10>
			<ep5.11>11 LA FAMILIA</ep5.11>
			<id5.11>5341365cd9ab6dba0f33c9b06b22b907c4476469</id5.11>
			<ep5.12>12 EN DIQUE SECO</ep5.12>
			<id5.12>fe6432bc7e771ce81e5295b0cb92a8708750ffd7</id5.12>
			<ep5.13>13 DOLORES DEL CRECIMIENTO</ep5.13>
			<id5.13>e5644227e03fc21a9da4beae7768f106148d8340</id5.13>
			<ep5.14>14 EL BUFE</ep5.14>
			<id5.14>ce67f903f1404036bc651dbde074c7c30db65c1b</id5.14>
			<ep5.15>15 A MI MANERA</ep5.15>
			<id5.15>b161ea57cb4c8ebda8f1a40d001e6f03a6b025c5</id5.15>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE GOOD FIGHT</title>
		<info>(2017) 6 temporadas. 50 episodios.  El spin-off de la aclamada serie "The Good Wife" se centra en los personajes de Diane Lockhart y Lucca Quinn, un año después de los sucesos del último episodio de su predecesora</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/x7KjZQfmRMnPR57VslQbf5XPLLr.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/to9i5kqa01VNRD9ZZi6cddNQX7L.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/x7KjZQfmRMnPR57VslQbf5XPLLr.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/to9i5kqa01VNRD9ZZi6cddNQX7L.jpg</fanart.1>
			<ep1.1>1 A OTRO SITIO</ep1.1>
			<id1.1>619ffa963d61d459f68b5c5fec7d2e91c4d74fe9</id1.1>
			<ep1.2>2 LA PRIMERA SEMANA</ep1.2>
			<id1.2>cb6b1b1aba08c7574f2232490f2d0864e5c87e45</id1.2>
			<ep1.3>3 LA LISTA SCHTUP</ep1.3>
			<id1.3>dddb57750dc8864eb4a2f6f4fd736f3416a39e8c</id1.3>
			<ep1.4>4 EN ADELANTE PROPIEDADES</ep1.4>
			<id1.4>3406accb3c6fe08a6cefd66439e1fbe9b0c4fc05</id1.4>
			<ep1.5>5 FECHA DE EMISION</ep1.5>
			<id1.5>a4322e96420f8c569dd53465b59723a223b378b5</id1.5>
			<ep1.6>6 COMISION DE CENSURA</ep1.6>
			<id1.6>69d986a8d6b80599cf37a8c62bacf90ca7989833</id1.6>
			<ep1.7>7 NO TAN GRAN JURADO</ep1.7>
			<id1.7>6fe8fb80cb35c80f72f577f568f7b56ec74b1dbc</id1.7>
			<ep1.8>8 REDDICK CONTRA BOSEMAN</ep1.8>
			<id1.8>7b0add679ade7abf9562b205355695ec6add6a80</id1.8>
			<ep1.9>9 PERO HA MENTIDO</ep1.9>
			<id1.9>c8616bdec9c2ee84ce42cf9f605be94f14ef6286</id1.9>
			<ep1.10>10 LA GRAN PIFIA</ep1.10>
			<id1.10>3f2d7affb1e11e627efd08fc94818a9b7018831b</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/p1uQrPakuPUXk6nqCZUoPzVo84T.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/to9i5kqa01VNRD9ZZi6cddNQX7L.jpg</fanart.2>
			<ep2.1>1 DIA 408</ep2.1>
			<id2.1>bdc641fb0edbbea02c8e26ee46fc998f0825832d</id2.1>
			<ep2.2>2 DIA 415</ep2.2>
			<id2.2>d076257fd4a9caa1f3997516acf54508362a1ae7</id2.2>
			<ep2.3>3 DIA 422</ep2.3>
			<id2.3>6166aca1d8c07126966c712037f46c82caf6e264</id2.3>
			<ep2.4>4 DIA 429</ep2.4>
			<id2.4>5117735e478329fc35d0cce6189aca09200546f8</id2.4>
			<ep2.5>5 DIA 436</ep2.5>
			<id2.5>32e4d002eba8ff192e71db7b1fc7b4454bf8d291</id2.5>
			<ep2.6>6 DIA 443</ep2.6>
			<id2.6>3d8c552585c172a5a4971301cb12e0fa5228592a</id2.6>
			<ep2.7>7 DIA 450</ep2.7>
			<id2.7>aa41de203a6560a4847251faf5dab88f5705f115</id2.7>
			<ep2.8>8 DIA 457</ep2.8>
			<id2.8>a627d353d0f2171e48ef322c76905bedea77258c</id2.8>
			<ep2.9>9 DIA 464</ep2.9>
			<id2.9>5bfadbd9e639943e068fff92293edea24189a4cb</id2.9>
			<ep2.10>10 DIA 471</ep2.10>
			<id2.10>3319719986b61a5062d8e6ca7fee3fac7fe475a9</id2.10>
			<ep2.11>11 DIA 478</ep2.11>
			<id2.11>1a7ddbd7d46345b377fbaf10d8df640f311afe1d</id2.11>
			<ep2.12>12 DIA 485</ep2.12>
			<id2.12>e08d5db90a088a99bf6891edfb88fec9119a3b9e</id2.12>
			<ep2.13>13 DIA 492</ep2.13>
			<id2.13>692df5643e2ec92045bafe1bde310fa3af913a07</id2.13>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/iHztcV0dCj8M8DWvsEvxcudB22K.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/to9i5kqa01VNRD9ZZi6cddNQX7L.jpg</fanart.3>
			<ep3.1>1 BASADO EN LOS PROBLEMAS RECIENTES</ep3.1>
			<id3.1>59a634239edf17d96ce33c290edd00a52d3deab8</id3.1>
			<ep3.2>2 BASADO EN ROY COHN</ep3.2>
			<id3.2>45901864755aec1728cf6c44596d46d9c7fcc32e</id3.2>
			<ep3.3>3 EL DE DIANE EN LA RESISTENCIA</ep3.3>
			<id3.3>52cca3e1fed53922ecefc5275381784d81f92e2c</id3.3>
			<ep3.4>4 EL DE LUCCA Y SU MEME</ep3.4>
			<id3.4>c8361cf18da323af1f6eefd0b6d7f05d08c7eecf</id3.4>
			<ep3.5>5 EL DEL PUÑETAZO AL NAZI</ep3.5>
			<id3.5>95547a46f07bd6b7db9a8a02f63076dd259fdffa</id3.5>
			<ep3.6>6 EL DEL FAMOSO DIVORCIO</ep3.6>
			<id3.6>6b46933a4c41e86c6838bb347d7d73c1b04b5678</id3.6>
			<ep3.7>7 EL DE DIANA Y LIZ CONSPIRANDO</ep3.7>
			<id3.7>17a6b94374e56777408b4eb95b8a40467b5d51f4</id3.7>
			<ep3.8>8 EL DE KURT SALVANDO A DIANE</ep3.8>
			<id3.8>b9ef6b9b8f495ec785335f8e04cebd211ec5a745</id3.8>
			<ep3.9>9 EL DE EN UNA HORA EL SOL SE PONDRA</ep3.9>
			<id3.9>1ac17b316990699476b73ef6bf681ea96e873bea</id3.9>
			<ep3.10>10 EL FIN DEL MUNDO</ep3.10>
			<id3.10>27822d660baa8e7b386772d16301b0b50ca6fd02</id3.10>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/eNOeNrNEXMnxg1YZCKogBaEzi20.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/oLeIp3ZkPXBAehPwgkF8kKFlbFf.jpg</fanart.4>
			<ep4.1>1 LOS DE LA REALIDAD ALTERNATIVA</ep4.1>
			<id4.1>aa1227ebc5fed5f54168458a9f25833d7fe9db3a</id4.1>
			<ep4.2>2 LOS DEL INTENTO DE CITACION</ep4.2>
			<id4.2>d05dab419741cd7ea464008fa74cf475d782a192</id4.2>
			<ep4.3>3 LOS DE LA LLAMADA DE RECURSOS HUMANOS</ep4.3>
			<id4.3>5791b6b086e79245c5a0c29dedbf5bc3056ddaae</id4.3>
			<ep4.4>4 LOS DE LA SATIRA QUE NO GUSTA</ep4.4>
			<id4.4>771f241b929224073458cff19ed6520ee2563065</id4.4>
			<ep4.5>5 LOS QUE VAN A LA GUERRA</ep4.5>
			<id4.5>35921025d0d3e650e50efc13a955349bbdfae72d</id4.5>
			<ep4.6>6 LOS QUE OFENDEN A TODOS</ep4.6>
			<id4.6>e29fe8459c3443cf0055ab020d8584a4b1e98fc1</id4.6>
			<ep4.7>7 LOS DEL QUIEN MATO A JEFFREY EPSTEIN</ep4.7>
			<id4.7>bb72e8449ef065333fec770ca664d8ba29e738d1</id4.7>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/l432nAaRgqCtMTNcI1XPouhWaNX.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/oLeIp3ZkPXBAehPwgkF8kKFlbFf.jpg</fanart.5>
			<ep5.1>1 ANTERIORMENTE...</ep5.1>
			<id5.1>d0e31ab619d23e00b21ceb27e6cce18f38d82c79</id5.1>
			<ep5.2>2 HABIA UNA VEZ UN JUZGADO...</ep5.2>
			<id5.2>7d7d4e7c0e3660ec9d1733682973c3e10c69d3d0</id5.2>
			<ep5.3>3 Y EN EL JUZGADO UNA SECRETARIA...</ep5.3>
			<id5.3>4f1b654af98a20711af5c8ba352d7e669fc87ca4</id5.3>
			<ep5.4>4 Y LA SECRETARIA TENIA UNA FIRMA...</ep5.4>
			<id5.4>775bad6940b254eb9d092df1396c0e85aa9c8ef2</id5.4>
			<ep5.5>5 Y LA FIRMA TIENE DOS SOCIOS...</ep5.5>
			<id5.5>2cf1e521020b9c9b716e075b6670311ce757fe17</id5.5>
			<ep5.6>6 Y LAS DOS SOCIAS UNA LUCHA ...</ep5.6>
			<id5.6>06a6d0dfadd04c55770968c052584c6ada7de34f</id5.6>
			<ep5.7>7 Y LA LUCHA UNA TREGUA</ep5.7>
			<id5.7>0c13bd137687c67af3c16aafbd04a574f538c15b</id5.7>
			<ep5.8>8 Y LA TREGUA UN FINAL…</ep5.8>
			<id5.8>21d560afba983000f1a9e03a87953c927d1ebe2a</id5.8>
			<ep5.9>9 Y EL FINAL FUE VIOLENTO ...</ep5.9>
			<id5.9>2e1688585335f9f5629b442034a812b82fe0769c</id5.9>
			<ep5.10>10 Y LA VIOLENCIA SE EXTENDIO…</ep5.10>
			<id5.10>c71f383e7716a901a238d9c386711fab7cd125eb</id5.10>
		</t5>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE GOOD WIFE</title>
		<info>(2009-2016) 7 temporadas. 156 episodios. Peter es un popular político que acaba en la cárcel por malversación de fondos y escándalo sexual. A partir de entonces, la vida de Alicia Florrick (Julianna Margulies), su esposa durante 13 años, se desmorona. Deberá rehacer su vida, tras sufrir la humillación de comparecer en público a su lado. Para ello, reanuda su trabajo de abogada en un prestigioso bufete, sin descuidar por ello la vida familiar. De hecho, procurará por todos los medios que sus hijos adolescentes se mantengan al margen del escándalo. La serie se inspira en un caso real: el del Gobernador de Nueva York Eliot Spitzer, que perdió su cargo por un escándalo sexual con una prostituta de lujo.</info>
		<year>2009</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/fOAnnIXv9ZnnhmWUdJpq4Ll87Do.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fV740jZ1ZIwRcLwgBEsiEHrdjPG.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/fOAnnIXv9ZnnhmWUdJpq4Ll87Do.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fV740jZ1ZIwRcLwgBEsiEHrdjPG.jpg</fanart.1>
			<ep1.1>1 AL 23</ep1.1>
			<id1.1>f29c60816544471277b53a17c475568feedacfda</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/IyF8eBVlGlCz40535CQHSAbdb6.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/1cFc8B7htOArpPq3LsrI3BCE7fE.jpg</fanart.2>
			<ep2.1>1 AL 23</ep2.1>
			<id2.1>b14732c9acea0afa9e04daee96f9a13c03e9d607</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/nBUpU7OLsK84GkN8XeHadLdtK0Y.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/bWz3EnJ6DN9hYa7xdKOKdpB7zmT.jpg</fanart.3>
			<ep3.1>1 AL 22</ep3.1>
			<id3.1>bf60b41ebef4b0e7a29ee9917d423557ad9d5c25</id3.1>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/xcJtbbCsaDHH9aE89R2Uy70VdGo.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/8cBw6OOrYe6ThGKu50lodFlL8DN.jpg</fanart.4>
			<ep4.1>1 AL 22</ep4.1>
			<id4.1>ceb8256af595a429e8b3bba55466a71b2232e185</id4.1>
		</t4>
		<t5>
			<thumb.5>https://www.themoviedb.org/t/p/original/zWq6Jtma8iyXTn0YnxPKXocpJgW.jpg</thumb.5>
			<fanart.5>https://www.themoviedb.org/t/p/original/oOZxuwKLJFAWzmqu2FtqYkmOLy7.jpg</fanart.5>
			<ep5.1>1 AL 22</ep5.1>
			<id5.1>60b30a359f866205d4547aeaf63214c946507f89</id5.1>
		</t5>
		<t6>
			<thumb.6>https://www.themoviedb.org/t/p/original/1VMDrwWNdLlU81dA0oCF7EP8UAA.jpg</thumb.6>
			<fanart.6>https://www.themoviedb.org/t/p/original/ftXlKlQjyApf1dDRYLMm2iRv27S.jpg</fanart.6>
			<ep6.1>1 AL 22</ep6.1>
			<id6.1>de6ae41a8b92b8699afdb9a5b95193757c69eacf</id6.1>
		</t6>
		<t7>
			<thumb.7>https://www.themoviedb.org/t/p/original/lU8bLhqxUEmTYeMiKeDnnuGSio.jpg</thumb.7>
			<fanart.7>https://www.themoviedb.org/t/p/original/sIUQNfI100w2DtycDEjaeraLXWi.jpg</fanart.7>
			<ep7.1>1 AL 22</ep7.1>
			<id7.1>8acf1b6d6be77e9603ddfc44aa7866a01a022999</id7.1>
		</t7>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE GREAT</title>
		<info>(2020)  2 temporadas. 20 episodios. Serie que relata el ascenso al poder de Catalina la Grande (Catalina II de Rusia), que fue emperatriz de Rusia durante 34 años, desde el 1762 hasta su muerte a los 67 años.</info>
		<year>2020</year>
		<genre>Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/85oRnKwJIicUlLNFhYlY3nmEimX.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/egNaTb0VftKz1QK2mFpY2yAdJTC.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/85oRnKwJIicUlLNFhYlY3nmEimX.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/egNaTb0VftKz1QK2mFpY2yAdJTC.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>6ec9f21d24b2d26bb34464eab80e60dec3b72b65</id1.1>
			<ep1.2>4 LA BARBA</ep1.2>
			<id1.2>bb38976558d031893bd7a24c22ad13c8614254ba</id1.2>
			<ep1.3>5 AL 10</ep1.3>
			<id1.3>27aca39f38543f4f5c027a0571e1edb450138e22</id1.3>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/43S9yjW9IVJRCLDo2rBte0rIV4d.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/2K84O2cr0jRH7ROeeHJ3zbTR8Uf.jpg</fanart.2>
			<ep2.1>1 AQUÍ MANDO YO</ep2.1>
			<id2.1>ce892f69766bb4d85624d3efac439b700e156aa7</id2.1>
			<ep2.2>2 IDIOTA</ep2.2>
			<id2.2>97661bcd4b0bba918179f0f1d26ffc5cde304df2</id2.2>
			<ep2.3>3 POR FIN SOLOS</ep2.3>
			<id2.3>e0dc58fd6039e07edc7369684097aa6c12bba1ba</id2.3>
			<ep2.4>4 EL ALMUERZO DEL DIABLO</ep2.4>
			<id2.4>ff5f2192bf5bbcd89b56c890ae24f90091bc36ba</id2.4>
			<ep2.5>5 INSTINTOS ANIMALES</ep2.5>
			<id2.5>01ae7ce56c301090aa5234b98b900e0562faa62d</id2.5>
			<ep2.6>6 UNA SIMPLE BUFONADA</ep2.6>
			<id2.6>98330256364251a0d4446c520e88617edb7e5740</id2.6>
			<ep2.7>7 GRAPADORA</ep2.7>
			<id2.7>b37d50278fd1583d6c4c53986f6d1c2e407ad1d0</id2.7>
			<ep2.8>8 SIETE DIAS</ep2.8>
			<id2.8>6d692982038cbb4d101275a18ee62424b0639d2d</id2.8>
			<ep2.9>9 TEMPORADA DE NUECES</ep2.9>
			<id2.9>06301ab2654ef0a56cea37fb88f3e387708dab0b</id2.9>
			<ep2.10>10 CASAMIENTO</ep2.10>
			<id2.10>47110809471e26fedd79df5e879111b6492e7f93</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE HOT ZONE</title>
		<info>(2019)  2 temporadas. 12 episodios. En 1989, un nuevo brote de ébola aparece en Estados Unidos. Los científicos del ejército arriesgaron sus vidas para combatir esta amenaza. Lucharon contra un enemigo invisible que podía matarlos con solo tocarlos... Inspirada en hechos reales, esta es la historia de un grupo de gente que hizo todo lo posible para frenar la expansión del virus más letal de la Tierra.</info>
		<year>2019</year>
		<genre>Thriller. Ciencia ficción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/x0ia6NxbbbFcq5gv0u19jVHxZkO.jpg</thumb>
		<fanart>https://i.imgur.com/mdMeQBg.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/73w8Q1aY0XkRJyyy43sZ02tTcra.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/npCaBQi941PRhHlUEWPws4qi10U.jpg</fanart.1>
			<ep1.1>1 LA LLEGADA</ep1.1>
			<id1.1>4e0f44e499858de7458c265bc144a2d795769da0</id1.1>
			<ep1.2>2 CELULA H</ep1.2>
			<id1.2>3898014819f98a9461373c9c57f63369412d0a04</id1.2>
			<ep1.3>3 AL 4</ep1.3>
			<id1.3>02b10dfead56568972e6e68bd47275f6f7b2fed7</id1.3>
			<ep1.4>5 CUARENTENA</ep1.4>
			<id1.4>4b62928695b91d665dbdff4d284da71870039f0a</id1.4>
			<ep1.5>6 ESCONDIDO</ep1.5>
			<id1.5>20646c2577624160a605c88cd30786cefb4712a8</id1.5>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/6CSPieuzXvp4gcSSPZzuUwBXjaP.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/yts6QpUTEhE4pDw0PHgILTkyY4f.jpg</fanart.2>
			<ep2.1>1 AGUILA NOBLE</ep2.1>
			<id2.1>79e6c26c3acca04c2341d6d301226d195a4cb51f</id2.1>
			<ep2.2>2 CHIMENEA DEL INFIERNO</ep2.2>
			<id2.2>de69e26b721c3082f227faf117b6cf95d95de747</id2.2>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE INVESTIGATION</title>
		<info>(2020) 6 episodios. Copenhague, agosto de 2017. La policía recibe la denuncia de desaparición de una periodista sueca llamada Kim Wall. Lo último que se sabe de ella es que subió a bordo de un submarino casero con la intención de entrevistar a su constructor y propietario. Pocas horas después, encuentran el submarino hundido y consiguen rescatar a su capitán, a quien interrogan sobre los hechos. Pero no hay rastro de la periodista ni pruebas concluyentes, y las declaraciones son contradictorias. Jens Møller, jefe de la unidad de homicidios de la policía, se hace cargo del retorcido caso, que no para de complicarse a cada paso. Una investigación que avanza bajo la presión de un fiscal que necesita pruebas para construir un caso sólido, unos padres en busca de respuestas y unos medios de comunicación hambrientos de titulares.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://i.imgur.com/mFh9O46.jpg</thumb>
		<fanart>https://i.imgur.com/lWXAcU3.jpg</fanart>
		<t1>
			<thumb.1>https://i.imgur.com/mFh9O46.jpg</thumb.1>
			<fanart.1>https://i.imgur.com/mFh9O46.jpg</fanart.1>
			<ep1.1>1 AL 2</ep1.1>
			<id1.1>f5dfe146cf326a52fc6ec3575ad057dd38dd027e</id1.1>
			<ep1.2>3 AL 4</ep1.2>
			<id1.2>503568547179500aef09b4a2002fd656abc595fc</id1.2>
			<ep1.3>5 AL 6</ep1.3>
			<id1.3>bb7f43603b9215dfcc603f162075b8cdb8a92cea</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE MANDALORIAN</title>
		<info>(2019) 2 temporadas. 16 episodios. Ambientada tras la caída del Imperio y antes de la aparición de la Primera Orden, la serie sigue los pasos de Mando, un cazarrecompensas perteneciente a la legendaria tribu de los mandalorianos, un pistolero solitario que trabaja en los confines de la galaxia, donde no alcanza la autoridad de la Nueva República.</info>
		<year>2019</year>
		<genre>Ciencia ficción. Aventuras. Acción</genre>
		<thumb>https://image.tmdb.org/t/p/original/BbNvKCuEF4SRzFXR16aK6ISFtR.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/o7qi2v4uWQ8bZ1tW3KI0Ztn2epk.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/BbNvKCuEF4SRzFXR16aK6ISFtR.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/iN5aM3P2wbhv5S9I9Tc3WhOCNal.jpg</fanart.1>
			<ep1.1>1 AL 8 (4K)</ep1.1>
			<id1.1>344f688d372356fea087e12beaa80b40c9239ae9</id1.1>
			<ep1.2>1 EL MANDALORIANO</ep1.2>
			<id1.2>89094c7dde19214ea38909f89020e38df35d34f8</id1.2>
			<ep1.3>2 EL NIÑO</ep1.3>
			<id1.3>1c15ef5f32101f2b45dd2e6e029fa00397710c92</id1.3>
			<ep1.4>3 EL PECADO</ep1.4>
			<id1.4>3665b8e292808fff361d9cd5186e182fafbcc11f</id1.4>
			<ep1.5>4 SANTUARIO</ep1.5>
			<id1.5>5df7b5ef2d7a388b7af2e18b3a4c9636f91cb3a8</id1.5>
			<ep1.6>5 EL PISTOLERO</ep1.6>
			<id1.6>67635a1dc41a55a9b481dd3410d00400e1046c96</id1.6>
			<ep1.7>6 EL PRISIONERO</ep1.7>
			<id1.7>b85c4cfc29db81cf2759c2b3c5b06af52beebed4</id1.7>
			<ep1.8>7 EL AJUSTE DE CUENTAS</ep1.8>
			<id1.8>91eed492581dd571f0bd054140b7ec4d9ea87405</id1.8>
			<ep1.9>8 REDENCION</ep1.9>
			<id1.9>43e04ac26d49de8139eafb43c46135d5a9efa146</id1.9>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/b8cs9DzhxRQLQ7TvfLihYbAC6fg.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/9zcbqSxdsRMZWHYtyCd1nXPr2xq.jpg</fanart.2>
			<ep2.1>1 AL 8 (4K)</ep2.1>
			<id2.1>3b0154860e776aee5df34e0384bd67e9ad66aaab</id2.1>
			<ep2.2>1 EL MARSHAL</ep2.2>
			<id2.2>224c514f9475016d5ba273688e0e8db27aba5bc1</id2.2>
			<ep2.3>2 LA PASAJERA</ep2.3>
			<id2.3>32506e7c5312920991e37f5e1ee9ad94205d8bf9</id2.3>
			<ep2.4>3 LA HEREDERA</ep2.4>
			<id2.4>2cfc114f00aa7e88465e10484a1cc1c4540aa78e</id2.4>
			<ep2.5>4 EL ASEDIO</ep2.5>
			<id2.5>5d5ef981a25c6ad3e58c9b96d549c5d6c7ff7078</id2.5>
			<ep2.6>5 EL JEDI</ep2.6>
			<id2.6>0dddec1ebf3baa5e76a9cec335bd1af4d1ac6e79</id2.6>
			<ep2.7>6 LA TRAGEDIA</ep2.7>
			<id2.7>f24263cf29c63ca46da18f39829e310fb1d2249e</id2.7>
			<ep2.8>7 EL CREYENTE</ep2.8>
			<id2.8>4291cf24dfae2e7e8e5bf6008dc5278393661a3e</id2.8>
			<ep2.9>8 EL RESCATE</ep2.9>
			<id2.9>ac490057386d359424ed5952e5cd5ac164bd4060</id2.9>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE NEVERS</title>
		<info>(2021) 6 episodios. Un grupo de mujeres de la época victoriana con habilidades inusuales se ven obligadas a hacer frente a una serie de enemigos y llevar a cabo una misión que podría cambiar el destino del planeta.</info>
		<year>2021</year>
		<genre>Ciencia ficción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/v6Xmj8Fy7ZruVTz3y2Po7O0TQh4.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/6x8vW5utEZahp6V3sSWxCTW0mHW.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/v6Xmj8Fy7ZruVTz3y2Po7O0TQh4.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/6x8vW5utEZahp6V3sSWxCTW0mHW.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>4de15959c3fcf0703697fe922d770db514231f96</id1.1>
			<ep1.2>2 EXPOSICION</ep1.2>
			<id1.2>e61940b0eef443ac9fa4da5c0ca2a1c8547c63d0</id1.2>
			<ep1.3>3 IGNICION</ep1.3>
			<id1.3>d10144ed04a3094065234210468949a7ce556036</id1.3>
			<ep1.4>4 COMPROMISO</ep1.4>
			<id1.4>3ad97fefe0bc254762bd423e39b58c59d2882bc0</id1.4>
			<ep1.5>5 COLGADO</ep1.5>
			<id1.5>e6395f324d090dd48cf09c5ad747f364096af2c0</id1.5>
			<ep1.6>6 VERDAD</ep1.6>
			<id1.6>48557a485c437f8ac371f8f946bdafca957e4bc9</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE ONE</title>
		<info>(2021) 8 episodios. Un simple hisopo bucal es todo lo que se necesita. Una prueba de ADN rápida para encontrar tu pareja perfecta, para la que estás creado genéticamente. Una década después de que los científicos descubrieran que todos tienen un gen que comparten con una sola persona, millones se han sometido a la prueba, desesperados por encontrar el amor verdadero en la empresa The One, fundada por la ambiciosa Rebecca (Hannah Ware). Ahora, cinco personas más conocen su Match. Pero incluso las almas gemelas tienen secretos. Y algunos son más impactantes y mortíferos que otros.</info>
		<year>2021</year>
		<genre>Ciencia ficción. Drama. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/klL9vBIRdEeSLtjhn83pCNcVguZ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/fraOZc00RdZ9LuH5UmVMUFaqA3.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/klL9vBIRdEeSLtjhn83pCNcVguZ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/fraOZc00RdZ9LuH5UmVMUFaqA3.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>c5ad122c10608faec8cd9c893e9ebc42a20e5d52</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE PALE HORSE</title>
		<info>(2020) 2 episodios. Basada en la novela homónima de Agatha Christie, narra la historia de Mark Easterbrook, cuyo nombre aparece en una lista que se encuentra dentro del zapato de una mujer muerta. Comienza una investigación sobre cómo y por qué su nombre apareció en la lista que lo lleva a The Pale Horse, el hogar de tres presuntas brujas.</info>
		<year>2020</year>
		<genre>Intriga. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/x2wu80dxUAHVmksi33rST1AbkWE.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/8gYgZeVddiiAvDzanZudU0D5OPv.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/x2wu80dxUAHVmksi33rST1AbkWE.jpg</thumb.1>
			<fanart.1>https://image.tmdborg/t/p/original/8gYgZeVddiiAvDzanZudU0D5OPv.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>c42f8840ea1e2040c1f9d2932124c4b0f8496ccd</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>a1ad25f98dc1772231257ff8b5d1e96b43676918</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE RESPONDER</title>
		<info>(2022) 5 episodios. Chris (Martin Freeman) es un agente de policía de emergencias poco convencional, moralmente comprometido, que afronta una serie de turnos nocturnos en las peligrosas calles de Liverpool. Noche tras noche se enfrenta a la delincuencia, la violencia y la adicción, mientras lucha contra sus demonios personales que amenazan con desestabilizar su trabajo, su matrimonio y su salud mental. Mientras intenta mantenerse a flote tanto personal como profesionalmente, Chris se ve obligado a trabajar con una nueva compañera novata, Rachel. Pronto descubren que la supervivencia en este implacable mundo nocturno dependerá de que se ayuden o se destruyan mutuamente. La tensión crece para Chris cuando decide proteger a una joven drogadicta que huye de un matón por el robo de un importante alijo de cocaína.</info>
		<year>2022</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/9rG7UmjRCtSgHq8QzW07Drdq9HB.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/9fLzWNnsLxYJYVqEJBlVO2CmnRC.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/9rG7UmjRCtSgHq8QzW07Drdq9HB.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/9fLzWNnsLxYJYVqEJBlVO2CmnRC.jpg</fanart.1>
			<ep1.1>1 Episodio</ep1.1>
			<id1.1>c017808dee12facf605ae81e43525c6638fd7fa2</id1.1>
			<ep1.2>2 Episodio</ep1.2>
			<id1.2>8e5288ef739d71202b415c560310e938a0588d26</id1.2>
			<ep1.3>3 Episodio</ep1.3>
			<id1.3>adf797072a6b53cf9a65788194f8a6589cbbd314</id1.3>
			<ep1.4>4 Episodio</ep1.4>
			<id1.4>683d1c30d6d038a392011980f3082b46d76fb653</id1.4>
			<ep1.5>5 Episodio</ep1.5>
			<id1.5>77ad5e462dc4c16182a21668e2d696925a72e185</id1.5>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE SINNER</title>
		<info>(2017) 4 temporadas. 32 episodios. The Sinner es una serie de televisión estadounidense de misterio basada en la novela homónima de Petra Hammesfahr. El detective Harry Ambrose es el encargado de investigar a fondo en cada temporada diferentes crimenes.</info>
		<year>2017</year>
		<genre>Intriga. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/mzQyhOoKxWTFtIKjQfPx2ttYyQQ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/o36ITLqVgZZ4hY1V5aPZPqZzVRx.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/mzQyhOoKxWTFtIKjQfPx2ttYyQQ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/o36ITLqVgZZ4hY1V5aPZPqZzVRx.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>5640cb36e66d8a28122f59e984b971550083206c</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/oxFlfbCuDmgIDEsi0JRfaGeKgQ7.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/cvGZ42YsWHcdIa3rXGuoMqU2xSw.jpg</fanart.2>
			<ep2.1>1 AL 6</ep2.1>
			<id2.1>f3161a50498790c21970fd7239470014ac219aea</id2.1>
			<ep2.2>7 AL 8</ep2.2>
			<id2.2>8068e610639e720ec41baf8a1060b395f4811d71</id2.2>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/kqyo8BQRDRNxuo6RWRmFPFNCPgg.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/lVWku9LX2qdJTtAbRzaCDhhWeTK.jpg</fanart.3>
			<ep3.1>1 AL 2</ep3.1>
			<id3.1>ac0fc37e62a0c221e58cf89eac89b3a837f02e67</id3.1>
			<ep3.2>3 AL 4</ep3.2>
			<id3.2>742b32b8f90d2869772c46fd94f26f1b61b17c22</id3.2>
			<ep3.3>5 AL 6</ep3.3>
			<id3.3>a86b1af2a12b66c9879cf17202c1bcc4fc104e92</id3.3>
		</t3>
		<t4>
			<thumb.4>https://www.themoviedb.org/t/p/original/rmibFGdqOe0kKKhPls0jVOdZCWw.jpg</thumb.4>
			<fanart.4>https://www.themoviedb.org/t/p/original/AhhABp3IjJgb1q00ZSLwBO5VwMB.jpg</fanart.4>
			<ep4.1>1 AL 2</ep4.1>
			<id4.1>5b101eada3d1940e97b1518c0ea52d169dc77e32</id4.1>
			<ep4.2>3 AL 4</ep4.2>
			<id4.2>5b4a8e16f62ed03ba11f79021f5a871a97519a32</id4.2>
			<ep4.3>5 AL 6</ep4.3>
			<id4.3>4c5cab334b7d8671196fc8e2db5b26de8d8a3371</id4.3>
			<ep4.4>7 AL 8</ep4.4>
			<id4.4>40ec86fe6f7cc6b18687eee6c55dc83dddf90636</id4.4>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE SPANISH PRINCESS</title>
		<info>(2019) 2 temporadas. 16 episodios Catalina de Aragón es la princesa de España, a quien le han prometido el trono inglés toda su vida. Ella llega a una Inglaterra azotada por la lluvia con su corte gloriosa y diversa, incluidas sus damas de compañía Lina y Rosa. Cuando su esposo muere repentinamente, el trono parece perdido para Catalina hasta que pone su mirada en el nuevo heredero, el futuro rey Enrique VIII.</info>
		<year>2019</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/1CcnnDZf2ievdquCa2Jh8yIBtPD.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/fqW2YVp0JspWG9DDdBy4WVtOVxH.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/1CcnnDZf2ievdquCa2Jh8yIBtPD.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/atLyBlRj1lEcDnDOvmfoazG9HAA.jpg</fanart.1>
			<ep1.1>1 EL NUEVO MUNDO</ep1.1>
			<id1.1>f8bc1b245b2faa65899e2cfd6995680ec3948692</id1.1>
			<ep1.2>2 DELIRIO</ep1.2>
			<id1.2>884596866b7222c53dd3f102fd1f01d44ad37c60</id1.2>
			<ep1.3>3 UN PLAN AUDAZ</ep1.3>
			<id1.3>9ae5e4fc0a6b6cc06c8986f2f314fde284b34189</id1.3>
			<ep1.4>4 LA LUCHA POR HARRY</ep1.4>
			<id1.4>7179acdd2e472208b116af5f4520d7fdcaf0f117</id1.4>
			<ep1.5>5 CORAZON CONTRA DEBER</ep1.5>
			<id1.5>5bc9fb099e6194a9be66ec836d76c9a5204882fe</id1.5>
			<ep1.6>6 UN SECUESTRO RESPETUOSO</ep1.6>
			<id1.6>861320eaf343b350e6ebe2b85ae65e35569dca15</id1.6>
			<ep1.7>7 TODO ESTA PERDIDO</ep1.7>
			<id1.7>1bb589779ae05c65e96b57539def64255f56ed6c</id1.7>
			<ep1.8>8 DESTINO</ep1.8>
			<id1.8>d970861308ca014b4b4681d6a389d31706c0e163</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/4Ec0jIkctpiyKuywPSRj4Pd2jEl.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/eqOAW52GkgbMDGIzpqlz8zwTIiO.jpg</fanart.2>
			<ep2.1>1 CAMELOT</ep2.1>
			<id2.1>ac2455157ad691f01e80cde8c7cda4cea1c783cb</id2.1>
			<ep2.2>2 FLODDEN</ep2.2>
			<id2.2>9738356cd91c59b049657a3bb9427dee386d1fb0</id2.2>
			<ep2.3>3 DOLOR</ep2.3>
			<id2.3>cc923614ac545b9f16206f475d7753c4d7bb249f</id2.3>
			<ep2.4>4 LA OTRA MUJER</ep2.4>
			<id2.4>c0a17f5123ea2bc7070949791031383de077c666</id2.4>
			<ep2.5>5 PLAGA</ep2.5>
			<id2.5>4e4ce01499589cb37cd33678745c7dc95e82779f</id2.5>
			<ep2.6>6 EL CAMPO DEL PAÑO DE ORO</ep2.6>
			<id2.6>c3e78e2d6f558f19e175133c9660330105c92e3a</id2.6>
			<ep2.7>7 FE</ep2.7>
			<id2.7>88eb56d079dba87b9a020d5e8b31218bc77bbf72</id2.7>
			<ep2.8>8 PAZ</ep2.8>
			<id2.8>f6afcd1580c172c93f84155add7a06d730c85991</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE STAIRCASE</title>
		<info>(2022) 8 episodios. El escritor Michael Peterson, afligido por la muerte de su esposa Kathleen, que cayó por una escalera, es acusado de asesinarla.</info>
		<year>2022</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://www.themoviedb.org/t/p/original/jiAcdCyf1UbWYBImcNtVeLW0pRj.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/prH8hQPN28NPn2eJyyRaMHcOlAS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/jiAcdCyf1UbWYBImcNtVeLW0pRj.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/prH8hQPN28NPn2eJyyRaMHcOlAS.jpg</fanart.1>
			<ep1.1>1 911</ep1.1>
			<id1.1>f9aa52047ca8d627b01afb1e44c7b40d2b1c0c41</id1.1>
			<ep1.2>2 QUIROPTEROS</ep1.2>
			<id1.2>a72016a5fe7c00c31f21bf5c427f60d43df59358</id1.2>
			<ep1.3>3 EL GRAN DESARMADOR</ep1.3>
			<id1.3>e4c7d24028bf06b3921e0b7124f7637d3fbbe960</id1.3>
			<ep1.4>4 SENTIDO COMUN</ep1.4>
			<id1.4>7ddb20767acbaf3972eab08b8cbd1c9f31ebadfc</id1.4>
			<ep1.5>5 EL CORAZON LATENTE</ep1.5>
			<id1.5>7897595dcc7a201548b7b91e506cdddd1a6a67ec</id1.5>
			<ep1.6>6 EL SUEÑO DE LA RAZON</ep1.6>
			<id1.6>fa57d32d69704a63e28ca7b6b8b62589db86a710</id1.6>
			<ep1.7>7 BUSCA Y LO HARAS</ep1.7>
			<id1.7>303b19dc2e7aa264cca60c59a47059978b8726ef</id1.7>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE TERROR</title>
		<info>(2018) 2 temporadas. 20 episodios. En el año 1847, un equipo de expedición de la Marina Real británica es enviado al Ártico, en busca del Paso del Noroeste. Pero una vez allí es atacado por un misterioso depredador que acecha a los barcos y a sus tripulaciones en un juego desesperado de incertidumbre y supervivencia.  THE TERROR: INFAMY. Ambientada durante la Segunda Guerra Mundial, la serie se centra en una serie de extraños y terribles acontecimientos que tuvieron lugar en una comunidad norteamericana de origen japonés, y en la aventura que protagoniza un hombre para entender y combatir a la malévola entidad responsable de los hechos</info>
		<year>2018</year>
		<genre>Aventuras. Fantástico.Terror</genre>
		<thumb>https://image.tmdb.org/t/p/original/qh57Yx2jBjg5KXKTpBRQVVv04b9.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/vCFSNLP4glLfvT3RJ98iYTNPpZ3.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/qh57Yx2jBjg5KXKTpBRQVVv04b9.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/vCFSNLP4glLfvT3RJ98iYTNPpZ3.jpg</fanart.1>
			<ep1.1>1 EL TODO POR EL TODO</ep1.1>
			<id1.1>97a6195ba03ed00208e3f38efc2f2239f2dce23e</id1.1>
			<ep1.2>2 GORE</ep1.2>
			<id1.2>ceba4af27183c0772aff0e6826fdc14a46bbdcd5</id1.2>
			<ep1.3>3 EL CISNE DE PLATA</ep1.3>
			<id1.3>9503bb9c4c10c4f7d117b6ff84e6a71dae47253d</id1.3>
			<ep1.4>4 CASTIGADO COMO UN NIÑO</ep1.4>
			<id1.4>baeb8dbfb8bd1ea3de8dff13a9b4a2a153f46372</id1.4>
			<ep1.5>5 EL PRIMERO QUE LE DE GANA</ep1.5>
			<id1.5>8afaaffcc119b7e3fb207a60f926f3dbe168657a</id1.5>
			<ep1.6>6 PIEDAD</ep1.6>
			<id1.6>55cf37e5503096d95cdbc92dd1f7a4bb35b59f34</id1.6>
			<ep1.7>7 EL HORROR DE LA CENA</ep1.7>
			<id1.7>c9cba6e619448bb704f26295799807345e41b933</id1.7>
			<ep1.8>8 CAMPAMENTO TERROR DESPEJADO</ep1.8>
			<id1.8>90e6598a92aa707fbab956f4235d412d46bfbc40</id1.8>
			<ep1.9>9 EL MAS EL MAR EL ABIERTO MAR</ep1.9>
			<id1.9>afc175834af6735dd0da9685a3e0c521323d1e96</id1.9>
			<ep1.10>10 YA NO ESTAMOS</ep1.10>
			<id1.10>376bd31526f06ccc303bec04fe14e82dce3c6946</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/j8lakGwVuPonS2wKNePFy35QbT.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/ssyZgYScV9eClakZDwu4sPrgMmy.jpg</fanart.2>
			<ep2.1>1 UN GORRION EN UN NIDO DE GOLONDRINAS</ep2.1>
			<id2.1>5566db5bdb3eeb10f103cdd9587bd0ffb5aa0789</id2.1>
			<ep2.2>2 TODOS LOS DEMONIOS SIGUEN EN EL INFIERNO</ep2.2>
			<id2.2>c44be558f976173f641159f2de2df3d83be7e5e0</id2.2>
			<ep2.3>3 GAMAN</ep2.3>
			<id2.3>a59a852b82fdb68b9baf84615c32f932422e3957</id2.3>
			<ep2.4>4 LOS DEBILES SON COMIDA</ep2.4>
			<id2.4>fa1c7dd8e438165c34691aca821a0733775f1816</id2.4>
			<ep2.5>5 ESTALLAR COMO COMO PERLA</ep2.5>
			<id2.5>a4647dfb07b8532136a6475f7371669f365b0bc2</id2.5>
			<ep2.6>6 TAIZO</ep2.6>
			<id2.6>afc54a16cccd360bed2f050ffcb42b987b3f1c2f</id2.6>
			<ep2.7>7 MI MUNDO PERFECTO</ep2.7>
			<id2.7>fa2fe07fa3664320f9b610065c4393c95bd0dda8</id2.7>
			<ep2.8>8 MI NIÑO</ep2.8>
			<id2.8>a1393d0aefdf89384b13322c919f42715da82843</id2.8>
			<ep2.9>9 MI MUNDO PERFECTO</ep2.9>
			<id2.9>c24a63ed582f7ff9e28cc6b455b2893575e08e7d</id2.9>
			<ep2.10>10 EN EL MAS ALLA</ep2.10>
			<id2.10>3a98083a135925b058ff103b8cfee5d923b5d24f</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE UMBRELLA ACADEMY</title>
		<info>(2019) 2 temporadas. 20 episodios. Narra la vida de los miembros separados de una familia de superhéroes llamada Umbrella Academy -Monocle, Spaceboy, Kraken, Rumor, Séance, Number Five, Horror y White Violin-, quienes trabajan juntos para resolver la misteriosa muerte de su padre mientras se enfrentan a muchos conflictos debido a sus personalidades y habilidades divergentes.</info>
		<year>2019</year>
		<genre>Fantástico. Acción</genre>
		<thumb>https://image.tmdb.org/t/p/original/uYHdIs5O8tiU5p6MvUPd2jElOH6.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/5VQx3WcYdwKiAHorLG71IkuovwQ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/5VQx3WcYdwKiAHorLG71IkuovwQ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/mE3zzMkpP8yqlkzdjPsQmJHceoe.jpg</fanart.1>
			<ep1.1>1 SOLO NOS VEMOS EN BODAS Y FUNERALES</ep1.1>
			<id1.1>4f9d4232c8bb1a6f85ae0091063fd93526912e43</id1.1>
			<ep1.2>2 CORRE, CHICO, CORRE</ep1.2>
			<id1.2>fa18e58e7528bec6f8e7c12fd947db4927904591</id1.2>
			<ep1.3>3 EXTRA ORDINARIA</ep1.3>
			<id1.3>3b8bfec70a872fb551d56de0f836257eb5e18099</id1.3>
			<ep1.4>4 HOMBRE EN LA LUNA</ep1.4>
			<id1.4>91e0ed01a188c64d37e1a82f25612d52575d9053</id1.4>
			<ep1.5>5 NUMERO CINCO</ep1.5>
			<id1.5>33765155e522c5bec08773f887ce26feb8975695</id1.5>
			<ep1.6>6 EL DIA QUE NO OCURRIO</ep1.6>
			<id1.6>8c27adc42a2fb1f50606dbfb769cafdb9e845a10</id1.6>
			<ep1.7>7 EL DIA QUE SI OCURRIO</ep1.7>
			<id1.7>89eb918bcf32f338a04fe38690ad5ebfc3b312b4</id1.7>
			<ep1.8>8 CORRE EL RUMOR</ep1.8>
			<id1.8>47d6ce51757af3308aec457dc5c0458e5621b4eb</id1.8>
			<ep1.9>9 CAMBIOS</ep1.9>
			<id1.9>02c048d761e687604b671a11df54f73a6bb7b3ff</id1.9>
			<ep1.10>10 EL VIOLIN BLANCO</ep1.10>
			<id1.10>99ec504f31374fa47845cf45e64ea838f92b6734</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/scZlQQYnDVlnpxFTxaIv2g0BWnL.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/bntDWbCcZJBmMlC8Gwg64oOx8zm.jpg</fanart.2>
			<ep2.1>1 DE VUELTA AL PRINCIPIO</ep2.1>
			<id2.1>ed9226bfae2399f45d7c6b79521d04237161c427</id2.1>
			<ep2.2>2 LA GRABACION FRANKEL</ep2.2>
			<id2.2>554f06c8a9c2d4f7a1d7ca41d92dc4fb8eccffe8</id2.2>
			<ep2.3>3 LOS SUECOS</ep2.3>
			<id2.3>ceb264382d57bddcb779baa8f3e6739c94de65e1</id2.3>
			<ep2.4>4 LOS MAJESTIC 12</ep2.4>
			<id2.4>21387d6df08ac11dd07ecf9b2fc47ceb8d88303e</id2.4>
			<ep2.5>5 VALHALA</ep2.5>
			<id2.5>9518d601af333177438f556d39b03e9dfe3ed428</id2.5>
			<ep2.6>6 UNA CENA LIGERA</ep2.6>
			<id2.6>81809390266a0664e9aa8ce2df0d2f6908cc7306</id2.6>
			<ep2.7>7 OJO POR OJO</ep2.7>
			<id2.7>5d99266a6c2d0632acfa91f31edbb4328995cbb9</id2.7>
			<ep2.8>8 LAS SIETE FRASES</ep2.8>
			<id2.8>0d2f30d14164f99c686a5973c67531f95679f678</id2.8>
			<ep2.9>9 743</ep2.9>
			<id2.9>b9642920e0512a1857612c37af36e6f5e0e5ff36</id2.9>
			<ep2.10>10 EL FIN DE ALGO</ep2.10>
			<id2.10>d5e9513ca50ce741d7700ad77d3c7ea44f0ac055</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE UNDOING</title>
		<info>(2020) 6 episodios. Grace Sachs (Nicole Kidman) es una mujer que ha cumplido todos los sueños de su vida: es una psicóloga que está a punto de publicar su primer libro, tiene un marido devoto, Jonathan Fraser (Hugh Grant), oncólogo pediatra de prestigio, y un hijo que va a uno de los colegios más importantes de Nueva York. Sin embargo, pocas semanas antes de que su libro se publique, la vida de Grace cambia por completo. Una trágica muerte y la desaparición de su marido Jonathan alteran drásticamente su forma de ver el mundo, y la obligan a comenzar de cero. Su única preocupación ahora es garantizar que su hijo pueda vivir la vida que se merece. Por desgracia para ambos, la tarea es mucho más complicada de lo que parece</info>
		<year>2020</year>
		<genre>Thriller. Intriga. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/evyguEm9eUWUenXbxGnWyKG8brq.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/tJjwEzTglpxoWJXwEbZlKLppMhr.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/evyguEm9eUWUenXbxGnWyKG8brq.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/tJjwEzTglpxoWJXwEbZlKLppMhr.jpg</fanart.1>
			<ep1.1>1 LA RUINA</ep1.1>
			<id1.1>80f63b035663786d526de103a4c517e3d76d96d3</id1.1>
			<ep1.2>2 LA DESAPARICION</ep1.2>
			<id1.2>9d521cf7a15c1221d9cc0639f29ebbe58f5c76ff</id1.2>
			<ep1.3>3 NO HAGAS DAÑO</ep1.3>
			<id1.3>8e45300e49e176d6f0ecbf1d5192403a6fccb35a</id1.3>
			<ep1.4>4 NO VEAS EL MAL</ep1.4>
			<id1.4>0e407f4ce5f657fc07bdfe18c8d3c1d42ee814ad</id1.4>
			<ep1.5>5 JUICIO POR FURIA</ep1.5>
			<id1.5>76f38aa26e0b74fd54fb04959e0dc2b1129f7e76</id1.5>
			<ep1.6>6 LA MALTIDA VERDAD</ep1.6>
			<id1.6>66737211fda3e9c30434ce678cabf6efb1e533c6</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE WALKING DEAD</title>
		<info>(2010) 11 temporadas. 177 episodios. Tras un apocalipsis zombie, un grupo de supervivientes, dirigidos por el policía Rick Grimes, recorre los Estados Unidos para ponerse a salvo. Aunque el leit motiv de la serie -cuyo episodio piloto fue dirigido y escrito por Frank Darabont- sea el apocalipsis zombie, la narración se centra más en las relaciones entre los personajes, su evolución y comportamiento en las situaciones críticas. Entre ellos destaca el ayudante del sheriff de un pueblo de Georgia que entró en coma durante la catástrofe.</info>
		<year>2010</year>
		<genre>Terror. Ciencia ficción</genre>
		<thumb>https://image.tmdb.org/t/p/original/2RfMqVgtzWREOZcZufv07W6ouVm.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/fFwBxjmUATOGSceZlG2xBZ8qNSE.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/bM1qlZdSrXSSqhTVgXbwJYoGV77.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/eN6bS41VECYgRc6pITyHjwlMqau.jpg</fanart.1>
			<ep1.1>1 AL 6 (1080)</ep1.1>
			<id1.1>8ecbd3fa691ca8ff0c9d6bb27700c7e057bd5b75</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/nLMEMCqWiz7QVG1AChapIGCEyEh.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/8ClaFpA23sUeLFNRsnKwSXthHVc.jpg</fanart.2>
			<ep2.1>1 AL 13 (1080)</ep2.1>
			<id2.1>17623012dd790ab2045f17031b1d34dc7bde287a</id2.1>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/cGrhsd3inKcZZndfQ3tdfuDFgFQ.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/8i9OIrueYpRRRy0bbRTtvNNZdv6.jpg</fanart.3>
			<ep3.1>1 AL 16 (1080)</ep3.1>
			<id3.1>76d5b1d620149aad4594ae1ee2cdf35711a4d087</id3.1>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/rHQDAd1t5x7QEeefPvBEGmvIKUG.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/s3pSmt4w1WzkLLQfy9CnOLbmTcB.jpg</fanart.4>
			<ep4.1>1 AL 16 (1080)</ep4.1>
			<id4.1>1574439f94f6d8f1b3d667910f2645b86b490d68</id4.1>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/fJdXRjLxmUPMNUwA5e8iHTQdYkQ.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/fEA544oMSmy6bfJY058ui0f7xYx.jpg</fanart.5>
			<ep5.1>1 AL 16 (1080)</ep5.1>
			<id5.1>d1827d6fe96a0e8bf0b224186f6707016dd4d7fb</id5.1>
			<ep5.2>1 NO HAY SANTUARIO</ep5.2>
			<id5.2>c04a75f23124c2afaaf75eca0a613704e3a6d85d</id5.2>
			<ep5.3>2 DESCONOCIDOS</ep5.3>
			<id5.3>830e988d7ecade7920c8b81fa8dac1ecb186890b</id5.3>
			<ep5.4>3 CUATRO PAREDES Y UN TECHO</ep5.4>
			<id5.4>55f10c84fd52eaf46b8af47622c90ee274a7d4ec</id5.4>
			<ep5.5>4 LA CIUDAD DE LAS MESAS AUTOPSIAS</ep5.5>
			<id5.5>e0a7d4e24ce03547da4a235ae041fa027edbffe8</id5.5>
			<ep5.6>5 AUTOAYUDA</ep5.6>
			<id5.6>b3f8eefb4b04e677018337f6bd9b34cf638e7058</id5.6>
			<ep5.7>6 CONSUMIDOS</ep5.7>
			<id5.7>9ce2acef0df135044ecba5094fbc812ea6f1028c</id5.7>
			<ep5.8>7 CRUZADOS</ep5.8>
			<id5.8>17bdfb51d586262fb001e57abe26426fa8a67f00</id5.8>
			<ep5.9>8 CODA</ep5.9>
			<id5.9>882afc2b340ed0dbee918553f17d7d6df8776d9f</id5.9>
			<ep5.10>9 LO QUE PASO Y LO QUE ESTA OCURRIENDO</ep5.10>
			<id5.10>4282798a8714edcc200196e91a11595a365f17b8</id5.10>
			<ep5.11>10 ELLOS</ep5.11>
			<id5.11>a85fc4ec74b1e315b28670392b35708a04500812</id5.11>
			<ep5.12>11 LAS DISTANCIAS</ep5.12>
			<id5.12>72d9e5e851a577950ceb091738bb00b23af153e6</id5.12>
			<ep5.13>12 RECUERDA</ep5.13>
			<id5.13>d5a241ab0adf62e9bd9c47ab451fc6875f850dcf</id5.13>
			<ep5.14>13 OLVIDA</ep5.14>
			<id5.14>c7450d4c8a738235f52c8892ddc827b6b04a1b61</id5.14>
			<ep5.15>14 DESGASTE</ep5.15>
			<id5.15>ecfa4efad20fa34539e2fb53bcd2453fd833ff2e</id5.15>
			<ep5.16>15 INTENTALO</ep5.16>
			<id5.16>15e2151898d870a707cf767f524203bc3476954a</id5.16>
			<ep5.17>16 VENCE</ep5.17>
			<id5.17>c5eadbade3eb225b746298d9160df75e8c266584</id5.17>
		</t5>
		<t6>
			<thumb.6>https://image.tmdb.org/t/p/original/uNWCBdqPrXe7i2CWqC4sDItSM1A.jpg</thumb.6>
			<fanart.6>https://image.tmdb.org/t/p/original/78A09OrR2CdnJX8pQA7H1x0VswC.jpg</fanart.6>
			<ep6.1>1 AL 16 (1080)</ep6.1>
			<id6.1>ec2e3ab57439db8521281c8a3c38211baef62ab5</id6.1>
			<ep6.2>1 LA PRIMERA VEZ,OTRA VEZ</ep6.2>
			<id6.2>b2a11129c61aa3792b8eda74c86e4d840197466d</id6.2>
			<ep6.3>2 JSS</ep6.3>
			<id6.3>c41ac1d23ec92f3f4cf168c665584e3e4b32adb9</id6.3>
			<ep6.4>3 GRACIAS</ep6.4>
			<id6.4>29bad8982cb52d6d39ca3ddff3b8d60c6ef1d35a</id6.4>
			<ep6.5>4 AQUI NO ES AQUI</ep6.5>
			<id6.5>f8eaa02a380385d0bb06a5c70718ef33c1f9979f</id6.5>
			<ep6.6>5 AHORA</ep6.6>
			<id6.6>be32f4352bb4e24e20a0f2c97aa670a8c30cfcee</id6.6>
			<ep6.7>6 SIEMPRE RESPONSABLE</ep6.7>
			<id6.7>84ddc8644f8b008dc6618bae3ab90e46ecba2af9</id6.7>
			<ep6.8>7 OJO AVIZOR</ep6.8>
			<id6.8>8dea8b803b1d695e8e4b3c7b9a747ce89292dc11</id6.8>
			<ep6.9>8 DEL PRINCIPIO AL FIN</ep6.9>
			<id6.9>8aa2c153ecc61575918f3a88086164efe2d3b1d7</id6.9>
			<ep6.10>9 SIN SALIDA</ep6.10>
			<id6.10>a0d6de60663b9c31eeb4c25adbe518cdd8f5b1bd</id6.10>
			<ep6.11>10 EL NUEVO MUNDO</ep6.11>
			<id6.11>49b8e527f9a45007d9efe781f31d650b49159050</id6.11>
			<ep6.12>11 SOLTAR AMARRAS</ep6.12>
			<id6.12>5161e132b083d1251258a2623e80df728217a7e6</id6.12>
			<ep6.13>12 AUN NO ES MAÑANA</ep6.13>
			<id6.13>a68a7ca57c9e01150e6eff888c63897a630910ec</id6.13>
			<ep6.14>13 EL MISMO BARCO</ep6.14>
			<id6.14>0f6745a0152bea5cb4087f220b4d465219bfc672</id6.14>
			<ep6.15>14 EL DOBLE DE LEJOS</ep6.15>
			<id6.15>0ef3427fab795ad15aa2904c961ddb32f88cfe56</id6.15>
			<ep6.16>15 AL ESTE</ep6.16>
			<id6.16>dfd4e979529a01442de9b4941077103349a99eda</id6.16>
			<ep6.17>16 EL ULTIMO DIA DE LA TIERRA</ep6.17>
			<id6.17>9bae2e15b84edb1d85f44b0f25b5b188c22b44fc</id6.17>
		</t6>
		<t7>
			<thumb.7>https://image.tmdb.org/t/p/original/tPQZkQCikCvLhjuyOMDLtBvVJgZ.jpg</thumb.7>
			<fanart.7>https://image.tmdb.org/t/p/original/fPJbTqEOCHGemXDxCJDAnbe777R.jpg</fanart.7>
			<ep7.1>1 AL 16 (1080)</ep7.1>
			<id7.1>29d5231882cd7cf747bc84bcc137a586d3385d3d</id7.1>
			<ep7.2>1 LLEGARA UN DIA EN QUE NO ESTARAS</ep7.2>
			<id7.2>0a8eb76c7fa2d7b8f83cf2f380a0fe1457ecdd52</id7.2>
			<ep7.3>2 EL POZO</ep7.3>
			<id7.3>e0e4000a6a0f2d5168fd14c8915094b3d7e55c0f</id7.3>
			<ep7.4>3 LA CELDA</ep7.4>
			<id7.4>d7dc872bcae22fdbe1184cc588fd343e5431ec1e</id7.4>
			<ep7.5>4 SERVICIO</ep7.5>
			<id7.5>62d06a26abb060a37b41aaae7b32f36dbf919349</id7.5>
			<ep7.6>5 BUSCAVIDAS</ep7.6>
			<id7.6>0b75cb7b3e6e98576ba9c42efe706a0560100296</id7.6>
			<ep7.7>6 LA PROMESA</ep7.7>
			<id7.7>0a6f81b4519ef01a5440cb47de109cfd50a204b1</id7.7>
			<ep7.8>7 CANTAME UNA CANCION</ep7.8>
			<id7.8>7e518dd2f56313887e883294ddf5f49c0b69e1f6</id7.8>
			<ep7.9>8 CORAZONES QUE AUN LATEN</ep7.9>
			<id7.9>a2b0a672318fd180167efc5a79ac1b15728b3a24</id7.9>
			<ep7.10>9 PIEDRA EN EL CAMINO</ep7.10>
			<id7.10>735587bd64468125506cacac552fc437cc8bc1db</id7.10>
			<ep7.11>10 NUEVOS MEJORES AMIGOS</ep7.11>
			<id7.11>9d7ffd5d4307062c883560f0abf3e72dd31c2ea2</id7.11>
			<ep7.12>11 HOSTILES Y DESGRACIADOS</ep7.12>
			<id7.12>825ff3f914e6c41f9052c918c8fa8f5d78d698f9</id7.12>
			<ep7.13>12 DI QUE SI</ep7.13>
			<id7.13>7a57272802d0e240826b3923a080d0b5bb32f3ac</id7.13>
			<ep7.14>13 ENTERRADME AQUI</ep7.14>
			<id7.14>b8452887d928d666399bf96d953e0e8ea518a3e8</id7.14>
			<ep7.15>14 EL OTRO LADO</ep7.15>
			<id7.15>60abde3962dcfe035bf6d987c0bb4a198d08aeeb</id7.15>
			<ep7.16>15 ALGO QUE NECESITAN</ep7.16>
			<id7.16>349745ffe47cefc2605c9e8b255f97536ad168f9</id7.16>
			<ep7.17>16 EL PRIMER DIA DEL RESTO DE TU VIDA</ep7.17>
			<id7.17>b091b1e76f66354d298e06a65c18366181d2985e</id7.17>
		</t7>
		<t8>
			<thumb.8>https://image.tmdb.org/t/p/original/fMDMxn5rnCuOObOXpUuMtr1AtYF.jpg</thumb.8>
			<fanart.8>https://image.tmdb.org/t/p/original/pwA0criaevjVcg2JMRNUFSyXf5r.jpg</fanart.8>
			<ep8.1>1 AL 16 (1080)</ep8.1>
			<id8.1>563bf5ff251bdb591b691f771ac6eece7602893f</id8.1>
			<ep8.2>1 PIEDAD</ep8.2>
			<id8.2>5c00dbde912cf27121d84f94d816a8ec04caa7ec</id8.2>
			<ep8.3>2 LOS MALDITOS</ep8.3>
			<id8.3>8fba34e404e7e9676d2bd8a7727af3a37e21fdcd</id8.3>
			<ep8.4>3 MONSTRUOS</ep8.4>
			<id8.4>cb5fca92cd4a45ad56fda62d01db58aa8508bed2</id8.4>
			<ep8.5>4 UN TIPO CUALQUIERA</ep8.5>
			<id8.5>5728f2dd1ff8df62003af66f8f336b0241054258</id8.5>
			<ep8.6>5 EL GRAN ATERRADOR Y TU</ep8.6>
			<id8.6>3b5e37641595f7cfb0608d7cfdeda40579abf26d</id8.6>
			<ep8.7>6 EL REY, LA VIUDA, Y RICK</ep8.7>
			<id8.7>45e6525e396c33fa4b71ac5a54505a60aecd0f28</id8.7>
			<ep8.8>7 LLEGO LA HORA</ep8.8>
			<id8.8>f9fbb63515823b953605041033a0a38b6b2e4ba2</id8.8>
			<ep8.9>8 LO QUE HAY QUE HACER</ep8.9>
			<id8.9>eccdcb93d8a56e32aab022b9c6b1852e1eb22e52</id8.9>
			<ep8.10>9 HONOR</ep8.10>
			<id8.10>24f4a0b8662329e3752e74b13d433160700b646e</id8.10>
			<ep8.11>10 LOS PERDIDOS Y LOS SAQUEADORES</ep8.11>
			<id8.11>ea1dadfb10033ba4f93ae1dec26e608321f72dbe</id8.11>
			<ep8.12>11 VIVOS O MUERTOS</ep8.12>
			<id8.12>0f7626dd77caa8a8986c62ffd4f6b58aa72cdee7</id8.12>
			<ep8.13>12 LA CLAVE</ep8.13>
			<id8.13>b3632a52370400c4ea2132e274ba0324c82d5d61</id8.13>
			<ep8.14>13 NO NOS HAGAS EXTRAVIARNOS</ep8.14>
			<id8.14>bcb2fdf4d3b6768c4fbdd14531198cf12665a23d</id8.14>
			<ep8.15>14 TODAVIA DEBE SIGNIFICAR ALGO</ep8.15>
			<id8.15>2d7032bdaf2366955e8e8f86219362b2bc2caf9e</id8.15>
			<ep8.16>15 VALIA</ep8.16>
			<id8.16>1eedcc87b9372ce6b79424940296d67455627b82</id8.16>
			<ep8.17>16 IRA</ep8.17>
			<id8.17>cadd6fedfbfaf8c23fc3e126996938fb31294752</id8.17>
		</t8>
		<t9>
			<thumb.9>https://image.tmdb.org/t/p/original/n5E9lBEBArA6bY6fDXfkpB3jv5l.jpg</thumb.9>
			<fanart.9>https://image.tmdb.org/t/p/original/lhDw84LpRKdQU3jadUQPGJszeUS.jpg</fanart.9>
			<ep9.1>1 AL 16 (1080)</ep9.1>
			<id9.1>fec421cadd16ab3f285601f866eb3d9213648103</id9.1>
			<ep9.2>1 UN NUEVO COMIENZO</ep9.2>
			<id9.2>661193f359f878e130b9e479e4215b37336d6ffd</id9.2>
			<ep9.3>2 EL PUENTE</ep9.3>
			<id9.3>9705721f08d91de932730c9ac5089cec29eec8f4</id9.3>
			<ep9.4>3 SEÑALES DE AVISO</ep9.4>
			<id9.4>f601780983cddc02684d7dbe6761552c38b18201</id9.4>
			<ep9.5>4 AGRADECIDOS</ep9.5>
			<id9.5>f87e9a393180ad224b9f953e79545575d89f9593</id9.5>
			<ep9.6>5 QUE VIENE DESPUES</ep9.6>
			<id9.6>0c1fc3b16fbfea359690851392713d5a48ca5509</id9.6>
			<ep9.7>6 ¿QUIENES SOMOS AHORA?</ep9.7>
			<id9.7>b06896e6822b164fc028960c16a1fb2fc5759249</id9.7>
			<ep9.8>7 STRADIVARIUS</ep9.8>
			<id9.8>efe36d8a25164c73879c7142076e62794a0ac8e0</id9.8>
			<ep9.9>8 EVOLUCION</ep9.9>
			<id9.9>c982f8db1c3a4c3fd5647f90e304b339a4aca203</id9.9>
			<ep9.10>9 ADAPTACION</ep9.10>
			<id9.10>bfad1f01070174ed042863cdb42e4c908ea044c9</id9.10>
			<ep9.11>10 OMEGA</ep9.11>
			<id9.11>8c6dae54dba618d9947ef54c32f6d46479b7f9e7</id9.11>
			<ep9.12>11 RECOMPENSA</ep9.12>
			<id9.12>b1b0ec6baea843621e4bd0c77f7b98157ccf7683</id9.12>
			<ep9.13>12 GUARDIANES</ep9.13>
			<id9.13>3ee983b9b5ca9a83a741e5bdbc2f1cc3ef4856da</id9.13>
			<ep9.14>13 EMBUDO</ep9.14>
			<id9.14>440d41d3fb3a16d058af8154bdfc5b4653b285b2</id9.14>
			<ep9.15>14 CICATRICES</ep9.15>
			<id9.15>cb67b118359f9e7a655e96ed07535064ed65c7b1</id9.15>
			<ep9.16>15 LA CALMA ANTERIOR A</ep9.16>
			<id9.16>7abbfba648e97f00aa007018b24e222469cc014d</id9.16>
			<ep9.17>16 LA TORMENTA</ep9.17>
			<id9.17>89eb5e10b56711458772f8cc57d0f21ce248e7bc</id9.17>
		</t9>
		<t10>
			<thumb.10>https://image.tmdb.org/t/p/original/vSaAu7avl7sbxMnygLRpcqnu4om.jpg</thumb.10>
			<fanart.10>https://image.tmdb.org/t/p/original/j55MwsdBFbkVnXBe4auVWqawRy7.jpg</fanart.10>
			<ep10.1>1 LAS LINEAS QUE CRUZAMOS</ep10.1>
			<id10.1>170799fa844e7078d63979564ffc21034c4eb0f7</id10.1>
			<ep10.2>2 NOSOTROS SOMOS EL FIN DEL MUNDO</ep10.2>
			<id10.2>a1e9dcd117f3e02275bf3a361bb2f8cdbf1bd258</id10.2>
			<ep10.3>3 FANTASMAS</ep10.3>
			<id10.3>46bbc5a8725c096b9facda759274c7cdd86ba0de</id10.3>
			<ep10.4>4 HAZ CALLAR LOS SUSURROS</ep10.4>
			<id10.4>85d3dc0e8c1530606fb90cec047003cd6ff67e0e</id10.4>
			<ep10.5>5 LO QUE SIEMPRE ES</ep10.5>
			<id10.5>443adf8ff002f43e09ca6bdfc06f79e6deb0b2f9</id10.5>
			<ep10.6>6 VINCULOS</ep10.6>
			<id10.6>5dcc341c71c566f5874a9bec3216150cb65c49f4</id10.6>
			<ep10.7>7 ABRE LOS OJOS</ep10.7>
			<id10.7>2fe7b1b27a2ecc57d61ad7951b649d1cb6de776e</id10.7>
			<ep10.8>8 EL MUNDO ANTES</ep10.8>
			<id10.8>3306e7911cda09530acb1984a4f2bbcf264074e6</id10.8>
			<ep10.9>9 PRESION</ep10.9>
			<id10.9>494465409be317741a57ea1f37d4a74905561f1b</id10.9>
			<ep10.10>10 ACECHO</ep10.10>
			<id10.10>77d4cf1d6e0c2f0750cf4a0856f0f414e6c78e50</id10.10>
			<ep10.11>11 LUCERO DEL ALBA</ep10.11>
			<id10.11>8a95ce2d6d0b338698a8a01a6a1e8f17ca43b422</id10.11>
			<ep10.12>12 CAMINA CON NOSOTROS</ep10.12>
			<id10.12>cec4be9380cf3e04522842370654692d917ad03a</id10.12>
			<ep10.13>13 COMO NOS VOLVEMOS</ep10.13>
			<id10.13>10c49cb4c1754c7eeab844560dd01ad508d713c9</id10.13>
			<ep10.14>14 MIRA LAS FLORES</ep10.14>
			<id10.14>7a840480f575b265ceec02bd53bc5bba2a9317ca</id10.14>
			<ep10.15>15 LA TORRE</ep10.15>
			<id10.15>cb026d51cbdf4c70fcaf31ff85e5e2b1e56f071f</id10.15>
			<ep10.16>16 UN FUTURO INCIERTO</ep10.16>
			<id10.16>c8f8495f6ff40868c202f0070641f894dca4e882</id10.16>
			<ep10.17>17 HOGAR, DULCE HOGAR</ep10.17>
			<id10.17>b614e100f1493cd99765fea9457cc1933911a882</id10.17>
			<ep10.18>18 ENCUENTRAME</ep10.18>
			<id10.18>0279a59e0fd93c6ed9d5d0d22ca72d1ac574cf0a</id10.18>
			<ep10.19>19 UNO MAS</ep10.19>
			<id10.19>13201685eca8afc9c5da09737093f62d6c7dabca</id10.19>
			<ep10.20>20 ASTILLA</ep10.20>
			<id10.20>4a018fd9ff59e51665c0152e13621deed5bc3a98</id10.20>
			<ep10.21>21 DIVERGENCIA</ep10.21>
			<id10.21>28fd09209405205dbb782bef0bec1fd01428eae9</id10.21>
			<ep10.22>22 AQUI ESTA NEGAN</ep10.22>
			<id10.22>811bb94dc6ead1a0b56b82c33ee07907a7202424</id10.22>
		</t10>
		<t11>
			<thumb.11>https://www.themoviedb.org/t/p/original/wsq1xCuC9kDhFeoE7xuQFDC5iRC.jpg</thumb.11>
			<fanart.11>https://www.themoviedb.org/t/p/original/wvdWb5kTQipdMDqCclC6Y3zr4j3.jpg</fanart.11>
			<ep11.1>1 ACHERON: PARTE 1</ep11.1>
			<id11.1>0da34f96fad573e3a6dcdce512f5f545ec395039</id11.1>
			<ep11.2>2  ACHERON: PARTE 2</ep11.2>
			<id11.2>c642de01d33576d58875a64d67ae807864dd5ffc</id11.2>
			<ep11.3>3 CAZADO</ep11.3>
			<id11.3>e47a5447bc7496e0612082828df78e61498c0dc5</id11.3>
			<ep11.4>4 RENDICION</ep11.4>
			<id11.4>154e745c788aa1ea004c12039a5d84621596effa</id11.4>
			<ep11.5>5 DE LAS CENIZAS</ep11.5>
			<id11.5>02e970ab6de9a6aad2a2770db9a459fdc5180ef5</id11.5>
			<ep11.6>6 POR DENTRO</ep11.6>
			<id11.6>5f036752750415492e0d0a277195a388a8e9716d</id11.6>
			<ep11.7>7 AL 9</ep11.7>
			<id11.7>A30E6ABBBD84A73A8E2E18CF8E07AC7A3C4104B9</id11.7>
			<ep11.8>10 NUEVAS RUTAS</ep11.8>
			<id11.8>BDE28FBFE80B47238B39627DF10E79D697A386DB</id11.8>
			<ep11.9>11 INDIVIDUOS REBELDES</ep11.9>
			<id11.9>A112EF2D302D26B22E90CB778456EB99BE039654</id11.9>
			<ep11.11>12 LOS AFORTUNADOS</ep11.11>
			<id11.11>B32807C6908145A4A0FA5FCF3C4D317778492705</id11.11>
			<ep11.12>13 CAUDILLOS</ep11.12>
			<id11.12>B88E7BDAA9FDC883BB98BB23F1132421264FE511</id11.12>
			<ep11.13>14 NUCLEO PODRIDO</ep11.13>
			<id11.13>B390A4938CB07CDFF4CDF8C02F73263FE8C97F73</id11.13>
			<ep11.14>15 CONFIANZA</ep11.14>
			<id11.14>C712F47CF01D5900A490FA7118FB34AF847AFF4D</id11.14>
			<ep11.15>16 ACTO DE DIOS</ep11.15>
			<id11.15>64B11F72BAC18D5838171CC80C3A898D4E9955A5</id11.15>
		</t11>
	</serie>
	<serie>
		<title>THE WALKING DEAD: WORLD BEYOND</title>
		<info>(2020) 2 temporadas. 20 episodios. Spin-of de la famosa serie "The Walking Dead" que se centrará en la primera generación de supervivientes nacidos durante el apocalipsis zombie. Creada por Scott M. Gimple y Matt Negrete</info>
		<year>2020</year>
		<genre>Terror. Ciencia ficción. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/8zu2ESmRrCsZrdG1t2sPqXk9tkF.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/uOWq2oHeJ7assluRPv48MQciGCX.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/8zu2ESmRrCsZrdG1t2sPqXk9tkF.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/uOWq2oHeJ7assluRPv48MQciGCX.jpg</fanart.1>
			<ep1.1>1 VALOR</ep1.1>
			<id1.1>3b3da7492502bf2565d16b44ac1f04d12459d927</id1.1>
			<ep1.2>2 LA PIRA DE LA DESESPERANZA</ep1.2>
			<id1.2>e8731681384920296d4796c5505b16eba8e0b649</id1.2>
			<ep1.3>3 EL TIGRE Y EL CORDERO</ep1.3>
			<id1.3>eb0296439df6c66a1d592af4831c8eab5b8690e7</id1.3>
			<ep1.4>4 EL EXTREMO EQUIVOCADO DE UN PERISCOPIO</ep1.4>
			<id1.4>7db5999260ca008e03297d39b2b58b1fcbdd38e4</id1.4>
			<ep1.5>5 SURCANDO LAS AGUAS</ep1.5>
			<id1.5>71220df7ac6f4d8160563e752767866bd6ce3369</id1.5>
			<ep1.6>6 TITERES DE SOMBRA</ep1.6>
			<id1.6>f23ab69123c1f9976b9e1f6f5b7d0bd4b8e7988d</id1.6>
			<ep1.7>7 VERDAD O RETO</ep1.7>
			<id1.7>911fad6fe3dcaafddc544b4e6f5c973e540e7879</id1.7>
			<ep1.8>8 EL CIELO ES UN CEMENTERIO</ep1.8>
			<id1.8>bd89b6d380081021e34d8e743f37f87ada640ff6</id1.8>
			<ep1.9>9 EL CORTE MAS PROFUNDO</ep1.9>
			<id1.9>0cf7451fafbd6f03003e645e5b214cf27acf8b3a</id1.9>
			<ep1.10>10 EN ESTA VIDA</ep1.10>
			<id1.10>2a5476ce61cfe051eb9cde3e01effe9d180ceb09</id1.10>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/6HanIV2hTLE2w7A5bI1KJb3bTL7.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/hS0IN656SbRe23TyH0PdaGytIuL.jpg</fanart.2>
			<ep2.1>1 CONSECUENCIAS</ep2.1>
			<id2.1>bcc73dbe3aa61e784e47eb33f1429c6341b04691</id2.1>
			<ep2.2>2 ASIDERO PARA EL PIE</ep2.2>
			<id2.2>72ef3859d6ce770e27b54ba99cd861c341cd72f6</id2.2>
			<ep2.3>3 HERIDAS DE SALIDA</ep2.3>
			<id2.3>cfae5cc385027f589ab36b98a057988927c68b9a</id2.3>
			<ep2.4>4 FAMILIA ES UNA PALABRA DE CUATRO LETRAS</ep2.4>
			<id2.4>a5a4a5b023f1970513c6802e19e5a61827be6c53</id2.4>
			<ep2.5>5 PUNTO DECISIVO</ep2.5>
			<id2.5>7457f033f2d552322106b2988c2496623cf3b04b</id2.5>
			<ep2.6>6 ¿QUIEN ERES TU ?</ep2.6>
			<id2.6>23bff64bd03839649a2a5ed4ce554a692537ae9a</id2.6>
			<ep2.7>7 SANGRE Y MENTIRAS</ep2.7>
			<id2.7>670ec5588332930f9433fcb07166bd7dda64a4ce</id2.7>
			<ep2.8>8 PUNTO DE RETORNO</ep2.8>
			<id2.8>9897b665c5914ea8ec7ad4b37a9e1831a137bef7</id2.8>
			<ep2.9>9 LA MUERTE Y LOS MUERTOS</ep2.9>
			<id2.9>b2ac16cb2f15ec2390deec0f7182e05415736362</id2.9>
			<ep2.10>10 LA ULTIMA LUZ</ep2.10>
			<id2.10>f6bab610523f6c7b768d491a1a2f163f6eadaaa2</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE WHITE LOTUS</title>
		<info>(2021) 6 episodios. Retrata las vacaciones de varios huéspedes de un resort tropical durante una semana mientras se relajan y rejuvenecen en el paraíso. Pero cada día que pasa, surge una complejidad más oscura en la vida de estos viajeros perfectos, los risueños empleados del hotel y ese lugar idílico.</info>
		<year>2021</year>
		<genre>Comedia. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/bOSbq08LfILmWSBhMHuC6Xlnhb9.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/gGs308fHMGRX8kIYtMEEpyhIrAK.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/bOSbq08LfILmWSBhMHuC6Xlnhb9.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/gGs308fHMGRX8kIYtMEEpyhIrAK.jpg</fanart.1>
			<ep1.1>1 LLEGADAS</ep1.1>
			<id1.1>c6bf2ca03da199ccb052970eab01f2f005429e1c</id1.1>
			<ep1.2>2 NUEVO DIA</ep1.2>
			<id1.2>54652062b8288b9336965e79ea56e1fae4cf97ed</id1.2>
			<ep1.3>3 MONOS MISTERIOSOS</ep1.3>
			<id1.3>44db73682123d198e55dbc43e82b4f44fbaa328f</id1.3>
			<ep1.4>4 RECENTRANDO</ep1.4>
			<id1.4>8cf3d7f23806933a64093df2657c03e0c201f09c</id1.4>
			<ep1.5>5 LOS COME-LOTOS</ep1.5>
			<id1.5>1c785602a5b3d9f2f920967f943429da5d2109a8</id1.5>
			<ep1.6>6 SALIDAS</ep1.6>
			<id1.6>6ec49c03881e84133a2cffe75d2689e46ddb355a</id1.6>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE WILDS (SALVAJES)</title>
		<info>(2020) 2 temporadas. 18 episodios. Un grupo de adolescentes de diferentes orígenes deben luchar por la supervivencia después de que un accidente de avión las deje varadas en una isla desierta. Las náufragas se enfrentan y se unen a medida que aprenden más sobre las otras, los secretos que guardan y los traumas que han sufrido.</info>
		<year>2019</year>
		<genre>Drama. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/a8YOS7zzyYwMzDA9G6JTBT43NQP.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/rgBxEVr44C00hBg3jLicxWMy9yy.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/a8YOS7zzyYwMzDA9G6JTBT43NQP.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/rgBxEVr44C00hBg3jLicxWMy9yy.jpg</fanart.1>
			<ep1.1>1 al 10</ep1.1>
			<id1.1>b38e0ef124e1b0b455f5acf05dbf9af6f0b50437</id1.1>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/fJLfcjjZF6vhSFntfOnpirow6ub.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/jke7vRfAfwViuwfJhuhzlpxQuHY.jpg</fanart.2>
			<ep2.1>1 DIA 30 / 1</ep2.1>
			<id2.1>3cb73ba578708bf08e41d8359d0cebed60973737</id2.1>
			<ep2.2>2 DIA 34 / 12</ep2.2>
			<id2.2>95635ece083ce56f144c457d3b9af5e7a2a11e64</id2.2>
			<ep2.3>3 DIA 36 / 14</ep2.3>
			<id2.3>6959889f936f4902bb62473e3d0bdc988b7cd17c</id2.3>
			<ep2.4>4 DIA 42 / 15</ep2.4>
			<id2.4>b89b5260de9b101094300d7e31cad6793924a477</id2.4>
			<ep2.5>5 DIA 45 / 16</ep2.5>
			<id2.5>cb9535d702492bf1db37f126218d8d8dd157b129</id2.5>
			<ep2.6>6 DIA 46 / 26</ep2.6>
			<id2.6>2f14f0afb93bd784e42dc42c5159d383545ea822</id2.6>
			<ep2.7>7 DIA 50 /33</ep2.7>
			<id2.7>c319c7d5acca72bc0cdc367a740c0748e3c41d77</id2.7>
			<ep2.8>8 EXODO</ep2.8>
			<id2.8>44fd775d43687534b5c524e50646183237809134</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>THE WITCHER</title>
		<info>(2019) 2 temporadas. 16 episodios. Geralt de Rivia, un cazador de monstruos mutante, viaja en pos de su destino por un mundo turbulento en el que, a menudo, los humanos son peores que las bestias. Adaptación a la televisión de la saga literaria de Andrzej Sapkowski, que dio a su vez origen a una trilogía de prestigiosos videojuegos.</info>
		<year>2019</year>
		<genre>Fantástico. Aventuras</genre>
		<thumb>https://image.tmdb.org/t/p/original/zrPpUlehQaBf8YX2NrVrKK8IEpf.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/kysKBF2CJG9qfQDSCDaboJrkZy1.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/zrPpUlehQaBf8YX2NrVrKK8IEpf.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/kysKBF2CJG9qfQDSCDaboJrkZy1.jpg</fanart.1>
			<ep1.1>1 EL PRINCIO DEL FIN</ep1.1>
			<id1.1>814712ba69cdc146273442a67be1153103de1594</id1.1>
			<ep1.2>2 CUATRO MARCOS</ep1.2>
			<id1.2>5122698058c7eee5c3557186b1a8183d44142667</id1.2>
			<ep1.3>3 LUNA TRAICIONERA</ep1.3>
			<id1.3>8ee8c6ca0e469f67361ae51772e2773f6c21b24b</id1.3>
			<ep1.4>4 DE BANQUETES BASTARDOS Y ENTIERROS</ep1.4>
			<id1.4>d7703a9622ed59369af36660e552bf02cb4fa049</id1.4>
			<ep1.5>5 DESEOS INCONTENIBLES</ep1.5>
			<id1.5>2f3ea3b6fa3de8d8e01488bb4bb8dfc175462404</id1.5>
			<ep1.6>6 ESPECIES RARAS</ep1.6>
			<id1.6>1c9a1a4e333451adab051653c448b93d05dabb52</id1.6>
			<ep1.7>7 ANTES DE UNA CAIDA</ep1.7>
			<id1.7>52aa2400cd97c4e74af9117e5203fc916a139771</id1.7>
			<ep1.8>8 MUCHO MAS</ep1.8>
			<id1.8>5830d65daeb974c4ea71f1e064f65bc4e01d67d2</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/d9zQQkSp1EWKz2jLcwFnpbazKwW.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/3JBth1q5aTZfc1zZI9Xms68MHzs.jpg</fanart.2>
			<ep2.1>1 AL 8</ep2.1>
			<id2.1>C6AB67DA77ADD49D0094338C0B841F667B6DF050</id2.1>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TIEMPO DE VICTORIA: LA DINASTIA DE LOS LAKERS</title>
		<info>(2022) 10 episodios. La historia de cómo Los Angeles Lakers se convirtieron en el equipo de baloncesto profesional más exitoso de la década de 1980.</info>
		<year>2022</year>
		<genre>Comedia. Drama </genre>
		<thumb>https://www.themoviedb.org/t/p/original/hK5xQciXZ5gVVScgvPtNPwR7XOj.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/vI63XTySC4HRLlxa7gIknYiK17a.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/hK5xQciXZ5gVVScgvPtNPwR7XOj.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/vI63XTySC4HRLlxa7gIknYiK17a.jpg</fanart.1>
			<ep1.1>1 EL CISNE</ep1.1>
			<id1.1>c439b5c3101bef81178394bb5e8c02802278c3bb</id1.1>
			<ep1.2>2 ¿ESO ES TODO LO QUE HAY?</ep1.2>
			<id1.2>dbee422138c56321bcbfb3d577a7e3e7ff7cf782</id1.2>
			<ep1.3>3 LO MEJOR ESTA POR VENIR</ep1.3>
			<id1.3>c4d99f18e3afd851c62c658e352a35fb945b9c7d</id1.3>
			<ep1.4>4 ¿QUIEN DIABLOS ES JACK MCKINNEY?</ep1.4>
			<id1.4>f93582a5dd695e50ed7c0f491a39eab44a2b9ff3</id1.4>
			<ep1.5>5 PIEZAS DE UN HOMBRE</ep1.5>
			<id1.5>e2d6b9e0c2e47c41a23096056399ed72ab6de82d</id1.5>
			<ep1.6>6 MEMENTO MORI</ep1.6>
			<id1.6>1585f7c08bd55ef5e5f984f0633a5a87245baa03</id1.6>
			<ep1.7>7 EL HOMBRE INVISIBLE</ep1.7>
			<id1.7>cd0f19e9f1bbdcb77ef6f62c239aeaa28e2bf715</id1.7>
			<ep1.8>8 CALIFORNIA DREAMING</ep1.8>
			<id1.8>99f923c42d88383a051d6e877dc9f0adad1012ea</id1.8>
			<ep1.9>9 PERDIDA ACEPTABLE</ep1.9>
			<id1.9>0c8ad0ef96f425dac8cada8dfa0e5176a843761d</id1.9>
			<ep1.10>10 TIERRA PROMETIDA</ep1.10>
			<id1.10>878e270b35b3897b149dd83b6c52d9fa3c28dcab</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TIN STAR</title>
		<info>(2017)  3 temporadas . 26 episodios. A Little Bear, un pequeño pueblo de las montañas de Canadá llega Jim Worth (Tim Roth), un antiguo detective británico que, buscando una vida más tranquila junto a su familia, ha sido nombrado jefe de policía de la población canadiense. Serie que mezcla drama, thriller y comedia negra.</info>
		<year>2017</year>
		<genre>Drama. Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/vBOyjQB6LdWYnLTdeb1d6pw1B35.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/skkcEdM79CMANypRHx9ip7JPOPI.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/vBOyjQB6LdWYnLTdeb1d6pw1B35.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/skkcEdM79CMANypRHx9ip7JPOPI.jpg</fanart.1>
			<ep1.1>1 DIVERSION Y (DES)GRACIA</ep1.1>
			<id1.1>5754f13abaad828e757ad1ef530ffe788b758fde</id1.1>
			<ep1.2>2 EL CHICO</ep1.2>
			<id1.2>aba97d888c4567dfc307f8dc9eb4afc604babe97</id1.2>
			<ep1.3>3 EL PLACER DE LOS EXTRAÑOS</ep1.3>
			<id1.3>f551f5712d2e9eb93f4242974f214a7c37527454</id1.3>
			<ep1.4>4 JACK</ep1.4>
			<id1.4>c3916ca9820f0ac48ee6d53c9abdb006a3c86fcd</id1.4>
			<ep1.5>5 CARNADA</ep1.5>
			<id1.5>b66641d336527155dbb8188b396e6538215454b2</id1.5>
			<ep1.6>6 CHIFLADO</ep1.6>
			<id1.6>df27de25fff397e6e02e6f9d59edfd916c39bbb6</id1.6>
			<ep1.7>7 LA REVELACION</ep1.7>
			<id1.7>a0d15c459df9efad9e5b72f1d21c88413d298dce</id1.7>
			<ep1.8>8 QUE ESTE SEA EL VERSO</ep1.8>
			<id1.8>10bcce9343d486f5d038fc6f89b06ab8bf07ec4a</id1.8>
			<ep1.9>9 UN CHICO AFORTUNADO</ep1.9>
			<id1.9>7326c0c3c979a6aa90f9b6e1423a0d5944213c91</id1.9>
			<ep1.10>10 MI AMOR ES LA VENGANZA</ep1.10>
			<id1.10>9cc49a17fe41fe48199687898e65b29114661851</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/1JQYYHhGJQJj7EoI6ZGTMb0md3t.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/skkcEdM79CMANypRHx9ip7JPOPI.jpg</fanart.2>
			<ep2.1>1 PRADERA GOTICA (PARTE 1)</ep2.1>
			<id2.1>58356d0a7de49e1aa95e08ef9f2c68577858e6d5</id2.1>
			<ep2.2>2 PRADERA GOTICA (PARTE 2)</ep2.2>
			<id2.2>83ed87a1728845821a21944c0ee211b35d571759</id2.2>
			<ep2.3>3 LA FERIA DE LAS TINIEBLAS</ep2.3>
			<id2.3>9ad6119496b7722b535ba7516cb0de37fa5857d5</id2.3>
			<ep2.4>4 NO OS RESISTAIS AL MALVADO</ep2.4>
			<id2.4>0c45991a5c0d6c8037cf7d36f96c33906ed91c62</id2.4>
			<ep2.5>5 JACK Y LA COCA</ep2.5>
			<id2.5>cc68260aba74a57cb49a171e8a46622336688189</id2.5>
			<ep2.6>6 CONSECUENCIAS</ep2.6>
			<id2.6>fe174c33132d88851e5d501609246f32189a93f4</id2.6>
			<ep2.7>7 LA ERA DE LA ANSIEDAD</ep2.7>
			<id2.7>28ef50c62bd39db631c54c51c67919dcb7059893</id2.7>
			<ep2.8>8 INCENCIO SUBTERRANEO</ep2.8>
			<id2.8>af207a5c578f572ff7b2473963502c0eb7e34b3e</id2.8>
			<ep2.9>9 FLOR SILVESTRE</ep2.9>
			<id2.9>9c24a0ca123d9181fe6ce6e49bdf8c8f2ec19761</id2.9>
			<ep2.10>10 LO INVISIBLE</ep2.10>
			<id2.10>a5da0351395a0d4897c9b46ec781e4f2d72edc9b</id2.10>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/lFHZBtugqDiLtjVP9GC3Ig5GfwU.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/1hDcM8TIvw17rFIJeSImEarDYz3.jpg</fanart.3>
			<ep3.1>1 REGRESO A CASA</ep3.1>
			<id3.1>e7716e723394465ef8e1ffcd36d2898ef5622f0b</id3.1>
			<ep3.2>2 EL COMPROMISO</ep3.2>
			<id3.2>b47d1f86d8c4c5c396da8b70a2af7b86f48871c2</id3.2>
			<ep3.3>3 AMA EL SUEÑO JOVEN</ep3.3>
			<id3.3>4ac5a11d21c2b04547677c2f3b2f3e67131a4ab6</id3.3>
			<ep3.4>4 COLATERAL</ep3.4>
			<id3.4>40dfdb10283aec6c02add40264a21f4a30a6b85c</id3.4>
			<ep3.5>5 TODOS LOS CAMINOS</ep3.5>
			<id3.5>e6dd830bde9b6ddc816605bd9c4ce713fde8c51f</id3.5>
			<ep3.6>VEN AL BORDE</ep3.6>
			<id3.6>59f7966e7433f390a44e0d997e70240e82393d46</id3.6>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TODOS MIENTEN</title>
		<info>(2022) 6 episodios. La apacible vida de los vecinos de la urbanización costera de Belmonte cambia radicalmente el día en que aparece en las redes un vídeo sexual de Macarena, profesora de la escuela, con Iván, uno de sus alumnos mayor de edad. Este hecho sacude la vida de todos ellos, especialmente de Macarena, quien es repudiada por su familia y sus vecinos, incluyendo a Ana, su mejor amiga y madre de Iván. Las cosas en el pueblo se complican aún más cuando aparece el cuerpo sin vida de uno de los habitantes de Belmonte en el acantilado. ¿De quién es el cuerpo? ¿Tiene algo que ver esta muerte con el vídeo sexual de la profesora y el alumno?</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/39WhAU8LqcQNIbT1GObCICt5Ryg.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/3EG6OhdSUNBWgIy67rchK7BClTE.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/39WhAU8LqcQNIbT1GObCICt5Ryg.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/3EG6OhdSUNBWgIy67rchK7BClTE.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>649b1a8cab7a7a78c13f929b9958dd51a7d01242</id1.1>
			<ep1.2>4 AL 6</ep1.2>
			<id1.2>775c5deec56ee10846662b1e2913e2e3c336997d</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TOKYO VICE</title>
		<info>(2022) 8 episodios. Finales de los años 90. Jake Adelstein es un joven periodista norteamericano que trabaja para un importante periódico de Tokio. Bajo la supervisión de un veterano detective de la policía de la ciudad, Jake comienza a investigar el oscuro mundo de la Yakuza, la peligrosa mafia japonesa controlada por algunos de los criminales más poderosos del país</info>
		<year>2022</year>
		<genre>Thriller. Drama </genre>
		<thumb>https://www.themoviedb.org/t/p/original/za5QWRfCLwgRLLVXUkx3NUSAm6G.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/knbNvXLYeSyE1BJfFiIMJTktMkS.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/za5QWRfCLwgRLLVXUkx3NUSAm6G.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/knbNvXLYeSyE1BJfFiIMJTktMkS.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>184f97c33c1857b6bee277753da48fbcd4442a73</id1.1>
			<ep1.2>2 KISHI KAISEI</ep1.2>
			<id1.2>8868ee21ac5163c67641c50a817b383b1e06e70a</id1.2>
			<ep1.3>3 LEER EL AIRE</ep1.3>
			<id1.3>8523147d4527eb7300bf073c96ad45db4838b85b</id1.3>
			<ep1.4>4 LO QUIERO DE ESA MANERA</ep1.4>
			<id1.4>254232ba2f0375b691bcc77257a017e040772541</id1.4>
			<ep1.5>5 TODOS PAGAN</ep1.5>
			<id1.5>d48ad3274c72bfc3bc469a0c80edc43861dd3d8b</id1.5>
			<ep1.6>6 EL NEGOCIO DE LA INFORMACION</ep1.6>
			<id1.6>7b920104c2f9fb192be49922c60cf266c77e7261</id1.6>
			<ep1.7>7 A VECES ELLOS DESAPARECEN</ep1.7>
			<id1.7>d955d016055e3320ac71fa10237468b7052492e4</id1.7>
			<ep1.8>8 YOSHINO</ep1.8>
			<id1.8>6958f2d7abb36d6dad8da2593d2d69d66a9c7e2b</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TOP BOY</title>
		<info>(2019) 19 episodios. Tras verse obligado a huir de sus propiedades en Londres, un exmafioso regresa a casa con la intención de recuperar su trono en el mundo del crimen.</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/mGZpOEaLZTRzWeQMq5SZM5BbDZg.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/4CqfqazO2EBhN469XfIp7RSJZ7h.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/mGZpOEaLZTRzWeQMq5SZM5BbDZg.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/4CqfqazO2EBhN469XfIp7RSJZ7h.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>d2ca14dd6d9d06fc1494cf3cfc401c9f8a71daf6</id1.1>
			<ep1.2>6 AL 10</ep1.2>
			<id1.2>c0d8e703a0623ff3502dcc8a8845d8876d073596</id1.2>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/cfqHWQQDwvKT8Xoe0AXzHBXcF1u.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/jAz9AZpznu3ELhUp1XiMEeQv2Se.jpg</fanart.2>
			<ep2.1>1 BUENA MORAL</ep2.1>
			<id2.1>a00dddb0fb4c8b70efabb4666ba2fb9e87396796</id2.1>
			<ep2.2>2 ¿COMO PUEDO SOLUCIONAR ESTO?</ep2.2>
			<id2.2>2ff79ae7ae5a9e30cecfca853483ea35a7d52f50</id2.2>
			<ep2.3>3 FAVORITO</ep2.3>
			<id2.3>4e5f32b290fd39f6f8ee1088c9b6b865d30a3467</id2.3>
			<ep2.4>4 DOLOR DE CABEZA COMPLETAMENTE CARGADO</ep2.4>
			<id2.4>d8d6eb5efb3ee3e1b5c34102cffad8116dcf63c0</id2.4>
			<ep2.5>5 15 PUNTOS</ep2.5>
			<id2.5>61b51673b24210e814e0fcbb883326f1b68e522c</id2.5>
			<ep2.6>6 DE CAPA Y ESPADA</ep2.6>
			<id2.6>5d4d44c8753339287a4c52cf7755d803754ef89c</id2.6>
			<ep2.7>7 NOSOTROS FORMAMOS LA FAMILIA</ep2.7>
			<id2.7>c78b6527387eba913f42942175ddbd1c4508b1c4</id2.7>
			<ep2.8>8 PROBARTE A TI MISMO</ep2.8>
			<id2.8>57160653173edadfb1b2c050f6cd58911a91a776</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TOY BOY</title>
		<info>(2019) 2 temporadas. 21 episodios. Hugo Beltrán (Jesús Mosquera) es un stripper joven, guapo y despreocupado. Una madrugada se despierta en un velero, tras una noche de fiesta y excesos, al lado del cadáver quemado de un hombre. Es el marido de Macarena, su amante (Cristina Castaño), una mujer madura y poderosa con la que mantenía una tórrida relación. Hugo no recuerda nada de lo que ocurrió la noche del crimen, pero está seguro de que él no es el asesino, sino la víctima de un montaje para inculparlo. Tras un rápido juicio es condenado a quince años de cárcel. Siete años más tarde, en la cárcel recibe la visita de Triana Marín (María Pedraza), una abogada joven que, en representación de un importante bufete, se ofrece a ayudarlo, reabrir el caso y tratar de demostrar su inocencia en un nuevo juicio.</info>
		<year>2019</year>
		<genre>Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/92W96Y0kvcHj1HbUqs4H3Hv2NZi.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/6c7jtKFCjKF0x8uIJIbUfnvtisv.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/92W96Y0kvcHj1HbUqs4H3Hv2NZi.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/6c7jtKFCjKF0x8uIJIbUfnvtisv.jpg</fanart.1>
			<ep1.1>1 PILOTO</ep1.1>
			<id1.1>1bcac41315d2ba6190db5e8517b2e33731fb7fc3</id1.1>
			<ep1.2>2 DE ENTRE LOS MUERTOS</ep1.2>
			<id1.2>e96dff87bbd2522a9e9be43120552cf5cb99543f</id1.2>
			<ep1.3>3 EL JUICIO FINAL</ep1.3>
			<id1.3>0ee97ae66e2dddd07899e8746be977dceb9b35ee</id1.3>
			<ep1.4>4 CASILLA DE SALIDA</ep1.4>
			<id1.4>22fbd8b76fc3d83e63f06092fbb88923abcaac99</id1.4>
			<ep1.5>5 EL PULSO DEL ASESINO</ep1.5>
			<id1.5>2cd25241129949e1a812c6664f12335042898c0c</id1.5>
			<ep1.6>6 EL FINAL DEL TUNEL</ep1.6>
			<id1.6>a96a8574a0900d3e5998dac32eae3be837a9b963</id1.6>
			<ep1.7>7 JUEGO DE MASCARAS</ep1.7>
			<id1.7>e29dc8a0f63eddd1b922925c00ca5820b2e0241d</id1.7>
			<ep1.8>8 UNICO TESTIGO</ep1.8>
			<id1.8>24b315996a84c8ece4dc7210dcaf0ef7ce806bae</id1.8>
			<ep1.9>9 AMOR DE MADRE</ep1.9>
			<id1.9>0898c252af586a181073af395835a9dcb87d63eb</id1.9>
			<ep1.10>10 POLAROIDS</ep1.10>
			<id1.10>c8163af536e6f6d241f830079812145d65ed602c</id1.10>
			<ep1.11>11 CACHORRO</ep1.11>
			<id1.11>1b9040d68d5b058253321a2fd6829bf3bc848930</id1.11>
			<ep1.12>12 REDENCION</ep1.12>
			<id1.12>e81c50d60ebe84944c8f698c1a02d66efa506ff5</id1.12>
			<ep1.13>13 ANGELES CAIDOS</ep1.13>
			<id1.13>448b32354efc4ddb73e2446af36803f9baf2757e</id1.13>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/bWC1GLZVBuYU4U0iadUfrVAtV2n.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/sOCKmOE9dT6STuR41Q1oVNNpMKR.jpg</fanart.2>
			<ep2.1>1 LA PERSONA A LA QUE MAS QUIERES</ep2.1>
			<id2.1>d324609d790b12caeb8e273f15c38bb53034c0fb</id2.1>
			<ep2.2>2 QUE ES EL SEXO PARA TI</ep2.2>
			<id2.2>be438dc052b5b9bee4db9811cdac93cf929db245</id2.2>
			<ep2.3>3 EL PRECIO DE LOS DIOSES</ep2.3>
			<id2.3>bcee32b0190c3f56ae6d14a927e5a9bf5fdcaee6</id2.3>
			<ep2.4>4 CIELO E INFIERNO</ep2.4>
			<id2.4>01ef049c284598774532d17aff1fc9cb13389381</id2.4>
			<ep2.5>5 TEMPLOS PROFANOS</ep2.5>
			<id2.5>6658a083dde5bb46faf7e4ecac35cdd6aa48f80f</id2.5>
			<ep2.6>6 UN GESTO DE AMOR</ep2.6>
			<id2.6>21aab7479fa68c7765c691dd1d223a66acf1197d</id2.6>
			<ep2.7>7 ¿POR QUE ME LLAMAN EL TURCO?</ep2.7>
			<id2.7>5dd5774c137924f7b37359cf833eed77246a5b89</id2.7>
			<ep2.8>8 PUERTAS QUE SE ABREN</ep2.8>
			<id2.8>2385cc0b74a1e09a644f7b05e1112ab80f461281</id2.8>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TRIBUS DE EUROPA</title>
		<info>(2021) 6 Episodios. En 2074, tres hermanos luchan por cambiar el destino de Europa, gobernada ahora por diferentes tribus desde que una catástrofe global fracturara el continente en innumerables microestados enfrentados entre sí.</info>
		<year>2021</year>
		<genre>Ciencia ficción. Fantástico. Aventuras</genre>
		<thumb>https://www.themoviedb.org/t/p/original/ewEgxHvSJnQxH4nwYUr8yajqHOQ.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/1bztL5ZgA9m8OSxO51TBYyoVO8P.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/ewEgxHvSJnQxH4nwYUr8yajqHOQ.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/1bztL5ZgA9m8OSxO51TBYyoVO8P.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>380e96c07fbae4ce31516afce87dc117f592b3ae</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>TRUE DETECTIVE</title>
		<info>(2014-2019) 24 episodios. True Detective es una serie de televisión estadounidense dramática de género policíaco. La serie sigue un formato de antología, así que en cada temporada contará una historia diferente con un elenco de personajes distintos.</info>
		<year>2014</year>
		<genre>Intriga. Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/mGnuYyAGbn4EKxIj61JYIlNJmim.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/bPLRjO2pcBx0WL73WUPzuNzQ3YN.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/mGnuYyAGbn4EKxIj61JYIlNJmim.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/bPLRjO2pcBx0WL73WUPzuNzQ3YN.jpg</fanart.1>
			<ep1.1>1 LA LARGA Y CLARA OSCURIDAD</ep1.1>
			<id1.1>2ce349cc24b6cd8f28e614a4ce1e929b3243b1f3</id1.1>
			<ep1.2>2 VISIONES</ep1.2>
			<id1.2>a1b8aec9424e6fa7370bc8f5691e3ace45b93224</id1.2>
			<ep1.3>3 LA HABITACION CERRADA</ep1.3>
			<id1.3>ba89c65d5b0106876443c609e790b8cfa29b834a</id1.3>
			<ep1.4>4 QUIEN ANDA AHI</ep1.4>
			<id1.4>43e843bf440560fa32c7e7faa9b5cf50909b43e5</id1.4>
			<ep1.5>5 EL SINO SECRETO DE TODA VIDA</ep1.5>
			<id1.5>5c98d17bc4c684e377bea31b1866ec2759433954</id1.5>
			<ep1.6>6 CASAS ENCANTADAS</ep1.6>
			<id1.6>d4e098a2fd9b78c124077f6173147e7c97ce388c</id1.6>
			<ep1.7>7 DESPUES DE IRTE</ep1.7>
			<id1.7>77ae7252d340faf281b56199a6903f206a3cb138</id1.7>
			<ep1.8>8 FORMA Y VACIO</ep1.8>
			<id1.8>640f22e5a867a7fe2155cb1eab4f4169f5c50b11</id1.8>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/owTL3WTY4Yax2qClnwbaDsOO72r.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/8x4umpJVPgTplAmEIojVVYWAhum.jpg</fanart.2>
			<ep2.1>1 EL LIBRO DE LOS MUERTOS DEL OESTE</ep2.1>
			<id2.1>edb4770e03e1bf966c1d6a692604f9e3df3fc56b</id2.1>
			<ep2.2>2 LA NOCHE TE ENCUENTRA</ep2.2>
			<id2.2>8bccb3f9c8a6b07a270cd0a4687cc5d20abee89e</id2.2>
			<ep2.3>3 QUIZA MAÑANA</ep2.3>
			<id2.3>71fa6a83a725f5b82453d86bd9e1cccd4cb5f239</id2.3>
			<ep2.4>4 LA BAJADA VENDRA</ep2.4>
			<id2.4>1298e37941687043977e8de7ece5a84b4ac7fb44</id2.4>
			<ep2.5>5 OTRAS VIDAS</ep2.5>
			<id2.5>5cfbfc1fc19d3c1c1ca18df9d1fb389673369724</id2.5>
			<ep2.6>6 IGLESIA EN RUINAS</ep2.6>
			<id2.6>d4e044ad97d7e7bffa12ff8e68cf80073ee1b9c5</id2.6>
			<ep2.7>7 MAPAS NEGROS Y HABITACIONES DE HOTEL</ep2.7>
			<id2.7>c2031c1ed91b853d7780d7e6f3ca387f22b8bdeb</id2.7>
			<ep2.8>8 ESTACION OMEGA</ep2.8>
			<id2.8>a8dd2caa67eeb3ed337556761326598805627943</id2.8>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/aowr4xpLP5sRCL50TkuADomJ98T.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/2Ahm0YjLNQKuzSf9LOkHrXk8qIE.jpg</fanart.3>
			<ep3.1>1 LA GRAN GUERRA Y LA MEMORIA MODERNA</ep3.1>
			<id3.1>eb58bec071731853f2b412a01aab66d526ee2426</id3.1>
			<ep3.2>2 DESPIDETE DEL MAÑANA</ep3.2>
			<id3.2>63fe6190704d46945f1e30cb051ab7421b9b9a78</id3.2>
			<ep3.3>3 EL GRAN NUNCA</ep3.3>
			<id3.3>7d567fecc1cad0e2d30e47e107969a48ebec24f0</id3.3>
			<ep3.4>4 LA HORA Y EL DIA</ep3.4>
			<id3.4>68980609470ad0ff1c63b3328c7270811faaa34d</id3.4>
			<ep3.5>5 SI TIENES FANTASMAS</ep3.5>
			<id3.5>ce0e2743811f86ec586c7bd8f0b1908ee6ade415</id3.5>
			<ep3.6>6 CAZADORES EN LA OSCURIDAD</ep3.6>
			<id3.6>2b71fa1dc9812c21f1f375f751e4c5233881bea7</id3.6>
			<ep3.7>7 EL ULTIMO PAIS</ep3.7>
			<id3.7>ca9bf49688afd09097219b97c883d9b33d77ad20</id3.7>
			<ep3.8>8 AHORA DAN CONMIGO</ep3.8>
			<id3.8>0a40e02fe51a6323a86a7ab9d25fa25ac3b82ce7</id3.8>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>UN PLANETA PERFECTO</title>
		<info>(2021) 5 episodios. El planeta Tierra es perfecto. Todo lo relacionado con nuestro mundo, su tamaño, su distancia del Sol, su giro e inclinación, su Luna, se adapta perfectamente a nuestra existencia, y las fuerzas naturales de nuestro planeta nutren perfectamente la vida. Un sistema meteorológico global circula y distribuye agua a todos los rincones del mundo, las corrientes marinas aportan nutrientes a los confines más profundos del océano, la luz solar calienta y da energía a todo lo que toca, y poderosos volcanes crean y fertilizan la tierra. Como resultado, no hay parte de nuestro planeta donde no se pueda encontrar vida.</info>
		<year>2021</year>
		<genre>Documental</genre>
		<thumb>https://www.themoviedb.org/t/p/original/1UafOca28JySZThxIBLWcKf2pZX.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/pRSldTvXOXHSIOFpfg4rTSdZw3m.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/1UafOca28JySZThxIBLWcKf2pZX.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/pRSldTvXOXHSIOFpfg4rTSdZw3m.jpg</fanart.1>
			<ep1.1>1 AL 5</ep1.1>
			<id1.1>46639c4f97601498b0ede60f5a7fb558ebbe19e9</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>UNA HISTORIA MUY REAL</title>
		<info>(2021) 7 episodios. Después de una noche en Filadelfia con su hermano que amenaza con sabotear algo más que su éxito, un famoso humorista busca desesperadamente una escapatoria.</info>
		<year>2021</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/8Ld8RjI0rYX1ucZRtSh82MADG11.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/yGpmZYJ8fb543B3tIKjzcRHgJsX.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/8Ld8RjI0rYX1ucZRtSh82MADG11.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/yGpmZYJ8fb543B3tIKjzcRHgJsX.jpg</fanart.1>
			<ep1.1>1 AL 3</ep1.1>
			<id1.1>f6bc88d8ceadb6a4fe625ed4e8176df7ae1a4b51</id1.1>
			<ep1.2>4 AL 5</ep1.2>
			<id1.2>d5bd0ef6677d0152fb551748eac30546cdb5e17d</id1.2>
			<ep1.3>6 AL 7</ep1.3>
			<id1.3>37d56eb26114639b0f6542c4491c3d9f85a3b7de</id1.3>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>UNORTHODOX</title>
		<info>(2020) 4 episodios. Una joven judía ortodoxa abandona un matrimonio concertado en Nueva York y pone rumbo a Berlín, donde vive su madre. La historia se inspira en las memorias de Deborah Feldman, en las que narra en primera persona cómo huyó de su estricta comunidad religiosa cuando era joven.</info>
		<year>2020</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/mdH0FbD328BIann1TPVwsPdCi0G.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/3DLJeCXNl8RcYeu1MNCzl6wae3c.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/mdH0FbD328BIann1TPVwsPdCi0G.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/3DLJeCXNl8RcYeu1MNCzl6wae3c.jpg</fanart.1>
			<ep1.1>1 AL 4</ep1.1>
			<id1.1>7efe8f044c92b3d2f65bed807cac6c778df70754</id1.1>
			<ep1.2>1 AL 4 (720)</ep1.2>
			<id1.2>8e7b8b04d4ca694f78fa237391e50793dbb23d2d</id1.2>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VALERIA</title>
		<info>(2020) 2 temporadas. 16 episodios. Valeria es una escritora en crisis, tanto por sus novelas como por su marido y la distancia emocional que les separa. Por todo ello Val se refugia en sus mejores amigas: Carmen, Lola y Nerea, quienes la apoyan durante su viaje emocional. Las tres están inmersas en un torbellino de emociones sobre amor, amistad, celos, infidelidad, dudas, desamores, secretos, trabajo, preocupaciones, alegrías y sueños sobre el futuro.Durante la segunda temporada, Valeria deberá enfrentarse a una decisión que podría marcar su futuro como escritora: esconderse tras un pseudónimo y vivir finalmente de su profesión o renunciar a la publicación de su novela y seguir exprimiendo el inagotable mundo de los contratos basura. Todo esto mientras su vida sentimental continúa tambaleándose pero sigue contando con el apoyo de sus tres amigas, que también pasarán por diferentes situaciones que cambiarán sus vidas para siempre.</info>
		<year>2020</year>
		<genre>Drama. Romance</genre>
		<thumb>https://image.tmdb.org/t/p/original/70Fku4Gle1dJlBFXIEAT1WxWJfJ.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/pFuQB6zUy8Oz0FFAJdKUZQPX9Js.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/70Fku4Gle1dJlBFXIEAT1WxWJfJ.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/pFuQB6zUy8Oz0FFAJdKUZQPX9Js.jpg</fanart.1>
			<ep1.1>1 LA IMPOSTORA</ep1.1>
			<id1.1>0e11cb790f0d119b4d938dbc784e51bd7e50d828</id1.1>
			<ep1.2>2 SEÑALES</ep1.2>
			<id1.2>9eb4e25de69d0407e8a07284f49b596c143412b3</id1.2>
			<ep1.3>3 ALASKA</ep1.3>
			<id1.3>20170e7be7079f282f87013fa7f6afbb9cdc75a6</id1.3>
			<ep1.4>4 LA CHARCA</ep1.4>
			<id1.4>ac3fabe00eb5912eee7ae3d7549dae53042d4f5c</id1.4>
			<ep1.5>5 MR CHAMPI</ep1.5>
			<id1.5>b878f14510e3847f2d515e305bf5ce0eb13b0a54</id1.5>
			<ep1.6>6 LA REGADERA</ep1.6>
			<id1.6>f1588f517bd04f4c884f76bd4089f3d414e3c200</id1.6>
			<ep1.7>7 EL PAQUETE</ep1.7>
			<id1.7>8eeefca8db92a69c539470a77185eb846fc9a0aa</id1.7>
			<ep1.8>8 PUNTOS</ep1.8>
			<id1.8>6c54934219ecf8a52c12ece42a73acec184fe9f0</id1.8>
		</t1>
		<t2>
			<thumb.2>https://www.themoviedb.org/t/p/original/fQuHD60Za7xLLMYLH4AXKhWnxFw.jpg</thumb.2>
			<fanart.2>https://www.themoviedb.org/t/p/original/lefZez6aV7f0LXdjbgzOkxNPm8B.jpg</fanart.2>
			<ep2.1>1 DEJA DE CORRER</ep2.1>
			<id2.1>554d1d60902cec282176ea8484d0adb0a902d556</id2.1>
			<ep2.2>2 SI NO SABE QUE HACER, ESCRIBA</ep2.2>
			<id2.2>5cf90022b080155f979d7383c8024e6f352b6003</id2.2>
			<ep2.3>3 AL 4</ep2.3>
			<id2.3>e145826778aa7fdec71ee5f253b7595779c4b8cf</id2.3>
			<ep2.4>5 AL 6</ep2.4>
			<id2.4>60083d6365a5480dcb69fe99a6f7742cc3a611a2</id2.4>
			<ep2.5>7 AL 8</ep2.5>
			<id2.5>405a446f8a578e8d8036ff24e4699dc0aa012d1a</id2.5>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VENENO</title>
		<info>(2020) 8 episodios. Miniserie sobre una famosa y controvertida transexual de los años 90 conocida como "la Veneno". Una serie inspirada en las memorias oficiales de Cristina Ortiz, "la Veneno", tituladas "¡Digo! Ni puta ni santa", escritas por Valeria Vegas, que cuenta la historia de la realidad transexual en España desde los años 60 hasta la actualidad. Cuando la autora del libro, Valeria, era una niña pequeña, nunca entendió por qué la gente la llamaba por un nombre que no era el suyo. Lo mismo le ocurrió a Cristina, por entonces llamada “Joselito”, que tuvo que sobrevivir a una violenta y cruel infancia en la España de los 60. Dos mujeres que nacieron en épocas muy diferentes pero que, por casualidad o por el destino, acaban unidas para siempre, cuando Valeria, estudiante de Periodismo, decide escribir un libro sobre la vida del icónico y mediático personaje. Adorada por su carisma y su forma de expresarse, libre, deslenguada y divertida, "la Veneno" alcanzó la popularidad gracias a sus apariciones televisivas en los años 90. Sin embargo, su vida y -sobre todo- su muerte siguen siendo un enigma.</info>
		<year>2020</year>
		<genre>Drama. Comedia</genre>
		<thumb>https://image.tmdb.org/t/p/original/tATGQfEe2KSWebtG5L0MIhhzakU.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/1NeUpmJUXOcVtiQqviYQvQierSZ.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/tATGQfEe2KSWebtG5L0MIhhzakU.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/1NeUpmJUXOcVtiQqviYQvQierSZ.jpg</fanart.1>
			<ep1.1>1 LA NOCHE QUE CRUZAMOS EL MISSISSIPPI</ep1.1>
			<id1.1>7f450a9dc417e27fe29e5975b531430686276d90</id1.1>
			<ep1.2>2 UN VIAJE EN EL TIEMPO</ep1.2>
			<id1.2>b2474aa76f0bf61c019e78b5452ad4c71f590a5e</id1.2>
			<ep1.3>3 ACARICIAME</ep1.3>
			<id1.3>8a24ba698af054d7c50394fa5c7d3f57f7c65c15</id1.3>
			<ep1.4>4 LA MALDICION DE ONASSIS</ep1.4>
			<id1.4>515e232cf09ca0a845f8ac73bdc19644b1dc2a02</id1.4>
			<ep1.5>5 CRISTINA A TRAVES DEL ESPEJO</ep1.5>
			<id1.5>681c8fd3411f87cec2e36ed644e047582a12504f</id1.5>
			<ep1.6>6 UNA DE LAS NUESTRAS</ep1.6>
			<id1.6>4df510e34824fb23837590176f1680625ce7d532</id1.6>
			<ep1.7>7 FUE MAS O MENOS ASI</ep1.7>
			<id1.7>db64d7bf85684efe728ca0db7d493859682235a2</id1.7>
			<ep1.8>8 LOS TRES ENTIERROS DE CRISTINA ORTIZ</ep1.8>
			<id1.8>2d4607abe0c30232819634d551323a1f1e5556ef</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VIKINGOS</title>
		<info>(2013-2020) 6 temporadas . 89 episodios. Narra las aventuras del héroe Ragnar Lothbrok, de sus hermanos vikingos y su familia, cuando él se subleva para convertirse en el rey de las tribus vikingas. Además de ser un guerrero valiente, Ragnar encarna las tradiciones nórdicas de la devoción a los dioses. Según la leyenda era descendiente directo del dios Odín.</info>
		<year>2013</year>
		<genre>Acción. Drama. Bélico</genre>
		<thumb>https://image.tmdb.org/t/p/original/fGb2THKXCQK1SiPT59ARt5Sg9kY.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/wPOmXBgjy4xrxxw5gyVFDfhx8Ii.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/uYaskJzmBhBdvkitDTjlH6gj9Pt.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/eIuUgn7ZUMCE2pQU4Omg9SuGYWT.jpg</fanart.1>
			<ep1.1>1 AL 9 (1080)</ep1.1>
			<id1.1>78e3a93ca2f6275ee8d1c9148df66c9af5048254</id1.1>
			<ep1.2>1 RITOS DE INICIACION</ep1.2>
			<id1.2>e427d332d3d6657fa75ef20db8baa420e220922e</id1.2>
			<ep1.3>2 LA IRA DE LOS VIKINGOS</ep1.3>
			<id1.3>61e66b7c410cea0f0023408ed76800b62661cbbb</id1.3>
			<ep1.4>3 DESPOJADOS</ep1.4>
			<id1.4>2980fa903f55b82a640a627bdb54b92ae5f5514f</id1.4>
			<ep1.5>4 PRUEBA</ep1.5>
			<id1.5>dbc590818038901a89be154a96cc690a5e4655f1</id1.5>
			<ep1.6>5 ATAQUE</ep1.6>
			<id1.6>b6753c62a4c53de16112b7224b6172c47020bd7f</id1.6>
			<ep1.7>6 SEPULTURA PARA LOS MUERTOS</ep1.7>
			<id1.7>52a5edf4e89aeabfd499dba535e2f6da73dd4bab</id1.7>
			<ep1.8>7 RESCATE REAL</ep1.8>
			<id1.8>6ee3ae3e34efb503a3174c7b209cc033258d2ffc</id1.8>
			<ep1.9>8 SACRIFICIO</ep1.9>
			<id1.9>95fb693a92a3d4d1e321e7c2ac99e1f61e3f54f1</id1.9>
			<ep1.10>9 FIN DE TRAYECTO</ep1.10>
			<id1.10>9e71a59843ddbbd465f15f73169a6e8e317dfe3a</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/g2Pgu5Dae9P99yJs0xZQtEagA76.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/dNeSt5qfIOOjLAOkW4z0Q5wmZLq.jpg</fanart.2>
			<ep2.1>1 AL 10 (1080)</ep2.1>
			<id2.1>934b749ab036d635dc80cb0b9d40e40e8cf1f17b</id2.1>
			<ep2.2>1 GUERRA DE HERMANOS</ep2.2>
			<id2.2>1d9e1ecfa166ced3b62780c1a38b2933dacf23f4</id2.2>
			<ep2.3>2 INVASION</ep2.3>
			<id2.3>42e42ae3bead189e18e38de188b7e5564b0dd2ad</id2.3>
			<ep2.4>3 TRAICION</ep2.4>
			<id2.4>2ea330f856be97ed760d73fb21c29449b5a52a8e</id2.4>
			<ep2.5>4 OJO POR OJO</ep2.5>
			<id2.5>95201e7ca1734ff578eb9a82bcd06656670200b8</id2.5>
			<ep2.6>5 RESPUESTAS SANGRIENTAS</ep2.6>
			<id2.6>5d710a4652544b2d3ee041cdf86c843f17570470</id2.6>
			<ep2.7>6 SIN PERDON</ep2.7>
			<id2.7>8036db85f319efd98fc59466cc01626766722ada</id2.7>
			<ep2.8>7 EL AGUILA DE SANGRE</ep2.8>
			<id2.8>effb75e9b17d9d73c17022a3b4866cb7b2834d64</id2.8>
			<ep2.9>8 SIN HUESO</ep2.9>
			<id2.9>803f108803b0dfc331db651591c1360847cec18a</id2.9>
			<ep2.10>9 LA ELECCION</ep2.10>
			<id2.10>75d0f0c30dcb64648cd2db98eacff997d3eccacb</id2.10>
			<ep2.11>10 LA ORACION DEL SEÑOR</ep2.11>
			<id2.11>150853fcb0932126b0a097064800923529ecab3a</id2.11>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/jICv5UxLDvUqCp1D2jAb0PSJWAw.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/54kFJxbMdlCwGZstCGGcLNXIisU.jpg</fanart.3>
			<ep3.1>1 AL 10 (1080)</ep3.1>
			<id3.1>8a33806e436f40cb0c7d7243ca00c1d7fa1d3b3d</id3.1>
			<ep3.2>1 MERCENARIO</ep3.2>
			<id3.2>8a9113f7b12cf404e3f0cba34acebe8be7225591</id3.2>
			<ep3.3>2 EL ERRANTE</ep3.3>
			<id3.3>320a57cd4c23ae1dc34a9bec380cd2395a9558c2</id3.3>
			<ep3.4>3 EL SINO DEL GUERRERO</ep3.4>
			<id3.4>fe5dc8b2eb8fdb5816b796d69f996f24dd79b099</id3.4>
			<ep3.5>4 MARCADA</ep3.5>
			<id3.5>fe4092d9964e7aafa556f2637b2c45509dd93842</id3.5>
			<ep3.6>5 EL USURPADOR</ep3.6>
			<id3.6>922eb6cc2feb1955202694eedd1d4394c66e6418</id3.6>
			<ep3.7>6 RENACIDO</ep3.7>
			<id3.7>6b333cab57b9e4450ea74dcb2ea81be1eb1b4768</id3.7>
			<ep3.8>7 PARIS</ep3.8>
			<id3.8>c22cd6c2f002bfae22238d87e948a74673d73d2d</id3.8>
			<ep3.9>8 A LAS PUERTAS</ep3.9>
			<id3.9>9a1ceb5d273a8b4fb7dac84637525d3f8dbb4a3c</id3.9>
			<ep3.10>9 PUNTO CRITICO</ep3.10>
			<id3.10>a48f8f91551139f2aa47fc5322d52cef815c9529</id3.10>
			<ep3.11>10 LOS MUERTOS</ep3.11>
			<id3.11>5faf9ed5939981cd5c83df8916c0416da9e2f3b8</id3.11>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/nHJHNRApy8vmTUvZx59l1f1GXzB.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/yzDvrRi8JrrG9rePRmswfUTWe0D.jpg</fanart.4>
			<ep4.1>1 AL 20 (1080)</ep4.1>
			<id4.1>ec718fb6681b568b740c51ce177d3c59e1444d6f</id4.1>
			<ep4.2>1 UNA BUENA TRAICION</ep4.2>
			<id4.2>cc58ea1e54d9fc43bac77e25abb338721b91dfef</id4.2>
			<ep4.3>2 MATAR A LA REINA</ep4.3>
			<id4.3>4c66a62e41b4d390b1bb0e33f077d0a21daf72d1</id4.3>
			<ep4.4>3 MISERICORDIA</ep4.4>
			<id4.4>37cdda61388d3caa50c9e9cf26ce3a555d98b15f</id4.4>
			<ep4.5>4 EL SOLSTICIO DE INVIERNO</ep4.5>
			<id4.5>fc3ea84ba843473c59fafe0d1e8e7a1cc51ba9d4</id4.5>
			<ep4.6>5 PROMETIDA</ep4.6>
			<id4.6>d7718ebceaa6c9b2fe5670288b148b26d0acea39</id4.6>
			<ep4.7>6 LO QUE PODRIA HABER SIDO</ep4.7>
			<id4.7>15936ab094811b34b14b0a73af61edc85ffa50c7</id4.7>
			<ep4.8>7 EL BENEFICIO Y LA PERDIDA</ep4.8>
			<id4.8>1f38a943c42fb0bb097bb1de12293f6e0417749b</id4.8>
			<ep4.9>8 PORTEO</ep4.9>
			<id4.9>7c892a54ecdfcb4c5c41d45b9eac540b3baee52f</id4.9>
			<ep4.10>9 MUERTE ALREDEDOR</ep4.10>
			<id4.10>509fec9858f2b54e77a9bd582182ddd78bc6e9d4</id4.10>
			<ep4.11>10 EL ULTIMO BARCO</ep4.11>
			<id4.11>655deeaa0b65d906b38d68ae81aae755469ae12e</id4.11>
			<ep4.12>11 EL FORASTERO</ep4.12>
			<id4.12>0155ac08cb4631293dbd6d55f48aa9556799aacd</id4.12>
			<ep4.13>12 LA VISION</ep4.13>
			<id4.13>bf8006141259a5270bd9e13ad369537f4e6c9410</id4.13>
			<ep4.14>13 DOS VIAJES</ep4.14>
			<id4.14>b005c0c796cbe71f8babd850938d900f4a6d015b</id4.14>
			<ep4.15>14 EN ESA HORA INCIERTA DEL ALBA</ep4.15>
			<id4.15>5bccf1e880956556874984b3931112f1c4363b49</id4.15>
			<ep4.16>15 TODOS SUS ANGELES</ep4.16>
			<id4.16>cbb262540ae794e40d32071e86e99caee1079cde</id4.16>
			<ep4.17>16 VIAJES</ep4.17>
			<id4.17>fa3b9a588f083cc95023d7788fa1997818e3670a</id4.17>
			<ep4.18>17 EL GRAN EJERCITO</ep4.18>
			<id4.18>fe68081cb1b10fff366f2a45b6e003d93bd8d0f4</id4.18>
			<ep4.19>18 VENGANZA</ep4.19>
			<id4.19>dbc573cf053b4a133a12f3f70cbd782dcf099a89</id4.19>
			<ep4.20>19 EN VISPERAS</ep4.20>
			<id4.20>d5da79cf412b693abf7d2d7ecd61ca88e2f98ef8</id4.20>
			<ep4.21>20 AJUSTES DE CUENTAS</ep4.21>
			<id4.21>2bf72a9c03bdeb867bc9066ba62f3ecd1df3a1d8</id4.21>
		</t4>
		<t5>
			<thumb.5>https://image.tmdb.org/t/p/original/745J5LgH1Qlq82QX3BxekPsqXt4.jpg</thumb.5>
			<fanart.5>https://image.tmdb.org/t/p/original/wPOmXBgjy4xrxxw5gyVFDfhx8Ii.jpg</fanart.5>
			<ep5.1>1 AL 20 (1080)</ep5.1>
			<id5.1>1e73f9bad0f11735c89bbe6e7e7f01ca5d752167</id5.1>
			<ep5.2>1 EL REY PESCADOR</ep5.2>
			<id5.2>5237de6be4166d7c41a81f981d92a14cd97e412a</id5.2>
			<ep5.3>2 LOS DIFUNTOS</ep5.3>
			<id5.3>7cfc5522163ab46fcc2438a843b8a025a57255d0</id5.3>
			<ep5.4>3 PATRIA</ep5.4>
			<id5.4>e49c55412d0a80a459cf60f26265958566564512</id5.4>
			<ep5.5>4 EL PLAN</ep5.5>
			<id5.5>662a6031d43babb01cd48be49f57993a3357e739</id5.5>
			<ep5.6>5 EL PRISIONERO</ep5.6>
			<id5.6>FF329AB775A15F30F593242C66685BB8C5C4A6FE</id5.6>
			<ep5.7>6 EL MENSAJE</ep5.7>
			<id5.7>8902b5264dcb6705760977ae8f9c6b7f96b73bad</id5.7>
			<ep5.8>7 LUNA LLENA</ep5.8>
			<id5.8>beb0739bb30499ee52720409484cd606aef517cc</id5.8>
			<ep5.9>8 EL CHISTE</ep5.9>
			<id5.9>f0460786bf285fa679ef4e49bb2748aa09d2704b</id5.9>
			<ep5.10>9 UNA HISTORIA SENCILLA</ep5.10>
			<id5.10>7f51c9bb61683b9bac042ec336ac061c0c45cdd7</id5.10>
			<ep5.11>10 MOMENTOS DE VISION</ep5.11>
			<id5.11>1f562925bc70e513311eae5056b24e90de73fbb0</id5.11>
			<ep5.12>11 LA REVELACION</ep5.12>
			<id5.12>ecc7d48b380e364349681d74db2ac9d2b8d8e6d9</id5.12>
			<ep5.13>12 EL ASESINATO MAS VIL</ep5.13>
			<id5.13>eb638105a8b88a482666f6d05f3ced90e1336b27</id5.13>
			<ep5.14>13 UN NUEVO DIOS</ep5.14>
			<id5.14>5135a6e1c5753bf14b1810ea446303078dd4b979</id5.14>
			<ep5.15>14 EL MOMENTO PERDIDO</ep5.15>
			<id5.15>57c88acf5df558dd1eb4c75cc92496a139832879</id5.15>
			<ep5.16>15 INFIERNO</ep5.16>
			<id5.16>2355f36d560f23b3dc3c1656397ca751a51cc7bc</id5.16>
			<ep5.17>16 EL BUDA</ep5.17>
			<id5.17>38f2241540c30432e9885cf2896a554587fb757b</id5.17>
			<ep5.18>17 COSAS TERRIBLES</ep5.18>
			<id5.18>5982c411742aab9ea605010c8c4e530189c9d321</id5.18>
			<ep5.19>18 BALDUR</ep5.19>
			<id5.19>dc9e00f4a02fa081b56a0ea2dd613787b413c6fc</id5.19>
			<ep5.20>19 LO QUE OCURRE EN LA CUEVA</ep5.20>
			<id5.20>f59568d57fabfccb7d8ee48665166ecd096a9749</id5.20>
			<ip5.21>20 RAGNAROK</ip5.21>
			<id5.21>d3941d4ff46bec1c3de2bb7a8f2b0847601aad7d</id5.21>
		</t5>
		<t6>
			<thumb.6>https://image.tmdb.org/t/p/original/ff1zhqvwfS5HvRNcA5UFrH0PA2q.jpg</thumb.6>
			<fanart.6>https://image.tmdb.org/t/p/original/aq2yEMgRQBPfRkrO0Repo2qhUAT.jpg</fanart.6>
			<ep6.1>1 AL 10 (1080)</ep6.1>
			<id6.1>a54c6e3a61044f68a8b414622aaf4fc26bf32eb9</id6.1>
			<ep6.2>1 NUEVOS COMIENZOS</ep6.2>
			<id6.2>bd739f0642e6d7def711f5fb715d41c3a8a9c379</id6.2>
			<ep6.3>2 EL PROFETA</ep6.3>
			<id6.3>dc65d547f8353cb09c5bc986d4b991caf61799a8</id6.3>
			<ep6.4>3 FANTASMAS DIOSES Y LACAYOS</ep6.4>
			<id6.4>c98b21ad5fd9da31a29ca5e04ecc4fdce7e4a806</id6.4>
			<ep6.5>4 TODOS LOS PRISIONEROS</ep6.5>
			<id6.5>7f039c153b4707ee721a63f6d35b9efa0de5003d</id6.5>
			<ep6.6>5 LA LLAVE</ep6.6>
			<id6.6>53202f1b740ebd87b548dbc235e5d95518d3eab3</id6.6>
			<ep6.7>6 LA MUERTE Y LA SERPIENTE</ep6.7>
			<id6.7>8184460f0415e3c2faeb7e22645ee927a1e4f39d</id6.7>
			<ep6.8>7 LA DONCELLA DE HIELO</ep6.8>
			<id6.8>53970f5c2b73193dbfaf8cc47b949ed39a3b0d28</id6.8>
			<ep6.9>8 VALHALLA PUEDE ESPERAR</ep6.9>
			<id6.9>94aeaaaf7a2b40a48802e1eca97f22532683882d</id6.9>
			<ep6.10>9 RESURRECCION</ep6.10>
			<id6.10>0f340a88270a4783cab9b72053b154a5bbaa5b6a</id6.10>
			<ep6.11>10 LOS PLANES MEJOR TRAZADOS</ep6.11>
			<id6.11>13dfa8cc9ff6b7ddcfe0998ef1cbf86970ac7732</id6.11>
		</t6>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VIKINGOS: VALHALLA</title>
		<info>(2022) 12 episodios. Secuela de 'Vikingos' ambientada 100 años después y centrada en las aventuras de Leif Erikson, Freydis, Harald Hardrada y el rey normando Guillermo el Conquistador.</info>
		<year>2022</year>
		<genre>Aventuras. Acción. Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/izIMqapegdEZj0YVDyFATPR8adh.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/izIMqapegdEZj0YVDyFATPR8adh.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/izIMqapegdEZj0YVDyFATPR8adh.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/izIMqapegdEZj0YVDyFATPR8adh.jpg</fanart.1>
			<ep1.1>1 AL 8</ep1.1>
			<id1.1>DEDFC5B0C88154356ECFD58F5627B77BB4BA2C09</id1.1>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VIS A VIS</title>
		<info>(2015-2019) 4 temporadas. 40 episodios. Narra las vivencias en prisión de Macarena, una joven frágil e inocente que nada más ingresar en la cárcel se ve inmersa en una difícil situación a la que tendrá que aprender a adaptarse. Además del shock que le supone acabar de golpe con su placentera vida acomodada, pronto descubrirá que demasiada gente en la cárcel está tras la pista de nueve millones de euros robados de un furgón.</info>
		<year>2015</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/aaVkpxuhpfn09wKl6KAyahYtZfn.jpg</thumb>
		<fanart>https://imgur.com/QfHKTb9.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/aaVkpxuhpfn09wKl6KAyahYtZfn.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/lEfjcTqToU4K5zJgN16CnU9C8hT.jpg</fanart.1>
			<ep1.1>1 UN MAL DIA</ep1.1>
			<id1.1>2c14da8a18b8c2bcc24b6eca82ca24d395cbc5fc</id1.1>
			<ep1.2>2 COSIENDO A CAMPANILLA</ep1.2>
			<id1.2>e0525d2e46d4a0240d1da7d2450c5427220a8a3e</id1.2>
			<ep1.3>LO QUE SUCEDE, CONVIENE</ep1.3>
			<id1.3>a113fa7a97dcde7d777562816a8481d5520ca412</id1.3>
			<ep1.4>4 UN DIA DE CAMPO</ep1.4>
			<id1.4>52ea95b3e1b6482036c1f50024dce0424909f9a2</id1.4>
			<ep1.5>5 LA CRUDA REALIDAD</ep1.5>
			<id1.5>8dc990dd7a6d49b43d58b144094ce736e7bb1665</id1.5>
			<ep1.6>6 TE VENDRAN A BUSCAR...</ep1.6>
			<id1.6>f9129f105f6e9c3f70afbeb89481231e967f567c</id1.6>
			<ep1.7>7 A LAS CINCO EN PUNTO DE LA TARDE</ep1.7>
			<id1.7>44a99cfec48a53b9659c0a565e9f204f76cb53d8</id1.7>
			<ep1.8>8 PRETTY WOMAN</ep1.8>
			<id1.8>366056f66cdcddf5d661a3843847b7bc8ad012e9</id1.8>
			<ep1.9>9 NEGRA, NEGRITA, NEGRATA</ep1.9>
			<id1.9>a8cbacf14aa1ad5893cad90145186a738dc9b725</id1.9>
			<ep1.10>10 GATO GRIS</ep1.10>
			<id1.10>7e83ac5296a77112f828534660a56331bddcc7eb</id1.10>
			<ep1.11>11 EL PRINCIPIO DE LAS TORTUGAS</ep1.11>
			<id1.11>184b83b92c17143a84995d2e1266c487d7242161</id1.11>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/JHodKMDhQmZhUHZWmXdJlTJWaJ.jpg</thumb.2>
			<fanart.2>https://i.imgur.com/Pp5Y9hf.jpg</fanart.2>
			<ep2.1>1 A UN MONO DOS PISTOLAS</ep2.1>
			<id2.1>0753316938540D8C8705E61756EA701661CD83FA</id2.1>
			<ep2.2>2 HOGAR DULCE HOGAR</ep2.2>
			<id2.2>E508EC1AD8CD59A632590068AEFFFF52D65F08FD</id2.2>
			<ep2.3>3 LA GALLINITA</ep2.3>
			<id2.3>D22CF1BEEB48C001A5C60ABB5630A38C42B54A27</id2.3>
			<ep2.4>4 LA TEORIA DE DARWIN</ep2.4>
			<id2.4>DD6D9ABA88953D78CCF7CE322C3B15EA4E7BD3D7</id2.4>
			<ep2.5>5 EN LA LINEA DE FUEGO</ep2.5>
			<id2.5>9fd842ce69295d393e9a471222d4431e9fa3df96</id2.5>
			<ep2.6>6 LOS MANDRILES</ep2.6>
			<id2.6>b83ded565feac1995e0b896f4361b5a34eaeafe8</id2.6>
			<ep2.7>7 BON APPETIT</ep2.7>
			<id2.7>ca7e5595980cb1c5ddb53a36172a7fa237c441b0</id2.7>
			<ep2.8>8 LA VIDA SIGUE</ep2.8>
			<id2.8>0ee50472d49cff5e7d6af789e35555353832c9b2</id2.8>
			<ep2.9>9 PILLARSE LOS DEDOS</ep2.9>
			<id2.9>511dd010058f21a175f33c2ef99b79136aa5fcf0</id2.9>
			<ep2.10>10 PECADOS Y CONFUSIONES</ep2.10>
			<id2.10>b5ab5eed856dfda9c4886c3094c36e50f1e47a46</id2.10>
			<ep2.11>11 PATRONA DE LOS DESESPERADOS</ep2.11>
			<id2.11>ae636a02cba2a23f1c34b3f8ec75eb588337f7ec</id2.11>
			<ep2.12>12 PLATANO Y LIMON</ep2.12>
			<id2.12>e5fbd393db1ca1ab37547734c562da30a3bf0ee0</id2.12>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/ub4lfjF8t1N2Z8CJbj4Rkpbp0WD.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/ck4WJ3dLXzUd3cNgujxuuk0QvlW.jpg</fanart.3>
			<ep3.1>1 CRUZ DEL NORTE</ep3.1>
			<id3.1>ec23fac0f40bd70c20e5e422dd0659cd95850d0d</id3.1>
			<ep3.2>2 MUY FACIL O MUY DIFICIL</ep3.2>
			<id3.2>3aa0e64c69b5e9794a745288234f37605bfbe234</id3.2>
			<ep3.3>3 UN GRANO DE ARROZ</ep3.3>
			<id3.3>b9ee2919af09f2f24500ed94edc02aba2eb3945f</id3.3>
			<ep3.4>4 LA BANDERA EN EL MURO</ep3.4>
			<id3.4>2e7d0cd7d0f6f9a501e436f2eb8dd250a8873396</id3.4>
			<ep3.5>5 ALGUIEN A QUIEN LE IMPORTES UNA MIERDA</ep3.5>
			<id3.5>97c0e1127f08884af7eb8e8cbe657e4dc6dfab12</id3.5>
			<ep3.6>6 SEIS MESES DAN PARA MUCHO</ep3.6>
			<id3.6>2347c910177c5b59c4aa30bfa79612bd7d2d1fea</id3.6>
			<ep3.7>7 FUIMOS NIÑAS</ep3.7>
			<id3.7>1ad7caf43f640e0d586d7037128b6ebf8ea91098</id3.7>
			<ep3.8>8 LO QUE SABEMOS DE LOS MONSTRUOS</ep3.8>
			<id3.8>c331de019ed92fe79225664d5a337060f5dd69bd</id3.8>
		</t3>
		<t4>
			<thumb.4>https://image.tmdb.org/t/p/original/9LGeuwGWjeKT0owBUiXSTIZKyyM.jpg</thumb.4>
			<fanart.4>https://image.tmdb.org/t/p/original/iukGFUQQWUdgwTGGDLb1Pw3GoZP.jpg</fanart.4>
			<ep4.1>1 LA BARBIE</ep4.1>
			<id4.1>aef741ad66e7483959736cebe648c2f0cfb881b6</id4.1>
			<ep4.2>2 LA FUGA</ep4.2>
			<id4.2>5240728c0cfc5185fc6710ee08e81bb05f26a880</id4.2>
			<ep4.3>3 EL UMBRAL DEL DOLOR</ep4.3>
			<id4.3>cafb2d9012be52508253c1e4650c8bcaddbefe3a</id4.3>
			<ep4.4>4 MAMA</ep4.4>
			<id4.4>a35c7e812c15d0a733c4d5836e654c03e86aa01c</id4.4>
			<ep4.5>5 TRAICION</ep4.5>
			<id4.5>26bd207f0601a8c4b317915ed5986f416cdb332e</id4.5>
			<ep4.6>6 MALA PERSONA</ep4.6>
			<id4.6>ed9c2e1b8c291c6fb72988cd24fbb58963d59552</id4.6>
			<ep4.7>7 VUELTA A CASA</ep4.7>
			<id4.7>165c1616e98758c49189e6d4067c1d95fc5b1604</id4.7>
			<ep4.8>8 LA MAREA AMARILLA</ep4.8>
			<id4.8>62a2b7eb734458c84a060d82a8e09de64298a73e</id4.8>
		</t4>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VIS A VIS EL OASIS</title>
		<info>(2020) 8 episodios. Zulema y Maca serán las protagonistas de esta secuela / spin-of de "Vis a Vis", que contará con otras actrices de la serie original.</info>
		<year>2020</year>
		<genre>Thriller. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/7EOWkNGxXq2MBZxfV3ZxjRM4vlH.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/7wm4CH6LbsFul4wWmmfXGcStVsk.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/7EOWkNGxXq2MBZxfV3ZxjRM4vlH.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/7wm4CH6LbsFul4wWmmfXGcStVsk.jpg</fanart.1>
			<ep1.1>1 COMO ESOS MATRIMONIOS QUE ANTES DE SEPARARSE TIENEN UN HIJO</ep1.1>
			<id1.1>320d27c5ea4cf4677e5c675e499b0c418a19deee</id1.1>
			<ep1.2>2 LA BODA</ep1.2>
			<id1.2>62b476f0d6cc1a3a09da46292c17842ad223a524</id1.2>
			<ep1.3>3 COMO TE QUIERE UNA MADRE</ep1.3>
			<id1.3>4bc1fa2d0b076907779a147298350408d8a4af36</id1.3>
			<ep1.4>4 IGUALES O NADA</ep1.4>
			<id1.4>ef8157054a8d6573d6f93077b44e5c6483bee57b</id1.4>
			<ep1.5>5 LA VIDA DE LOS MOSQUITOS</ep1.5>
			<id1.5>d6691c316304e6bee7c4ad9375cd2b4c6bf6b5f9</id1.5>
			<ep1.6>6 ODIO EL SENTIMENTALISMO</ep1.6>
			<id1.6>be26f3ee20e43cf7106768e38f92f5c506cc6001</id1.6>
			<ep1.7>7 LA LINEA ROJA</ep1.7>
			<id1.7>6026d9b817da838ece50892f25ec184b94287515</id1.7>
			<ep1.8>8 ¿QUIEN ERA ZULEMA ZAHIR?</ep1.8>
			<id1.8>f6787afa832c992dd28eb13d1108a49455816bd0</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>VIVIR SIN PERMISO</title>
		<info>(2018-2020)  2 temporadas. 23 episodios. Nemesio 'Nemo' Bandeira (José Coronado) es un hombre que se enriqueció en el pasado con actividades ilegales pero que ha conseguido blanquear su trayectoria hasta erigirse en uno de los empresarios más influyentes de Galicia, a través de una importante compañía conservera. Cuando a Nemo le diagnostican alzheimer, tratará de ocultar su enfermedad para no mostrarse vulnerable mientras pone en marcha el proceso para elegir a su sucesor, lo que provoca una hecatombe en la familia. Sus dos hijos legítimos que nunca han tenido interés alguno por los negocios, de pronto intentan demostrar que cada cual es el candidato más adecuado. Su ahijado, el brillante e implacable abogado Mario Mendoza, es objetivamente el más preparado, aunque carece de algo vital: llevar la misma sangre que su padrino. Al saber que Nemo no le contempla como heredero del imperio, pondrá en marcha su propio plan sin abandonar su encantadora sonrisa, lo que le convertirá en el más peligroso y despiadado de sus enemigos.</info>
		<year>2018</year>
		<genre>Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/58Z3KlTeOe1rHwa6x3xSRRVTIXk.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/n5QKXx4XjbnqO5GbBAgowgfK6v6.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/58Z3KlTeOe1rHwa6x3xSRRVTIXk.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/8ajHJm9g6CRGdGeaoKHr3hD4n75.jpg</fanart.1>
			<ep1.1>1 LLAMADME NEMO</ep1.1>
			<id1.1>0affa7f588f6ebe9dc7dfafa71ecbe8817e6fc4b</id1.1>
			<ep1.2>2 EL VALOR DE LA PALABRA</ep1.2>
			<id1.2>c137229446418314d913f5b68aaf88bafda85f10</id1.2>
			<ep1.3>3 TIBURONES Y OTROS PECES</ep1.3>
			<id1.3>33be3b8d4334e8ffc8520919ff293a855d389e9f</id1.3>
			<ep1.4>4 LA BOFETADA</ep1.4>
			<id1.4>065fe766cfb9293dc6ef00fc8618a22d3e439161</id1.4>
			<ep1.5>5 LEJOS DE AQUI</ep1.5>
			<id1.5>e255695bc5a7215701e6877d48aa9004a2c45133</id1.5>
			<ep1.6>6 AIRES DE CAMBIO</ep1.6>
			<id1.6>c90db23c7780ab88d314c7eab2e10bd40ee5e0bb</id1.6>
			<ep1.7>7 SI ESTAS PERDIENDO TU ALMA</ep1.7>
			<id1.7>0a5b94266c1d9727dfc1b1532709e4ad6599de84</id1.7>
			<ep1.8>8 SACRIFICIOS</ep1.8>
			<id1.8>030c12600f47abcaf187d6b19f115099ddf012a6</id1.8>
			<ep1.9>9 UNA FAMILIA UNIDA</ep1.9>
			<id1.9>c62b12a737126a39c7c04034b3b8834fc758f15f</id1.9>
			<ep1.10>10 INCAPACES</ep1.10>
			<id1.10>5e23a9a3c4d314b6c2472c64303e94923d70eb61</id1.10>
			<ep1.11>1 A 10 ( 1080)</ep1.11>
			<id1.11>806300bbe34d2a24b56042371b9e0346593f706b</id1.11>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/58Z3KlTeOe1rHwa6x3xSRRVTIXk.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/28UmVogNlxAQmIHVcLnrPaEKMIS.jpg</fanart.2>
			<ep2.1>1 AL 10 (1080)</ep2.1>
			<id2.1>4d47eec6e2fc59eb10de0dbd80d4bfb97f068b85</id2.1>
			<ep2.2>1 MIL RAZONES PARA NO VOLVER</ep2.2>
			<id2.2>405d3938eeb34feb9ae235b90fe1a610821ba176</id2.2>
			<ep2.3>2 EL REY SOLITARIO</ep2.3>
			<id2.3>f225c12e902914b542609ed12682605ac94ec25c</id2.3>
			<ep2.4>3 CREE EN MI</ep2.4>
			<id2.4>e48d9bfa0d27091c5008e65ec059f8fa862047c1</id2.4>
			<ep2.5>4 EL LEON ENJAULADO</ep2.5>
			<id2.5>62ebb8bef7a53476cbbed6421cdec25846750503</id2.5>
			<ep2.6>5 EL PRIMER ERROR</ep2.6>
			<id2.6>199fd2d6caa189eb6ab461a4542888a324553b7d</id2.6>
			<ep2.7>6 EL ULTIMO DESEMBARCO</ep2.7>
			<id2.7>d5aa9c0e1376127d82637011438e92caafa51a6f</id2.7>
			<ep2.8>7 NO TE GUARDO RENCOR</ep2.8>
			<id2.8>87f714e7537cc76815ee4f6abd794d88d6bb9ff3</id2.8>
			<ep2.9>8 UN MAL MENOR</ep2.9>
			<id2.9>6e91b0145597dc738ef4bd2c5e4d807bcfdc9d78</id2.9>
			<ep2.10>9 UN LUGAR MUY OSCURO</ep2.10>
			<id2.10>1ee6b902910e2ca49409170062dec3838be17a59</id2.10>
			<ep2.11>10 ANTES DE IRME</ep2.11>
			<id2.11>fc2a522e6d736bafc0c551ade40032126b34f49f</id2.11>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>WARRIOR</title>
		<info>(2019) 2 temporadas. 20 episodios. A finales del siglo XIX, un prodigio de las artes marciales llega desde China a San Francisco, inmerso en una sangrienta guerra de bandas en el barrio de Chinatown, revolucionando la ciudad... Serie basada en algunos escritos de Bruce Lee.</info>
		<year>2019</year>
		<genre>Acción. Drama</genre>
		<thumb>https://image.tmdb.org/t/p/original/wMszXKAZrT9JhA1gaizIjPlFiXX.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/4EoVP87zJ7SDZGbYLjz439Avn2S.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/obT6uoln7cA0pvQlqqoZ4IvjPml.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/jSBUu67DL0ajUo2e8b2orVX2TOw.jpg</fanart.1>
			<ep1.1>1 EL CHINCHE QUE PICA</ep1.1>
			<id1.1>50a0ead7d394af1fab6ead412dfa6bbb1d384b5a</id1.1>
			<ep1.2>2 EN LA BIBLIA NO HAY CHINOS</ep1.2>
			<id1.2>48edb1b00b58520bee148a6ea1d9c56bb2bcb766</id1.2>
			<ep1.3>3 JOHN DE LA CHINA</ep1.3>
			<id1.3>22018a0e7d136e9de0afb7f3fce70806f268bae0</id1.3>
			<ep1.4>4 LA MONTAÑA BLANCA</ep1.4>
			<id1.4>aefdc98faded857d59d97b84bfe8908ba96c5bf6</id1.4>
			<ep1.5>5 LA SANGRE Y LA MIERDA</ep1.5>
			<id1.5>6235128b150785d12f1b2443df694ed1941b4d3f</id1.5>
			<ep1.6>6 APALEADO, ESCUPIDO Y PISOTEADO</ep1.6>
			<id1.6>806575daa485d219390a2c2e43380110f3095c8c</id1.6>
			<ep1.7>7 EL TIGRE Y EL ZORRO</ep1.7>
			<id1.7>7c35fa59faf4592bac5b1427769ba58dd346e3f9</id1.7>
			<ep1.8>8 PENSAR NO ESTA INCLUIDO EN LA NOMINA</ep1.8>
			<id1.8>8b9419dfec2b5f7b91d9f55e96012ec71ccc9dea</id1.8>
			<ep1.9>9 BOXEO CHINO</ep1.9>
			<id1.9>bb093ac81be80e1c66912cbccf1b9959d49360db</id1.9>
			<ep1.10>10 SI VAS A INCLINARTE, DOBLATE BIEN</ep1.10>
			<id1.10>cf279d7ea811cd6c231635a2c51cb5177b885a1c</id1.10>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/eLPxEM7WAvljjFv4UwJ1awhANa9.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/baA21LCF2LSOrExY19e5RvTCZ1R.jpg</fanart.2>
			<ep2.1>1 APRENDE A AGUANTAR O CONTRATA UN GUARDAESPALDAS</ep2.1>
			<id2.1>350fb1792ec8608e21d8502f84373241de072948</id2.1>
			<ep2.2>2 LA CONEXION CHINA</ep2.2>
			<id2.2>fb794e03331f80927743d83539fac9849279708f</id2.2>
			<ep2.3>3 NO ES COMO HACEMOS NEGOCIOS</ep2.3>
			<id2.3>e12ee46bbcf3a5fecad7c03ec38854a375c44836</id2.3>
			<ep2.4>4 SI NO VES SANGRE, NO HAS VENIDO A JUGAR</ep2.4>
			<id2.4>617c23eb5ccd4a2b81a3b2d79d7d20b82add36e3</id2.4>
			<ep2.5>5 NI POR BEBER, NI POR COGER, NI POR UNA MALDITA PLEGARIA</ep2.5>
			<id2.5>3aa8aa02cb4c19d7d0ca38eaa1162ff27bbc6925</id2.5>
			<ep2.6>6 PARA UN HOMBRE CON UN MARTILLO, TODO PARECE UN CLAVO</ep2.6>
			<id2.6>4e557d9d2e46322f893dc9cab20ea00ca7ecf296</id2.6>
			<ep2.7>7 SI ESPERAS LO SUFICIENTE JUNTO AL RIO...</ep2.7>
			<id2.7>20c250caecd7d9b3f4650dd507bf81870eadda1d</id2.7>
			<ep2.8>8 TODOS LOS ENEMIGOS, EXTRANJEROS O INTERNOS</ep2.8>
			<id2.8>2f29d8d0ca5abf073d3a3c810c7c5ce10a171925</id2.8>
			<ep2.9>9 ENTRA EL DRAGON</ep2.9>
			<id2.9>2f29d8d0ca5abf073d3a3c810c7c5ce10a171925</id2.9>
			<ep2.10>10 HOMBRE EN LA PARED</ep2.10>
			<id2.10>82d78786bf3f016336c748a1d2e73c5204609ea9</id2.10>
		</t2>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>WATCHMEN</title>
		<info>(2019) 1 temporada. 9 episodios. Ambientada en un universo alternativo en el que los vigilantes enmascarados son tratados como criminales, la serie 'Watchmen' utiliza el universo del cómic original creado por Alan Moore para crear un contenido completamente nuevo.</info>
		<year>2019</year>
		<genre>Thriller. Fantástico. Ciencia ficción. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/m8rWq3j73ZGhDuSCZWMMoE9ePH1.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/gFeaXBnOO14aOQhMQrr5tbyhMTw.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/gFeaXBnOO14aOQhMQrr5tbyhMTw.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/m8rWq3j73ZGhDuSCZWMMoE9ePH1.jpg</fanart.1>
			<ep1.1>1 ES VERANO Y NOS ESTAMOS QUEDANDO SIN HIELO</ep1.1>
			<id1.1>09f1220d3fb77bdd56e41ad09f33084529e12003</id1.1>
			<ep1.2>2 PROEZAS DE EQUITACION COMANCHE</ep1.2>
			<id1.2>a76f71e0b1475318d7fbd4b176d050174cd3ea4a</id1.2>
			<ep1.3>3 ELLA FUE ASESINADA POR BASURA ESPACIAL</ep1.3>
			<id1.3>8186b07a118d7d3aabc8ee13e013fb8c1107e74b</id1.3>
			<ep1.4>4 SI NO TE GUSTA MI HISTORIA ESCRIBE LA TUYA</ep1.4>
			<id1.4>ef472b4065d6d19144723b153303e60c69ecd53a</id1.4>
			<ep1.5>5 POCO TEMOR A LOS RAYOS</ep1.5>
			<id1.5>7dfd4bc9debc49ca69a8149ce27e2a255612c822</id1.5>
			<ep1.6>6 ESTE SER EXTRAORDINARIO</ep1.6>
			<id1.6>b03aefc0da4fbec30d0599aa2312237b9f84761a</id1.6>
			<ep1.7>7 UN TEMOR CASI RELIGIOSO</ep1.7>
			<id1.7>d96cc7bf631b2c1362caab92a7f0676c6740cf4f</id1.7>
			<ep1.8>8 UN DIOS ENTRA EN UN BAR</ep1.8>
			<id1.8>2fd36fd8c0e0857e7e95f945675eca62958578bb</id1.8>
			<ep1.9>9 MIRA COMO VUELAN</ep1.9>
			<id1.9>2b1d5ef1863fc6aa03bdd3d34bd748427449d4a8</id1.9>
			<ep1.10>1 AL 9 ( 1080 )</ep1.10>
			<id1.10>647f806a5986d5ac04ccb4935ca3d48b612c27d2</id1.10>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>WECRASHED</title>
		<info>(2022) 8 episodios. Inspirada en hechos reales, se desarrolla en torno a la historia de amor de sus protagonistas. En menos de una década, WeWork pasó de ser una empresa de coworking a convertirse en una marca global valorada en 47 000 millones de dólares. Solo un año después, su valoración descendió en 40 000 millones. ¿Qué pasó?</info>
		<year>2022</year>
		<genre>Drama</genre>
		<thumb>https://www.themoviedb.org/t/p/original/rFlYeo84b5YtzNkN0IonN6ZCPic.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/x8KTttToqfHC92JiD2Cs8WWkBG7.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/rFlYeo84b5YtzNkN0IonN6ZCPic.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/x8KTttToqfHC92JiD2Cs8WWkBG7.jpg</fanart.1>
			<ep1.1>1 AQUÍ EMPIEZA TODO</ep1.1>
			<id1.1>0c9fb8d7d8f4362ca537183c14f4df4b5aca038a</id1.1>
			<ep1.2>2 MASHA MASHA MASHA</ep1.2>
			<id1.2>1884674bb68d6b7be156b2f231befeae4bd15ded</id1.2>
			<ep1.3>3 EL CAMPAMENTO</ep1.3>
			<id1.3>396d0f8aaa72aa707455a7abc3efd4738d84a899</id1.3>
			<ep1.4>4 4.4</ep1.4>
			<id1.4>978bf398006906ef0ff1f58dfd8133666873e5ed</id1.4>
			<ep1.5>5 CURRATELO</ep1.5>
			<id1.5>b504067c603c12f54c41562419a16e4797e15fa1</id1.5>
			<ep1.6>6 FORTALEZA</ep1.6>
			<id1.6>c0069bbd8cf8c41ded9f67c21b92e020e40e151d</id1.6>
			<ep1.7>7 NUESTRO PODER</ep1.7>
			<id1.7>419384ea546429177dc100a6aaec36c504328045</id1.7>
			<ep1.8>8 EL QUE TIENE TODO EL DINERO</ep1.8>
			<id1.8>419384ea546429177dc100a6aaec36c504328045</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>WESTWORLD</title>
		<info>(2016) 3 temporadas 28 episodios. Westworld es un parque de atracciones futurista y controlado por alta tecnología dirigido por el Dr. Robert Ford (Anthony Hopkins). Las instalaciones cuentan con androides cuya apariencia física es humana, y gracias a ellos los visitantes pueden dar rienda suelta a sus instintos y vivir cualquier tipo de aventura o fantasía, por muy oscura o peligrosa que sea, sabiendo que los robots no les harán daño.</info>
		<year>2016</year>
		<genre>Ciencia ficción. Western. Drama. Intriga</genre>
		<thumb>https://image.tmdb.org/t/p/original/pOGk7Hv1jwzVex10oMZ7psgEfVg.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/olPBWYdEX2NQmoczbgEppUyX57H.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/pOGk7Hv1jwzVex10oMZ7psgEfVg.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/olPBWYdEX2NQmoczbgEppUyX57H.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>f5f181fed62a0e103dc7c7d0ea0c127a5ed68af8</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/xiwhkYtnvzXMb3iqItkTcbwqzt7.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/oRvpF7Rnp76V8NNrPk4o5AzJ84N.jpg</fanart.2>
			<ep2.1>1 VIAJE A A NOCHE</ep2.1>
			<id2.1>205119c024746270e51adb5c161e9cb7a5e6a6fd</id2.1>
			<ep2.2>2 REUNION</ep2.2>
			<id2.2>209fed3cd82887fe326a891ba0e42779d7cf0b26</id2.2>
			<ep2.3>3 VIRTUD Y FORTUNA</ep2.3>
			<id2.3>dbc4079999d6a6a52228c108af028defb45147ff</id2.3>
			<ep2.4>4 AL ACERTIJO DE LA ESFINGE</ep2.4>
			<id2.4>ee9f7d1dc90aba3018d7c3ae70b7bc91a715ee13</id2.4>
			<ep2.5>5 AKANE NO MAI</ep2.5>
			<id2.5>61f19a33132457029bcd1491ebd207c7f27e5a32</id2.5>
			<ep2.6>6 ESPACIO FASICO</ep2.6>
			<id2.6>c008b19dea8f2943ff15b0d10686e6e71ed8645d</id2.6>
			<ep2.7>7 LES ESCORCHES</ep2.7>
			<id2.7>c72143ffe4b087f66d05537fe359b8939403d1aa</id2.7>
			<ep2.8>8 KIKSUYA</ep2.8>
			<id2.8>1685a5337ce19ddf35527a24b9ea5a876d332b2a</id2.8>
			<ep2.9>9 PUNTO DE FUGA</ep2.9>
			<id2.9>e5f181ecbb86b07a0816f9a69653670744898058</id2.9>
			<ep2.10>10 LA PASAJERA</ep2.10>
			<id2.10>2b5d8ccf91252df178b1a2d8d1c947e97c1b35dd</id2.10>
		</t2>
		<t3>
			<thumb.3>https://image.tmdb.org/t/p/original/gHr8blVOi7Zmbj9dQ98cixqvrQj.jpg</thumb.3>
			<fanart.3>https://image.tmdb.org/t/p/original/btmFNphBZurRjIYwP515BmOkNtr.jpg</fanart.3>
			<ep3.1>1 PARCE DOMINE</ep3.1>
			<id3.1>83c17a98851319512f5069004d339a1e3426afaf</id3.1>
			<ep3.2>2 LA LINEA INVERNAL</ep3.2>
			<id3.2>9ea051f16f53727e1d10e07c7f318d32af00f467</id3.2>
			<ep3.3>3 LA AUSENCIA DE CAMPO</ep3.3>
			<id3.3>49a3750450aceb3aba0b6639c622030cb66bf9d0</id3.3>
			<ep3.4>4 LA MADRE DE EXILIADOS</ep3.4>
			<id3.4>7002ee953e82f3ef9b87e74c72c0e99769d7536b</id3.4>
			<ep3.5>5 GENRE</ep3.5>
			<id3.5>2e958844315f05895fd7b2ddae7b9f027f3c81b6</id3.5>
			<ep3.6>6 DECOHERENCIA</ep3.6>
			<id3.6>c0916ceff97a3520f131ae7d8068ea01cd5d31c2</id3.6>
			<ep3.7>7 PEON DEL PASADO</ep3.7>
			<id3.7>88c58878498f0a45a9914fc2c389ec15c9d3f1e7</id3.7>
			<ep3.8>8 TEORIA DE LA CRISIS</ep3.8>
			<id3.8>8d43c577e58edf343d15c018017f70b7d85fc15f</id3.8>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>YELLOWJACKETS</title>
		<info>(2021) 7 episodios. Después de un accidente de avión en las profundidades de Ontario, los miembros de un equipo de fútbol femenino son los únicos supervivientes del accidente. Las chicas de secundaria tendrán que luchar por su supervivencia contra los clanes caníbales.</info>
		<year>2021</year>
		<genre>Thriller. Intriga </genre>
		<thumb>https://www.themoviedb.org/t/p/original/XtnjzjjFAnmTEiDk4xu7diCvMF.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/t9gfV0eM9nWqJ4vMey4CSYFiqmZ.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/XtnjzjjFAnmTEiDk4xu7diCvMF.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/t9gfV0eM9nWqJ4vMey4CSYFiqmZ.jpg</fanart.1>
			<ep1.1>1 ZUMBA</ep1.1>
			<id1.1>35e18d6fb71e5c1513ed79f26ea5f5fb2c54afd7</id1.1>
			<ep1.2>2 FA SOSTENIDO</ep1.2>
			<id1.2>7925debea213b8f71bcda67e4ec800f624cb4639</id1.2>
			<ep1.3>3 LA CASA DE MUÑECAS</ep1.3>
			<id1.3>abedc0b589e328f92e166f7d19185ae052d6d2cd</id1.3>
			<ep1.4>4 EMPUJA</ep1.4>
			<id1.4>cced34c780eeb71c59bb53320f345d243bad0b31</id1.4>
			<ep1.5>5 COLMENA DE SANGRE</ep1.5>
			<id1.5>bec911ae2ffa00bf18df4ceddffb31bf10ca6554</id1.5>
			<ep1.6>6 SANTOS</ep1.6>
			<id1.6>ea6de5d14e664c329db6398c4e4da01707275bc2</id1.6>
			<ep1.7>7 SIN BRUJULA</ep1.7>
			<id1.7>7e244b8b148aa442dd6db970f30b66b04042adb8</id1.7>
			<ep1.8>8 EL VUELO DEL ABEJORRO</ep1.8>
			<id1.8>bffcf5a542d50d3c6f7be910dba936dbc767b046</id1.8>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>YOU</title>
		<info>(2018) 3 temporadas. 30 episodios. Joe Goldberg (Penn Badgley), un neoyorquino obsesivo pero brillante, aprovecha las nuevas tecnologías para conquistar a Beck (Elizabeth Lail), la mujer de sus sueños. Gracias a la hiperconectividad que ofrece la tecnología moderna, Joe pasa de acosador a novio, pues usando Internet y las redes sociales consigue conocer sus detalles más íntimos para acercarse a ella. Así, lo que empezó como un flechazo encantador, se convertirá en una obsesión mientras él, de forma estratégica y silenciosa, se deshace de todos los obstáculos (y personas) que se crucen en su camino.En enero 2020 Netfix renovó la serie por una 3ª temporada, a estrenar en el 2021.</info>
		<year>2018</year>
		<genre>Thriller</genre>
		<thumb>https://image.tmdb.org/t/p/original/9qZUrhcFVI9MNznVJxdfcrmifof.jpg</thumb>
		<fanart>https://image.tmdb.org/t/p/original/q7T2MO1Vk0RQQhm9BHXXQSKc05u.jpg</fanart>
		<t1>
			<thumb.1>https://image.tmdb.org/t/p/original/6A1cljmEY8No7R1yu3yYGEXaCUa.jpg</thumb.1>
			<fanart.1>https://image.tmdb.org/t/p/original/sa7Ya0Rc1KgETjDSmdmfuZZO4kr.jpg</fanart.1>
			<ep1.1>1 AL 10</ep1.1>
			<id1.1>c9921404cbe6719590f1cb35cb6f856ddd44f903</id1.1>
		</t1>
		<t2>
			<thumb.2>https://image.tmdb.org/t/p/original/9qZUrhcFVI9MNznVJxdfcrmifof.jpg</thumb.2>
			<fanart.2>https://image.tmdb.org/t/p/original/gzOIymABxmetAECXtazEYCpMmfb.jpg</fanart.2>
			<ep2.1>1 AL 10</ep2.1>
			<id2.1>97e1665c5c515da18e57cc203bf81c260c899f1b</id2.1>
		</t2>
		<t3>
			<thumb.3>https://www.themoviedb.org/t/p/original/5CgYcf63vy3rqDn3cHqybNjHT8K.jpg</thumb.3>
			<fanart.3>https://www.themoviedb.org/t/p/original/e92qfYRVYUL602ztyEoujUtXlS1.jpg</fanart.3>
			<ep3.1>1 Y FUERON FELICES PARA SIEMPRE</ep3.1>
			<id3.1>43582f153bc361f070df7560f4a341335f436cb8</id3.1>
			<ep3.2>2 AL 6</ep3.2>
			<id3.2>5d310b46043518ef91be2b9ae9bd5205d8439a03</id3.2>
			<ep3.3>7 AL 10</ep3.3>
			<id3.3>abc3cdcfdb61321b9fedaf25e554cb0500033e6c</id3.3>
		</t3>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<title>YOUR HONOR</title>
		<info>(2020) 10 episodios. Cuando el hijo de un respetado juez (Bryan Cranston) de Nueva Orleans se ve involucrado en un atropello y fuga, el plan de su padre porque nadie se entere conducirá a un juego de alto riesgo lleno de mentiras, engaños y decisiones imposibles. Todo se complicará más cuando se descubre quién era la víctima y las consecuencias de su identidad.</info>
		<year>2020</year>
		<genre>Intriga. Thriller</genre>
		<thumb>https://www.themoviedb.org/t/p/original/xWJZjIKPeEZhK3JRYKOe06yW6IU.jpg</thumb>
		<fanart>https://www.themoviedb.org/t/p/original/4e1BHPLIdG46uA9qYdWORLKkNuG.jpg</fanart>
		<t1>
			<thumb.1>https://www.themoviedb.org/t/p/original/xWJZjIKPeEZhK3JRYKOe06yW6IU.jpg</thumb.1>
			<fanart.1>https://www.themoviedb.org/t/p/original/4e1BHPLIdG46uA9qYdWORLKkNuG.jpg</fanart.1>
			<ep1.1>1 AL 10 (1080)</ep1.1>
			<id1.1>BFEF75BAF24A04AB8C481325D8EB66AD23443495</id1.1>
			<id1.9>831c7af56eee42224ecf0f122e7e9c2841526233</id1.9>
		</t1>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<t1/>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<t1/>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<t1/>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<t1/>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
	<serie>
		<t1/>
		<t2/>
		<t3/>
		<t4/>
		<t5/>
		<t6/>
		<t7/>
		<t8/>
		<t9/>
		<t10/>
		<t11/>
	</serie>
</series>
